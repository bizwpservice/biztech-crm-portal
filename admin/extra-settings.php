<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if ( ! function_exists( 'scp_extra_settings_create_menu' ) ) {
    add_action( 'admin_menu', 'scp_extra_settings_create_menu' );
    function scp_extra_settings_create_menu() {
        add_submenu_page( 'biztech-crm-portal', __( 'Module Ordering' ), __( 'Module Ordering' ), 'manage_options', 'scp-extra-settings', 'scp_extra_settings_callback' );
    }
}

if ( ! function_exists( 'scp_extra_settings_callback' ) ) {
    function scp_extra_settings_callback() {
        global $objSCP, $sugar_crm_version, $bcp_framework;
        $crmresturl = get_option('biztech_scp_rest_url');
        $crmversionkey = get_option('biztech_scp_sugar_crm_version');
        $crmusername = get_option('biztech_scp_username');
        $scp_sugar_password = scp_decrypt_string(get_option('biztech_scp_password'));
        $objSCP = new SugarRestApiCall($crmresturl, $crmusername, $scp_sugar_password);
        ?>
        <div class="wrap">
            <h1><?php _e('Module Ordering'); ?></h1>
        <?php
        if ( ( ( $sugar_crm_version == 6 || $sugar_crm_version == 5 ) && $objSCP->session_id != '' ) ||
                ( $sugar_crm_version == 7 && $objSCP->access_token != '' ) ) {
            wp_enqueue_script( 'jquery-ui-sortable' );
            $modules = $objSCP->getAllAccessibleModules();
            $modules = (array) $modules;
            
            if(empty(get_option('es_modules_label'))){
                update_option( 'es_modules_label',  $modules );
                update_option( 'scp_menu_modules_label',  $modules );
            }
            if ( isset( $_REQUEST['submit'] ) ) {
                $j = 0;
                $es_modules_labels = $_REQUEST['es_modules_label'];
                $esmodulesLabels = array();
                foreach($es_modules_labels as $key => $value){
                    $separator = substr_compare($key, 'separator',0,9);
                    if($separator == 0){
                        $ke = 'separator-'.$j;
                        $esmodulesLabels[$ke] = $j;
                        $j++;
                    } else {
                        $esmodulesLabels[$key] = $value;
                    }
                }
                update_option( 'es_modules_label',  $esmodulesLabels );
            ?>
            <div class='updated settings-error hide_class' id='setting-error-settings_updated'>
                <p><strong><?php _e('Module order updated successfully.'); ?></strong></p>
            </div>
            <?php
            }
            if ( isset( $_REQUEST['reset'] ) ) {
                update_option( 'es_modules_label',  $modules );
                $es_modules_label = get_option('es_modules_label');
                $scp_custom_menu = get_option("scp_custom_menu");
                if(!empty($scp_custom_menu)){
                    foreach($scp_custom_menu as $key => $value){
                        $name = str_replace(" ", "_", $value['name']);
                        if ( ! array_key_exists( $name, $es_modules_label ) ) {
                            $es_modules_label[$name] = $value['name'];
                        }
                        update_option( 'es_modules_label',  $es_modules_label );
                    }
                }
            ?>
            <div class='updated settings-error hide_class' id='setting-error-settings_updated'>
                <p><strong><?php _e('Module order reset successfully.'); ?></strong></p>
            </div>
            <?php
            }
            
            $es_modules_label = get_option( 'es_modules_label' );
            ?>
                <style type="text/css">
                    table#modules-label { margin-top: 20px;}
                    table#modules-label .ui-state-default th{ position: relative;}
                    table#modules-label th label { border: 1px solid #000; width: 200px; display: block; padding: 5px; margin: 0; padding-right: 25px;}
                    table#modules-label .ui-sortable-helper th label { border: 1px dashed #000; }
                    table#modules-label .ui-state-default th .dashicons-move{ position: absolute; bottom: 0; right: 5px; font-size: 13px; line-height: 13px; height: 12px; width: 14px; top: 0; margin: auto; cursor: pointer;}
                    .add-separator-div { margin: 20px 0;}
                    .add-separator-div h2 { display: inline-block; margin: 6px 20px 0 0;}
                    table#modules-label .add-separator-div-line {
                        margin: 15px 0;
                        display: block;
                        width: 100%;
                        background: #cccccc;
                    }
                    table#modules-label .add-separator-div-line .dashicons-move {
                        font-size: 8px;
                        line-height: 10px;
                        height: 10px;
                        right: 0;
                    }
                    table#modules-label .add-separator-div-line label {
                        border: none;
                        padding: 0;
                        padding-right: 24px;
                        text-align: center;
                        height: 5px;
                        line-height: 5px;
                    }
                </style>
                <p><?php _e('Drag and Drop modules for reordering and save updated order.'); ?></p>
                <hr>
                <div class="add-separator-div">
                    <div class="add-separator-btn button-secondary"><?php _e('Add separator'); ?></div>
                    <?php
                        $count = 0;
                        if(!empty($es_modules_label)){
                            foreach ( $es_modules_label as $module_key => $module_value ) {
                                $separator = substr_compare($module_key, 'separator',0,9);
                                if($separator == 0){
                                    $count++;
                                }
                            }
                            $count += $count;
                        }
                    ?>
                    <div class="hidden add-separator">
                        <table>
                            <tbody>
                        <tr class="ui-state-default ui-separator add-separator-div-line">
                            <th scope="row">
                                <label>----------</label>
                                <div alt="f545" class="dashicons dashicons-move"></div>
                            </th>
                            <td>
                                <input type="hidden" name="es_modules_label[separator-<?php echo $count; ?>]" value="<?php echo $count; ?>" class="regular-text" />
                                <input type="hidden" value="<?php echo $count; ?>" class="separator-val" />
                            </td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
                <hr>
                <form method="post">
                    <table id="modules-label" class="form-table[]">
                        <tbody>
                            <?php
                                if(!empty($es_modules_label)){
                                    foreach ( $es_modules_label as $module_key => $module_value ) {
                                        $separator = substr_compare($module_key, 'separator',0,9);
                                        if($separator == 0){
                                        ?>
                                            <tr class="ui-state-default ui-separator add-separator-div-line">
                                                <th scope="row">
                                                    <label>----------</label>
                                                    <div alt="f545" class="dashicons dashicons-move"></div>
                                                </th>
                                                <td>
                                                    <td><input type="hidden" name="es_modules_label[<?php echo $module_key; ?>]" value="<?php echo $module_value; ?>" class="regular-text" />
                                                    <input type="hidden" value="<?php echo $module_value; ?>" class="separator-val" />
                                                </td>
                                            </tr>
                                        <?php } else { ?>
                                            <tr class="ui-state-default">
                                                <th scope="row">
                                                    <label><?php echo $module_value; ?></label>
                                                    <div alt="f545" class="dashicons dashicons-move"></div>
                                                </th>
                                                <td><input type="hidden" name="es_modules_label[<?php echo $module_key; ?>]" value="<?php echo $module_value; ?>" class="regular-text" /></td>
                                            </tr>
                                        <?php
                                        }
                                    }
                                }
                            ?>
                        </tbody>
                    </table>
                    <div class="form-group module-setting submit">
                        <input type="submit" name="submit" class="button-primary" value="<?php _e( 'Save Changes' ); ?>" />
                        <input type="submit" name="reset" class="button-primary" value="<?php _e( 'Reset Order' ); ?>" />
                    </div>
                </form>
                <script type="text/javascript">
                    jQuery( function() {
                        jQuery( '#modules-label tbody' ).sortable();
                        jQuery( '#modules-label tbody' ).disableSelection();
                        jQuery(".add-separator-div .add-separator-btn").click(function (e) {
                            e.preventDefault();
                            var $li = jQuery(".add-separator-div .add-separator table tbody").html();
                            var separator_val = jQuery(".add-separator-div .add-separator table .separator-val").val();
                            separator_val++;
                            jQuery("#modules-label tbody").append($li);
                            jQuery("#modules-label tbody").sortable('refresh');
                            jQuery(".add-separator-div .add-separator table .regular-text").attr('name','es_modules_label[separator-'+separator_val+']');
                            jQuery(".add-separator-div .add-separator table .separator-val, .add-separator-div .add-separator table .regular-text").val(separator_val);
                            jQuery('html, body').animate({
                                scrollTop: jQuery(".form-group.module-setting").offset().top
                            }, 1000);
                        });

                    });
                </script>
        <?php
        } else {
            $ismenable = $objSCP->check_enable_modules();

            if ( is_multisite() ) {
                if( $ismenable->success != 1 ) {
                    $error_msg = isset( $_SESSION['scp_admin_messages']->conn_problem ) ? __( $_SESSION['scp_admin_messages']->conn_problem ) : '';
                    $error_msg .= '<br>' . __('Please configure portal from') . ' <a href="' . network_site_url() . 'wp-admin/network/admin.php?page=biztech-crm-portal">' . __('here') . '</a> ' . __('or contact your network administrator.');
                } elseif ( $ismenable->fronend_framework != $bcp_framework ) {
                    $backend_error = ( $crmversionkey == 6 ) ? 'SugarCRM' : 'SuiteCRM';
                    $error_msg = ( isset( $_SESSION['scp_admin_messages']->combination_mismatch ) ? __( $_SESSION['scp_admin_messages']->combination_mismatch ) : '' ) . ' (' . $backend_error . ' + ' . $ismenable->fronend_framework . ')' . '<br>' . __('Please configure portal from') . ' <a href="' . network_site_url() . 'wp-admin/network/admin.php?page=biztech-crm-portal">' . __('here') . '</a> ' . __('or contact your network administrator.');
                }
            }
            else {
                if( $ismenable->success != 1 ) {
                    isset( $_SESSION['scp_admin_messages']->conn_problem ) ? __( $_SESSION['scp_admin_messages']->conn_problem ) : '';
                } elseif ( $ismenable->fronend_framework != $bcp_framework ) {
                    $backend_error = ( $crmversionkey == 6 ) ? 'SugarCRM' : 'SuiteCRM';
                    $error_msg = ( isset( $_SESSION['scp_admin_messages']->combination_mismatch ) ? __( $_SESSION['scp_admin_messages']->combination_mismatch ) : '' ) . ' (' . $backend_error . ' + ' . $ismenable->fronend_framework . ')';
                }
            }
            ?>
            <div class="error settings-error" id="setting-error-settings_updated">
                <p><strong><?php echo $error_msg; ?></strong></p>
            </div>
            <script type="text/javascript">jQuery("#submit").hide();</script>
            <script type="text/javascript">
                jQuery(".hide_class").hide();
                jQuery("#message").hide();
            </script>
            <?php } ?>
        </div>
        <?php
    }
}
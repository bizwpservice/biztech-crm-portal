<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
global $scp_error_msg, $language_array;
/**** Change***/
if(!is_array($scp_error_msg)){
    $scp_error_msg = (array) $scp_error_msg;
}

foreach ($results as $k => $vals_lbl) {
    foreach ($vals_lbl->rows as $k_lbl2 => $vals) {
        foreach ($vals as $key_fields => $val_fields) {
            if (isset($val_fields->readonly) && strtolower($val_fields->readonly) == 'true' && !isset($_REQUEST[$val_fields->name]) ) {
                continue;
            }
            if (!empty($val_fields)) {
                $name_arry = array();
                $ar_val = (isset($val_fields->name)) ? $val_fields->name : "";
                $name_arry[] = $ar_val;
                $req_value = (isset($_REQUEST[$ar_val]) ? $_REQUEST[$ar_val] : "");
                if(is_array($req_value)){
                    $req_value = $req_value;
                } else {
                    $req_value = strip_tags($req_value);
                }
                $req_value = str_replace("\'", "'", $req_value);
                $req_value = str_replace('\"', '"', $req_value);
                
                if ( isset( $val_fields->required ) && $val_fields->required == 'true' && $req_value == "" ) {
                    array_push($scp_error_msg, array(
                        "key"   => $key_fields,
                        "value" => $language_array['msg_field_required'],
                    ));
                } else {
                    if ($req_value != "") {
                        if ($val_fields->name == "account_name" && $module_name != "Leads") {
                            $pass_name_arry["account_id"] = sanitize_text_field($req_value);
                        } else if ( $val_fields->type == "datetimecombo" || $val_fields->type == "datetime" ) {
                            $GetDate = scp_browser_time_convert($_REQUEST[$ar_val]);
                            
                            $pass_name_arry[$ar_val] = $GetDate;
                        } else if ( $val_fields->type == "date" ) {
                            $GetDate = date("Y-m-d", strtotime($_REQUEST[$ar_val]));
                            $pass_name_arry[$ar_val] = $GetDate;
                        } else if ($ar_val == 'phone_work') {
                            $numbers = $_REQUEST[$ar_val];
                            if(is_array($numbers))  
                                $numbers = implode(',', $numbers);
                            $pass_name_arry[$ar_val] = sanitize_text_field($numbers);
                        } else if ($ar_val == "assigned_user_name") {
                            $pass_name_arry["assigned_user_id"] = sanitize_text_field($_REQUEST[$ar_val]);
                        } else if ($ar_val == "email" && $module_name == "Leads") {
                            $pass_name_arry["email"] = array(array('email_address' => stripslashes_deep($_REQUEST[$ar_val])));
                        } else if ($ar_val == "do_not_call" && $module_name == "Leads" && $_REQUEST[$ar_val] == '') {
                            $pass_name_arry["do_not_call"] = 0;
                        } else if ($ar_val == "is_template" && (stripslashes_deep($_REQUEST[$ar_val] == '') || stripslashes_deep($_REQUEST[$ar_val] == null))) {
                            $pass_name_arry["is_template"] = 0;
                        } else if ( ( $val_fields->type == "int" || $val_fields->type == "decimal" ) ) {
                            $pass_name_arry[$ar_val] = trim($_REQUEST[$ar_val]);
                        } else if ($ar_val == "parent_name") {
                            $pass_name_arry["parent_type"] = $req_value;
                            $pass_name_arry["parent_id"] = $_REQUEST["parent_id"];
                        } else if ($val_fields->type == 'multienum') {
                            $pass_name_arry[$ar_val] = implode('^,^', $req_value);
                        } else if ($ar_val == "embed_flag") {//Added by BC on 09-dec-2015
                            if (stripslashes_deep($_REQUEST[$ar_val] == '') || stripslashes_deep($_REQUEST[$ar_val] == null)) {
                                $pass_name_arry["embed_flag"] = 0;
                            } else {
                                $pass_name_arry["embed_flag"] = sanitize_text_field($req_value);
                            }
                        } elseif ( $val_fields->type == "relate" ) {
                            if ( isset ($val_fields->link) && $val_fields->link != null ) {
                                $field_relation_name = $val_fields->link;
                                $field_relate_to_module = $val_fields->relate_module;
                                $field_relate_array = array(
                                    "relate_module" => $field_relate_to_module,
                                    "relation_name" => $field_relation_name,
                                    "relate_id"     => $req_value,
                                );
                                $relationship_fields[$field_relation_name] = $field_relate_array;
                            } else {
                                $pass_name_arry[$fileds->id_name] = $req_value;
                            }
                        } else {
                            $pass_name_arry[$ar_val] = sanitize_text_field($req_value);
                        }
                    }
                }
            }
        }
    }
}
?>
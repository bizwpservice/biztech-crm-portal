<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
//to get name portal name
$biztech_scp_name = fetch_data_option('biztech_scp_name');
//to get upload logo
$biztech_upload_image = fetch_data_option('biztech_scp_upload_image');

$biztech_portal_visible_reCAPTCHA = (fetch_data_option("biztech_portal_visible_reCAPTCHA") != NULL) ? fetch_data_option("biztech_portal_visible_reCAPTCHA") : array();
$captch_form_css = "";
if ( in_array("portal-forgot-password", $biztech_portal_visible_reCAPTCHA) && get_option('biztech_scp_recaptcha_site_key') != "" && get_option('biztech_scp_recaptcha_secret_key') != "" ) {
    $captch_form_css = "width: 365px;";
}
$current_url = explode('?', $_SERVER['REQUEST_URI'], 2);
$current_url = $current_url[0];
$biztech_redirect_login = get_page_link(get_option('biztech_redirect_login'));
if ($biztech_redirect_login != NULL) {
    $redirect_url_login = $biztech_redirect_login;
} else {
    $redirect_url_login = home_url() . "/portal-login/";
}

$lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
?>

<div class=" border-0 rounded-lg w-100">
    <?php if ($biztech_upload_image != NULL) { 
        $sfcp_logo_url = wp_get_attachment_image_src( $biztech_upload_image,'full' );
    ?>
        <div class="d-block text-center">
            <img alt="<?php echo $biztech_scp_name; ?>" src="<?php echo $sfcp_logo_url[0]; ?>">
        </div>
    <?php } ?>
    <form action="<?php echo home_url(); ?>/wp-admin/admin-post.php" method="post" id="commentForm">
        <?php
        if ((isset($_REQUEST['error']) && !empty($_REQUEST['error'])) && (isset($_SESSION['bcp_frgtpwd']) && $_SESSION['bcp_frgtpwd'] != '')) {
            echo "<div class='alert alert-danger alert-dismissible fade show login_error'><span class='error'>".$_SESSION['bcp_frgtpwd']."</span><button type='button' class='btn-close' data-bs-dismiss='alert'></button></div>";
            unset($_SESSION['bcp_frgtpwd']);
        }
        if((isset($_REQUEST['sucess']) && !empty($_REQUEST['sucess'])) && (isset($_SESSION['bcp_frgtpwd']) && $_SESSION['bcp_frgtpwd'] != '')) {
            echo "<div class='alert alert-success alert-dismissible fade show bcp-success'><span>".$_SESSION['bcp_frgtpwd']."</span><button type='button' class='btn-close' data-bs-dismiss='alert'></button></div>";
            unset($_SESSION['bcp_frgtpwd']);
        }
        ?>
        <h1><?php echo $language_array['lbl_portal_forgot_password']; ?></h1>
        
        <?php if ($all_language != null && !empty($all_language) && count($all_language) > 1 && !isset($_SESSION['scp_otp_generated'])) { ?>
        <div class="dropdown">
            <button class="country-dropdown dropdown-toggle dropdown dcp_lang_dropdown" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                <?php
                $lang_selected = "English (US)";
                $flag_selected_class = "flag-us";
                foreach ($all_language as $key => $value) {
                    $country_code = $value['module'];    
                    $c_code_array = explode("_", $country_code);
                    $c_code = strtolower($c_code_array[1]);
                    $c_class = "flag-".$c_code;
                    $c_label = str_replace(" - ".$country_code, "", $value['label']);
                    if ($value['module'] == "en_UK") {
                        $c_class = "flag-england";
                    }
                    if ($value['module'] == "el_EL") {
                        $c_class = "flag-sv";
                    }
                    if ($value['module'] == $lang_code) {
                        $lang_selected = $c_label;
                        $flag_selected_class = $c_class;
                    }
                }
                ?>
                <span class="flag <?php echo $flag_selected_class; ?>"></span>
                <?php echo $lang_selected; ?>
                <em class="fas fa-chevron-down"></em>
            </button>
            <ul class="dropdown-menu scrollbar scrollbar-primary" aria-labelledby="dropdownMenuButton1">
                <?php
                foreach ($all_language as $key => $value) {
                $country_code = $value['module'];    
                $c_code_array = explode("_", $country_code);
                $c_code = strtolower($c_code_array[1]);
                $c_class = "flag-".$c_code;
                $lang_active = "";
                if ($value['module'] == "en_UK") {
                    $c_class = "flag-england";
                }
                if ($value['module'] == "el_EL") {
                    $c_class = "flag-sv";
                }
                if ($value['module'] == $lang_code) {
                    $lang_active = " active_lang";
                }
                $c_label = str_replace(" - ".$country_code, "", $value['label']);
                ?>
                <li class="<?php echo $lang_active; ?>">
                    <a href="javascript:void(0);" onclick="bcp_change_language('<?php echo $country_code ?>');" >
                        <span class="flag <?php echo $c_class; ?>" ></span>
                        <?php echo $c_label; ?>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
        <?php } ?>
        <div class="login-input">
            <input class="form-control" type="email" name="forgot-password-email-address" id="forgot-password-email-address" required="" placeholder="<?php echo $language_array['lbl_email_address']; ?>*">
        </div>
        <?php if ( in_array("portal-forgot-password", $biztech_portal_visible_reCAPTCHA) && get_option('biztech_scp_recaptcha_site_key') != "" && get_option('biztech_scp_recaptcha_secret_key') != "" ) { ?>
        <div class="re-capcha">
            <div id="scp-recaptcha"></div>
            <label id="scp-recaptcha-error-msg" style="display:none;" class="error">
                <?php echo $language_array['msg_captcha_validate']; ?>
            </label>
        </div>
        <?php } ?>
        <div class=" mt-4 mb-0 text-center">
            <input type="hidden" name="action" value="bcp_forgot_password">
            <input type="hidden" name="scp_current_url" value="<?php echo $current_url; ?>">
            
            <button type="submit" class="btn btn-primary"><?php echo $language_array['lbl_submit'] ?></button>
        </div>
    </form>
    <div class="card-footer card-footer-registration forgot-card-footer text-center">
        <?php echo $language_array['lbl_dont_get_email_label'] ?>
        <a href="<?php echo site_url() ?>/wp-admin/admin-post.php?action=bcp_forgot_password"> <?php echo $language_array['lbl_email_resend_label'] ?></a>
    </div>
    <div class="bcp-login text-center">
        <a href="<?php echo $redirect_url_login; ?>" class="signup-now float-left"><?php echo $language_array['lbl_back_to_login'] ?></a>
    </div>
</div>

<?php if ( in_array("portal-forgot-password", $biztech_portal_visible_reCAPTCHA) && get_option('biztech_scp_recaptcha_site_key') != "" && get_option('biztech_scp_recaptcha_secret_key') != "" ) { ?>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
    async defer>
</script>
<script type="text/javascript">
  var onloadCallback = function() {
    widget1 = grecaptcha.render('scp-recaptcha', {
      'sitekey' : '<?php echo get_option("biztech_scp_recaptcha_site_key"); ?>',
    });
  };
</script>
<?php } ?>
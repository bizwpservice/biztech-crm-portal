<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if ( ! function_exists( 'scp_product_sync_create_menu' ) ) {

    add_action( 'admin_menu', 'scp_product_sync_create_menu' );
    function scp_product_sync_create_menu() {
        add_submenu_page( 'biztech-crm-portal', __( 'Product Sync' ), __( 'Product Sync' ), 'manage_options', 'scp-product-sync', 'scp_product_sync_callback' );
    }
}

if ( ! function_exists( 'scp_product_sync_callback' ) ) {

    function scp_product_sync_callback() {

        global $objSCP, $sugar_crm_version, $bcp_framework;
        ?>
        <div class="wrap">
            <h1 style="margin-bottom: 20px;"><?php _e('Product Sync'); ?></h1>
            <?php
            if ( ( ( $sugar_crm_version == 6 || $sugar_crm_version == 5 ) && $objSCP->session_id != '' ) ||
                 ( $sugar_crm_version == 7 && $objSCP->access_token != '' ) ) {
                ?>
            <button id="btn_sync_product" class="button button-primary button-large">Synchronize Products</button>
            <img class="product_sync_loader_div" width="20px" style="margin-left: 20px;" src="<?php echo plugin_dir_url( __FILE__ )."../assets/images/ajax-loading.gif"; ?>" alt="">

            <div class="product_sync_loader_div">
                <p><?php _e( 'Product is synchronizing...' ); ?></p>
            </div>
            <div id="message_product_sync" class="notice notice-success">
                <p><strong id="tot_product"></strong> <?php _e( 'products synchronized successfully.' ); ?></p>
            </div>
                <?php
            } else {

                $crmresturl = get_option('biztech_scp_rest_url');
                $crmversionkey = get_option('biztech_scp_sugar_crm_version');
                $crmusername = get_option('biztech_scp_username');
                $scp_sugar_password = scp_decrypt_string(get_option('biztech_scp_password'));
                $objSCP = new SugarRestApiCall($crmresturl, $crmusername, $scp_sugar_password);
                $ismenable = $objSCP->check_enable_modules();

                if ( is_multisite() ) {

                    if( $ismenable->success != 1 ) {

                        $error_msg = isset( $_SESSION['scp_admin_messages']->conn_problem ) ? __( $_SESSION['scp_admin_messages']->conn_problem ) : '';
                        $error_msg .= '<br>' . __('Please configure portal from') . ' <a href="' . network_site_url() . 'wp-admin/network/admin.php?page=biztech-crm-portal">' . __('here') . '</a> ' . __('or contact your network administrator.');

                    } elseif ( $ismenable->fronend_framework != $bcp_framework ) {

                        $backend_error = ( $crmversionkey == 6 ) ? 'SugarCRM' : 'SuiteCRM';
                        $error_msg = ( isset( $_SESSION['scp_admin_messages']->combination_mismatch ) ? __( $_SESSION['scp_admin_messages']->combination_mismatch ) : '' ) . ' (' . $backend_error . ' + ' . $ismenable->fronend_framework . ')' . '<br>' . __('Please configure portal from') . ' <a href="' . network_site_url() . 'wp-admin/network/admin.php?page=biztech-crm-portal">' . __('here') . '</a> ' . __('or contact your network administrator.');

                    }
                } else {

                    if( $ismenable->success != 1 ) {

                        isset( $_SESSION['scp_admin_messages']->conn_problem ) ? __( $_SESSION['scp_admin_messages']->conn_problem ) : '';
                    } elseif ( $ismenable->fronend_framework != $bcp_framework ) {

                        $backend_error = ( $crmversionkey == 6 ) ? 'SugarCRM' : 'SuiteCRM';
                        $error_msg = ( isset( $_SESSION['scp_admin_messages']->combination_mismatch ) ? __( $_SESSION['scp_admin_messages']->combination_mismatch ) : '' ) . ' (' . $backend_error . ' + ' . $ismenable->fronend_framework . ')';
                    }
                }
                ?>
                <div class="error settings-error" id="setting-error-settings_updated">
                    <p><strong><?php echo $error_msg; ?></strong></p>
                </div>
                <script type="text/javascript">jQuery("#submit").hide();</script>
                <script type="text/javascript">
                    jQuery(".hide_class").hide();
                    jQuery("#message").hide();
                </script>
                <?php
            }
            ?>
        </div>
    <script>
        jQuery('.product_sync_loader_div').hide();
        jQuery('#message_product_sync').hide();
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        jQuery('#btn_sync_product').click(function(){

            jQuery('#message_product_sync').hide();
            jQuery('.product_sync_loader_div').show();
            var data = {
                'action': 'bcp_sync_product',
            };
            jQuery.post(ajaxurl, data, function (response) {

                jQuery('.product_sync_loader_div').hide();
                jQuery('#tot_product').text(response);
                jQuery('#message_product_sync').show();
            });
        });
    </script>
        <?php
    }
}

/**
 * Sync product ajax call from sync product page
 */
if (!function_exists('scp_sync_product')) {

    add_action('wp_ajax_bcp_sync_product', 'scp_sync_product');
    add_action('wp_ajax_nopriv_bcp_sync_product', 'scp_sync_product');
    function scp_sync_product(){

        global $objSCP, $wpdb, $sugar_crm_version;
        $modulename = 'ProductTemplates';
        $products = $objSCP->GetProductsListCall($modulename);
        $table = $wpdb->prefix."scp_products";
        $sql = "TRUNCATE TABLE " . $table;
        $wpdb->query($sql);
        $count = 0;
        if ( isset( $products->records ) && ! empty( (array)$products->records ) ) {

            if( $sugar_crm_version == 7 ) {

                foreach ( $products->records as $product ) {

                    $data = array('version' => $sugar_crm_version, 'product_id' => $product->id, 'product_info' => json_encode($product), 'date' => $product->date_modified );
                    $format = array('%d','%s','%s','%s');
                    $wpdb->insert( $table, $data, $format);
                }

            }
            if ( $sugar_crm_version == 5 ) {

                foreach ( $products->records as $product ) {

                    $data = array('version' => $sugar_crm_version, 'product_id' => $product->name_value_list->id->value, 'product_info' => json_encode($product), 'date' => $product->name_value_list->date_modified->value );
                    $format = array('%d','%s','%s','%s');
                    $wpdb->insert( $table, $data, $format);
                }
            }
            $count = count( (array)($products->records) );
        }
        echo $count;
        wp_die();
    }
}

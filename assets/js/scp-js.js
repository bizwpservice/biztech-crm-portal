jQuery(document).ready(function($) {
    // on click button js

    // for user registration form
    $("form#signup_form.signup_front").submit(function( e ){

        $('form#signup_form input').each( function() {
            if ( jQuery( this ).attr('id') != 'password_c' && jQuery( this ).attr('id') != 'password_confirm' && jQuery( this ).attr('type') != 'file' ) {
                this.value = $( this ).val().trim();
            }
        } );

        e.preventDefault();
        var $form = $(this);
        if( ! $form.valid() ) {
            return false;
        }

        jQuery(".scp_user_register_form").addClass('bcp-loader');
        

        var submit = jQuery("#signup_form #submitbtn"),
        message	= jQuery("#sucmsg_pro"),
        contents = new FormData(jQuery(this)[0]);
        // disable button onsubmit to avoid double submision
        submit.attr("disabled", "disabled").addClass('disabled');
        message.fadeOut();

        jQuery.ajax({
            url: this.ajax_theme.value + '?action=user_registration',
            type: 'POST',
            async:true,
            data: contents,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                
                if (data.indexOf('success') > -1) {
                    redirecturl = data.split("|");
                    window.location.href = redirecturl[1];
                    return;
                } else if(data.indexOf("missing") >= 0){
                    message.fadeIn().html( 'There is some error while signup, please try again' );
                } else {
                    message.fadeIn().html( data );
                }
                jQuery(".scp_user_register_form").removeClass('bcp-loader');
                submit.removeAttr("disabled").removeClass('disabled');
            }
        });

        return false;
    });
    
    jQuery( "#signup_form" ).validate({
        errorPlacement: function(error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter(element.parent().parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    jQuery(document).on('submit', '#change_pwd_form', function (e) {

        e.preventDefault();
        var $form = jQuery(this);
        if( ! $form.valid() ) return false;

        jQuery(".profile-password-changes").addClass('bcp-loader');
        var form_data = new FormData(jQuery(this)[0]);

        jQuery.ajax({
            url: ajaxurl + '?action=bcp_update_pwd',
            type: 'POST',
            data: form_data,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                jQuery(".profile-password-changes").removeClass('bcp-loader');
                if (jQuery.trim(response) == '-1') {
                    jQuery('#pwd-msg').html("<span class='error'>"+language_array.msg_invalid_old_pwd+"</span>");
                    jQuery('#old_password').addClass('error-line');
                } else if (jQuery.trim(response) == '1') {
                    jQuery('#pwd-msg').html("<span class='success'>"+language_array.msg_pwd_updated_successfully+"</span>");
                    jQuery('input[type="password"]').val("");
                } else {
                    jQuery('#pwd-msg').html("<span class='error'>"+language_array.msg_connection_failed+"</span>");
                }
            }
        });
    });
    
    jQuery('body').on("click", '.add', function() {
        if (jQuery(this).prev().val()) {
        jQuery(this).prev().val(+jQuery(this).prev().val() + 1);
            jQuery(this).parent().find('.input_qty').trigger('click');
        }
    });
    
    jQuery('body').on("click", '.sub', function() {
        if (jQuery(this).next().val() > 0) {
            if (jQuery(this).next().val() > 0) jQuery(this).next().val(+jQuery(this).next().val() - 1);
            jQuery(this).parent().find('.input_qty').trigger('click');
        }
    });
});

function read_more_less( param ) {

    var moretext, lesstext;
    moretext = jQuery( '.bcp_crm_read_more_text' ).val();
    lesstext = jQuery( '.bcp_crm_read_less_text' ).val();

    if( jQuery( param ).hasClass( "less" ) ) {

        jQuery( param ).removeClass( "less" );
        jQuery( param ).html( moretext );
        jQuery(param).attr( {
            'title': moretext
        } );
    } else {

        jQuery( param ).addClass( "less" );
        jQuery( param ).html( lesstext );
        jQuery(param).attr( {
            'title': lesstext
        } );
    }

    jQuery( param ).parent().prev().toggle();
    jQuery( param ).prev().toggle();
}


/* Start Under proposal module */
// add and remove service line item tr
/*jQuery(document).on('click','.scp_proposal_btn_add_more',function(){
    var service_box = jQuery(this).parent().parent().clone();
    var index = parseInt(jQuery(this).attr('data-index')) + 1;
    jQuery(this).parent().parent().before(service_box);
    jQuery(this).parent().parent().find('input').val("");
    jQuery(this).parent().parent().find('.custom-file-label').html("No file chosen");
    jQuery(this).parent().parent().find('.error-line').remove();
    jQuery(this).parent().parent().find('.scp_proposal_input_service_name').attr('name','service_lineitem['+index+'][name]');
    jQuery(this).parent().parent().find('.scp_proposal_input_service_qty').attr('name','service_lineitem['+index+'][qty]');
    jQuery(this).attr('data-index',index);
    jQuery(this).parent().parent().prev().find('.scp_proposal_btn_add_more').remove();
    jQuery(this).parent().parent().prev().find('.scp_proposal_btn_remove_more').show();
});*/

jQuery(document).on('click','.scp_proposal_btn_add_more',function(){
    var service_box = jQuery('.service-add-block .service-enter:last').clone();
    var index = parseInt(jQuery(this).attr('data-index'));
    jQuery('.service-add-block').append(service_box);
    jQuery('.service-add-block .service-enter:last').find('input').val("");
    jQuery('.service-add-block .service-enter:last').find('.custom-file-label').html("No file chosen");
    jQuery('.service-add-block .service-enter:last').find('.error-line').remove();
    jQuery('.service-add-block .service-enter:last').find('.scp_proposal_input_service_name').attr('name','service_lineitem['+index+'][name]');
    jQuery('.service-add-block .service-enter:last').find('.scp_proposal_input_service_qty').attr('name','service_lineitem['+index+'][qty]');
    jQuery(this).attr('data-index',index + 1);
    jQuery('.service-add-block .service-enter .close-service').removeClass('hide');
    jQuery('.service-add-block .service-enter .close-service').addClass('show');
});

jQuery(document).on('click','.scp_proposal_btn_remove_more',function(){
    jQuery(this).closest('.service-enter').remove();
    var length = jQuery('.service-enter').length;
    if(length == '1'){
        jQuery('.service-add-block .service-enter .close-service').addClass('hide');
        jQuery('.service-add-block .service-enter .close-service').removeClass('show');
    }
});


// Add and edit view - product qty update
jQuery(document).on("keydown", '.input_qty', function(e) {

    // Allow: backspace, delete, tab, escape, enter and .
    if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
} );

// Add and edit view - product qty update
jQuery(document).on("keyup click", '.input_qty', function() {

//        var pro_total = parseFloat(jQuery('.product_total').text());
//        var old_total = parseFloat(jQuery(this).parent().parent().find('.total_price').text());
    var cost_price = jQuery(this).attr('data-cost');
    var qty = jQuery(this).val();
    var total = parseFloat(cost_price * qty);
    var currency = jQuery(this).attr('data-currency');
    jQuery(this).parent().parent().next().text(currency + total.toFixed(2));
//        var attr = jQuery(this).attr('name');
//        if (typeof attr !== typeof undefined && attr !== false) {
//            pro_total = parseFloat(pro_total - old_total);
//            pro_total = parseFloat(pro_total + total);
//            jQuery('.product_total').text(pro_total.toFixed(2));
//        }
    var product_id = jQuery( this ).attr('data-product-id');

    if ( jQuery(this).parent().parent().parent().find('.input_checkbox').is(':checked') ) {
        bcp_manage_selected_product_items( product_id, qty, 'update');
    }
    if ( ! ( qty <= 0 ) ) {
        jQuery(this).closest('.qty-block').css('border','1px solid #727272');
    }
});

//        jQuery('.input_checkbox').change(function(){
jQuery(document).on("change", '.input_checkbox', function() {
//        var pro_total = parseFloat(jQuery('.product_total').text());
//        var total = parseFloat(jQuery(this).parent().parent().find('.total_price').text());
    var product_id, qty;
    product_id = jQuery(this).val();
    qty = jQuery(this).parent().parent().find('.input_qty').val();
    
    if (jQuery(this).is(':checked')) {
        var index = jQuery(this).attr('data-index');
        var qty_val = 'product_lineitem['+index+'][qty]';
        jQuery(this).parent().parent().parent().find('.input_qty').attr('name',qty_val);
        jQuery(this).parent().parent().parent().find('.input_qty').removeAttr('disabled');
        jQuery(this).parent().parent().parent().find('.input_qty').addClass('required_input_qty');
        bcp_manage_selected_product_items( product_id, qty, 'add');
        jQuery(this).parent().parent().parent().find('.qty-block').removeClass('disabled');
    } else {
        jQuery(this).parent().parent().parent().find('.input_qty').attr('disabled','disabled');
        jQuery(this).parent().parent().parent().find('.qty-block').addClass('disabled');
        jQuery(this).parent().parent().parent().find('.input_qty').removeAttr('name');
        jQuery(this).parent().parent().parent().find('.input_qty').val("0");
        var currency = jQuery(this).parent().parent().parent().find('.input_qty').attr('data-currency');
        jQuery(this).parent().parent().parent().find('.total_price').text(currency + '0.00');
        bcp_manage_selected_product_items( product_id, qty, 'remove');
        jQuery(this).parent().parent().parent().find('.input_qty').removeClass('required_input_qty');
    }
});

// Searchbox on enter search
jQuery(document).on("keydown", '#search-product', function(e) {
    var modulename = "";
    if (jQuery(this).attr('data-version') == '7') {
        modulename='ProductTemplates';
    }else{
        modulename='AOS_Products';
    }
    if (e.keyCode == 13 ) {
        bcp_product_search(modulename,"edit","","search");
        return false;
    } else {

        jQuery('.search-input').css('border', '1px solid #8D8D8D');
        return;
    }
});
/* End Under proposal module */

jQuery(document).ready(function(){
    
    if (jQuery('#scp-notification-counter').length > 0) {
    if (jQuery('#scp-notification-counter').text() == '0') {
        jQuery('#scp-notification-counter').hide();
    }else{
        jQuery('#scp-notification-counter').show();
    }
    setInterval(function(){
        var data = {
            'action': 'scp_update_notifications_counter',
        };
        jQuery.post(ajaxurl, data, function(response, status) {
           
            if (response.total_notification != 0) {
                if (response.total_notification == "-1") {
                        window.location.reload();
                } else {
                    jQuery('#scp-notification-counter').show();
                    //jQuery('.ajax-notification-counter').show();
                }
            }else{
                jQuery('#scp-notification-counter').hide();
                jQuery('.ajax-notification-counter').html('');
            }
            jQuery('#scp-notification-counter').text(response.total_notification);
            jQuery('.ajax-notification-counter').html(response.noti_data);
           
        }, "json");
    },10000);
    }

    //if (jQuery('.notification-icon').length > 0) {
    if (jQuery('#scp-notification-counter').length > 0) {
        jQuery(window).click(function(e){
            var containderDiv = jQuery(".scp-popup-notification-count");
            if (!containderDiv.is(e.target) && containderDiv.has(e.target).length === 0 ) {
                jQuery(".scp-notification-dropdown-div").css("display","none");
            }
        });
        jQuery('.notification-icon').click(function(e){
            jQuery('#scp-notification-counter').hide();
            var data = {
                'action': 'scp_notifications_page',
                'notification_status': 'unread',
                'view': 'popup',
            };
            hash = window.location.hash;
            data.current_url = hash;
            jQuery.post(ajaxurl, data, function(response, status) {
                if (response != '0') {
                        jQuery(".scp-drop-down-notification").html(response);
                        jQuery(".scp-notification-dropdown-div").css("display","block");
                } else {
                    if (jQuery('#scp-profile-notification-page').length > 0) {
                        window.location.href = jQuery('#scp-profile-notification-page').val();
                    }else{
                        if (hash == "#data/Notifications") {
                            window.location.reload();
                        }else{
                            window.location.href = "#data/Notifications";
                        }
                    }
                }
            });
        });
    }
    
    
    jQuery.validator.addMethod("check_username", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    }, language_array.msg_letters_allow_in_uname);
    
    jQuery.validator.addMethod("check_phone", function(value, element) {
        return this.optional(element) || /^[0-9-+()]+$/.test(value);
    }, language_array.msg_valid_number);
    
    jQuery.validator.addMethod("check_pwd", function(value, element) {
        var password = value;
        if (password.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[`~!@#$%^&*()\-_=+[\]{}|\\;:'",./<>?])/)) {
            return true;
        } else {
            return false;
        }
    }, language_array.msg_pwd_validation);
    
    jQuery.validator.addClassRules("validate_pwd", {
        required: true,
        check_pwd: true,
    });

    jQuery( '#change_pwd_form' ).validate( {
        rules: {
            password: {
                minlength: 6,
            },
            'cf_password':{
                'equalTo'   : "#new_password"
            },
        },
        messages: {
            'equalTo'   : language_array.msg_cnfm_pwd_must_match
        },
    } );
    
    jQuery('span.pro_delete_btn').click(function () {
        jQuery('#remove_pro_pic').val('true');
        jQuery('#signup_form .scp-profile-photo-input span').remove();
    });

//    jQuery.validator.addClassRules("validate_phone", {
//        required: false,
//        check_phone: true
//    });
//    jQuery( "#signup_form" ).submit(function(){
//        jQuery("input").each(function(){
//            jQuery(this).val(jQuery.trim(jQuery(this).val()));
//        });
//    });
//    jQuery( "#signup_form" ).validate( {
//        rules: {
//            'username_c': {
//                check_username: true,
//            },
//            'password_c': {
//                minlength: 6,
//                check_pwd: true,
//            },
//        },
//        messages: {
//            'password_c': {
//                minlength: language_array.msg_pwd_min_length,
//                check_pwd: language_array.msg_pwd_validation,
//            }
//        },
//        errorPlacement: function(error, element) {
//            if (element.attr("type") == "radio") {
//                error.insertAfter(element.parent().parent().parent());
//            } else {
//                error.insertAfter(element);
//            }
//        }
//    } );
    
    
    /* reset password*/
    jQuery("#scpResetPwdForm").submit(function(){
        if (jQuery('#scp-recaptcha').length > 0 && jQuery('#scp-recaptcha').html() != "") {
            var msg = grecaptcha.getResponse(widget1);
            if (jQuery.trim(msg) == "") {
                jQuery('#scp-recaptcha-error-msg').css("display","block");
                return false;
            } else {
                jQuery('#scp-recaptcha-error-msg').css("display","none");
            }
        }
    });
    
    jQuery("#scpResetPwdForm").validate({
        rules: {
            scp_new_password: {
                minlength: 6,
                check_pwd: true,
            },
            'scp_cfm_password':{
                'equalTo'   : "#scp_new_password"
            },
        },
        messages: {
            scp_new_password: {
                minlength: language_array.msg_pwd_min_length,
                check_pwd: language_array.msg_pwd_validation,
            },
            scp_cfm_password: {
                'equalTo'   : language_array.msg_cnfm_pwd_must_match,
            },
        },
    });
    
    jQuery('.scp-image').bind('change', function() {
        //this.files[0].size gets the size of your file.
        if(this.files[0].type != 'image/jpg' && this.files[0].type != 'image/jpeg' && this.files[0].type != 'image/png' && this.files[0].type != 'image/bmp'){//2 MB size allowed
            alert(language_array.msg_invalid_img_type);
            jQuery(this).val('');
            return false;
        }
    });

    jQuery.validator.addMethod('checkDate',
        function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                var selectedDate = new Date(value);
                var minDate = new Date(moment().add(1).format('MM/DD/YYYY hh:mm'));
                return selectedDate.getTime() > minDate.getTime();
            }
            if (value.trim() == '') {
                return true;
            }
            return isNaN(value) && isNaN($(params).val()) || (Number(value) > Number($(params).val()));
    },language_array.msg_enter_valid_date);
    
    jQuery.validator.addMethod("check_username", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    }, language_array.msg_letters_allow_in_uname);

    jQuery.validator.addMethod("check_phone", function(value, element) {
        return this.optional(element) || /^[0-9-+()]+$/.test(value);
    }, language_array.msg_valid_number);
    
    jQuery.validator.addClassRules("validate_phone", {
        required: false,
        check_phone: true
    });
    
    jQuery.validator.addMethod('greaterThan',
        function(value, element, params) {
            if (!/Invalid|NaN/.test(new Date(value))) {
                return new Date(value) >= new Date($(params).val());
            }
            if (value.trim() == '') {
                return true;
            }
            return isNaN(value) && isNaN($(params).val()) || (Number(value) > Number($(params).val()));
    }, language_array.msg_must_greate_than_start_date);

    jQuery.validator.addMethod('check_decimal', function(value, element) {
        return this.optional(element) || /^\d+(\.\d{1,2})?$/.test(value);
    }, language_array.msg_valid_number);

    jQuery.validator.addMethod('check_duration', function(value, element) {
        if (jQuery('#duration_hours').length > 0 && jQuery('#duration_hours').val() == '0') {
            if (jQuery('#duration_minutes').val() < 15) {
                return false;
            }
        }
        return true;
    }, language_array.lbl_msg_duration_time);
   
    
    jQuery('#duration_hours').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    jQuery('#amount').keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
        }
            // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
    
});


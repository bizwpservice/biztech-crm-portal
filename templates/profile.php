<?php
if (isset($_REQUEST['conerror']) && !empty($_REQUEST['conerror']) && (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'])) {
    $bcp_connection_error = $_SESSION['bcp_connection_error'];
    unset($_SESSION['bcp_connection_error']);
    return "<div class='error settings-error' id='setting-error-settings_updated'>
        <strong>".$bcp_connection_error."</strong>
    </div>";
}

$biztech_redirect_manage_page = get_page_link(get_option('biztech_redirect_manange'));
if ($biztech_redirect_manage_page != NULL) {
    $manage_page = $biztech_redirect_manage_page;
} else {
    $manage_page = home_url() . "/portal-manage-page/";
}
$biztech_redirect_profile = get_page_link(get_option('biztech_redirect_profile'));
if ($biztech_redirect_profile != NULL) {
    $profile_url = $biztech_redirect_profile;
} else {
    $profile_url = home_url() . "/portal-profile/";
}
$profile_name = $language_array['lbl_profile'];

$pagetemplate = get_post_meta(get_the_ID(), '_wp_page_template', true);
if (is_multisite() && $pagetemplate == NULL) {  //if this is multisite and subsite not set any template than catch from network
    $pagetemplate = (get_option('biztech_portal_template') == 0) ? 'page-crm-fullwidth.php' : 'page-crm-standalone.php';
}
$template = str_replace('.php', "", $pagetemplate);

if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
    $list_result_count_all = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], 'accounts', array('id'), '', '', 0);
    $countAcc = (isset($list_result_count_all->entry_list)) ? count($list_result_count_all->entry_list) : 0;
}
if ($sugar_crm_version == 7) {
    $list_result_count_all = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], 'accounts', 'id', array(), '', '', 'date_entered:DESC');
    if (!empty($list_result_count_all->records)) {
        $countAcc = count($list_result_count_all->records);
    } else {
        $countAcc = 0;
    }
}
$theme_name = wp_get_theme(); //20-aug-2016
if ($theme_name == "Twenty Fifteen") {
    $article_class_gen = "article-fifteen-theme";
}
if ($theme_name == "Twenty Sixteen") {
    $article_class_gen = "article-sixteen-theme";
}

$biztech_scp_name = fetch_data_option('biztech_scp_name');
$biztech_scp_name = ($biztech_scp_name != NULL) ? $biztech_scp_name : "All Modules";

$current_url = explode('?', $_SERVER['REQUEST_URI'], 2);
$current_url = $current_url[0];

if ($template != 'page-crm-standalone') {
    scp_full_width_menu_part($biztech_scp_name, $countAcc, $template);
}
?>
<div id="wrapper">
    
    <?php
    if ($template == 'page-crm-standalone') {
        scp_standalone_header_part("tachometer");
    }
    ?>
    <div class="container-fluid page-body-wrapper">
        <?php
        if ($template == 'page-crm-standalone') {
            scp_standalone_sidebar_menu_part($biztech_scp_name, $countAcc, $template);
        }
        ?>
        <div class="main-panel">
            <div id="content-wrapper" class="d-flex flex-column">
                <div class="signup-new-section scp-signup-form">
                    <div id="responsedata" data-count="0" class='scp-standalone-content row'>
                    <script>
                        scp_signup_form('Contacts', 'edit');
                    </script>
                    </div>
                    <div id="otherdata" style="display:none;"></div>
                    <input type="hidden" id="res_count" value="0" >
                </div>
            </div>
        </div>
    </div>
</div>

<?php

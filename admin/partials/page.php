<table class='form-table'>
    <tr class="hide_class">
        <th scope='row'><label><?php _e('Sign up Page') ?></label></th>
        <td>
            <select name = "biztech_redirect_signup" id="biztech_redirect_signup">
                <?php
                $pages = get_pages();
                foreach ($pages as $pagg) {
                    $option = '<option value="' . $pagg->ID . '" ';
                    if (get_option('biztech_redirect_signup') == $pagg->ID) {
                        $option .= 'selected=selected';
                    } else if ((get_option('biztech_redirect_signup') == NULL)) {
                        $option .= 'selected=selected';
                    }
                    $option .= '>';
                    $option .= $pagg->post_title;

                    $option .= '</option>';
                    echo $option;
                }
                ?>
            </select>
        </td>
    </tr>
    <!--Added By BC on 16-feb-016 to provide option for redirect page for login-->
    <tr class="hide_class">
        <th scope='row'><label><?php _e('Login Page') ?></label></th>
        <td>
            <select name = "biztech_redirect_login" id="biztech_redirect_login">
                <?php
                $page = get_page_by_path('portal-login');
                $login_page = $page->ID;
                foreach ($pages as $pagg) {
                    $option = '<option value="' . ($pagg->ID) . '" ';
                    if (get_option('biztech_redirect_login') == ($pagg->ID)) {
                        $option .= 'selected=selected';
                    } else if ((get_option('biztech_redirect_login') == NULL ) && $pagg->ID == $login_page) {
                        $option .= 'selected=selected';
                    }
                    $option .= '>';
                    $option .= $pagg->post_title;

                    $option .= '</option>';
                    echo $option;
                }
                ?>
            </select>
        </td>
    </tr>

    <!--Added By BC on 19-feb-016 to provide option for redirect page for changing profile-->
    <tr class="hide_class">
        <th scope='row'><label><?php _e('Profile Page') ?></label></th>
        <td>
            <select name = "biztech_redirect_profile" id="biztech_redirect_profile">
                <?php
                $page = get_page_by_path('portal-profile');
                $profile_page = $page->ID;
                foreach ($pages as $pagg) {
                    $option = '<option value="' . ($pagg->ID) . '" ';
                    if (get_option('biztech_redirect_profile') == ($pagg->ID)) {
                        $option .= 'selected=selected';
                    } else if ((get_option('biztech_redirect_profile') == NULL ) && $pagg->ID == $profile_page) {
                        $option .= 'selected=selected';
                    }
                    $option .= '>';
                    $option .= $pagg->post_title;

                    $option .= '</option>';
                    echo $option;
                }
                ?>
            </select>
        </td>
    </tr>
    <!--Added By BC on 16-feb-016 to provide option for redirect page for forgot password-->
    <tr class="hide_class">
        <th scope='row'><label><?php _e('Forgot Password Page') ?></label></th>
        <td>
            <select name = "biztech_redirect_forgotpwd" id="biztech_redirect_forgotpwd">
                <?php
                $page = get_page_by_path('portal-forgot-password');
                $forgot_page = $page->ID;
                foreach ($pages as $pagg) {
                    $option = '<option value="' . ($pagg->ID) . '" ';
                    if ((get_option('biztech_redirect_forgotpwd') != NULL) && (get_option('biztech_redirect_forgotpwd') == ($pagg->ID))) {
                        $option .= 'selected=selected';
                    } else if ((get_option('biztech_redirect_forgotpwd') == NULL ) && $pagg->ID == $forgot_page) {
                        $option .= 'selected=selected';
                    }
                    $option .= '>';
                    $option .= $pagg->post_title;

                    $option .= '</option>';
                    echo $option;
                }
                ?>
            </select>
        </td>
    </tr>
    <!-- Added by Akshay on 18-08-2018 to provide option for redirect page for reset password -->
    <tr class="hide_class">
        <th scope='row'><label><?php _e('Reset Password Page') ?></label></th>
        <td>
            <select name = "biztech_redirect_resetpwd" id="biztech_redirect_resetpwd">
                <?php
                $page = get_page_by_path('portal-reset-password');
                $reset_page = $page->ID;
                foreach ($pages as $pagg) {
                    $option = '<option value="' . ($pagg->ID) . '" ';
                    if ((get_option('biztech_redirect_resetpwd') != NULL) && (get_option('biztech_redirect_resetpwd') == ($pagg->ID))) {
                        $option .= 'selected=selected';
                    } else if ((get_option('biztech_redirect_resetpwd') == NULL ) && $pagg->ID == $reset_page) {
                        $option .= 'selected=selected';
                    }
                    $option .= '>';
                    $option .= $pagg->post_title;

                    $option .= '</option>';
                    echo $option;
                }
                ?>
            </select>
        </td>
    </tr>
    <!--Added By BC on 16-feb-016 to provide option for Manage Page -->
    <tr class="hide_class">
        <th scope='row'><label><?php _e('Manage Page') ?></label></th>
        <td>
            <select name = "biztech_redirect_manange" id="biztech_redirect_manange">
                <?php
                foreach ($pages as $pagg) {
                    $option = '<option value="' . ($pagg->ID) . '" ';
                    if (get_option('biztech_redirect_manange') == ($pagg->ID)) {
                        $option .= 'selected=selected';
                    } else if ((get_option('biztech_redirect_manange') == NULL)) {
                        $option .= 'selected=selected';
                    }
                    $option .= '>';
                    $option .= $pagg->post_title;

                    $option .= '</option>';
                    echo $option;
                }
                ?>
            </select>
        </td>
    </tr>
</table>
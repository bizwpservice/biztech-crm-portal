<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$scp_modules = $_SESSION['module_array'];
if (get_page_link(get_option('biztech_redirect_manange')) != NULL) {
    $redirectURL_manage = get_page_link(get_option('biztech_redirect_manange'));
}
else {
    $redirectURL_manage = home_url() . "/portal-manage-page/";
}
global $module_icon_content;
/*
1. New record creation - Calls,Cases,Invoices,Meetings,Quotes,Documents
2. On status change - Calls,Cases,Invoices,Meetings,Quotes,Proposals
3. Case comment add - Cases
 */
$module_action_notification = array();
if ($sugar_crm_version == 7) {
    $module_action_notification = array(
        'Bugs'          => array('new_record' => true, 'status_change' => true),
        'Calls'         => array('new_record' => true, 'status_change' => true),
        'Cases'         => array('new_record' => true, 'status_change' => true),
        'Contracts'     => array('new_record' => true, 'status_change' => true),
        'Documents'     => array('new_record' => true, 'status_change' => false),
        'Leads'         => array('new_record' => true, 'status_change' => true),
        'Meetings'      => array('new_record' => true, 'status_change' => true),
        'Opportunities' => array('new_record' => true, 'status_change' => false),
        'bc_proposal'   => array('new_record' => false, 'status_change' => true),
        'Quotes'        => array('new_record' => true, 'status_change' => true),
        'Tasks'         => array('new_record' => true, 'status_change' => true),
    );
} else {
    $module_action_notification = array(
        'Bugs'          => array('new_record' => true, 'status_change' => true),
        'Calls'         => array('new_record' => true, 'status_change' => true),
        'Cases'         => array('new_record' => true, 'status_change' => true),
        'AOS_Contracts' => array('new_record' => true, 'status_change' => true),
        'Documents'     => array('new_record' => true, 'status_change' => false),
        'AOS_Invoices'  => array('new_record' => true, 'status_change' => true),
        'Leads'         => array('new_record' => true, 'status_change' => true),
        'Meetings'      => array('new_record' => true, 'status_change' => true),
        'Opportunities' => array('new_record' => true, 'status_change' => false),
        'bc_proposal'   => array('new_record' => false, 'status_change' => true),
        'AOS_Quotes'    => array('new_record' => true, 'status_change' => true),
        'Tasks'         => array('new_record' => true, 'status_change' => true),
    );
}
$show_noti = 0;
foreach ($module_action_notification as $key => $value) {
    if (array_key_exists($key, $scp_modules)) {
        $show_noti = 1;
    }
}
$charts_in_array = array();
if ($sugar_crm_version == 7) {
    if (array_key_exists("Quotes", $scp_modules)) {
        $charts_in_array["Quotes"] = $scp_modules['Quotes'];
    }
    if (array_key_exists("Cases", $scp_modules)) {
        $charts_in_array["Cases"] = $scp_modules['Cases'];
    }
} else {
    if (array_key_exists("AOS_Invoices", $scp_modules)) {
        $charts_in_array["AOS_Invoices"] = $scp_modules['AOS_Invoices'];
    }
    if (array_key_exists("Cases", $scp_modules)) {
        $charts_in_array["Cases"] = $scp_modules['Cases'];
    }
    if (array_key_exists("AOS_Quotes", $scp_modules)) {
        $charts_in_array["AOS_Quotes"] = $scp_modules['AOS_Quotes'];
    }
}

$scp_counter_block = array();
$scp_recent_block = array();
$scp_chart_block = array();
$scp_today_schedule = 0;
if (isset($dashboard_pref->scp_contact_counter_blocks_modules)) {
    $scp_counter_block = $dashboard_pref->scp_contact_counter_blocks_modules;
    $scp_counter_block = (array) $scp_counter_block;
    $count_counter_block = count($scp_counter_block);
}
if (isset($dashboard_pref->scp_contact_recent_blocks_modules)) {
    $scp_recent_block = $dashboard_pref->scp_contact_recent_blocks_modules;
    $scp_recent_block = (array) $scp_recent_block;
}
if (isset($dashboard_pref->scp_contact_charts_blocks_modules)) {
    $scp_chart_block = $dashboard_pref->scp_contact_charts_blocks_modules;
    $scp_chart_block = (array) $scp_chart_block;
}
if (isset($dashboard_pref->scp_contact_enable_schedule_blocks)) {
    $scp_today_schedule = $dashboard_pref->scp_contact_enable_schedule_blocks;
}

if ($scp_modules != NULL) {
    
?>

<div class='white-body-shadow preferences-content overlay-bg-image'>
    <div class="preferences-header white-bg-heading">
        <h2><?php echo $language_array['lbl_preference']; ?></h2>
    </div>
    <div id="notification_setting_msg">
        <?php
        if (isset($_SESSION['preferences_success']) && $_SESSION['preferences_success'] != '') {
            echo "<div class='alert alert-success alert-dismissible fade show preferences_success bcp-success'><span>" . $_SESSION['preferences_success'] . "</span><button type='button' class='btn-close' data-bs-dismiss='alert'></button></div>";
            unset($_SESSION['preferences_success']);
        }
        ?>
    </div>
    <div class="preferences-tabs white-main-body">
        <form method="post" action="<?php echo site_url(); ?>/wp-admin/admin-post.php" name="settings_form" id="settings_form" enctype="multipart/form-data">
                <!-- tabs-menu start -->
                <div class="tabs-menu">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link link-secondary active" id="dashboard_preferences-tab" data-bs-toggle="tab" data-bs-target="#dashboard_preferences" href="JavaScript:void(0);"><?php echo $language_array['lbl_dashboard_preferences']; ?></a>
                        </li>
                        <?php if ($show_noti) { ?>
                            <li class="nav-item">
                                <a class="nav-link link-secondary" id="notifications-tab" data-bs-toggle="tab" data-bs-target="#notifications" href="JavaScript:void(0);"><?php echo $language_array['lbl_notifications_pref']; ?></a>
                            </li>
                        <?php } ?>
                    </ul>
                    <!-- tab-content start -->
                    <div class="tab-content" id="tabContent">
                        <!-- dashboard_preferences-tab start -->
                        <div class="tab-pane fade active show" id="dashboard_preferences" role="tabpanel" aria-labelledby="dashboard_preferences-tab">
                            <!-- dashboard_preferences-blocks start -->
                            <div class="dashboard-preference-blocks">
                                <!-- counter-block start -->
                                <div class="counter-block common-block">
                                    <h3><?php echo $language_array['lbl_counter_blocks']; ?> <span>(<?php echo $language_array['lbl_maximum_four_modules']; ?>)</span></h3>
                                    <ul class="counter-block-ul">
                                        <?php
                                        foreach ($scp_modules as $key => $module){
                                            if ( $key == 'KBContents' || $key == 'AOK_KnowledgeBase' ) { // ignore Knowledge-base documents from block
                                                continue;
                                            }
                                            $checked = "";
                                            $active = "";
                                            if($count_counter_block == 4) {
                                                $active = "disbled";
                                            }
                                            if (in_array($key, $scp_counter_block)) {
                                                $checked = "checked";
                                                $active = "active";
                                            }
                                            ?>
                                            <li class="nav-item <?php echo $active; ?>">
                                                <a href="JavaScript:void(0);" class="nav-link">
                                                    <div class="menu-image">
                                                        <?php 
                                                        if($module_icon_content[strtolower($key)]) {
                                                            echo $module_icon_content[strtolower($key)];
                                                        } else {
                                                           echo $module_icon_content['default']; 
                                                        }
                                                        ?>
                                                    </div>
                                                    <label class="menu-title" for="counter_block_<?php echo $key; ?>"><?php echo $module; ?></label>
                                                    <input <?php echo $checked; ?> type="checkbox" class="scp_contact_counter_blocks_modules no-checkbox" name="scp_contact_counter_blocks_modules[]" id="counter_block_<?php echo $key; ?>" value="<?php echo $key; ?>" />
                                                    <label for="counter_block_<?php echo $key; ?>"></label>
                                                    
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <!-- counter-block end -->
                                
                                <!-- counter-block start -->
                                <div class="counter-block common-block">
                                    <h3><?php echo $language_array['lbl_recent_block']; ?></h3>
                                    <ul>
                                        <?php
                                        foreach ($scp_modules as $key => $module){
                                            $checked = "";
                                            $active = "";
                                            if (in_array($key, $scp_recent_block)) {
                                                $checked = "checked";
                                                $active = "active";
                                            }
                                            ?>
                                            <li class="nav-item <?php echo $active; ?>">
                                                <a href="JavaScript:void(0);" class="nav-link">
                                                    <div class="menu-image">
                                                        <?php 
                                                        if($module_icon_content[strtolower($key)]) {
                                                            echo $module_icon_content[strtolower($key)];
                                                        } else {
                                                           echo $module_icon_content['default']; 
                                                        }
                                                        ?>
                                                    </div>
                                                    <label class="menu-title" for="scp_recent_block_<?php echo $key; ?>"><?php echo $module; ?></label>
                                                    <input <?php echo $checked; ?> type="checkbox" class="scp_contact_recent_blocks_modules no-checkbox" name="scp_contact_recent_blocks_modules[]" id="scp_recent_block_<?php echo $key; ?>" value="<?php echo $key; ?>" />
                                                    <label for="scp_recent_block_<?php echo $key; ?>"></label>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <!-- counter-block end -->
                                
                                <!-- counter-block start -->
                                <?php if (!empty($charts_in_array)) { ?>
                                <div class="counter-block common-block">
                                    <h3><?php echo $language_array['lbl_charts']; ?></h3>
                                    <ul>
                                        <?php
                                            foreach ($charts_in_array as $key => $value) {
                                                $checked = "";
                                                $active = "";
                                                if (in_array($key, $scp_chart_block)) {
                                                    $checked = "checked";
                                                    $active = "active";
                                                }
                                                if (isset($scp_modules[$key])) {
                                                ?>
                                                <li class="nav-item <?php echo $active; ?>">
                                                    <a href="JavaScript:void(0);" class="nav-link">
                                                        <div class="menu-image">
                                                            <?php 
                                                            if($module_icon_content[strtolower($key)]) {
                                                                echo $module_icon_content[strtolower($key)];
                                                            } else {
                                                               echo $module_icon_content['default']; 
                                                            }
                                                            ?>
                                                        </div>
                                                        <label class="menu-title" for="scp_charts_<?php echo $key; ?>"><?php echo $scp_modules[$key]; ?></label>
                                                        <input <?php echo $checked; ?> type="checkbox" class="scp_contact_charts_blocks_modules no-checkbox" name="scp_contact_charts_blocks_modules[]" id="scp_charts_<?php echo $key; ?>" value="<?php echo $key; ?>" />
                                                        <label for="scp_charts_<?php echo $key; ?>"></label>
                                                       
                                                    </a>
                                                </li>
                                                <?php
                                                }
                                            }
                                        ?>
                                    </ul>
                                </div>
                                <?php } ?>
                                <!-- counter-block end -->
                                
                                <!-- counter-block start -->
                                <?php if (array_key_exists( 'Calls', (array) $scp_modules ) || array_key_exists( 'Meetings', (array) $scp_modules )) { ?>
                                <div class="counter-block common-block">
                                    <h3><?php echo $language_array['lbl_today_schedule_blocks']; ?></h3>
                                    <ul>
                                        <?php
                                        $schedule_checked = "";
                                        $active = "";
                                        if ($scp_today_schedule){ 
                                            $schedule_checked = "checked";
                                            $active = "active";
                                        }
                                        ?>
                                        <li class="nav-item <?php echo $active; ?>">
                                            <a href="JavaScript:void(0);" class="nav-link">
                                                <div class="menu-image">
                                                    <?php echo $module_icon_content['tick-square']; ?>
                                                </div>
                                                <label class="menu-title" for="scp_contact_enable_schedule_blocks"><?php echo $language_array['lbl_enable']; ?></label>
                                                <input <?php echo $schedule_checked; ?> type="checkbox" class="scp_contact_enable_schedule_blocks no-checkbox" id="scp_contact_enable_schedule_blocks" name="scp_contact_enable_schedule_blocks" value="1" />
                                                <label for="scp_contact_enable_schedule_blocks"></label>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <?php } ?>
                                <!-- counter-block end -->
                                
                            </div>
                            <!-- dashboard_preferences-blocks end -->
                        </div>
                        <!-- dashboard_preferences-tab end -->
                        
                        <?php if ($show_noti) { ?>
                        <!-- notifications-tab start -->
                        <div class="tab-pane fade" id="notifications" role="tabpanel" aria-labelledby="notifications-tab">
                            <!-- notifications-preference-blocks start -->
                            <div class="notifications-preference-blocks">
                                <?php
                                foreach ($scp_modules as $key => $module){
                                    if (isset($module_action_notification[$key])) {
                                ?>
                                <!-- preference-blocks start -->
                                <div class="preferences-block common-block d-flex align-items-center">
                                    <h3><?php echo $module; ?></h3>
                                    <ul>
                                        <?php foreach ($module_action_notification[$key] as $action => $value){ 
                                            if ($value) {
                                                $checked = "";
                                                $active = "";
                                                if ((isset($notification_permission->$key->$action) && $notification_permission->$key->$action) || $notification_permission == NULL) {
                                                    $checked = "checked=''";
                                                    $active = "active";
                                                }
                                                if($action == 'new_record'){
                                                    $name = $language_array['lbl_create'];
                                                    $noti_svg = $module_icon_content['create'];
                                                } else {
                                                    $name = $language_array['lbl_status_changes'];
                                                    $noti_svg = $module_icon_content['status-change'];
                                                }
                                                ?>
                                                <li class="nav-item <?php echo $active; ?>">
                                                    <a class="nav-link" href="JavaScript:void(0);">
                                                        <div class="menu-image">
                                                            <?php echo $noti_svg; ?>
                                                        </div>
                                                        <label class="menu-title" for="checkbox<?php echo $key.$name; ?>"><?php echo $name; ?></label>
                                                        <input type="checkbox" value="<?php echo $value; ?>" name="scp_notification_permission[<?php echo $key; ?>][<?php echo $action; ?>]" <?php echo $checked ?> id="checkbox<?php echo $key.$name; ?>" class="no-checkbox" />
                                                        <label for="checkbox<?php echo $key.$name; ?>"></label>
                                                    </a>
                                                </li>
                                            
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <!-- preference-blocks end -->
                                <?php }
                                } 
                                ?>
                                
                                <?php
                                    if (isset($scp_modules['Cases'])) {
                                    $comment_checked = "";
                                    $active = "";
                                        if ((isset($notification_permission->Cases->case_update) && $notification_permission->Cases->case_update) || $notification_permission == NULL) {
                                            $comment_checked = "checked=''";
                                            $active = "active";
                                        }
                                ?>
                                <!-- preference-blocks start -->
                                <div class="preferences-block common-block d-flex align-items-center">
                                    <h3><?php echo $language_array['lbl_case_update']; ?></h3>
                                    <ul>
                                        <li class="nav-item <?php echo $active; ?>">
                                            <a class="nav-link" href="JavaScript:void(0);">
                                                <div class="menu-image">
                                                    <?php echo $module_icon_content['create']; ?>
                                                </div>
                                                <label class="menu-title" for="checkbox"><?php echo $language_array['lbl_create']; ?></label>
                                                <input type="checkbox" <?php echo $comment_checked; ?> value="1" name="scp_notification_permission[Cases][case_update]" class="no-checkbox" />
                                                <label for="checkbox"></label>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- preference-blocks end -->
                                <?php } ?>
                                
                            </div>
                            <!-- notifications-preference-blocks end -->
                        </div>
                        <!-- notifications-tab end -->
                        <?php } ?>
                        
                    </div>
                    <!-- tab-content end -->
                </div>
                <!-- tabs-menu end -->

                <div class="preferences-button-group d-inline-block w-100">
                    <div class="case-button-group">
                        <input type='submit' name='update_notification_settings' value='<?php echo $language_array['lbl_save']; ?>' class='scp-Accounts scp-button btn' />
                        <input type='button' value='<?php echo $language_array['lbl_cancel']; ?>' class='btn border-button' onclick="window.location = '<?php echo $redirectURL_manage; ?>#data/Dashboard/'"/>
                    </div>
                </div>


        </form>
    </div>
</div>
<script>
jQuery('.nav-link input').change(function(){
    
    if (jQuery(this).hasClass("scp_contact_counter_blocks_modules")) {
        if (jQuery(".scp_contact_counter_blocks_modules:checked").length > 4 && jQuery(this).is(":checked")) {
            jQuery(this).removeAttr("checked");
            jQuery(this).prop('checked', false);
            jQuery(this).parents('li').removeClass('active');
            jQuery('.counter-block-ul li').each(function() {
                if (!jQuery(this).hasClass("active")) {
                    jQuery(this).addClass('disbled');
                }
            });
        } else {
            if(jQuery(this).is(":checked")) {
                jQuery(this).parents('li').addClass('active');
                if(jQuery(".scp_contact_counter_blocks_modules:checked").length == 4) {
                    jQuery('.counter-block-ul li').each(function() {
                        if (!jQuery(this).hasClass("active")) {
                            jQuery(this).addClass('disbled');
                        }
                    });
                }
            } else {
                jQuery(this).parents('li').removeClass('active');
                jQuery('li.nav-item').removeClass('disbled');
            }
        }
    } else {
        if(jQuery(this).is(":checked")) {
            jQuery(this).parents('li').addClass('active');
        } else {
            jQuery(this).parents('li').removeClass('active');
        }
    }
    
});

jQuery(document).on('submit', '#settings_form', function (e) {

        e.preventDefault();
        var $form = $(this);
        if( ! $form.valid() ) return false;
        var counter_checked = jQuery(".scp_contact_counter_blocks_modules:checked").length;
        var recent_checked = jQuery(".scp_contact_recent_blocks_modules:checked").length;
        var charts_checked = jQuery(".scp_contact_charts_blocks_modules:checked").length;
        var schedule_checked = jQuery(".scp_contact_enable_schedule_blocks:checked").length;
        if (counter_checked == 0 && recent_checked == 0 && charts_checked == 0 && schedule_checked == 0) {
            alert("<?php echo $language_array['msg_dashboard_not_empty']; ?>");
            return false;
        }

        jQuery(".preferences-tabs").addClass('bcp-loader');
        var form_data = new FormData(jQuery(this)[0]);

        jQuery.ajax({
            url: ajaxurl + '?action=bcp_update_notification_settings',
            type: 'POST',
            data: form_data,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                var response = jQuery.parseJSON(response);
                jQuery(".preferences-tabs").removeClass('bcp-loader');
                window.location.reload();
            }
        });
    });
</script>
<?php } else { ?>
<label class='error'><?php echo $language_array['msg_no_permission_data']; ?></label>
<?php
}



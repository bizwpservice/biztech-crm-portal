<div class="row">
    <?php
    $old_module_name = $module_name;

    $module_name = 'AOS_Products';
    $view = 'edit';
    $limit = fetch_data_option('biztech_scp_case_per_page');
    $offset = '0';
    $contact_group_id = isset($_SESSION['contact_group_id']) ? $_SESSION['contact_group_id'] : "";
    $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='list' AND contact_group='" . $contact_group_id . "' AND lang_code='" . $lang_code . "'";
    $result_count = $wpdb->get_row($sql_count);
    $json_data = $result_count->json_data;
    $results = json_decode($json_data);

    $where_con = "visible_portal = 1";
    $order_by_query = "date_entered desc";

    if (!empty($results)) {

        $id_show = false;
        foreach ($results as $key_n => $val_n) {
            if ($key_n == 'ID') {
                $id_show = true;
            }
            if (!empty($val_n->related_module)) {
                if ($val_n->type == 'relate' && (!in_array($val_n->related_module, $_SESSION['module_array']))) {//if relate module is not in active module array
                    continue;
                }
            }
            $n_array[] = strtolower($key_n);
        }
        if (!in_array('id', $n_array)) {
            array_push($n_array, 'id');
        }
        array_push($n_array, 'currency_id');
    }

    $where_con = 'visible_portal = 1';
    $selected_product_lineitems = array();

    if (isset($record_detail->entry_list[0]->name_value_list->line_items->value) && $record_detail->entry_list[0]->name_value_list->line_items->value != '') {  // If request for edit proposal
        $proposal_lineitem = json_decode(html_entity_decode($record_detail->entry_list[0]->name_value_list->line_items->value));

        $product_lineitem = $proposal_lineitem->product_lineitem;
        $service_lineitem = $proposal_lineitem->service_lineitem;
        $product_array = array();
        $qty_array = array();

        foreach ($product_lineitem as $key => $value) {

            array_push($product_array, $value->id);
            $selected_product_lineitems[] = array(
                'product_id' => $value->id,
                'product_qty' => $value->qty,
            );
            $qty_array[$value->id] = $value->qty;
        }
        if (!empty($product_array)) {
            $where_con .= ' AND ' . strtolower($module_name) . '.id NOT IN("' . implode('","', $product_array) . '")';
        }
    }

    $list_result = $objSCP->get_entry_list($module_name, $where_con, $n_array, $order_by_query, 0, $limit);
    $total_results = $objSCP->get_entry_list($module_name, 'visible_portal = 1', $n_array, $order_by_query, 0, $limit);

    if ($_SESSION['module_array']) {
        $arry_lable = (array) $_SESSION['module_array'];
        $product_name_label = $arry_lable[$module_name];
    }
    ?>
    <div class="col-md-8 first-div-product">
        <div class="imex-content-table bg-white shadow add-proposals-table product-proposals-block">
            <div class='error-line-item'></div>
            <?php
            if (!empty($results) && isset($_SESSION['module_array']['AOS_Products'])) {
                $tot_count = $total_results->total_count;
                $totPages = ceil($tot_count / $limit);
                $crmcaseperpage = ((get_option('biztech_scp_case_per_page') != NULL) ? get_option('biztech_scp_case_per_page') : "5");
                $cnt = count($total_results->entry_list);
                ?>
                <div class='main-heading-table d-flex justify-content-between scp_product_search_action'>
                    <h3 class="cases-heading d-flex align-items-center">
                        <span><?php echo $product_name_label; ?></span>
                        <a href="#data/<?php echo $module_name; ?>/list/" class="see-all" title="<?php echo $language_array['lbl_view_all']; ?>">
                            <span><?php echo $language_array['lbl_view_all']; ?></span>
                           <em class="fas fa-chevron-right"></em>
                        </a>
                    </h3>
                    <div class="nav-item search-box scp_product_search_div">
                        <div class="input-group">
                            <div class="d-flex search-main-proposals">
                                <div class="search-input">
                                    <?php echo $module_icon_content['search']; ?>
                                    <input type='text' class='form-control rounded' placeholder='<?php echo $language_array['lbl_search_by_product_name']; ?>' title='<?php echo $language_array['lbl_enter_product_name']; ?>' data-version='<?php echo $sugar_crm_version; ?>' id='search-product' aria-label="Search" aria-describedby="search-addon">
                                    <a href="JavaScript:void(0);" title='<?php echo $language_array['lbl_clear']; ?>' onclick='bcp_product_search("<?php echo $module_name; ?>", "<?php echo $view; ?>", "", "reset");' class="clear-btn"><em class="fa fa-remove" aria-hidden="true"></em></a>
                                </div>
                                <a href="JavaScript:void(0);" title='<?php echo $language_array['lbl_search']; ?>' onclick='bcp_product_search("<?php echo $module_name ?>", "<?php echo $view; ?>", "", "search");' class="btn btn-grey-curve btn-primary">
                                    <?php echo $language_array['lbl_search']; ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class='table-responsive table-content custom-scrollbar proposals-table'>
                    <?php if ($cnt > 0) { ?>
                        <?php include( TEMPLATE_PATH . 'bcp_product_list_page.php'); ?>
                        <input type='hidden' id='pageno' value='2'>
                        <input type='hidden' class='product_lineitems_id' value=''>
                        <input type='hidden' class='order_by' value='date_entered'>
                        <input type='hidden' class='order' value='desc'>
                        <?php if ($totPages > 1) { ?>
                            <a href='javascript:void(0);' id='show_more_btn' onclick='bcp_product_show_more("<?php echo $module_name; ?>", "<?php echo $view; ?>", "<?php echo $current_url; ?>");'><?php echo $language_array['lbl_show_more']; ?></a>
                            <?php
                        }
                    } else {
                        ?>
                        <strong><?php echo isset($_SESSION['portal_message_list']->module_no_records) ? __($_SESSION['portal_message_list']->module_no_records) : ''; ?></strong>
                <?php } ?>

                </div>
<?php } ?>
        </div>
    </div>
    <div class="col-md-4 service-proposal-block">
        <div class="white-body-shadow preferences-content service-block">
            <div class="preferences-header white-bg-heading justify-content-between d-flex">
                <h3><?php echo $language_array['lbl_service']; ?></h3>
                <?php 
                $count = 1;
                if (isset($service_lineitem) && !empty($service_lineitem)) {
                    $count = count($service_lineitem);
                }
                ?>
                <a href='javascript:void(0);' data-index='<?php echo $count; ?>' class='scp_proposal_btn_add_more add-more-btn btn'><?php echo $language_array['lbl_add']; ?></a>
            </div>
            <div class="service-add-block">
                <?php
                    $index = 0;
                    if (isset($service_lineitem) && !empty($service_lineitem)) {
                        foreach ($service_lineitem as $index => $single_service_lineitem) {
                ?>
                <div class="service-enter">
                    <input type='text' placeholder='<?php echo $language_array['lbl_enter_service_name']; ?>' title='<?php echo $language_array['lbl_enter_service_name']; ?>' class='form-control scp_proposal_input_service_name' name='service_lineitem[<?php echo $index; ?>][name]' value='<?php echo $single_service_lineitem->name; ?>'>
                    <div class="service-action-btn active">
                        <div class="close-service action-btn">
                            <a href='javascript:void(0);' title='<?php echo $language_array['lbl_remove']; ?>' class='scp_proposal_btn_remove_more'>
                                <?php echo $module_icon_content['close']; ?>
                            </a>
                            
                        </div>
                    </div>
                </div>
                <?php
                    }
                    $index++;
                }
                $cls = 'show';
                if($index == 0){
                    $cls = 'hide';
                }
                ?>
                <div class="service-enter">
                    <input type='text' placeholder='<?php echo $language_array['lbl_enter_service_name']; ?>' title='<?php echo $language_array['lbl_enter_service_name']; ?>' class='input-text form-control scp_proposal_input_service_name' name='service_lineitem[<?php echo $index; ?>][name]' >
                    <div class="service-action-btn active">
                        <div class="close-service action-btn <?php echo $cls; ?>">
                            <a href='javascript:void(0);' title='<?php echo $language_array['lbl_remove']; ?>' class='scp_proposal_btn_remove_more'>
                                <?php echo $module_icon_content['close']; ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <input type="hidden" class="selected_product_lineitems" name="selected_product_lineitems" value="<?php echo (!empty($selected_product_lineitems) ? htmlspecialchars(json_encode($selected_product_lineitems)) : '' ); ?>" />
<?php $module_name = $old_module_name; ?>
</div>
<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if ( ! function_exists( 'add_bcp_knowledge_base_shortcode' ) ) {
    add_shortcode( 'bcp-knowledge-base', 'add_bcp_knowledge_base_shortcode' );
    function add_bcp_knowledge_base_shortcode( $content = '' ) {

        global $objSCP, $sugar_crm_version;

        wp_enqueue_script( 'jquery-ui-accordion' );

        ob_start();
        if ( $objSCP ) {
            ?>
                <div class="bcp-kb">
                    <form id="bcp-kb-search" method="post">
                        <input type="text" name="search" placeholder="<?php _e( 'Search...' ); ?>" />
                        <?php if ( $sugar_crm_version == 5 ) { ?>
                                <select name="category">
                                    <option value=""><?php _e( 'Select a category' ); ?></option>
                                    <?php
                                    $categories = $objSCP->get_entry_list( 'AOK_Knowledge_Base_Categories', '' , array( 'id', 'name' ), 'name ASC' );
                                    if ( isset( $categories->entry_list ) && $categories->entry_list != null ) {
                                        foreach ( $categories->entry_list as $category ) {
                                            ?><option value="<?php echo $category->name_value_list->id->value; ?>"><?php echo $category->name_value_list->name->value; ?></option><?php
                                        }
                                    }
                                ?></select>
                                <?php } ?>
                        <button type="submit"><?php _e( 'Search' ); ?></button>
                        <button type="reset"><?php _e( 'Clear' ); ?></button>
                    </form>
                    <div id="bcp-kb-response"></div>
                </div>
                <script type="text/javascript">
                    var ajax_url = '<?php echo admin_url( 'admin-ajax.php' ); ?>';

                    jQuery( document ).ready( function( $ ) {
                        $( 'body' ).addClass( 'bcp-kb-template' );
                        bcp_kb_shortcode();

                        $( '#bcp-kb-search').on( 'submit', function() {
                            var search = $( '#bcp-kb-search input[name="search"]').val();
                            var category = $( '#bcp-kb-search select[name="category"]').val();

                            if ( search || category ) {
                                bcp_kb_shortcode( search, category );
                            }

                            return false;
                        });

                        $( '#bcp-kb-search').on( 'reset', function() {
                            bcp_kb_shortcode();
                        });
                    });

                    function bcp_kb_shortcode( search, category ) {
                        var data = {
                            'action': 'bcp_kb_shortcode'
                        };

                        if ( search ) {
                            data.search = search;
                        }

                        if ( category ) {
                            data.category = category;
                        }

                        jQuery( '#bcp-kb-response' ).addClass( 'bcp-kb-loading' );
                        jQuery.post( ajax_url, data, function( response ) {
                            jQuery( '#bcp-kb-response' ).html( response );
                            jQuery( '#bcp-kb-response' ).removeClass( 'bcp-kb-loading' );
                        });
                    }
                </script>
            <?php
        }
        $content .= ob_get_clean();

        return $content;
    }
}

if ( ! function_exists( 'bcp_kb_shortcode_callback' ) ) {
    add_action( 'wp_ajax_bcp_kb_shortcode', 'bcp_kb_shortcode_callback' );
    add_action( 'wp_ajax_nopriv_bcp_kb_shortcode', 'bcp_kb_shortcode_callback' );
    function bcp_kb_shortcode_callback() {

        global $objSCP, $sugar_crm_version;

        $search = $_REQUEST['search'];
        $category = $_REQUEST['category'];

        $content = '';
        ob_start();
            if ( $objSCP ) {
                if ( $sugar_crm_version == 5 ) {
                    $fields = array( 'id', 'name', 'description' );
                    $where = 'aok_knowledgebase.status="published_public"';
                    if ( $search ) {
                        $where .= ' AND aok_knowledgebase.name like "%'.$search.'%"';
                    }

                    if ( $category ) {
                        $kbs = $objSCP->get_relationships( 'AOK_Knowledge_Base_Categories', $category, 'aok_knowledgebase_categories', $fields, $where );
                    } else {
                        $kbs = $objSCP->get_entry_list( 'AOK_KnowledgeBase', $where, $fields );
                    }

                    if ( isset( $kbs->entry_list ) && $kbs->entry_list != null ) {
                        ?>
                        <div class="bcp-kb-accordion">
                        <?php foreach ( $kbs->entry_list as $kb ) { ?>
                            <h3 class="bcp-kb-title"><?php echo $kb->name_value_list->name->value; ?></h3>
                                <div class="bcp-kb-description"><?php echo html_entity_decode( $kb->name_value_list->description->value ); ?></div>
                            <?php } ?></div>
                        <?php } else { ?>
                            <p><?php _e( 'No records found.' ); ?></p>
                        <?php
                    }
                } else if ( $sugar_crm_version == 7 ) {
                    $fields = 'id,name,kbdocument_body,category_name';
                    $where = array(
                        'status'  => array(
                            '$contains' => 'Published',
                        )
                    );

                    if ( $search ) {
                        $where = array(
                            '$and' => array(
                                array(
                                    'status' => array(
                                        '$contains' => 'Published',
                                    )
                                ),
                                array(
                                    'name' => array(
                                        '$contains' => $search,
                                    )
                                )
                            ),
                        );
                    }

                    $kbs = $objSCP->get_entry_list_custom( 'KBContents', $fields, $where, '', '', 'date_entered:DESC' );

                    if ( isset( $kbs->records ) && $kbs->records != null ) {
                        ?>
                        <div class="bcp-kb-accordion"><?php
                        foreach ( $kbs->records as $kb ) {
                            ?>
                            <h3 class="bcp-kb-title"><?php echo $kb->name; ?><span class="kb-cat"><?php echo $kb->category_name; ?></span></h3>
                                <div class="bcp-kb-description"><?php echo html_entity_decode( $kb->kbdocument_body ); ?></div>
                            <?php
                        }
                        ?></div><?php
                    } else {
                        ?>
                            <p><?php _e( 'No records found.' ); ?></p>
                        <?php
                    }
                } 
            } else {
                ?>
                    <div class="bcp-error-msg">
                        <p><?php echo ( isset( $_SESSION['bcp_connection_error'] ) ? $_SESSION['bcp_connection_error'] : '' ); ?></p>
                    </div>
                <?php
            }
            if ( isset( $_SESSION['bcp_connection_error'] ) ) {
                unset( $_SESSION['bcp_connection_error'] );
            }
            ?>
            <script type="text/javascript">
                jQuery( document ).ready( function( $ ) {
                    $( '.bcp-kb-accordion' ).accordion( {
                        heightStyle: 'content',
                        collapsible: true,
                    } );
                });
            </script>
            <style>
                <?php
                    $theme_color = get_option( 'biztech_scp_theme_color' );
                    if ( $theme_color == null ) {
                        $theme_color = get_option( 'biztech_scp_theme_color' );
                    }
                    if ( $theme_color ) {
                        ?>.bcp-kb .bcp-kb-title span::before {color: <?php echo $theme_color; ?>;}
                    <?php } ?>
            </style>
            <?php
        $content .= ob_get_clean();

        echo $content;

        wp_die();
    }
}
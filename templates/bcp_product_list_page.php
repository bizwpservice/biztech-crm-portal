<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
$i=0;
?>
<?php if ($offset == 0) { ?>
<table id='bcp_product_list_table' class='table w-100'>
<?php } ?>

<?php
if ($sugar_crm_version == 7) {
    if ($offset == 0) { ?>
        <tr class='main-col'>
    <?php }
    if (($view != 'detail' && empty($id)) || $view == 'edit' ) {
        if ($offset == 0) { ?>
            <th class='check_all_product'></th>
            <?php
        }
    }

    if ( ( ( $view == 'edit' && $offset == 0 ) ||
           ( isset( $_REQUEST['request_type'] ) && $_REQUEST['request_type'] == 'reset' ) ) &&
         ( ! empty( $product_array ) ) ) {

        if ( isset( $where_con['id'] ) ) {
            $where_con['id'];
        }
        $where_con['id'] = array('$in' => $product_array );
        $tmp_where_con['$and'] = array( $where_con );
        $selected_items_result = $objSCP->get_entry_list_custom( $module_name, implode(',', $n_array), $tmp_where_con, '-1', '0', $order_by );
        if ( isset( $selected_items_result->records ) && count((array)$selected_items_result->records) > 0 ) {
            $list_result->records = array_merge($selected_items_result->records, $list_result->records);
        }
    }

    if ( ( ( ! isset( $qty_array ) ) || ( isset( $qty_array ) && empty( $qty_array ) ) ) && ( isset( $selected_product_lineitems ) && ! empty( $selected_product_lineitems ) ) ) {
        foreach ($selected_product_lineitems as $key => $value) {
            $qty_array[$value->product_id] = $value->product_qty;
        }
    }

    foreach ($results as $key_name => $val_array) {
        if ($key_name == "SET_COMPLETE") {
            continue;
        }
        
        $val_ary_type = (isset($val_array->type)) ? $val_array->type : NULL;
        if (isset($val_array->related_module)) {
            if ($val_ary_type == 'relate' && (!array_key_exists($val_array->related_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array
                continue;
            }
        }
        $ar_val = strtolower($key_name);
        $name_arry[] = $ar_val;
        //make type array

        $fields_meta[$ar_val] = $val_ary_type;

        if (isset($val_array->label_value)) {
            $field_label = str_replace(':', '', $val_array->label_value);
        } else {
            $field_label = '';
        }
        $field_name = $ar_val;
        if ($field_label == "modified_user_id" || $field_label == "created_by" || ($field_label == "id" && $id_show == false)) {
            continue;
        }
        if ($offset == 0 && $field_label != '') {
            ?>
            <th class="text-left"><a><?php echo $field_label; ?></a></th>
            <?php
        }
    }
    if ($offset == 0) { ?>
       <th class='action-th'><a><?php echo $language_array['lbl_qty']; ?></a></th>
        
        <th class='action-th'><a><?php echo $language_array['lbl_total']; ?></a></th>
        
        </tr>
    <?php }
    foreach ($list_result->records as $list_result_s) {

        if (isset($list_result_s)) {
            $id = $list_result_s->id;
        }
        if (isset($list_result_s->parent_type)) {
            $parent_type = $list_result_s->parent_type;
        }

       if ( isset( $list_result_s->currency_id ) && $list_result_s->currency_id != '' ) {
            $curreny_id = $list_result_s->currency_id;
            $currency_symbol = $_SESSION['scp_all_currency']->$curreny_id->currency_symbol;
        }

        $team_arr = array();
        ?>
        <tr class='<?php echo $id; ?>' data-page_type='list-view'>
        <?php if (($view != 'detail' && empty($id)) || $view == 'edit' ) {
            if (isset($product_array) && in_array($id,$product_array)) {
                ?>
                <td>
                    <div class='custom-control custom-checkbox'>
                    <input type='checkbox' data-index='<?php echo $i; ?>' value='<?php echo $id; ?>' class='input-text scp-form-control scp-special-checkbox input_checkbox' name='product_lineitem[<?php echo $i; ?>][id]' checked='checked'>
                    </div>
                </td>
            <?php }else{ ?>
                <td>
                    <div class='custom-control custom-checkbox'>
                    <input type='checkbox' data-index='<?php echo $i; ?>' value='<?php echo $id; ?>' class='input-text scp-form-control scp-special-checkbox input_checkbox' name='product_lineitem[<?php echo $i; ?>][id]' >
                    </div>
                </td>
                <?php
            }
        }
        $cost_price = 0;
        foreach ($name_arry as $nkey => $nval) {
            switch ($nval) {
                case 'assigned_user_id':
                    $get_entrty_module = "Users";
                    break;
                case 'modified_user_id':
                    $get_entrty_module = "Users";
                    break;
                case 'created_by':
                    $get_entrty_module = "Users";
                    break;
                case 'parent_id':
                    if ($parent_type) {
                        $get_entrty_module = $parent_type;
                    } else {
                        $get_entrty_module = "";
                    }
                    break;
                case 'account_name':
                    $get_entrty_module = "Accounts";
                    break;
                default:
                    $get_entrty_module = "";
                    break;
            }
            $hlink_st = $hlink_en = $cls = '';
            $value = (isset($list_result_s->$nval)) ? $list_result_s->$nval : NULL;

            if ($nval == 'list_price') {
                $cost_price = $value;
            }
            if ($nval == "filename") {
                $download_file = 0;
                if (!empty($value)) {
                    $cls = "general-link-btn scp-download-btn";
                    if ($module_name == "Notes") {
                        $hlink_st = "<a title='" . $language_array['lbl_download'] . "' href='javascript:void(0);' onclick='form_submit_note_document(\"$id\",\"filename\");' class='kb-attachment $cls scp-$module_name-font'>";
                        $hlink_en = "</a>";
                        $download_file = 1;
                    }
                    if ($module_name == "Documents") {
                        $hlink_st = "<a title='" . $language_array['lbl_download'] . "' href='javascript:void(0);' onclick='form_submit_document(\"$id\");' class='kb-attachment $cls scp-$module_name-font'>";
                        $hlink_en = "</a>";
                        $download_file = 1;
                    }
                }
            }
            if ($nval == "date_modified" || $nval == "date_start" || $nval == "date_end" || $nval == "date_entered" || $nval == "date_due") {
                if ($value != '') { // aaded to display blank date instead of default date
                    if ( $_SESSION['browser_timezone'] != NULL ) {
                        $value = scp_user_time_convert( $value );
                    } else {
                        $UTC = new DateTimeZone("UTC");
                        $newTZ = new DateTimeZone($result_timezone);
                        $date = new DateTime($value, $UTC);
                        $date->setTimezone($newTZ);
                        $date_format = $_SESSION['user_date_format'];
                        $time_format = $_SESSION['user_time_format'];
                        $value = $date->format($date_format . " " . $time_format);
                    }
                }
            }
            if ($fields_meta[$nval] == 'date') {//for date field
                $value = (!empty($value)) ? date($objSCP->date_format, strtotime($value)) : '';
            }
            if ($get_entrty_module) {
                if ($nval != "account_name" && $module_name != "Leads") {
                    $select_fields = array('name');
                    $record_detail = $objSCP->getModuleRecords($get_entrty_module, $value, $select_fields);
                    if (isset($record_detail) && !empty($record_detail)) {
                        $value = $record_detail->records[0]->name;
                    }
                }
            }

            if ($fields_meta[$nval] == 'enum' || $fields_meta[$nval] == 'multienum' || $fields_meta[$nval] == 'dynamicenum') {
                $res_getEnum = $objSCP->getEnumValues($module_name, $nval);
                foreach ($res_getEnum as $k_opt => $v_opt) {
                    if ($value == $k_opt) {
                        $value = (!empty($value)) ? $v_opt : '';
                    }
                }
            }
            if ($fields_meta[$nval] == 'currency' && !empty($value)) {
                $value = $currency_symbol . number_format($value, 2);
            }
            if ( $type_array[$nval] == 'bool' ) {
                if ($value == 1) {
                    $value = $language_array['lbl_yes'];
                } else {
                    $value = $language_array['lbl_no'];
                }
            }
            if ($nval == "team_name") {
                foreach ($value as $teams) {
                    $team_arr[] = $teams->name;
                }
                $value = implode(', ', $team_arr);
            }

            if ($nval == 'link') {
                continue;
            }
            if ($fields_meta[$nval] == 'url' && !empty($value)) {
                if (strpos($value, 'http') === false) {
                    $value = 'http://' . $value;
                }
                $hlink_st = "<a title='" . $language_array['lbl_download'] . "' href='" . $value . "' target='_blank'>";
                $hlink_en = "</a>";
            }
            if ($fields_meta[$nval] == 'email') {//for email field
                $value = (!empty($value[0]->email_address)) ? $value[0]->email_address : '';
            }
            if ( $nval == 'currency_id' ) {
                $value = $_SESSION['scp_all_currency']->$value->currency_symbol;
            }
            if ($fields_meta[$nval] == 'name') {//for name field

                $hlink_st = "<a href='#data/".$module_name."/detail/".$id."/'>";
                $hlink_en = "</a>";
            }

            if ($value == '' || empty($value) && !$type_array[$nval] == 'int') {
                $value = "-";
            }
            if ($nval == "filename" && !empty($value) && $value != '-') {
                ?>
                <td><?php echo ( ( strlen( $value ) > 100 ) ? substr( $value, 0, 100 ).'...' : $value ); ?></td>
            <?php } else { ?>
                <td><?php echo $hlink_st . ( ( strlen( $value ) > 100 ) ? substr( $value, 0, 100 ).'...' : $value ) . $hlink_en; ?></td>
                <?php
            }
        }

        $deleted = 1;

        if (($view != 'detail' && empty($id)) || $view == 'edit' ) {

            $product_qty = '0';
            $product_price = '0.00';
            $name = "disabled='disabled'";
            if ( isset( $qty_array ) && ! empty( $qty_array ) ) {
                if ( isset( $qty_array[ $id ] ) ) {
                    $product_qty = $qty_array[ $id ];
                    $product_price = number_format($cost_price * $qty_array[$id], 2);
                    $name = "name='product_lineitem[".$i."][qty]'";
                }
            }
            ?>
            <td>
                <input type='number' data-cost='<?php echo $cost_price; ?>' min='0' value='<?php echo $product_qty; ?>'  class='input-text scp-form-control input_qty' id='qty_<?php echo $id; ?>' data-currency='<?php echo $currency_symbol; ?>' data-product-id='<?php echo $id; ?>' <?php echo $name; ?>>
            </td>
            <td class='total_price'><?php echo $currency_symbol.''.$product_price; ?></td>
        <?php } else { ?>

            <td><?php echo $qty_array[$id]; ?></td>
            <?php
            $total_price = $currency_symbol . number_format($cost_price * $qty_array[$id], 2);
            ?>
            <td class='total_price'><?php echo $total_price; ?></td>
            <?php
        }
        $i++;
        ?>
        </tr>
        <?php
    }

} else {
    $total_cnt = $list_result->total_count;
    if ($offset == 0) {
        ?>
        <thead>
        <tr>
    <?php
    }
    if ($view != 'detail') {
        if ($offset == 0) { ?>
            <th class='check_all_product'></th>
            <?php
        }
    }

    if ( ( ( $view == 'edit' && $offset == 0 ) ||
           ( isset( $_REQUEST['request_type'] ) && $_REQUEST['request_type'] == 'reset' ) ) &&
         ( ! empty( $product_array ) ) ) {

        $tmp_where_con = 'visible_portal = 1 AND ' . strtolower($module_name) . '.id IN("' . implode('","', $product_array) . '")';
        if (isset($search_name) && !empty($search_name)) {
            $tmp_where_con .= ' AND ' . strtolower($module_name) . ".name like '%{$search_name}%'";
        }
        $selected_items_result = $objSCP->get_entry_list($module_name, $tmp_where_con, $n_array, $order_by_query, 0, '-1', 0);
        if ( isset( $selected_items_result->entry_list ) && count((array)$selected_items_result->entry_list) > 0) {
            $list_result->entry_list = array_merge($selected_items_result->entry_list, $list_result->entry_list);
        }

    }

    if ( ( ( ! isset( $qty_array ) ) || ( isset( $qty_array ) && empty( $qty_array ) ) ) && ( isset( $selected_product_lineitems ) && ! empty( $selected_product_lineitems ) ) ) {
            foreach ($selected_product_lineitems as $key => $value) {
                $qty_array[$value->product_id] = $value->product_qty;
            }
        }

    foreach ($results as $key_name => $val_array) {
        if ($key_name == "SET_COMPLETE") {
            continue;
        }
        
        if (!empty($val_array->related_module)) {
            if ($val_array->type == 'relate' && (!array_key_exists($val_array->related_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array
                continue;
            }
        }
        $ar_val = strtolower($key_name);
        $name_arry[] = $ar_val;
        //make type array

        $type_array[$ar_val] = $val_array->type;
        $res2 = $objSCP->get_module_fields($module_name, array($ar_val));
        $fields_meta[$ar_val] = $res2;
        foreach ($res2->module_fields as $k_fileds => $fileds) {
            if ($k_fileds == "modified_user_id" || $k_fileds == "created_by" || ($k_fileds == "id" && $id_show == false)) {
                continue;
            }
            $field_label = str_replace(':', '', $fileds->label);
            $field_name = $fileds->name;
            if ($offset == 0) {
            ?>
                <th class="text-left">
                    <?php
                    if ($order_by == $ar_val) {
                        if ($order == 'asc') {
                                $or = 'desc';
                            } else {
                                $or = 'asc';
                            }
                        ?>
                        <a href='javascript:void(0);' onclick='bcp_product_order(0,"<?php echo $module_name; ?>","<?php echo $ar_val; ?>","<?php echo $or; ?>","<?php echo $view ?>","");' >
                            <?php 
                            echo $field_label;
                            if ($order == 'asc') {
                                echo $module_icon_content['arrow-up'];
                            } else {
                                echo $module_icon_content['arrow-down'];
                            }
                            ?>
                        </a>
                        <?php } else { ?>
                        <a href='javascript:void(0);' onclick='bcp_product_order(0,"<?php echo $module_name; ?>","<?php echo $ar_val; ?>","asc","<?php echo $view ?>","");' >
                            <?php echo $field_label; ?>
                            <?php echo $module_icon_content['arrow']; ?>
                        </a>
                        <?php
                    }
                    ?>
                </th>
            <?php
            }
        }
    }

    if ($offset == 0) { ?>
        <th class='action-th'><a><?php echo $language_array['lbl_qty']; ?></a></th>
        <th class='action-th'><a><?php echo $language_array['lbl_total']; ?></a></th>
        </tr>
        </thead>
        <tbody>
        <?php } ?>
    
        <?php
    foreach ($list_result->entry_list as $key => $list_result_s) {

        $id = $list_result_s->name_value_list->id->value;
        if (isset($list_result_s->name_value_list->parent_type->value)) {
            $parent_type = $list_result_s->name_value_list->parent_type->value;
        }

        if ( isset( $list_result_s->name_value_list->currency_id->value ) && $list_result_s->name_value_list->currency_id->value != '' ) {
            $curreny_id = $list_result_s->name_value_list->currency_id->value;
            $currency_symbol = $_SESSION['scp_all_currency']->$curreny_id->currency_symbol;
        }
        ?>

        <tr class='<?php echo $id; ?>' data-page_type='list-view'>
            <?php
        if ($view != 'detail') {
            $checked_product = '';
            if ( isset( $product_array ) && ! empty( $product_array ) ) {
                if ( in_array( $id, $product_array ) ) {
                    $checked_product = 'checked="checked"';
                }
            }
            ?>
            <td>
                <label class="checkbox-button">
                
                    <input type='checkbox' data-index='<?php echo $i; ?>' value='<?php echo $id; ?>' class='input_checkbox checkbox-button__input' name='product_lineitem[<?php echo $i; ?>][id]' <?php echo $checked_product; ?> id='customCheck<?php echo $key; ?>'>
                    <span class="checkbox-button__control"></span>
                
                </label>
            </td>
            <?php
        }
        $cost_price = 0;

        foreach ($name_arry as $nkey => $nval) {
            switch ($nval) {
                case 'assigned_user_id':
                    $get_entrty_module = "Users";
                    break;
                case 'modified_user_id':
                    $get_entrty_module = "Users";
                    break;
                case 'created_by':
                    $get_entrty_module = "Users";
                    break;
                case 'parent_id':
                    if ($parent_type) {
                        $get_entrty_module = $parent_type;
                    } else {
                        $get_entrty_module = "";
                    }
                    break;
                default:
                    $get_entrty_module = "";
                    break;
            }
            $hlink_st = $hlink_en = $cls = '';

            $value = $list_result_s->name_value_list->$nval->value;

            if ($nval == 'price') {
                $cost_price = $value;
            }
            if ($nval == "filename") {
                $download_file = 0;
                if (!empty($value)) {//Added by BC on 08-jul-2016
                    $cls = "general-link-btn scp-download-btn";
                    if ($module_name == "Notes") {
                        $hlink_st = "<a title='" . $language_array['lbl_download'] . "' href='javascript:void(0);' onclick='form_submit_note_document(\"$id\",\"filename\");' class='kb-attachment $cls scp-$module_name-font'>";
                        $hlink_en = "</a>";
                        $download_file = 1;
                    }
                    if ($module_name == "Documents") {
                        $hlink_st = "<a title='" . $language_array['lbl_download'] . "' href='javascript:void(0);' onclick='form_submit_document(\"$id\");' class='kb-attachment $cls scp-$module_name-font'>";
                        $hlink_en = "</a>";
                        $download_file = 1;
                    }
                }
            }
            if ($nval == "date_modified" || $nval == "date_start" || $nval == "date_end" || $nval == "date_entered" || $nval == "date_due") {
                if ($value != '') { // aaded to display blank date instead of default date
                    if ( $_SESSION['browser_timezone'] != NULL ) {
                        $value = scp_user_time_convert( $value );
                    } else {
                        $UTC = new DateTimeZone("UTC");
                        $newTZ = new DateTimeZone($result_timezone);
                        $date = new DateTime($value, $UTC);
                        $date->setTimezone($newTZ);
                        $value = $date->format($objSCP->date_format . " " . $objSCP->time_format);
                    }
                }
            }
            if ($type_array[$nval] == 'date') {
                $value = (!empty($value)) ? date($objSCP->date_format, strtotime($value)) : '';
            }
            if ($get_entrty_module) {
                $select_fields = array('name');
                $record_detail = $objSCP->get_entry($get_entrty_module, $value, $select_fields);
                if (isset($record_detail) && !empty($record_detail)) {
                    $value = $record_detail->entry_list[0]->name_value_list->name->value;
                }
            }

            if ($fields_meta[$nval]->module_fields->$nval->type == 'enum' || $fields_meta[$nval]->module_fields->$nval->type == 'multienum' || $fields_meta[$nval]->module_fields->$nval->type == 'dynamicenum') {
                $value = (!empty($value)) ? $fields_meta[$nval]->module_fields->$nval->options->$value->value : '';
            }
            if ($fields_meta[$nval]->module_fields->$nval->type == 'currency' && $value != '') {//Added by BC on 02-jul-2016 for currency
                $value = $currency_symbol . number_format($value, 2);
            }
            if ( $type_array[$nval] == 'bool' ) {
                if ($value == "1") {
                    $value = $language_array['lbl_yes'];
                } else {
                    $value = $language_array['lbl_no'];
                }
            }
            if ($type_array[$nval] == 'url' && !empty($value)) {//for url
                if (strpos($value, 'http') === false) {
                    $value = 'http://' . $value;
                }
                $hlink_st = "<a title='" . $language_array['lbl_download'] . "' href='" . $value . "' target='_blank'>";
                $hlink_en = "</a>";
            }
            if ( $nval == 'currency_id' ) {
                $value = $_SESSION['scp_all_currency']->$value->currency_symbol;
            }
            if ($value == '' || empty($value) && !$type_array[$nval] == 'int') {
                $value = "-";
            }

            if ($type_array[$nval] == 'name') {//for name field

                $hlink_st = "<a href='#data/".$module_name."/detail/".$id."/'>";
                $hlink_en = "</a>";
            }
            if ($nval != 'link') {
                if ($nval == "filename" && !empty($value) && $value != '-') {
                    ?>
                    <td><?php echo ( ( strlen( $value ) > 100 ) ? substr( $value, 0, 100 ).'...' : $value ); ?></td>
                    <?php } else { ?>
                    <td><?php echo $hlink_st . ( ( strlen( $value ) > 100 ) ? substr( $value, 0, 100 ).'...' : $value ) . $hlink_en ; ?></td>
                    <?php
                }
            }
        }
        $deleted = 1;

        if ($view != 'detail') {
            $product_qty = '0';
            $product_price = '0.00';
            $name = "disabled='disabled'";
            $cls = "disabled";
            if ( isset( $qty_array ) && ! empty( $qty_array ) && isset( $qty_array[ $id ] ) ) {
                $product_qty = $qty_array[ $id ];
                $product_price = number_format($cost_price * $qty_array[$id], 2);
                $name = "name='product_lineitem[".$i."][qty]'";
                $cls = "";
            }
            ?>
            <td>
                <div id="field1" class="qty-block <?php echo $cls; ?>">
                    <button type="button" id="sub" class="sub">-</button>
                    <input type='number' data-cost='<?php echo $cost_price; ?>' min='0' value='<?php echo $product_qty; ?>'  class='input-text scp-form-control input_qty' id='qty_<?php echo $id; ?>' data-currency='<?php echo $currency_symbol; ?>' data-product-id='<?php echo $id; ?>' <?php echo $name; ?>>
                    <button type="button" id="add" class="add">+</button>
                </div>
            </td>
            <td class='total_price'><?php echo $currency_symbol.''.$product_price; ?></td>
        <?php } else { ?>
            <td><?php echo $qty_array[$id]; ?></td>
            <?php
            $total_price = $currency_symbol . number_format($cost_price * $qty_array[$id], 2);
            ?>
            <td class='total_price'><?php echo $total_price; ?></td>
            <?php
        }
        $i++;
        ?>
        </tr>
        <?php } ?>
        <input type='hidden' id='product_search_all' data-limit='<?php echo $limit; ?>' value='<?php echo $total_cnt; ?>'>
        <?php if ($offset == 0) { ?>
        </tbody>
        <?php } ?>
<?php } ?>
    <?php if ($offset == 0) { ?>
</table>
        <?php } ?>


<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if( ! function_exists('scp_user_registration') ) {
    
    function scp_user_registration($pass_name_arry = array()) {
        global $objSCP, $wpdb, $sugar_crm_version, $lang_code, $language_array;
        $response['success'] = FALSE;
        $response['data'] = array(
            "redirect"  => 0,
            "redirect_url"  => "",
            "module_name"   => "Contacts",
        );
        
        
        if ( ! isset( $_POST['sfcp_portal_nonce_field'] ) || ! wp_verify_nonce( $_POST['sfcp_portal_nonce_field'], 'sfcp_portal_nonce' ) ) {
            $response['data']['redirect_url'] = "";
            $response['data']['success'] = FALSE;
            $response['data']['msg'] = __("The form has expired due to inactivity. Please try again.");
            echo json_encode($response);
            exit();
        }
        
        $secret_key = get_option('biztech_scp_recaptcha_secret_key');
        $captcha_responce = ( isset($_REQUEST['g-recaptcha-response']) ? $_REQUEST['g-recaptcha-response'] : "" );
        if ( $captcha_responce != "" ) {
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            if ( isset($responseData->success) && ! $responseData->success ) {
                $response['data']['msg'] = $language_array['msg_captcha_key_validate'];
                return $response;
            }
        }
        
        $username_c = sanitize_text_field( $_POST['username_c'] );
        $password_c = sanitize_text_field( $_POST['password_c'] );
        $last_name = sanitize_text_field( $_POST['last_name'] );
        $email1 = sanitize_text_field( $_POST['email1'] );
        if( $username_c == NULL || $password_c == NULL || $last_name == NULL || $email1 == NULL ) {
            $response['data']['msg'] = $language_array['lbl_missing'];
            return $response;
        } else if ( !is_email( $email1 ) ){
            $response['data']['msg'] = $language_array['lbl_email_error'] . ( isset( $_SESSION['scp_admin_messages']->signup_invalid_email ) ? __( $_SESSION['scp_admin_messages']->signup_invalid_email ) : '' );
            return $response;
        }
        $first_name = (isset($_POST['first_name'])) ? sanitize_text_field($_POST['first_name']) : '';
        if( isset($_FILES["contact_photo"]) && $_FILES["contact_photo"]['size'] != 0 ) {
            $img_error = '';
            $check = getimagesize($_FILES["contact_photo"]["tmp_name"]);
            if( $check !== false ) {  //check if uploaded file is image or not
                if ( ( $check['mime'] == 'image/png' ) || ( $check['mime'] == 'image/jpg' ) || ( $check['mime'] == 'image/jpeg' ) ) {  //check uploaded file jpg,png or not
                    $image_size = $_FILES["contact_photo"]["size"];
                    if( $image_size > 2000000 ) {     //check uploaded image-file's size
                        $img_error = ( isset( $_SESSION['scp_admin_messages']->invalid_image_size ) ? __( $_SESSION['scp_admin_messages']->invalid_image_size ) : '');
                    }
                } else {
                    $img_error = ( isset($_SESSION['scp_admin_messages']->invalid_image_format) ? __( $_SESSION['scp_admin_messages']->invalid_image_format ) : '' );
                }
            } else {
                $img_error = ( isset($_SESSION['scp_admin_messages']->invalid_image_type) ? __( $_SESSION['scp_admin_messages']->invalid_image_type ) : '' );
            }
            if( isset($img_error) && $img_error != '' ){
                $response['data']['msg'] = $language_array['lbl_image_error'].' '.$img_error;
                return $response;
            }
        }
        $biztech_redirect_login = get_page_link( get_option('biztech_redirect_login') );
        $username_c = sanitize_user( $username_c, true ); //Removes tags, octets, entities
        if (isset($username_c) && $username_c != '') {  //username is not blank then
            $addSignUp = $pass_name_arry; 
            $current_blog_id = get_current_blog_id();
            if(is_multisite()){ //if this is Multi-site
                $currentblogurl = get_blog_details($current_blog_id)->siteurl;
            } else{   //if this is single site
                $currentblogurl = get_site_url();
            }
            $addSignUp['register_from_c'] = $currentblogurl;
            $addSignUp['password_c'] = encryptPass($password_c);
            $addSignUp['is_verified_c'] = '1';
            $addSignUp['is_varified_off'] = TRUE;
            if (fetch_data_option("biztech_portal_enable_user_verification")) {
                $addSignUp['is_verified_c'] = '0';
                $addSignUp['is_varified_off'] = FALSE;
            }
            $addSignUp['is_approved_c'] = '1';
            if (fetch_data_option("biztech_portal_user_approval")) {
                $addSignUp['is_approved_c'] = '0';
            }
            if (get_option('portal_assigned_user_id')) {
                $addSignUp['assigned_user_id'] = get_option('portal_assigned_user_id');
            } else {
                $addSignUp['assigned_user_id'] = $objSCP->user_id;
                $addSignUp['assigned_user_name'] = $objSCP->username;
            }
            $checkUserExists = $objSCP->getPortalUserExists($username_c);
            $checkEmailExists = $objSCP->getPortalEmailExists($email1);
            $user_id = username_exists($username_c);
            $wp_email_exist = email_exists($email1);
            if (($checkUserExists) && ($checkEmailExists) && ($user_id) && $wp_email_exist) {
                $redirect_url = esc_url($_REQUEST['scp_current_url']) . '?error=1';
                $signup_msg = ( isset( $_SESSION['scp_admin_messages']->signup_exists_username_email ) ? __( $_SESSION['scp_admin_messages']->signup_exists_username_email ) : '' );
                $response['data']['msg'] = $signup_msg;
                return $response;
            } else if (($checkUserExists || $user_id) && ($checkEmailExists || $wp_email_exist)) {
                $redirect_url = esc_url($_REQUEST['scp_current_url']) . '?error=1';
                $signup_msg = ( isset( $_SESSION['scp_admin_messages']->signup_exists_username_email ) ? __( $_SESSION['scp_admin_messages']->signup_exists_username_email ) : '' );
                $response['data']['msg'] = $signup_msg;
                return $response;
            } else if ($checkUserExists || $user_id) {
                $redirect_url = esc_url($_REQUEST['scp_current_url']) . '?error=1';
                $signup_msg = ( isset( $_SESSION['scp_admin_messages']->signup_exists_username ) ? __( $_SESSION['scp_admin_messages']->signup_exists_username ) : '' );
                $response['data']['msg'] = $signup_msg;
                return $response;
            } else if ($checkEmailExists || $wp_email_exist) {
                $redirect_url = esc_url($_REQUEST['scp_current_url']) . '?error=1';
                $signup_msg = ( isset( $_SESSION['scp_admin_messages']->signup_exists_email ) ? __( $_SESSION['scp_admin_messages']->signup_exists_email ) : '' );
                $response['data']['msg'] = $signup_msg;
                return $response;
            } else if (!is_email($email1)) {//check proper email validation
                $redirect_url = esc_url($_REQUEST['scp_current_url']) . '?error=1';
                $signup_msg = ( isset( $_SESSION['scp_admin_messages']->signup_invalid_email ) ?__( $_SESSION['scp_admin_messages']->signup_invalid_email ) : '' );
                $response['data']['msg'] = $signup_msg;
                return $response;
            } else {    //If New User
                $isSignUp = $objSCP->set_entry('Contacts', $addSignUp);
                if ($isSignUp != NULL) {
                    if(isset($_FILES["contact_photo"]) && $_FILES["contact_photo"]['size'] != 0){  //If user uploading image
                        $filename = $_FILES['contact_photo']['name'];
                        $file_path = base64_encode(file_get_contents($_FILES['contact_photo']['tmp_name']));
                        $isUpdate2 = $objSCP->upload_profile_photo( $filename, $file_path, $isSignUp, "add", $lang_code );
                    }

                    // Account set
                    $user_group_details = $objSCP->get_relationships('Contacts', $isSignUp,"bc_user_group_contacts",array('id'));
                    $bcp_user_group = $user_group_details->entry_list[0]->id;
                    $contact_group_id = (isset($bcp_user_group)) ? $bcp_user_group : "";
                    $list_result = $objSCP->get_entry("bc_user_group", $contact_group_id);
                    $user_group_type = $list_result->entry_list[0]->name_value_list->user_group_type->value;
                    if($user_group_type == "account_based"){
                        $pass_arry = array();
                        $pass_arry['name'] = $username_c;
                        $pass_arry['sentFrom'] = 'portal';
                        $pass_arry['id'] = '';
                        $pass_arry['deleted'] = 0;
                        $new_id = $objSCP->set_entry('Accounts', $pass_arry);
                        $rel_id = $objSCP->set_relationship('Contacts', $isSignUp, 'accounts', array($new_id), 0);
                    }
                    
                    /* When SSo enable register in wordpress 21/04/2022 */
                    $biztech_scp_single_signin = get_option('biztech_scp_single_signin');
                    if($biztech_scp_single_signin!=''){
                    
                        //create user in wp when entry entered in
                        $new_user_id = wp_insert_user(array(
                            'user_login' => $username_c,
                            'user_pass' => $password_c,
                            'user_email' => $email1,
                            'first_name' => $first_name,
                            'last_name' => $last_name,
                            )
                        );
                        if(is_multisite() && get_option('biztech_multi_signin') == "multi-signin-enable"){
                            $blogids = $wpdb->get_results("SELECT blog_id FROM {$wpdb->blogs} WHERE site_id = '{$wpdb->siteid}'");
                            foreach ($blogids as $blog){
                                switch_to_blog($blog->blog_id);
                                add_user_to_blog($blog->blog_id,$new_user_id,'Subscriber');
                                restore_current_blog();
                            }
                        }
                    }
                    
                    $signup_msg = isset( $_SESSION['scp_admin_messages']->signup_success ) ? __( $_SESSION['scp_admin_messages']->signup_success ) : '';
                    if( isset($isUpdate2) && $isUpdate2->success != 1 ){    //if user upload image but can't get by sugar
                        $signup_msg = isset( $_SESSION['scp_admin_messages']->signup_error_profile_upload ) ? __( $_SESSION['scp_admin_messages']->signup_error_profile_upload ) : '';
                    }

                    $_SESSION['bcp_signup_suc'] = $signup_msg;
                    $response['data']['msg'] = $signup_msg;
                    $response['data']['redirect'] = 1;
                    $response['success'] = TRUE;
                    $redirect_url = home_url() . '/portal-login?signup=true';
                    if (isset($biztech_redirect_login) && !empty($biztech_redirect_login)) {
                        $redirect_url = $biztech_redirect_login . '?signup=true';
                    }
                    $response['data']['redirect_url'] = $redirect_url;
                } else {
                    $signup_msg = isset( $_SESSION['scp_admin_messages']->signup_error ) ? __( $_SESSION['scp_admin_messages']->signup_error ) : '';
                    $response['data']['msg'] = $signup_msg;
                }
            }
        } else {    //if username is blank then
            $signup_msg = isset( $_SESSION['scp_admin_messages']->signup_invalid_username ) ? __( $_SESSION['scp_admin_messages']->signup_invalid_username ) : '';
            $response['data']['msg'] = $signup_msg;
        }
        return $response;
    }
}

if ( !function_exists('scp_authorize_user') ) {
    
    function scp_authorize_user($isLogin) {

        global $objSCP, $wpdb, $sugar_crm_version, $lang_code, $language_array, $custom_relationships;
        if ($objSCP == "") {
            scp_create_connection();
        }
        $redirection_url = '';
        
        $_SESSION['browser_timezone'] = $_REQUEST['browser_timezone'];
        $scp_user_id = $isLogin->entry_list[0]->id;
        $_SESSION['scp_user_id'] = $scp_user_id;
        $scp_username = $isLogin->entry_list[0]->name_value_list->username_c->value;
        $_SESSION['scp_user_account_name'] = $scp_username;
        $_SESSION['scp_user_mailid'] = $isLogin->entry_list[0]->name_value_list->email1->value;
        $profile_pic = $objSCP->get_profile_picture( $scp_user_id, $lang_code );
        
        if( ( isset($profile_pic->success) && $profile_pic->success == true ) && ( isset( $profile_pic->profile_photo ) && $profile_pic->profile_photo != "" ) ) {
            $_SESSION['scp_user_profile_pic'] = base64_to_image( $profile_pic->profile_photo, 'user_profile_pic', $_SESSION['scp_user_id'] );
        } else {
            $_SESSION['scp_user_profile_pic'] = '';
        }
        
        $record_detail = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], 'accounts', array('id'), '');
        if(!empty($record_detail->entry_list)){
            $account_id = $record_detail->entry_list[0]->name_value_list->id->value;
            $_SESSION['scp_account_id'] = $account_id;
        }


        if( ( is_multisite() ) && ( get_site_option('biztech_multi_signin')!=NULL ) && ( get_site_option('biztech_multi_signin')=='multi-signin-enable' ) ){  //if this is multisite and multisignin is enabled
            $redirection_url = get_page_link( get_option('biztech_redirect_manange') );
            $_SESSION['scp_use_blog_id'] = get_current_blog_id();
        } else{
            if(get_user_by('login',$scp_username) != NULL){
                $user_data = get_user_by('login',$scp_username);
                $user_meta = get_user_meta($user_data->data->ID);
                $redirection_url = isset( $user_meta['biztech_user_redirection_url'][0] ) ? $user_meta['biztech_user_redirection_url'][0] : '';  // Get user's redirection url
            }
            if($redirection_url != NULL && $redirection_url != '0'){   //get redirection url, if user redirected by wordpress
                switch_to_blog( str_replace('blog_id_', '', $redirection_url ) );
                $redirection_url = get_page_link(get_option('biztech_redirect_manange'));
                $_SESSION['scp_use_blog_id'] = get_current_blog_id();
                restore_current_blog();
            } else {   //if redirection url NULL
                $signupfrom = $isLogin->entry_list[0]->name_value_list->register_from_c->value; //signup from url
                if(is_multisite()){ //if multisite check all urls which macth to our signup
                    $all_blog = get_sites();
                    foreach ($all_blog as $current_blog) {
                        $current_blog = (array) $current_blog;
                        switch_to_blog($current_blog['blog_id']);
                        if(get_site_url() == $signupfrom){
                            $redirection_url = get_page_link(get_option('biztech_redirect_manange')); //new redirection url will be set
                            $_SESSION['scp_use_blog_id'] = get_current_blog_id();
                            restore_current_blog();
                            break;
                        }
                        restore_current_blog();
                    }
                } else{
                    if(get_site_url() == $signupfrom){
                        $redirection_url = get_page_link(get_option('biztech_redirect_manange'));
                        $_SESSION['scp_use_blog_id'] = get_current_blog_id();
                    }
                }
            }
            if($redirection_url == NULL){   //if still redirection url is null redirect to home page
                if(is_multisite()){
                    switch_to_blog(get_current_site()->id); //for network's main site's url
                    $redirection_url = get_page_link(get_option('biztech_redirect_manange'));
                    $_SESSION['scp_use_blog_id'] = get_current_blog_id();
                    restore_current_blog();
                } else{
                    $redirection_url = get_page_link(get_option('biztech_redirect_manange'));
                    $_SESSION['scp_use_blog_id'] = get_current_blog_id();
                }
            }
        }
        $redirection_url = rtrim($redirection_url,'/') . '/#data/Dashboard/';
        $_SESSION['scp_use_blog_url'] = $redirection_url;   //we will set current blogid for other page reference

        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $modules_array = $objSCP->getPortal_accessibleModules(array(),$lang_code); //Added by BC on 16-jun-2016
        
        //For Relationship setting save @since v3.3.1
        if ($sugar_crm_version == 7) {
            $custom_relationships = array(
                'bc_proposal'       => array(
                    'Quotes'        => 'bc_proposal_quotes_1',
                ),
                'Quotes'            => array(
                    'bc_proposal'   => 'bc_proposal_quotes_1',
                ),
                'bc_feedback'       => array(
                    'Contacts'      => 'contacts_bc_feedback_1',
                ),
            );
        } else {
            $custom_relationships = array(
                'bc_proposal'       => array(
                    'AOS_Quotes'    => 'bc_proposal_aos_quotes',
                ),
                'AOS_Quotes'        => array(
                    'bc_proposal'   => 'bc_proposal_aos_quotes',
                ),
                'bc_feedback'       => array(
                    'Contacts'      => 'contacts_bc_feedback_1',
                ),
            );
        }
        if (isset($_SESSION['scp_custom_relationships'])) {
            unset($_SESSION['scp_custom_relationships']);
        }
        $contact_group_id = (isset($modules_array->contact_group_id)) ? $modules_array->contact_group_id : "";

        $list_result = $objSCP->get_entry("bc_user_group", $contact_group_id);
        $user_group_type = $list_result->entry_list[0]->name_value_list->user_group_type->value;
       
        if (isset($modules_array->relationship_setting)) {
            $relationship_setting = $modules_array->relationship_setting;
            foreach ($relationship_setting as $r_key => $r_value) {
                $custom_relationships[$r_key]['Contacts'] = $r_value->relationship_name;
                $custom_relationships[$r_key]['Type'] = $r_value->relationship_type;
            }
            $_SESSION['scp_custom_relationships'] = $custom_relationships;
        }
        
        //For Addition setting save
        if (isset($modules_array->additional_setting)) {
            $_SESSION['scp_additional_setting'] = $modules_array->additional_setting;
        }
        //For KB Categories setting save
        if (isset($modules_array->knowledgebase_categories) && !empty($modules_array->knowledgebase_categories)) {
            $_SESSION['scp_knowledgebase_categories'] = $modules_array->knowledgebase_categories;
        }
        $wpdb->query("TRUNCATE TABLE {$wpdb->prefix}module_layout ");

        if (!empty($modules_array->layouts)) {//Insert or update layout
            foreach ($modules_array->layouts as $k_module => $v_module_list) {
                foreach ($v_module_list as $k_view => $v_panel) {
                    if ( ! isset( $v_panel->panels ) && ($k_view != 'ConditionLogic' && !empty($v_panel) ) ) {
                        continue;
                    }
                    if(isset( $v_panel->panels )) {
                        $panel_data = json_encode($v_panel->panels);
                        if (!is_object($v_panel)) {
                            $panel_data = $v_panel;
                        }
                    }
                    
                    $combined_fields = '';
                    if ( isset( $v_panel->combined_fields ) ) {
                        $combined_fields = json_encode($v_panel->combined_fields);
                    }
                    $conditional_statements = '';
                    if ( isset( $v_panel->conditional_statements ) ) {
                        $conditional_statements = json_encode($v_panel->conditional_statements);
                    }
                    $modified_date = '';
                    if ( isset( $v_panel->modified_date ) ) {
                        $modified_date = $v_panel->modified_date;
                    }

                    if ( is_multisite() ) {
                        $blogids = $wpdb->get_results("SELECT blog_id FROM {$wpdb->blogs} WHERE site_id = '{$wpdb->siteid}' AND spam = '0' AND deleted = '0' AND archived = '0'");  //get all available sub websites
                        foreach ($blogids as $blog){
                            switch_to_blog($blog->blog_id); //switch to all websites one by one
                            //get record exists
                            $sql_count = "SELECT id,date,COUNT(*) as count FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $k_module . "' AND layout_view='" . $k_view . "' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
                            $result_count = $wpdb->get_row($sql_count) or die(mysql_error());
                            $rowCount = $result_count->count;
                            if ($rowCount > 0) {
                                $db_date = $result_count->date;
                                $record_id = $result_count->id;
                                if (strtotime( $v_panel->modified_date ) > strtotime($db_date)) {
                                    $wpdb->update(
                                        $wpdb->prefix . "module_layout",
                                        array(
                                                'version' => $sugar_crm_version,
                                                'module_name' => $k_module,
                                                'layout_view' => $k_view,
                                                'json_data' => $panel_data,
                                                'all_fields' => $combined_fields,
                                                'conditional_fields' => $conditional_statements,
                                                'date' => $modified_date,
                                                'contact_group' => $contact_group_id,
                                                'lang_code' => $lang_code,
                                        ),
                                        array( 'id' => $record_id )
                                    );
                                }
                            } else {
                                $wpdb->insert($wpdb->prefix . 'module_layout', array(
                                    'version' => $sugar_crm_version,
                                    'module_name' => $k_module,
                                    'layout_view' => $k_view,
                                    'json_data' => $panel_data,
                                    'all_fields' => $combined_fields,
                                    'conditional_fields' => $conditional_statements,
                                    'date' => $modified_date,
                                    'contact_group' => $contact_group_id,
                                    'lang_code' => $lang_code,
                                ));
                            }
                            restore_current_blog(); //restore to current blog, so that other options can't conflict
                        }
                    } else {
                        //get record exists
                        $sql_count = "SELECT id,date,COUNT(*) as count FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $k_module . "' AND layout_view='" . $k_view . "' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
                        $result_count = $wpdb->get_row($sql_count);
                        $rowCount = isset($result_count->count) ? $result_count->count : 0;

                        if ($rowCount > 0) {
                            $db_date = $result_count->date;
                            $record_id = $result_count->id;
                            if (isset( $v_panel->modified_date ) && strtotime($v_panel->modified_date) > strtotime($db_date)) {
                                $wpdb->update(
                                        $wpdb->prefix . "module_layout",
                                        array(
                                                'version' => $sugar_crm_version,
                                                'module_name' => $k_module,
                                                'layout_view' => $k_view,
                                                'json_data' => $panel_data,
                                                'all_fields' => $combined_fields,
                                                'conditional_fields' => $conditional_statements,
                                                'date' => $modified_date,
                                                'contact_group' => $contact_group_id,
                                                'lang_code' => $lang_code,
                                        ),
                                        array( 'id' => $record_id )
                                    );
                            }
                        } else {
                            $wpdb->insert($wpdb->prefix . 'module_layout', array(
                                'version' => $sugar_crm_version,
                                'module_name' => $k_module,
                                'layout_view' => $k_view,
                                'json_data' => $panel_data,
                                'all_fields' => $combined_fields,
                                'conditional_fields' => $conditional_statements,
                                'date' => $modified_date,
                                'contact_group' => $contact_group_id,
                                'lang_code' => $lang_code,
                            ));
                        }
                    }
                }
            }
        }

        $modulesAry = array();
        $module_action_Ary = array();
        foreach ($modules_array->accessible_modules as $key_1 => $value_1) {
            if ( isset( $value_1->module_name ) && $value_1->module_name != '' ) {
                $modulesAry[$key_1] = $value_1->module_name;
                foreach ($value_1->action as $key_2 => $value_2) {
                    $module_action_Ary[$key_1][$key_2] = $value_2;
                }
            }
        }
        unset($modulesAry['Contacts']);
        $es_modules_label = get_option('es_modules_label');
        $scp_custom_menu = get_option("scp_custom_menu");
        $scp_menu_modules_label = get_option("scp_menu_modules_label");
        $filter_modules = array();
        if ( ! empty($es_modules_label) && ! empty($modulesAry) ) {
            foreach ( $es_modules_label as $key => $value ) {
                if ( in_array( $key, $modulesAry ) ) {
                    $keys = array_search ($value, $modulesAry);
                    $filter_modules[ $keys ] = $modulesAry[ $keys ];
                } else if(!empty($scp_custom_menu)){ 
                    foreach($scp_custom_menu as $value1){
                        $name = str_replace(" ", "_", $value1['name']);
                        if ($key ==  $name) {
                            $custom_menu_flg = 1;
                            $filter_modules[$name] = $value1['name'];
                        }
                    }
                }
            }
            foreach ($modulesAry as $key => $value) {
                if ( ! in_array( $key, $filter_modules ) ) {
                    $filter_modules[ $key ] = $modulesAry[ $key ];
                    $es_modules_label[$modulesAry[ $key ]] = $modulesAry[ $key ];
                    $scp_menu_modules_label[$modulesAry[ $key ]] = $modulesAry[ $key ];
                    update_option('es_modules_label', $es_modules_label);
                    update_option('scp_menu_modules_label', $scp_menu_modules_label);
                }
            }
        }
        $_SESSION['module_array'] = $modulesAry;
        $_SESSION['module_filter_array'] = $filter_modules;
        foreach ($modules_array->accessible_modulesLablesType as $key_1 => $value_1) {
            $modulesAry_without_s[$key_1] = $value_1->singluar;
        }
        $_SESSION['module_array_without_s'] = $modulesAry_without_s;

        $_SESSION['module_action_array'] = $module_action_Ary;
        //currency array store in session
        $_SESSION['Currencies'] = $modules_array->Currencies;
        //Contact group
        $_SESSION['contact_group'] = $modules_array->contact_group;
        //Contact group Id
        $_SESSION['contact_group_id'] = $contact_group_id;
        //timezone
        $user_timezone = $objSCP->getUserTimezone();

        $_SESSION['user_timezone'] = $user_timezone;

         /***** Change***/
        $user_timezone_date = isset($user_timezone->date) ? $user_timezone->date : '';
        $user_timezone_time = isset($user_timezone->time) ? $user_timezone->time : '';
        $_SESSION['user_date_format'] = $user_timezone_date;
        $_SESSION['user_time_format'] = $user_timezone_time;

         if($user_group_type != ""){
            $_SESSION['scp_user_group_type'] = $user_group_type;
        }else{
            $_SESSION['scp_user_group_type'] = "contact_based";
        }
       
        //For Update Dashboard Preferences v3.2.0
        $pref_table = $wpdb->prefix . "scp_portal_contact_info";
        $contact_id = $_SESSION['scp_user_id'];
        $sql = "SELECT * FROM ".$wpdb->prefix . "scp_portal_contact_info WHERE contact_id = '".$contact_id."'";
        $result = $wpdb->get_row($sql);
        $scp_modules = (array) $modulesAry;
        $cal_checked = (isset($_SESSION['scp_additional_setting']->calender) ? $_SESSION['scp_additional_setting']->calender : 0);
        $flag = 0;
        if ($result != NULL) {
            $dashboard_pref = json_decode($result->dashboard_preferences);
            $scp_counter_block = isset($dashboard_pref->scp_contact_counter_blocks_modules) ? $dashboard_pref->scp_contact_counter_blocks_modules : array();
            $scp_recent_block = isset($dashboard_pref->scp_contact_recent_blocks_modules) ? $dashboard_pref->scp_contact_recent_blocks_modules : array();
            $scp_chart_block = isset($dashboard_pref->scp_contact_charts_blocks_modules) ? $dashboard_pref->scp_contact_charts_blocks_modules : array();
            $scp_counter_block = (array) $scp_counter_block;
            $scp_chart_block = (array) $scp_chart_block;
            $scp_recent_block = (array) $scp_recent_block;
            sort($scp_counter_block);
            sort($scp_chart_block);
            sort($scp_recent_block);
            $scp_today_schedule = isset($dashboard_pref->scp_contact_enable_schedule_blocks) ? $dashboard_pref->scp_contact_enable_schedule_blocks : 0;
            $cnt = 0;
            foreach ($scp_counter_block as $key => $value) {
                if ( ! array_key_exists( $value, $filter_modules ) && $cnt < 4) {
                    unset( $scp_counter_block[$key] );
                    $cnt++;
                }
            }
            foreach ($scp_recent_block as $key => $value) {
                if ( ! array_key_exists( $value, $filter_modules ) ) {
                    unset( $scp_recent_block[$key] );
                }
            }
            foreach ($scp_chart_block as $key => $value) {
                if ( ! array_key_exists( $value, $filter_modules ) ) {
                    unset( $scp_chart_block[$key] );
                }
            }
            if ($scp_today_schedule && $cal_checked && (isset($scp_modules['Calls']) || isset($scp_modules['Meetings']))) {
                $scp_today_schedule = 1;
            } else {
                $scp_today_schedule = 0;
            }
            $dashboard_pref = array();
            if (empty($scp_chart_block) && empty($scp_recent_block) && empty($scp_chart_block) && !$scp_today_schedule) {
                $flag = 1;
            } else {
                if (!empty($scp_counter_block)) {
                    $dashboard_pref['scp_contact_counter_blocks_modules'] = $scp_counter_block;
                }

                //Recent blocks
                if (!empty($scp_recent_block)) {
                    $dashboard_pref['scp_contact_recent_blocks_modules'] = $scp_recent_block;
                }

                //Charts blocks
                if (!empty($scp_chart_block)) {
                    $dashboard_pref['scp_contact_charts_blocks_modules'] = $scp_chart_block;
                }

                //Schedule blocks
                if ($scp_today_schedule) {
                    $dashboard_pref['scp_contact_enable_schedule_blocks'] = $scp_today_schedule;
                }
                
                $wpdb->update(
                        $pref_table,
                        array(
                                'contact_id' => $contact_id,    // integer (number)
                                'dashboard_preferences' => json_encode($dashboard_pref),
                        ),
                        array( 'contact_id' => $contact_id ),
                        array(
                                '%s'    // value2
                        ),
                        array( '%s' )
                );
            } 
        } else {
            $flag = 1;
        }

        if ( $flag ) {
            $dashboard_pref = array();
            $charts_in_array = array();
            if (array_key_exists("Cases", $scp_modules)) {
                $charts_in_array[] = 'Cases';
            }
            if (!empty($charts_in_array)) {
                $dashboard_pref['scp_contact_charts_blocks_modules'] = $charts_in_array;
            }
            $top_modules = get_option('biztech_scp_counter');
            $recent_modules = get_option('biztech_scp_recent_activity');
            foreach ($top_modules as $key => $value) {
                if ( ! array_key_exists( $value, $filter_modules )) {
                    unset( $top_modules[$key] );
                }
            }
            foreach ($recent_modules as $key => $value) {
                if ( ! array_key_exists( $value, $filter_modules ) ) {
                    unset( $recent_modules[$key] );
                }
            }
            $dashboard_pref['scp_contact_counter_blocks_modules'] = $top_modules;
            $dashboard_pref['scp_contact_recent_blocks_modules'] = $recent_modules;
            if ($cal_checked && (isset($scp_modules['Calls']) || isset($scp_modules['Meetings']))) {
                $dashboard_pref['scp_contact_enable_schedule_blocks'] = 1;
            }
            if ($result != NULL) {
                $wpdb->update(
                        $pref_table,
                        array(
                                'contact_id' => $contact_id,    // integer (number)
                                'dashboard_preferences' => json_encode($dashboard_pref),
                        ),
                        array( 'contact_id' => $contact_id ),
                        array(
                                '%s'    // value2
                        ),
                        array( '%s' )
                );
            } else {
                $wpdb->insert($pref_table, array(
                    'contact_id' => $contact_id,
                    'dashboard_preferences' => json_encode($dashboard_pref),
                ));
            }
        }

        // if wp user is not logged in login by auth cookie
        if (get_option('biztech_scp_single_signin') != '' && !is_user_logged_in()) {
            // Automatic login, without any password! //
            $user = get_user_by('login', $scp_username);
            if (!is_wp_error($user)) {
                if(is_multisite()){
                    $_SESSION['biztech_single_signin'] = 'enable';
                }
                wp_clear_auth_cookie();
                wp_set_current_user($user->ID);
                wp_set_auth_cookie($user->ID);
            }
        }
        logincall_chat_feature();
        // if wp user is not logged in
        if(defined('BIZTECH_PORTAL_ENABLE') && BIZTECH_PORTAL_ENABLE=='MULTIDOMAIN' && get_current_blog_id()!=$_SESSION['scp_use_blog_id']){ //if user redirect to another website in MULTIDOMAIN destroy all session in current session
            sugar_crm_portal_logout('true');
        }
        echo '<script>window.location = "'.$redirection_url.'";</script>';
        exit();
    }
}

if( ! function_exists('scp_user_login') ) {
    add_action('admin_post_bcp_login', 'scp_user_login');
    add_action('admin_post_nopriv_bcp_login', 'scp_user_login');
    function scp_user_login() {

        global $objSCP, $wpdb, $sugar_crm_version, $lang_code, $language_array, $custom_relationships;
        if ($objSCP == "") {
            scp_create_connection();
        }
        $sess_id = $objSCP->session_id;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        $redirection_url = "";
        if ( $sess_id != '' ) {
       // For Captcha validation
            $secret_key = get_option('biztech_scp_recaptcha_secret_key');
            $captcha_responce = ( isset($_REQUEST['g-recaptcha-response']) ? $_REQUEST['g-recaptcha-response'] : "" );
            if ( $captcha_responce != "" ) {
                $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$_POST['g-recaptcha-response']);
                $responseData = json_decode($verifyResponse);
                if ( isset($responseData->success) && ! $responseData->success ) {
                    $redirect_url = esc_url($_REQUEST['scp_current_url']) . '?error=1';
                    $error_msg = $language_array['msg_captcha_key_validate'];
                    $_SESSION['bcp_login_error'] = $error_msg;
                    $current_url = isset( $_REQUEST['scp_current_url'] ) ? esc_url( $_REQUEST['scp_current_url'] ) : '';
                    $redirect_url = $current_url . '?error=1';
                    wp_redirect($redirect_url);
                    exit();
                }
            }
            $bcp_login_password_enable = get_option('bcp_login_password_enable');
            
            $ip = bcp_getUserIpAddr();
            $record = $wpdb->get_row("SELECT * FROM " . $wpdb->base_prefix . "bcp_login_attempts WHERE ip_address='{$ip}'");
            if (!empty($record) && $bcp_login_password_enable) {
                $sfcp_portal_login_min = fetch_data_option("bcp_lockout_effective_period");
                $bcp_maximum_login_attempts = fetch_data_option('bcp_maximum_login_attempts');
                $login_attempt = isset($record->login_attempt) ? $record->login_attempt : 1;
                $last_login_date = isset($record->last_login_date) ? $record->last_login_date : date("Y-m-d H:i:s");
                $bcp_last_login_date = new DateTime($last_login_date);
                $since_start = $bcp_last_login_date->diff(new DateTime());
                $min_diff = $since_start->i;
                if ($login_attempt >= $bcp_maximum_login_attempts && $min_diff < $sfcp_portal_login_min) {
                    $current_url = isset( $_REQUEST['scp_current_url'] ) ? esc_url( $_REQUEST['scp_current_url'] ) : '';
                    $_SESSION['bcp_login_attempt_error'] = $failed_login_error_msg;
                    $redirect_url = $current_url . '?error=1';
                    wp_redirect($redirect_url);
                    exit();
                }
            }
            
            $is_login_by_email = 0;
            $is_generate_otp = 0;
            if ( get_option('biztech_scp_single_signin') != '' && ( is_user_logged_in() ) ) {   //if single signin is enabled and user logged in
                global $current_user;
                $scp_username = $current_user->user_login;
                $scp_password = $current_user->user_email;
                $is_login_by_email = 1;
            } else {
                $scp_username = sanitize_text_field($_REQUEST['scp_username']);
                $scp_password = encryptPass(sanitize_text_field($_REQUEST['scp_password']));
                //get otp setting enable or not
                $is_otp_enable = fetch_data_option("biztech_scp_otp_enable");
                //get cookies to check otp verified once before
                $tmp_contact_usernames = isset($_COOKIE['scp_sugar_suite_330']) ? unserialize(stripslashes($_COOKIE['scp_sugar_suite_330'])) : array();
                $contact_usernames = array();
                foreach ($tmp_contact_usernames as $value) {
                    $tmp_uname = decryptPass($value);
                    array_push($contact_usernames, $tmp_uname);
                }
                if (in_array($scp_username, $contact_usernames)) {
                    $is_otp_enable = 0;
                }
                if ($is_otp_enable == "1") {
                    $is_generate_otp = 1;
                }                
            }
            if ($is_generate_otp) {
                $fields = array(
                    'id',
                    'username_c',
                    'enable_portal_c',
                    'is_verified_c',
                    'is_approved_c',
                );
            } else {
                $fields = array(
                    'id',
                    'username_c',
                    'salutation',
                    'first_name',
                    'last_name',
                    'email1',
                    'account_id',
                    'title',
                    'phone_work',
                    'phone_mobile',
                    'phone_fax',
                    'register_from_c',
                    'enable_portal_c',
                    'is_verified_c',
                    'is_approved_c',
                );
            }
            $isLogin = $objSCP->PortalLogin( $scp_username, $scp_password, $is_login_by_email, $fields, $lang_code );
            
            $portalMessageLists = $objSCP->getPortalMessagesLists($lang_code);
            $_SESSION['portal_message_list'] = $portalMessageLists;
           
            if ( ( isset( $isLogin->entry_list[0] ) && $isLogin->entry_list[0] != NULL ) && ( $scp_username != NULL ) && ( $scp_password != NULL ) && ( isset( $isLogin->entry_list[0]->name_value_list->enable_portal_c->value ) && $isLogin->entry_list[0]->name_value_list->enable_portal_c->value ) ) {  //If user logged in

                //check user is verified email or not for v3.0-----------------
                if (isset( $isLogin->entry_list[0]->name_value_list->is_verified_c->value ) && $isLogin->entry_list[0]->name_value_list->is_verified_c->value != 0) {

                    if (isset( $isLogin->entry_list[0]->name_value_list->is_approved_c->value ) && $isLogin->entry_list[0]->name_value_list->is_approved_c->value != 0) {
                        
                        /* rember me dharti.gajera 20/04/2022 */
                        $remember_password = isset($_REQUEST['remember_password']) ? $_REQUEST['remember_password'] : "0";
                        if($remember_password){
                            setcookie("bcp_remember_password[username]", $scp_username, time() + (60*60*24*30), "/");
                            setcookie( "bcp_remember_password[password]", $scp_password, time() + (60*60*24*30), "/" );
                        } else {
                            setcookie("bcp_remember_password[username]", "", time() + (60*60*24*30), "/");
                            setcookie( "bcp_remember_password[password]", "", time() + (60*60*24*30), "/" );
                        }
                        
                        if ($bcp_login_password_enable) {
                            $sfcp_portal_login_min = fetch_data_option("bcp_lockout_effective_period");
                            $bcp_maximum_login_attempts = fetch_data_option('bcp_maximum_login_attempts');

                            if(!empty($record)) {
                                $wpdb->update(
                                        $wpdb->base_prefix . 'bcp_login_attempts', array(
                                    'ip_address' => $ip,
                                    'login_attempt' => '0',
                                    'last_login_date' => date("Y-m-d H:i:s"),
                                    'bcp_user_login' => $scp_username,
                                    'ip_status' => "active",
                                        ), array('id' => $record->id)
                                );

                            }
                            
                        }
                        

                        if ($is_generate_otp) {
                            $contactId = $isLogin->entry_list[0]->id;
                            $isGenerateOtp = $objSCP->PortalGenerateOTP($contactId);
                            $encryptContactId = encryptPass($contactId);
                            
                            $current_url = isset( $_REQUEST['scp_current_url'] ) ? esc_url( $_REQUEST['scp_current_url'] ) : '';
                            if (isset($isGenerateOtp->result) && $isGenerateOtp->result == "false") {
                                $otp_msg = isset( $isGenerateOtp->message ) ? $isGenerateOtp->message : __("OTP not sent. Please contact to Administrator.");
                                $_SESSION['bcp_login_error'] = $otp_msg;
                                $redirect_url = $current_url . '?error=1';
                            } else {
                                $otp_msg = isset( $isGenerateOtp->message ) ? $isGenerateOtp->message : __("OTP sent successfully on your email address");
                                $_SESSION['scp_otp_contact_id'] = $encryptContactId;
                                $_SESSION['scp_otp_generated'] = 1;
                                $_SESSION['bcp_login_otp_suc'] = $otp_msg;
                                $redirect_url = $current_url . '?otp=1';
                            }
                            wp_redirect($redirect_url);
                            
                        } else {
                            scp_authorize_user($isLogin);
                        }
                    } else {
                       
                        $error_msg = isset( $_SESSION['portal_message_list']->portal_user_approve_error ) ? __( $_SESSION['portal_message_list']->portal_user_approve_error ) : __("Your account is not approved by admin.");
                        $_SESSION['bcp_login_error'] = $error_msg;
                        $current_url = isset( $_REQUEST['scp_current_url'] ) ? esc_url( $_REQUEST['scp_current_url'] ) : '';
                        $redirect_url = $current_url . '?error=1';
                        wp_redirect($redirect_url);
                    }
                } else {
                     
                    $error_msg = $language_array['msg_verify_mail'];
                    $_SESSION['bcp_login_error'] = $error_msg;
                    $current_url = isset( $_REQUEST['scp_current_url'] ) ? esc_url( $_REQUEST['scp_current_url'] ) : '';
                    $redirect_url = $current_url . '?error=1';
                    wp_redirect($redirect_url);
                }
            } else {
                $ERROR_CODE = (isset($isLogin->error)) ? $isLogin->error : 1;
                if ($ERROR_CODE == 1) {
                    $error_msg = isset( $_SESSION['portal_message_list']->log_invalid_user_pass ) ? __( $_SESSION['portal_message_list']->log_invalid_user_pass ) : '';
                } else {
                    $error_msg = $isLogin->message;
                }
                if(isset($isLogin->entry_list[0]) && $isLogin->entry_list[0]->name_value_list->enable_portal_c->value == 0){
                    $error_msg =  isset( $_SESSION['portal_message_list']->log_user_disabled ) ? __( $_SESSION['portal_message_list']->log_user_disabled ) : '';
                }
                
                if ($bcp_login_password_enable) {
                    $sfcp_portal_login_min = fetch_data_option("bcp_lockout_effective_period");
                    $bcp_maximum_login_attempts = fetch_data_option('bcp_maximum_login_attempts');
                    
                    if (empty($record)) {
                        $login_attempt = 1;
                        $wpdb->insert(
                                $wpdb->base_prefix . 'bcp_login_attempts', array(
                            'ip_address' => $ip,
                            'login_attempt' => $login_attempt,
                            'bcp_user_login' => $scp_username,
                            'last_login_date' => date("Y-m-d H:i:s"),
                            'ip_status' => "active",
                                )
                        );
                    } else {
                        $login_attempt = isset($record->login_attempt) ? $record->login_attempt : 1;
                        $login_attempt += 1;
                        $today = date("Y-m-d H:i:s");
                        $today_time = strtotime($today);
                        $last_login_time = strtotime($record->last_login_date);
                        $expire_time = strtotime(date("Y-m-d H:i:s", strtotime('-20 minutes')));

                        if($expire_time < $today_time && $expire_time > $last_login_time){
                            $login_attempt = 1;
                        }
                        
                        $wpdb->update(
                                $wpdb->base_prefix . 'bcp_login_attempts', array(
                            'ip_address' => $ip,
                            'login_attempt' => $login_attempt,
                            'last_login_date' => date("Y-m-d H:i:s"),
                            'bcp_user_login' => $scp_username,
                            'ip_status' => "active",
                                ), array('id' => $record->id)
                        );
                        
                        $bcp_last_login_date = new DateTime(date("Y-m-d H:i:s"));
                        $since_start = $bcp_last_login_date->diff(new DateTime());
                        $min_diff = $since_start->i;
                        if ($login_attempt >= $bcp_maximum_login_attempts && $min_diff < $sfcp_portal_login_min) {
                            $objSCP->getPortalUserExists($scp_username, 1);
                        }
                    
                    }
                    $remain_login_attempt = $bcp_maximum_login_attempts - $login_attempt;
                    $failed_login_error_msg =  isset( $_SESSION['portal_message_list']->failed_login_attempt ) ? __( $_SESSION['portal_message_list']->failed_login_attempt ) : '';
                    
                    $_SESSION['bcp_login_attempt_error'] = str_replace('[n]', $remain_login_attempt, $failed_login_error_msg);
                }
               
                $_SESSION['bcp_login_error'] = $error_msg;
                $current_url = isset( $_REQUEST['scp_current_url'] ) ? esc_url( $_REQUEST['scp_current_url'] ) : '';
                $redirect_url = $current_url . '?error=1';
                if (get_option('biztech_scp_single_signin') != '' && is_user_logged_in()) {
                    $_SESSION['bcp_auth_error'] = $language_array['msg_no_permission_data'];
                    if (!isset($_REQUEST['error'])) {
                        wp_redirect($redirect_url);
                    }
                } else {
                    wp_redirect($redirect_url);
                }
            }
        } else {    //If connection Failed
            $objSCP = 0;
            $conn_err = $language_array['msg_portal_is_disabled_contact_admin'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            $redirect_url = esc_url($_REQUEST['scp_current_url']) . '?conerror=1';
            wp_redirect($redirect_url);
        }
    }
}

if( ! function_exists('scp_verify_otp') ) {
    add_action('admin_post_bcp_verify_otp', 'scp_verify_otp');
    add_action('admin_post_nopriv_bcp_verify_otp', 'scp_verify_otp');
    
    function scp_verify_otp() {
        global $objSCP;
        $otp = $_REQUEST['scp_otp'];
        $module_name = "Contacts";
        $contactId = decryptPass($_SESSION['scp_otp_contact_id']);
        $verifyOtp = $objSCP->verifyOTP($contactId, $otp);
        if (isset($verifyOtp->success) && $verifyOtp->success == "1") {
            $select_fields_array = array(
                'id',
                'username_c',
                'salutation',
                'first_name',
                'last_name',
                'email1',
                'account_id',
                'title',
                'phone_work',
                'phone_mobile',
                'phone_fax',
                'register_from_c',
                'enable_portal_c',
                'is_verified_c',
                'is_approved_c',
            );
            $contact_lists = $objSCP->get_entry($module_name, $contactId, $select_fields_array);
            if (isset($contact_lists->entry_list) && !empty($contact_lists->entry_list)) {
                $c_otp = $contact_lists->entry_list[0]->name_value_list->otp_number_c->value;
                $c_otp_date = $contact_lists->entry_list[0]->name_value_list->otp_number_date_c->value;
                $username = $contact_lists->entry_list[0]->name_value_list->username_c->value;
                $contact_usernamesArray = isset($_COOKIE['scp_sugar_suite_330']) ? unserialize(stripslashes($_COOKIE['scp_sugar_suite_330'])) : array();
                $encryptedUsername = encryptPass($username);
                array_push($contact_usernamesArray, $encryptedUsername);
                $contact_usernames = serialize($contact_usernamesArray);
                setcookie("scp_sugar_suite_330", $contact_usernames, time() + (10 * 365 * 24 * 60 * 60), "/");
                unset($_SESSION['scp_otp_contact_id']);
                unset($_SESSION['scp_otp_generated']);
                scp_authorize_user($contact_lists);
            }
        } else {
            $biztech_redirect_login = get_page_link(get_option('biztech_redirect_login'));
            if ($biztech_redirect_login != NULL) {
                $redirect_url_login = $biztech_redirect_login;
            } else {
                $redirect_url_login = home_url() . "/portal-login/";
            }
            $otp_msg = ( isset($verifyOtp->msg) ? $verifyOtp->msg : __("OTP expired. Please resend otp.") );
            $_SESSION['bcp_login_error'] = $otp_msg;
            $redirect_url = $redirect_url_login . '?error=1';
            wp_redirect($redirect_url);
        }
    }
}

if( ! function_exists('scp_resend_otp') ) {
    add_action('admin_post_scp_resend_otp', 'scp_resend_otp');
    add_action('admin_post_nopriv_scp_resend_otp', 'scp_resend_otp');
    
    function scp_resend_otp() {
        global $objSCP;
        $biztech_redirect_login = get_page_link(get_option('biztech_redirect_login'));
        if ($biztech_redirect_login != NULL) {
            $redirect_url_login = $biztech_redirect_login;
        } else {
            $redirect_url_login = home_url() . "/portal-login/";
        }
        $contactId = decryptPass($_SESSION['scp_otp_contact_id']);
        $isGenerateOtp = $objSCP->PortalGenerateOTP($contactId);
        if (isset($isGenerateOtp->result) && $isGenerateOtp->result == "false") {
            $otp_msg = isset( $isGenerateOtp->message ) ? $isGenerateOtp->message : __("Failed to send OTP");
            $_SESSION['bcp_login_error'] = $otp_msg;
            $redirect_url = $redirect_url_login . '?error=1';
        } else {
            $otp_msg = isset( $isGenerateOtp->message ) ? $isGenerateOtp->message : __("OTP sent successfully on your email address");
            $_SESSION['bcp_login_otp_suc'] = $otp_msg;
            $redirect_url = $redirect_url_login . '?otp=1';
        }
        wp_redirect($redirect_url);
    }
}

if( ! function_exists('scp_forgot_password_callback') ) {
    add_action('admin_post_bcp_forgot_password', 'scp_forgot_password_callback');
    add_action('admin_post_nopriv_bcp_forgot_password', 'scp_forgot_password_callback');
    function scp_forgot_password_callback() {
        global $objSCP, $lang_code, $language_array;
        if ($objSCP == "") {
            scp_create_connection();
        }
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        //20-aug-2016
        $sess_id = $objSCP->session_id;
        if ($sess_id != '') {
            
            // For Captcha validation
            $secret_key = get_option('biztech_scp_recaptcha_secret_key');
            $captcha_responce = ( isset($_REQUEST['g-recaptcha-response']) ? $_REQUEST['g-recaptcha-response'] : "" );
            if ( $captcha_responce != "" ) {
                $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$_POST['g-recaptcha-response']);
                $responseData = json_decode($verifyResponse);
                if ( isset($responseData->success) && ! $responseData->success ) {
                    $redirect_url = esc_url($_REQUEST['scp_current_url']) . '?error=1';
                    $disply_msg = $language_array['msg_captcha_key_validate'];
                    $_SESSION['bcp_frgtpwd'] = $disply_msg;
                    wp_redirect($redirect_url);
                    exit();
                }
            }
            $scp_current_url = isset($_REQUEST['scp_current_url']) ? esc_url($_REQUEST['scp_current_url']): '';
            if($scp_current_url == ''){
                if (get_page_link(get_option('biztech_redirect_forgotpwd')) != NULL) {
                    $scp_current_url = get_page_link(get_option('biztech_redirect_forgotpwd'));
                } else {
                    $scp_current_url = home_url() . "/portal-forgot-password/";
                }
            }
            
            $checkEmialAddress = isset($_REQUEST['forgot-password-email-address']) ? sanitize_text_field($_REQUEST['forgot-password-email-address']): '';
            if( $checkEmialAddress == '' && isset($_COOKIE['resend_email']) && $_COOKIE['resend_email']!='') {
                $checkEmialAddress = sanitize_text_field($_COOKIE['resend_email']);
                $redirect_url = $scp_current_url . '?sucess=1';
            }
            
            if( $checkEmialAddress != '' ) {
                $forgot_data = $objSCP->forgotpassword ( $checkEmialAddress, $lang_code );
                if( isset( $forgot_data->result ) && $forgot_data->result == 'true' ) {
                    $redirect_url = esc_url($scp_current_url) . '?sucess=1';
                    $disply_msg = isset( $_SESSION['scp_admin_messages']->forgot_success ) ? __( $_SESSION['scp_admin_messages']->forgot_success ) : '';
                    $_SESSION['bcp_frgtpwd'] = $disply_msg;
                    setcookie('resend_email', $_REQUEST['forgot-password-email-address'], time() + (300), "/");
                    wp_redirect($redirect_url);
                } else {
                    $redirect_url = esc_url($scp_current_url) . '?error=1';
                    $disply_msg = ( ( isset( $forgot_data->message ) && $forgot_data->message != '' ) ?
                                    ( __( $forgot_data->message ) ) :
                                    ( ( $_SESSION['scp_admin_messages']->forgot_error ) ?
                                        __( $_SESSION['scp_admin_messages']->forgot_error ) :
                                        '' ) );
                    $_SESSION['bcp_frgtpwd'] = $disply_msg;
                    wp_redirect($redirect_url);
                }
            } else {
                $redirect_url = esc_url($scp_current_url) . '?error=1';
                $disply_msg = isset( $_SESSION['scp_admin_messages']->forgot_invalid ) ? __( $_SESSION['scp_admin_messages']->forgot_invalid ) : '';
                $_SESSION['bcp_frgtpwd'] = $disply_msg;
                wp_redirect($redirect_url);
            }
        } else {
            $objSCP = 0;
            $conn_err = $language_array['msg_portal_is_disabled_contact_admin'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            $redirect_url = esc_url($scp_current_url) . '?conerror=1';
            wp_redirect($redirect_url);
        }
    }
}

if( ! function_exists('get_user_group_modules_callback') ) {
    add_action('wp_ajax_get_user_group_modules', 'get_user_group_modules_callback');
    add_action('wp_ajax_nopriv_get_user_group_modules', 'get_user_group_modules_callback');
    function get_user_group_modules_callback() {
        global $objSCP;
        $sess_id = $objSCP->session_id;
        if ($sess_id != '') {
            $user_group = sanitize_text_field($_REQUEST['user_group']);
            $list_result = $objSCP->get_entry("bc_user_group", $user_group);
            $accessable_modules = $list_result->entry_list[0]->name_value_list->accessible_modules->value;
            $str = $objSCP->unencodeMultienum($accessable_modules);
            if (count($accessable_modules) > 0) {
                $final_str = implode(',', $str);
            } else {
                $final_str = "-";
            }
            $html = "<br><b>".__("Accessible module list")."</b> : " . $final_str . "";
            echo $html;
            wp_die();
        }
    }
}

if( ! function_exists('scp_get_doc_attachment') ) {
    add_action('admin_post_bcp_get_doc_attachment', 'scp_get_doc_attachment');
    add_action('admin_post_nopriv_bcp_get_doc_attachment', 'scp_get_doc_attachment');
    function scp_get_doc_attachment() {

        global $objSCP;
        //20-aug-2016
        $sess_id = $objSCP->session_id;
        $custom_relationships = $_SESSION['scp_custom_relationships'];
        $redirectURL_manage = get_page_link(get_option('biztech_redirect_manange'));
        if ($sess_id != '') {
            
            if ( !isset($_REQUEST['scp_doc_id']) && $_REQUEST['scp_doc_id'] != "" || !isset($_SESSION['scp_user_id']) ) {
                wp_redirect($redirectURL_manage);
                exit();
            }
            
            $doc_id = sanitize_text_field($_REQUEST['scp_doc_id']);
            $doc_view = isset($_REQUEST['scp_doc_view']) ? sanitize_text_field($_REQUEST['scp_doc_view']) : 0;
            $doc_field_name = isset($_REQUEST['scp_doc_field_name']) ? sanitize_text_field($_REQUEST['scp_doc_field_name']) : 'photo';
            $module_name = sanitize_text_field($_REQUEST['module_name']);
            
            
            $relation_name = isset($custom_relationships[$module_name]['Contacts']) ? $custom_relationships[$module_name]['Contacts'] : strtolower($module_name);
            
            $where_con = strtolower($module_name) . ".id = '{$doc_id}'";

            if($_SESSION['scp_user_group_type'] == "contact_based"){
                $record_detail = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], $relation_name, '', $where_con);
            }else{
                $record_detail = $objSCP->get_relationships('Accounts', $_SESSION['scp_account_id'], $relation_name, '', $where_con);
            }
            
            
            if ( $record_detail->entry_list == NULL ) {
                wp_redirect($redirectURL_manage."#access_denied");
                exit();
            }
            if ($module_name == "Documents") {
               $objSCP->getDocument($doc_id,$doc_view);
            } else {
               $objSCP->biz_getAttachmentData($module_name, $doc_id, "image", $doc_field_name, $doc_view);
            }
        } else {
            wp_redirect($redirectURL_manage);
            exit();
        }
    }
}

if( ! function_exists('scp_get_pdf_quote') ) {
    add_action('admin_post_bcp_get_pdf_quote', 'scp_get_pdf_quote');
    add_action('admin_post_nopriv_bcp_get_pdf_quote', 'scp_get_pdf_quote');
    function scp_get_pdf_quote() {

        global $objSCP, $lang_code, $language_array;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        //20-aug-2016
        $sess_id = $objSCP->session_id;
        $manage_page_id = get_option('biztech_redirect_manange');
        $redirect_url = get_permalink($manage_page_id) . '#data/' .$_REQUEST['scp_module_name_download'] . '/list/';
        if ($sess_id != '') {
            $download_pdf_id = sanitize_text_field($_REQUEST['scp_module_pdf_id']);
            $module_name_pdf = sanitize_text_field($_REQUEST['scp_module_name_download']);
            $data = $objSCP->getModuleDataPDF( $download_pdf_id, $module_name_pdf);
        } else {
            $conn_err = $language_array['msg_portal_is_disabled_contact_admin'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            wp_redirect($redirect_url);
        }
        if ( isset( $data ) && $data != '' ){
            $data = $data . ' ' . $language_array['msg_contact_admin'];
            $_SESSION['bcp_pdf_error'] = $data;
            wp_redirect($redirect_url);
        }
    }
}

if( ! function_exists('scp_list_module') ) {
    add_action('wp_ajax_bcp_list_module', 'scp_list_module');
    add_action('wp_ajax_nopriv_bcp_list_module', 'scp_list_module');
    function scp_list_module() {
        global $objSCP, $wpdb, $sugar_crm_version, $custom_relationships, $lang_code, $language_array,$module_icon_content;
        $custom_relationships = $_SESSION['scp_custom_relationships'];

        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        $current_url = (isset($_POST['current_url'])) ? esc_url($_POST['current_url']) : "";
        $filter_where_condition = (isset($_POST['filter_where_condition'])) ? $_POST['filter_where_condition'] : array();
        $filter_where_currency_condition = (isset($_POST['filter_where_currency_condition'])) ? $_POST['filter_where_currency_condition'] : array();

        if (isset($current_url) && $current_url != '') {
            $current_url = explode('?', $current_url, 2);
            $current_url = $current_url[0];
        }
        //20-aug-2016
        $sess_id = $objSCP->session_id;

        if ($sess_id != '') {
            $module_name = sanitize_text_field($_POST['modulename']);
            if (!isset($_REQUEST['parentmodule']) || (isset($_REQUEST['parentmodule']) && $_REQUEST['parentmodule'] != 'solutions') ) {
                check_permission($module_name,'list');
            }
            if (is_array((array) $_SESSION['module_array'])) {//Added for getting module key OR lable added from sugar side
                $arry_lable = (array) $_SESSION['module_array'];
                $module_name_label = $arry_lable[$module_name];
            }
            $view = sanitize_text_field($_POST['view']);
            $view1 = $view;
            
            /*set for paging for each module--- start-----  */
            $limit = fetch_data_option('biztech_scp_case_per_page');
            
            if (!isset($_REQUEST['relate_to_module'])) {
                $limit_array = array();
                
                if (isset($_COOKIE['scp_module_paging_limit'])) {
                    $cookieLimit = stripslashes($_COOKIE['scp_module_paging_limit']);
                    $limit_array = json_decode($cookieLimit, 1);
                    if (is_array($limit_array) && array_key_exists($module_name, $limit_array) && $limit_array[$module_name] != null) {
                        $limit = $limit_array[$module_name];
                    }
                }
                
                if (isset($_REQUEST['limit'])) {
                    $limit = $_REQUEST['limit'];
                }
                
                $limit_array[$module_name] = $limit;
                $limit_data = json_encode($limit_array);
                setcookie("scp_module_paging_limit", $limit_data, time()+3600, "/");
            }
            /*set for paging for each module--- end-----  */
            
            $page_no = (isset($_POST['page_no'])) ? intval($_POST['page_no']) : 1;
            $offset = ($page_no * $limit) - $limit;
            $offset = ( $offset < 0 ) ? '0' : $offset ;
            //Updated by BC on 20-jun-2016 for timezone store in session
            if (empty($_SESSION['user_timezone'])) {
                $result_timezone = $objSCP->getUserTimezone();
            } else {
                $result_timezone = $_SESSION['user_timezone'];
            }
            date_default_timezone_set($result_timezone);
            $result_timezone = date_default_timezone_get();
            $order_by = isset($_POST['order_by']) ? trim( sanitize_text_field( $_POST['order_by'] )) : "";
            $order = isset($_POST['order']) ? trim( sanitize_text_field( $_POST['order'] )) : "";
            $search_name = null;
            if ( array_key_exists('searchval', $_POST) ) {
                $search_name = sanitize_text_field( str_replace( '%20', ' ', $_POST['searchval'] ) );
            }
            
            $AOK_Knowledge_Base_Categories = array_key_exists('AOK_Knowledge_Base_Categories', $_POST) ? trim(sanitize_text_field($_POST['AOK_Knowledge_Base_Categories'])) : null;
            $lcfirst_module_name = strtolower($module_name);
            $where_con = '';
            if (isset($search_name) && !empty($search_name)) {
                if ($module_name == "Contacts" || $module_name == "Leads") {
                    $where_con = "(" . $lcfirst_module_name . ".first_name like '%{$search_name}%' OR " . $lcfirst_module_name . ".last_name like '%{$search_name}%'" . ")";
                } else if ($module_name == "Documents") {
                    $where_con = $lcfirst_module_name . ".document_name like '%{$search_name}%'";
                } else {
                    $where_con = $lcfirst_module_name . ".name like '%{$search_name}%'";
                }
            }
            /* advanced_filter */
            $advanced_filter = array();
            $product_data_filter = '';
            
            if(isset($filter_where_condition) && !empty($filter_where_condition)){
                foreach ($filter_where_condition as $key => $val) {
                    $module = $lcfirst_module_name;
                    if(strpos($key, "_c") !== false || (isset($val['key']) && strpos($val['key'], "_c") !== false ) ){
                        $module = $lcfirst_module_name.'_cstm';
                    }
                    if(is_array($val)){
                        if(isset($val['name']) && $val['name'] == 'boolean') {
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                if($val['compare'] == 0){
                                    $product_data_filter .= $add_and."(".$module . ".".$key." = ".$val['compare'] ." OR ".$module . ".".$key." is null)";
                                } else {
                                    $product_data_filter .= $add_and.$module . ".".$key." = ".$val['compare'];
                                }
                            } else {
                                if($val['compare'] == 0){
                                    $advanced_filter[] = "(".$module . ".".$key." = ".$val['compare'] ." OR ".$module . ".".$key." is null)";
                                } else {
                                    $advanced_filter[] = $module . ".".$key." = ".$val['compare'];
                                }
                            }
                        } else if(isset($val['name']) && $val['name'] == 'selectbox') {
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                $product_data_filter .= $add_and.$module . ".".$key." IN ( '".implode("', '",$val['compare'])."')";
                            } else {
                                $advanced_filter[] = $module . ".".$key." IN ( '".implode("', '",$val['compare'])."')";
                            }
                        } else if(isset($val['name']) && $val['name'] == 'single') {
                            $GetDate = str_replace('/', '-', $val['range_'.$val['key']]);
                            $GetDate = date("Y-m-d", strtotime($GetDate));
                            if($val['compare'] == '='){
                                $compare = 'like';
                            } else if($val['compare'] == '!='){
                                $compare = 'NOT like';
                            } else if($val['compare'] == '>'){
                                $compare = '>=';
                            } else if($val['compare'] == '<'){
                                $compare = '<=';
                            }
                            
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                $product_data_filter .= $add_and.$module . ".".$val['key']. " ". $compare . " '%{$GetDate}%'" ;
                            } else {
                                $advanced_filter[] = $module . ".".$val['key']. " ". $compare . " '%{$GetDate}%'" ;
                            }
                        } else if(isset($val['name']) && $val['name'] == 'range') {
                            $start_range = str_replace('/', '-', $val['start_range_'.$val['key']]);
                            $start_range = date("Y-m-d", strtotime($start_range));
                            $end_range = str_replace('/', '-', $val['end_range_'.$val['key']]);
                            $end_range = date("Y-m-d", strtotime($end_range));
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                $product_data_filter .= $add_and.$module . ".".$val['key']." >= '" .$start_range."' AND ".$module . ".".$val['key']." <= '" .$end_range."'" ;
                            } else {
                                $advanced_filter[] = $module . ".".$val['key']." >= '" .$start_range."' AND ".$module . ".".$val['key']." <= '" .$end_range."'" ;
                            }
                        } else if(isset($val['compare']) && $val['compare'] == 'last_7_days') {
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                $product_data_filter .= $add_and.$module . ".".$val['key']. " >= now() - interval 7 day";
                            } else {
                                $advanced_filter[] = $module . ".".$val['key']. " >= now() - interval 7 day";
                            }
                        } else if(isset($val['compare']) && $val['compare'] == 'next_7_days') {
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                $product_data_filter .= $add_and.$module . ".".$val['key']. " >= now() + interval 7 day";
                            } else {
                                $advanced_filter[] = $module . ".".$val['key']. " >= now() + interval 7 day";
                            }
                        } else if(isset($val['compare']) && $val['compare'] == 'last_30_days') {
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                $product_data_filter .= $add_and.$module . ".".$val['key']. " >= now() - interval 30 day";
                            } else {
                                $advanced_filter[] = $module . ".".$val['key']. " >= now() - interval 30 day";
                            }
                        } else if(isset($val['compare']) && $val['compare'] == 'next_30_days') {
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                $product_data_filter .= $add_and.$module . ".".$val['key']. " >= now() + interval 30 day";
                            } else {
                                $advanced_filter[] = $module . ".".$val['key']. " >= now() + interval 30 day";
                            }
                        } else if(isset($val['compare']) && $val['compare'] == 'last_month') {
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                $product_data_filter .= $add_and."MONTH(".$module . ".".$val['key'].") = MONTH(CURRENT_TIMESTAMP)-1 AND YEAR(".$module . ".".$val['key'].") = YEAR(CURRENT_TIMESTAMP)";
                            } else {
                                $advanced_filter[] = "MONTH(".$module . ".".$val['key'].") = MONTH(CURRENT_TIMESTAMP)-1 AND YEAR(".$module . ".".$val['key'].") = YEAR(CURRENT_TIMESTAMP)";
                            }
                        } else if(isset($val['compare']) && $val['compare'] == 'this_month') {
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                $product_data_filter .= $add_and."MONTH(".$module . ".".$val['key'].") = MONTH(CURRENT_TIMESTAMP) AND YEAR(".$module . ".".$val['key'].") = YEAR(CURRENT_TIMESTAMP)";
                            } else {
                                $advanced_filter[] = "MONTH(".$module . ".".$val['key'].") = MONTH(CURRENT_TIMESTAMP) AND YEAR(".$module . ".".$val['key'].") = YEAR(CURRENT_TIMESTAMP)";
                            }
                        } else if(isset($val['compare']) && $val['compare'] == 'next_month') {
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                $product_data_filter .= $add_and."MONTH(".$module . ".".$val['key'].") = MONTH(CURRENT_TIMESTAMP)+1 AND YEAR(".$module . ".".$val['key'].") = YEAR(CURRENT_TIMESTAMP)";
                            } else {
                                $advanced_filter[] = "MONTH(".$module . ".".$val['key'].") = MONTH(CURRENT_TIMESTAMP)+1 AND YEAR(".$module . ".".$val['key'].") = YEAR(CURRENT_TIMESTAMP)";
                            }
                        } else if(isset($val['compare']) && $val['compare'] == 'last_year') {
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                $product_data_filter .= $add_and."YEAR(".$module . ".".$val['key'].") = YEAR(CURRENT_TIMESTAMP)-1";
                            } else {
                                $advanced_filter[] = "YEAR(".$module . ".".$val['key'].") = YEAR(CURRENT_TIMESTAMP)-1";
                            }
                        } else if(isset($val['compare']) && $val['compare'] == 'this_year') {
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                $product_data_filter .= $add_and."YEAR(".$module . ".".$val['key'].") = YEAR(CURRENT_TIMESTAMP)";
                            } else {
                                $advanced_filter[] = "YEAR(".$module . ".".$val['key'].") = YEAR(CURRENT_TIMESTAMP)";
                            }
                        } else if(isset($val['compare']) && $val['compare'] == 'next_year') {
                            if ($module_name == "AOS_Products") {
                                $add_and = '';
                                if($product_data_filter != ""){
                                    $add_and = ' AND ';
                                }
                                $product_data_filter .= $add_and."YEAR(".$module . ".".$val['key'].") = YEAR(CURRENT_TIMESTAMP)+1";
                            } else {
                                $advanced_filter[] = "YEAR(".$module . ".".$val['key'].") = YEAR(CURRENT_TIMESTAMP)+1";
                            }
                        }
                    } else {
                        if ($module_name == "AOS_Products") {
                            $add_and = '';
                            if($product_data_filter != ""){
                                $add_and = ' AND ';
                            }
                            $product_data_filter .= $add_and.$module . ".".$key." like '%{$val}%'" ;
                        } else {
                            $advanced_filter[] = $module . ".".$key." like '%{$val}%'" ;
                        }
                    }
                }
            }
            if(isset($filter_where_currency_condition) && !empty($filter_where_currency_condition)){
                $module = $lcfirst_module_name;
                if(strpos($filter_where_currency_condition['key'], "_c") !== false){
                    $module = $lcfirst_module_name.'_cstm';
                }
                if($filter_where_currency_condition['name'] == 'single') {
                    if ($module_name == "AOS_Products") {
                        $add_and = '';
                        if($product_data_filter != ""){
                            $add_and = ' AND ';
                        }
                        $product_data_filter .= $add_and.$module . ".".$filter_where_currency_condition['key']." ".$filter_where_currency_condition['compare']." '" .$filter_where_currency_condition['range_total_amount_advanced']."'" ;
                    } else {
                        $advanced_filter[] = $module . ".".$filter_where_currency_condition['key']." ".$filter_where_currency_condition['compare']." '" .$filter_where_currency_condition['range_total_amount_advanced']."'" ;
                    }
                } else {
                    if ($module_name == "AOS_Products") {
                        $add_and = '';
                        if($product_data_filter != ""){
                            $add_and = ' AND ';
                        }
                        $product_data_filter .= $add_and.$module . ".".$filter_where_currency_condition['key']." >= '" .$filter_where_currency_condition['start_range_total_amount_advanced']."' AND ".$module . ".".$filter_where_currency_condition['key']." <= '" .$filter_where_currency_condition['end_range_total_amount_advanced']."'" ;
                    } else {
                        $advanced_filter[] = $module . ".".$filter_where_currency_condition['key']." >= '" .$filter_where_currency_condition['start_range_total_amount_advanced']."' AND ".$module . ".".$filter_where_currency_condition['key']." <= '" .$filter_where_currency_condition['end_range_total_amount_advanced']."'" ;
                    }
                }
            }
            
            $contact_group_id = isset($_SESSION['contact_group_id']) ? $_SESSION['contact_group_id'] : "";
            ob_start();
            
            if ($module_name != 'AOK_KnowledgeBase') {
                //Added by BC on 20-jun-2016 for list view-----------------
                $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='list' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";

                $sql_all_fields_count = "SELECT all_fields FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='list' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
                $result_count = $wpdb->get_row($sql_count);
                $all_fields_count = $wpdb->get_row($sql_all_fields_count);
                if (empty($result_count)) {
                    echo "<lable class='errors'>".$language_array['msg_set_layout']."</label>";
                    wp_die();
                }
                $json_data = $result_count->json_data;
                $results = json_decode($json_data);
                $resultsall_fields = json_decode($all_fields_count->all_fields);
                $n_array = array();
                if(!empty($resultsall_fields)){
                    $n_array = $resultsall_fields;
                }
            }
                        //End by BC on 20-jun-2016-----------------
                        if (!empty($results) || $module_name == 'AOK_KnowledgeBase') {
                            if (isset($order_by) == true && !empty($order_by) && isset($order) == true && !empty($order)) {
                                $order_by_query = "$order_by $order";
                                $_SESSION['scp_sorting_module'][$module_name]['scp_sorting_field'] = $order_by;
                                $_SESSION['scp_sorting_module'][$module_name]['scp_sorting_order'] = $order;
                            } else if ( isset( $_SESSION['scp_sorting_module'] ) && is_array( $_SESSION['scp_sorting_module'] ) && array_key_exists( $module_name, $_SESSION['scp_sorting_module'] ) ) {
                                $sorting_module = $_SESSION['scp_sorting_module'][$module_name];
                                $order_by = $sorting_module['scp_sorting_field'];
                                $order = $sorting_module['scp_sorting_order'];
                                $order_by_query = "$order_by $order";
                            } else {
                                $order_by_query = "date_entered desc";
                            }
                            
                            if(empty($n_array) && !empty($results)){
                                foreach ($results as $key_n => $val_n) {
                                    if (!empty($val_n->related_module)) {
                                        if ($val_n->type == 'relate' && (!in_array($val_n->related_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array
                                            continue;
                                        }
                                    }
                                    $n_array[] = strtolower($key_n);
                                }
                            }
                            if($module_name != 'AOK_KnowledgeBase') {
                                if (!in_array('id', $n_array) && !empty($n_array)) {
                                    array_push($n_array, 'id');
                                }
                                array_push($n_array, 'currency_id'); //Added by BC on 02-jul-2016 for getting currency id
                            }
                            
                            /*searchable fields*/
                            $searchable_fields = array();
                            if ($module_name != 'AOK_KnowledgeBase') {
                                
                                $results_data = (array) $results;
                                if ($module_name != 'Accounts') {
                                    foreach ($results_data as $val_array) {
                                        if(isset($val_array->searchable) && $val_array->searchable){
                                            $searchable_fields[] = $val_array->name;
                                        }
                                    }
                                }
                            }

                            $relate_to_module = isset($_REQUEST['relate_to_module']) ? $_REQUEST['relate_to_module'] : "";
                            $relate_to_id = isset($_REQUEST['relate_to_id']) ? $_REQUEST['relate_to_id'] : "";
                            
                            $relate_to_module_name = ( $relate_to_module != '' ) ? $relate_to_module : 'Contacts';
                            $relate_to_id = ( $relate_to_id != '' ) ? $relate_to_id : $_SESSION['scp_user_id'];
                            if ( isset($_REQUEST['subpanelname']) && $_REQUEST['subpanelname'] != ''){
                                $relationship_name = $_REQUEST['subpanelname'];
                            } else {
                                 $relationship_name = ( isset( $custom_relationships[$module_name][$relate_to_module_name] ) && $custom_relationships[$module_name][$relate_to_module_name] != '' ) ? $custom_relationships[$module_name][$relate_to_module_name] : strtolower( $module_name );
                            }

                            if ($module_name == 'AOK_KnowledgeBase') {//Added by BC on 19-may-2016
                                $n_array = array('id', 'name', 'date_entered', 'description', 'currency_id');
                                if ($where_con != "") {
                                    $where_con .= " OR ". $lcfirst_module_name . ".description like '%{$search_name}%'";
                                    $where_con .= " AND ";
                                }
                                $where_con .= $lcfirst_module_name.".status = 'published_public' AND ".$lcfirst_module_name."_cstm.portal_visible_c = 1";

                                if (isset($_SESSION['scp_knowledgebase_categories']) && isset($_REQUEST['parentmodule']) && $_REQUEST['parentmodule'] == 'solutions') {
                                    $scp_sol_cat = $_SESSION['scp_knowledgebase_categories'];
                                    $list_result = $objSCP->get_knowledgebase_categories_wise($scp_sol_cat, $search_name, $offset, $limit);
                                } else {
                                    if (isset($AOK_Knowledge_Base_Categories) && !empty($AOK_Knowledge_Base_Categories)) {
                                        $list_result = $objSCP->get_contact_filtered_relationships('bc_user_group', $contact_group_id, 'bc_user_group_aok_knowledgebase_1', $n_array, $where_con, $order_by_query, 0, $offset, $limit, $AOK_Knowledge_Base_Categories);
                                    } else {
                                        $list_result = $objSCP->get_relationships('bc_user_group', $contact_group_id, 'bc_user_group_aok_knowledgebase_1', $n_array, $where_con, $order_by_query, 0, $offset, $limit);
                                    }
                                }

                            } elseif ($module_name == 'AOS_Products' || ( ( $module_name == 'Meetings' || $module_name == 'Calls' ) && isset( $_REQUEST['relate_to_module'] ) && $_REQUEST['relate_to_module'] != '' ) ) {
                      
                                if ($module_name == 'AOS_Products') {
                                    if (!empty($n_array)) { 
                                        $pos = array_search('product_image', $n_array);
                                        if (!empty($pos)) { unset($n_array[$pos]); }
                                    }
                                    $product_where_con = 'visible_portal = 1';
                                    $where_cons = ( $where_con != '' ) ? $where_con . ' AND ' . $product_where_con : $product_where_con;
                                    $where_con = ( $product_data_filter != '' ) ? $where_cons . ' AND ' . $product_data_filter : $where_cons;
                                    $list_result = $objSCP->get_entry_list($module_name, $where_con, $n_array, $order_by_query, 0, $limit, $offset);
                                }else {
                                    $visble_where_con = 'portal_visible = 1';
                                    array_push($n_array, "portal_visible");
                                    $where_con = ( $where_con != '' ) ? $where_con . ' AND ' . $visble_where_con : $visble_where_con;
                                    
                                    $list_result = $objSCP->get_relationships($relate_to_module_name, $relate_to_id, $relationship_name, $n_array, $where_con, $order_by_query, 0, $offset, $limit,$advanced_filter);
                                }
                            } else {
                                //filtering for subpanel to get record with related module and contact module for v3.0
                                if ($relate_to_module == "") { // normal list call from menu
                                   
                                    if($_SESSION['scp_user_group_type'] == "contact_based"){
                                        if (isset($custom_relationships[$module_name]['Contacts'])) {
                                            $list_result = $objSCP->get_relationships($relate_to_module_name, $relate_to_id, $relationship_name, $n_array, $where_con, $order_by_query, 0, $offset, $limit,$advanced_filter);
                                        } else {
                                            $list_result = $objSCP->get_entry_list($module_name, $where_con, $n_array, $order_by_query, 0, $limit, $offset);
                                        }
                                    }else{
                                        if (($module_name != "Accounts" && $module_name != "Calls" && $module_name != "Notes" && $module_name != "Meetings" && $module_name != "bc_proposal" ) && (isset($custom_relationships[$module_name]['Contacts']))) {
                                           $list_result = $objSCP->get_relationships('Accounts', $_SESSION['scp_account_id'], $relationship_name, $n_array, $where_con, $order_by_query, 0, $offset, $limit,$advanced_filter);
                                        } else {
                                            $list_result = $objSCP->get_entry_list($module_name, $where_con, $n_array, $order_by_query, 0, $limit, $offset);
                                        }
                                    }

                                    if($module_name == "Accounts"  || $module_name == "Calls" || $module_name == "Notes" || $module_name == "Meetings" || $module_name == "bc_proposal"  ){ 
                                        if (isset( $custom_relationships[$module_name]['Contacts'])) { 
                                            $list_result = $objSCP->get_relationships($relate_to_module_name, $relate_to_id, $relationship_name, $n_array, $where_con, $order_by_query, 0, $offset, $limit,$advanced_filter);

                                        } else {
                                            $list_result = $objSCP->get_entry_list($module_name, $where_con, $n_array, $order_by_query, 0, $limit, $offset);
                                        }
                                    }
                                    
                                } else {
                                    if ( isset( $_REQUEST['relate_to_module'] ) && $_REQUEST['relate_to_module'] != '' ) {
                                        if ($module_name == 'Notes') {
                                            $note_where_con = "contact_id = '".$_SESSION['scp_user_id']."'";
                                            $where_con = ( $where_con != '' ) ? $where_con . ' AND ' . $note_where_con : $note_where_con;
                                        }
                                        $third_relationship_name = isset($custom_relationships[$module_name]["Contacts"]) ? $custom_relationships[$module_name]["Contacts"] : strtolower($module_name);
                                      
                                        if($_SESSION['scp_user_group_type'] == "contact_based"){
                                            if ($third_relationship_name == strtolower($module_name)) {
                                                $third_relationship_name = "contacts";
                                            }
                                             $list_result = $objSCP->get_contact_filtered_relationships($relate_to_module_name, $relate_to_id, $relationship_name, $n_array, $where_con, $order_by_query, 0, $offset, $limit, $_SESSION['scp_user_id'], $third_relationship_name);
                                        }else{
                                            if ($third_relationship_name == strtolower($module_name)) {
                                                $third_relationship_name = "accounts";
                                            }
                                             if($module_name == "Accounts"){
                                                $list_result = $objSCP->get_contact_filtered_relationships($relate_to_module_name, $relate_to_id, $relationship_name, $n_array, $where_con, $order_by_query, 0, $offset, $limit, $_SESSION['scp_user_id'], $third_relationship_name);
                                             }else{
                                                $list_result = $objSCP->get_contact_filtered_relationships($relate_to_module_name, $relate_to_id, $relationship_name, $n_array, $where_con, $order_by_query, 0, $offset, $limit, $_SESSION['scp_user_id'], $third_relationship_name,$_SESSION['scp_account_id']);
                                            }
                                        }
                                    } else {
                                        if($_SESSION['scp_user_group_type'] == "contact_based"){
                                            $list_result = $objSCP->get_relationships($relate_to_module_name, $relate_to_id, $relationship_name, $n_array, $where_con, $order_by_query, 0, $offset, $limit,$advanced_filter);
                                        }else{
                                            $list_result = $objSCP->get_relationships('Accounts', $_SESSION['scp_account_id'], $relationship_name, $n_array, $where_con, $order_by_query, 0, $offset, $limit,$advanced_filter);
                                        }
                                    }
                                }

                                if ($module_name == 'AOS_Invoices') {
                                    global $InvoiceStatus;
                                    $invoiceIds = array();
                                    foreach ($list_result->entry_list as $value) {
                                        array_push($invoiceIds, $value->id);
                                    }
                                    $InvoiceStatus = $objSCP->getInvoiceTransactionDetails($invoiceIds);
                                }
                            }

                            if ( ( $module_name == 'AOS_Products' || $module_name == 'bc_proposal' ) &&
                            ( ( isset( $_SESSION['scp_all_currency'] ) && empty( $_SESSION['scp_all_currency'] ) ) || ( ! isset( $_SESSION['scp_all_currency'] ) ) ) ) {

                                $all_currentcy = $objSCP->portal_getCurrencylist();
                                if ( isset( $all_currentcy->currencyAdded ) && ! empty( $all_currentcy->currencyAdded ) ) {
                                    $_SESSION['scp_all_currency'] = $all_currentcy->currencyAdded;
                                }
                            }
                            $_SESSION['list_records'] = '';
                            $cnt = $countCases = 0;
                            if(!empty($list_result->entry_list)){
                                $_SESSION['list_records'] = json_encode($list_result->entry_list);
                                $cnt = count((array)$list_result->entry_list);
                                $countCases = $list_result->total_count;
                            }
                            
                            if(!empty($searchable_fields)){ 
                                include( TEMPLATE_PATH . 'bcp_advace_filter_page.php');
                            }
                    if ($relate_to_module == '') {
                            ?>
                    <div class="white-body-shadow listing-section preferences-content">
                        <div class="case-listing-block">
                            <?php
                    }
                            $html = "";
                            
                            //For case solutions search
                            if (!isset($_REQUEST['parentmodule']) || (isset($_REQUEST['parentmodule']) && $_REQUEST['parentmodule'] != 'solutions') ) {
                            //note attchment
                            if($_REQUEST['modulename'] != 'Accounts'){
                            ?>
                            <form action='<?php echo site_url(); ?>/wp-admin/admin-post.php' method='post' id='download_note_id'>
                                <input type='hidden' name='action' value='bcp_get_note_attachment'>
                                <input type = 'hidden' name = 'scp_current_url' value = '<?php echo $current_url; ?>'>
                                <input type='hidden' name='scp_note_id' value='' id='scp_note_id'>
                                <input type='hidden' name='scp_note_field' value='' id='scp_note_field'>
                                <input type='hidden' name='scp_note_view' value='' id='scp_note_view'>
                            </form>
                            <form action='<?php echo site_url(); ?>/wp-admin/admin-post.php' method='post' id='doc_submit'>
                                <input type='hidden' name='action' value='bcp_get_doc_attachment'>
                                <input type='hidden' name='module_name' id="scp_module_name" value='<?php echo $module_name; ?>'>
                                <input type = 'hidden' name = 'scp_current_url' value = '<?php echo $current_url; ?>'>
                                <input type='hidden' name='scp_doc_id' value='' id='scp_doc_id'>
                                <input type='hidden' name='scp_doc_view' value='' id='scp_doc_view'>
                                <input type='hidden' name='scp_doc_field_name' value='' id='scp_doc_field_name'>
                            </form>
                            
                            <form action = '<?php echo site_url(); ?>/wp-admin/admin-post.php' method = 'post' id='quote_pdf_submit'>
                                <input type='hidden' name='action' value='bcp_get_pdf_quote'>
                                <input type='hidden' name='scp_module_pdf_id' value='' id='scp_module_pdf_id'>
                                <input type = 'hidden' name = 'scp_current_url' value = '<?php echo $current_url; ?>'>
                                <input type = 'hidden' name = 'module_name' value = '<?php echo $module_name; ?>'>
                                <input type='hidden' name='scp_module_name_download' value='' id='scp_module_name_download'>
                            </form>
                            <form action = '<?php echo site_url(); ?>/wp-admin/admin-post.php' method = 'post' id='export_csv_submit'>
                                <input type='hidden' name='action' value='scp_export_data'>
                                <input type='hidden' name='scp_csv_module_id[]' value='' class='scp_csv_module_id'>
                                <input type = 'hidden' name = 'scp_current_url' value = '<?php echo $current_url; ?>'>
                                <input type = 'hidden' name = 'order_by' value = '<?php echo $order_by; ?>'>
                                <input type = 'hidden' name = 'order' value = '<?php echo $order; ?>'>
                                <input type = 'hidden' name = 'module_name' value = '<?php echo $module_name; ?> '>
                            </form>
                            <form action = '<?php echo site_url(); ?>/wp-admin/admin-ajax.php' method = 'post' enctype = 'multipart/form-data' id = 'actionform'>
                                <input type = 'hidden' name = 'action' value = 'bcp_add_moduledata_call'>
                                <input type = 'hidden' name = 'scp_current_url' value = '<?php echo $current_url; ?>'>
                                <input type = 'hidden' name = 'id' value = '' id = 'scp_id'>
                                <input type = 'hidden' name = 'delete' value = '1'>
                                <?php wp_nonce_field('sfcp_portal_nonce', 'sfcp_portal_nonce_field'); ?>
                                <input type = 'hidden' name = 'module_name' value = '<?php echo $module_name; ?>'>
                            </form>
                            <?php } ?>
                            <?php
                            if ($module_name == 'AOK_KnowledgeBase') {
                                $kb_class = 'scp-kb-btn';
                            } else {
                                $kb_class = '';
                            }

                            //hide title and heading in subpanel for v3.0
                            $case_deflection = 0;
                            //Check if solution categories are blank or not.
                            if (isset($_SESSION['scp_knowledgebase_categories']) && isset($_SESSION['scp_additional_setting']) && isset($_SESSION['scp_additional_setting']->case_deflection) && $_SESSION['scp_additional_setting']->case_deflection) {
                                $case_deflection = 1;
                            }
                            
                            $heading = '';
                            $addRecordUrl = '';
                            if ($relate_to_module == '') {
                                    //for add
                                    $heading = $module_name_label;

                                    if ($_SESSION['module_action_array'][$module_name]['create'] == 'true') {
                                        if (($module_name == 'Accounts' && $countCases == 0 && $search_name == "") || ($custom_relationships[$module_name]['Type'] != 'many-to-one' && $module_name != 'Accounts') || ($custom_relationships[$module_name]['Type'] == 'many-to-one' && $countCases == 0 && $module_name != 'Accounts')) {
                                            if ($module_name == "Cases" && $case_deflection) {
                                                $addRecordUrl = '#data/Solutions/Search/';
                                            } else {
                                                $addRecordUrl = "#data/".$module_name."/edit/";
                                            }
                                        }
                                    }
                                }
                            
                            ?>
                            <?php if($heading != "" || $addRecordUrl != ""){ ?>
                            <div class="preferences-header white-bg-heading justify-content-between d-flex">
                                <?php if($heading){ ?>
                                <h2><?php echo $heading; ?></h2>
                                <?php } ?>
                                <div class="d-flex case-dropdown-main align-items-center">
                                    <?php if ($addRecordUrl) { ?>
                                    <a href="<?php echo $addRecordUrl; ?>" class="add-more-btn btn" title="<?php echo $language_array['lbl_add']; ?>">
                                        <?php echo $language_array['lbl_add']; ?>
                                    </a>
                                    <?php } ?>
                                    <?php if(!empty($searchable_fields)){ ?>
                                    <div class="advance_filter">
                                        <a href="javascript:void(0)" class="btn btn-blue-curve dcp_theme_btn_hover float-right btn-filter" title="<?php echo $language_array['lbl_advance_filter']; ?>">
                                            <span class="fa fa-filter"></span>
                                        </a>
                                        <?php if(!empty($advanced_filter)) { ?>
                                        <p>
                                            <a href="javascript:void(0)" class="btn-filter-clear" title="<?php echo $language_array['lbl_advance_filter']; ?>">
                                            <em class="fa fa-remove" aria-hidden="true"></em>
                                        </a>
                                        </p>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if(isset($_REQUEST['subpanelname']) && $_REQUEST['subpanelname']){ ?>
                                <input type='hidden' id='subpanelname' value='<?php echo $_REQUEST['subpanelname']; ?>' />
                            <?php } ?>
                            <?php 
                            $advancefilter = '';
                            if(!empty($filter_where_condition) || !empty($filter_where_currency_condition)){
                                $advancefilter = '1';
                            }
                            ?>
                    <?php if ($relate_to_module == '') { ?>
                    <div class='preferences-tabs white-main-body'>  
                    <?php } ?>
                        <div class='imex-content-table bg-white today-schedule-table'>
                            <?php
                            if ($cnt > 0 && $relate_to_module == '' ) {
                                if ($module_name != "AOK_KnowledgeBase" && $module_name != 'Accounts') {
                                    $disabled = "disabled";
                                    if (isset($_REQUEST['select_all']) && $_REQUEST['select_all'] == '1') {
                                        $disabled = "";
                                    }
                                    ?>
                                    <div class='scp-list-action-div case-heading-action d-flex justify-content-between'>
                                        <div class='scp-left-list-action-div case-heading-action-left d-flex align-items-center <?php echo $disabled; ?>' id="delete-export-btn">
                                            <a href="javascript:void(0);" class="btn selected-case-button" onclick="scp_export_data('<?php echo $module_name; ?>','csv');">
                                                <span><?php echo $language_array['lbl_export']; ?></span>
                                               <?php echo $module_icon_content['export']; ?>
                                            </a>
                                            <?php if ($_SESSION['module_action_array'][$module_name]['delete'] == 'true') { ?>
                                            <a href="javascript:void(0);" class="btn selected-case-button" onclick="delete_multiple('0','<?php echo $module_name; ?>');">
                                                <span><?php echo $language_array['lbl_delete']; ?> </span>
                                                <?php echo $module_icon_content['delete']; ?>
                                            </a>
                                            <?php } ?>
                                            <span id="scp-checkbox-selected-span" class="m-0"></span>
                                        </div>
                                        <!-- scp-left-list-action-div end -->
                                        <!--paging div start--> 
                                        <?php 
                                        if ($module_name != 'Accounts') { ?>
                                       <div class='scp-right-list-action-div case-heading-action-right'>
                                           <select onchange='bcp_module_paging(1,"<?php echo $module_name; ?>","","","","list","","<?php echo $advancefilter; ?>",this);' class='scp-select-list-paging scp-form-control' value='<?php echo $limit; ?>' >
                                           <?php 
                                           $paging = array(5,10,25,50,100);
                                           foreach ($paging as $p) {
                                               $selected_limit = "";
                                               if ($p == $limit) {
                                                   $selected_limit = "selected='selected'";
                                               }
                                               ?>
                                               <option <?php echo $selected_limit; ?> value='<?php echo $p; ?>' ><?php echo $p; ?></option>
                                               <?php } ?>
                                           </select>
                                       </div>
                                       <?php } ?>
                                       <!--paging div end-->
                                        
                                    </div>
                                    <!-- scp-list-action-div end -->
                                    <?php
                                }
                            }
                        } else {
                            if ($cnt > 0) {
                                ?>
                                <div class="">
                                    <strong><?php echo $language_array["lbl_search_results"]; ?> : </strong>
                                </div>
                            <?php
                            }
                        }// End of Solution condition
                            
                         if ($module_name == 'AOK_KnowledgeBase') {  //Addded by BC on 20-may-2016
                            include( TEMPLATE_PATH . 'bcp_list_page_kb.php');
                        }
                        
                        if ($cnt > 0 && $module_name != 'AOK_KnowledgeBase') {
                            include( TEMPLATE_PATH . 'bcp_list_page_v6.php');
                            
                            ?>
                            <!--paging div start--> 
                            <?php 
                            if ($module_name != 'Accounts' && $cnt > 0 && $relate_to_module == '') { ?>
                            <div class='scp-list-action-div scp-bottom-action-div'>
                                <div class='scp-right-list-action-div'>
                                    <select onchange='bcp_module_paging(1,"<?php echo $module_name; ?>","","","","list","","<?php echo $advancefilter; ?>",this);' class='scp-select-list-paging scp-form-control' value='<?php echo $limit; ?>' >
                                    <?php 
                                    $paging = array(5,10,25,50,100);
                                    foreach ($paging as $p) {
                                        $selected_limit = "";
                                        if ($p == $limit) {
                                            $selected_limit = "selected='selected'";
                                        }
                                        ?>
                                        <option <?php echo $selected_limit; ?> value='<?php echo $p; ?>' ><?php echo $p; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        <?php } ?>
                            <!--paging div end-->
                            <?php
                        } else {
                            if($module_name != 'AOK_KnowledgeBase') {
                                if (isset($_SESSION['bcp_add_record']) && $_SESSION['bcp_add_record'] != '') {
                             ?>
                                    <span class='success' id='succid'><?php echo $_SESSION['bcp_add_record']; ?></span>
                                    <?php
                                    unset($_SESSION['bcp_add_record']);
                                }
                             ?>
                                <div class='detail-part'>
                                    <strong><?php echo ( isset($_SESSION['portal_message_list']->module_no_records) ? __($_SESSION['portal_message_list']->module_no_records) : '' ); ?></strong>
                                </div>
                                <?php
                            }
                        }

                        $pagination_url = "?";
                        echo scp_pagination($countCases, $limit, $page_no, $pagination_url, $module_name, $order_by, $order, $view1, $current_url,$advancefilter);
                        }
                        ?>
                        </div>
                        <!-- imex-content-table bg-white  today-schedule-table end -->
                    <?php if ($relate_to_module == '') { ?>
                    </div>
                    <!-- preferences-tabs white-main-body end -->
                </div>
                <!-- case-listing-block end -->
            </div>
            <!-- white-body-shadow preferences-content end -->
            <?php
                }
            $html = ob_get_clean();
            echo $html;
            wp_die();
        } else {
            $objSCP = 0;
            $conn_err = $language_array['msg_portal_is_disabled_contact_admin'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            echo "-1";
            wp_die();
        }
    }
}

if( ! function_exists('scp_view_module') ) {
    add_action('wp_ajax_bcp_view_module', 'scp_view_module');
    add_action('wp_ajax_nopriv_bcp_view_module', 'scp_view_module');
    function scp_view_module() {

        global $objSCP, $wpdb, $sugar_crm_version, $lang_code, $language_array, $custom_relationships, $module_icon_content;
        //20-aug-2016
        $sess_id = $objSCP->session_id;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        if ($sess_id != '') {
            $module_name = sanitize_text_field( $_POST['modulename'] );
            $view = sanitize_text_field( $_POST['view'] );
            $id = sanitize_text_field( $_POST['id'] );

            // checking permission
//            if ( $module_name != 'AOS_Products'){
//                check_permission($module_name,'view',$id);
//            }
            $current_url = isset($_POST['current_url']) ? esc_url( $_POST['current_url'] ) : "";
            if (isset($current_url) && $current_url != '') {
                $current_url = explode('?', $current_url, 2);
                $current_url = $current_url[0];
            }
            $html = '';
            //Added by BC on 20-jun-2016 for edit view-----------------
            $contact_group_id = isset($_SESSION['contact_group_id']) ? $_SESSION['contact_group_id'] : "";
            $list_records = isset($_SESSION['list_records']) ? json_decode($_SESSION['list_records']) : "";
            $custom_relationships = (isset($_SESSION['scp_custom_relationships'])) ? $_SESSION['scp_custom_relationships'] : "";
            
            $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
            $result_count = $wpdb->get_row($sql_count);
            if (empty($result_count)) {
                echo "<lable class='errors'>".$language_array['msg_set_layout']."</label>";
                wp_die();
            }
            
            $json_data = $result_count->json_data;
            $results = (array) json_decode($json_data);
            //End by BC on 20-jun-2016-----------------
            
            /* condition login [start] */
            $sql_conditionlogic_count = "SELECT conditional_fields FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
            $result_conditionlogic_count = $wpdb->get_row($sql_conditionlogic_count);
            if (!empty($result_conditionlogic_count->conditional_fields)) {
                $conditionlogic = json_decode($result_conditionlogic_count->conditional_fields);
                $conditionlogic = (array) $conditionlogic;
            }
            
            /* condition login [end] */
            
            //Updated by BC on 17-jun-2016 for timezone store in session
            if (empty($_SESSION['user_timezone'])) {
                $result_timezone = $objSCP->getUserTimezone();
            } else {
                $result_timezone = $_SESSION['user_timezone'];
            }
            date_default_timezone_set($result_timezone);
            $result_timezone = date_default_timezone_get();

            if($module_name == 'ProjectTask') {
                $where_con = "project_task.id = '{$id}'";
            } else {
                $where_con = strtolower($module_name) . ".id = '{$id}'";
            }
            
            $n_array = array();
            if(!empty($results)){
                foreach ($results as $k => $vals_lbl) {
                    $vals_lbl = (array) $vals_lbl;
                    foreach ($vals_lbl['rows'] as $k_lbl2 => $vals) {
                        foreach ($vals as $key_fields => $val_fields) {
                            if (!empty($val_fields->related_module)) {
                                if ($val_fields->type == 'relate' && (!in_array($val_fields->related_module, (array) $_SESSION['module_array']))) {
                                    continue;
                                }
                            }
                            if(isset($val_fields->name)){
                                $n_array[] = strtolower($val_fields->name);
                            }
                        }
                    }
                }
            }
            array_push($n_array, 'id');
            array_push($n_array, 'currency_id'); 
            if(!empty($list_records) && $module_name != 'bc_proposal') {
                $record_detail = new stdClass();
                $record_detail->entry_list = array();
                foreach ($list_records as $listRecords){
                    if($listRecords->id!='' && $id == $listRecords->id){
                        $record_detail->entry_list[] = $listRecords;
                        break;
                    }
                }
            }
           
            if( (!empty($list_records) && $record_detail->entry_list == null || empty($record_detail->entry_list) ) || $module_name == 'AOS_Quotes') {
                /* permission and detail page content get */
                if ( $module_name == 'AOS_Quotes' ) {
                    $record_detail = $objSCP->get_entry_quotes($module_name, $id);
                    if ( ! empty( $record_detail ) && isset( $record_detail->Result ) && ! empty( $record_detail->Result ) ) {
                        $record_detail->entry_list[0] = $record_detail->Result;
                    }
                } else {
                    $relation_name = ( isset( $custom_relationships[$module_name]["Contacts"] ) && $custom_relationships[$module_name]["Contacts"] != '' ) ? $custom_relationships[$module_name]["Contacts"] : strtolower( $module_name );
                    if($_SESSION['scp_user_group_type'] == "contact_based"){
                        $record_detail = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], $relation_name, $n_array, $where_con);
                    } else {
                        $record_detail = $objSCP->get_relationships('Accounts', $_SESSION['scp_account_id'], $relation_name, $n_array, $where_con);
                    }
                    
                    if (!empty($record_detail->entry_list)) {
                        if ( ( $module_name == 'Meetings' || $module_name == 'Calls' || $module_name == 'bc_proposal') || (!array_key_exists($module_name, $custom_relationships[$module_name]))) {
                            $record_detail = $objSCP->get_entry_list($module_name, $where_con);
                            
                        } else {
                            wp_die();
                        }
                    }
                }
                
            }
            
            if (!empty($record_detail->entry_list) ) {
                
                $name = isset( $record_detail->entry_list[0]->name_value_list->name->value ) ? $record_detail->entry_list[0]->name_value_list->name->value : '';
                if ($module_name == 'AOS_Quotes' || $module_name == 'AOS_Invoices' || $module_name == 'AOS_Contracts') {//Added by BC on 02-july-2016 for storing currency
                    $curreny_id = $record_detail->entry_list[0]->name_value_list->currency_id->value;
                    if (!empty($curreny_id)) {
                        $currency_symbol = $_SESSION['Currencies']->$curreny_id->symbol;
                    }
                }

                if ( ( $module_name == 'AOS_Products' || $module_name == 'bc_proposal' ) &&
                   ( ( isset( $_SESSION['scp_all_currency'] ) && ! empty( $_SESSION['scp_all_currency'] ) ) || ( ! isset( $_SESSION['scp_all_currency'] ) ) ) ) {

                    $all_currentcy = $objSCP->portal_getCurrencylist();
                    if ( isset( $all_currentcy->currencyAdded ) && ! empty( $all_currentcy->currencyAdded ) ) {
                        $_SESSION['scp_all_currency'] = $all_currentcy->currencyAdded;
                    }

                    if ( isset( $record_detail->entry_list[0]->name_value_list->currency_id->value ) && $record_detail->entry_list[0]->name_value_list->currency_id->value != '' && isset( $_SESSION['scp_all_currency'] ) && ! empty( $_SESSION['scp_all_currency'] ) ) {
                        $curreny_id = $record_detail->entry_list[0]->name_value_list->currency_id->value;
                        $currency_symbol = $_SESSION['scp_all_currency']->$curreny_id->currency_symbol;
                    }
                }

                if (!empty($results)) {
                    // for not set layout
                ob_start();
                ?>
                <form action = '<?php echo site_url(); ?>/wp-admin/admin-ajax.php' method = 'post' enctype = 'multipart/form-data' id = 'actionform'>
                <input type = 'hidden' name = 'action' value = 'bcp_add_moduledata_call'>
                <input type = 'hidden' name = 'scp_current_url' value = '<?php echo $current_url; ?>'>
                <input type = 'hidden' name = 'id' value = '' id = 'scp_id'>
                <input type = 'hidden' name = 'delete' value = '1'>
                <?php wp_nonce_field('sfcp_portal_nonce', 'sfcp_portal_nonce_field'); ?>
                <input type = 'hidden' name = 'module_name' value = '<?php echo $module_name; ?>'>
                </form>
                <?php
                $html .= ob_get_clean();
                    $html .= "<form action='" . site_url() . "/wp-admin/admin-post.php' method='post' id='doc_submit'>
                    <input type='hidden' name='action' value='bcp_get_doc_attachment'>
                    <input type='hidden' name='module_name' id='scp_module_name' value='".$module_name."'>
                    <input type='hidden' name='scp_doc_id' value='' id='scp_doc_id'>
                    <input type='hidden' name='scp_doc_view' value='' id='scp_doc_view'>
                    <input type='hidden' name='scp_doc_field_name' value='' id='scp_doc_field_name'>
                    </form>";
                     // note attchment
                    $html .= "<form action='" . site_url() . "/wp-admin/admin-post.php' method='post' id='download_note_id'>
                    <input type='hidden' name='action' value='bcp_get_note_attachment'>
                    <input type='hidden' name='scp_note_field' value='' id='scp_note_field'>
                    <input type='hidden' name='scp_note_id' value='' id='scp_note_id'>
                    <input type='hidden' name='scp_note_view' value='' id='scp_note_view'>
                    <input type='hidden' name='scp_note_case_id' value='' id='scp_note_case_id'>
                    </form>";
                    if ($module_name == 'AOS_Quotes' || $module_name == "AOS_Invoices" || $module_name == "AOS_Contracts") {
                        $html .= "<form action = '" . site_url() . "/wp-admin/admin-post.php' method = 'post' id='quote_pdf_submit'>
                        <input type='hidden' name='action' value='bcp_get_pdf_quote'>
                        <input type='hidden' name='scp_module_pdf_id' value='' id='scp_module_pdf_id'>
                        <input type = 'hidden' name = 'scp_current_url' value = '" . $current_url . "'>
                        <input type = 'hidden' name = 'module_name' value = '" . $module_name . "'>
                        <input type='hidden' name='scp_module_name_download' value='' id='scp_module_name_download'>
                        </form>";
                    }
                    $subpanels = $objSCP->getRecordSubpanelsDetail($module_name, $id);
                    //hiding relate_to_module and relate
                    $html .= "<input type='hidden' id='relate_to_module' value='".$module_name."' >";
                    $html .= "<input type='hidden' id='relate_to_id' value='".$id."' >";
                    include( TEMPLATE_PATH . 'bcp_view_page_v6.php');
                } else {
                    $html .= $json_data;
                }
                $html .= "</div>";
                echo $html;
                wp_die();
                
            } else {
                wp_die();
            }
        } else {
            $objSCP = 0;
            $conn_err = $language_array['msg_portal_is_disabled_contact_admin'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            echo "-1";
            wp_die();
        }
    }
}

if( ! function_exists('get_module_list_callback') ) {
    add_action('wp_ajax_get_module_list', 'get_module_list_callback');
    add_action('wp_ajax_nopriv_get_module_list', 'get_module_list_callback');
    function get_module_list_callback() {

        global $objSCP;
        $custom_relationships = $_SESSION['scp_custom_relationships'];
        $scp_sugar_rest_url = get_option('biztech_scp_rest_url');
        $scp_sugar_username = get_option('biztech_scp_username');
        $scp_sugar_password = scp_decrypt_string(get_option('biztech_scp_password'));

        $objSCP = new SugarRestApiCall($scp_sugar_rest_url, $scp_sugar_username, $scp_sugar_password);
        $logged_user_id = $_SESSION['scp_user_id'];

        $module_name = sanitize_text_field($_REQUEST['parent_type']);
        $relation_name = isset($custom_relationships[$module_name]['Contacts']) ? $custom_relationships[$module_name]['Contacts'] : strtolower($module_name);
        $select_fields = array('id', 'name');

        if($_SESSION['scp_user_group_type'] == "contact_based"){
            $record_detail = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], $relation_name, $select_fields);
        }else{
            $record_detail = $objSCP->get_relationships('Accounts', $_SESSION['scp_account_id'], $relation_name, $select_fields);
        }

        
        $html = "";
        $html .= "<select id='parent_id' name='parent_id' class='input-text form-control'><option value=''></option>";
        for ($m = 0; $m < count($record_detail->entry_list); $m++) {
            $mod_id = $record_detail->entry_list[$m]->name_value_list->id->value;
            $mod_name = $record_detail->entry_list[$m]->name_value_list->name->value;
            $html .= "<option value=" . $mod_id . ">" . htmlentities($mod_name) . "</option>";
        }
        $html .= "</select>";

        echo $html;
        wp_die();
    }
}

if( ! function_exists('scp_add_module') ) {
    add_action('wp_ajax_bcp_add_module', 'scp_add_module');
    add_action('wp_ajax_nopriv_bcp_add_module', 'scp_add_module');
    function scp_add_module() {
    
        global $objSCP, $wpdb, $sugar_crm_version, $lang_code, $language_array,$module_icon_content;
        //20-aug-2016
        $sess_id = $objSCP->session_id;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        if ($sess_id != '') {
            $module_name = sanitize_text_field( $_POST['modulename'] );
            if (isset($_SESSION['module_array']) && (is_array((array) $_SESSION['module_array'])) ) {//Added for getting module key OR lable added from sugar side
                $arry_lable = (array) $_SESSION['module_array'];
                $module_name_label = isset($arry_lable[$module_name]) ? $arry_lable[$module_name] : "";
            }
            $view = sanitize_text_field( $_POST['view'] );
            $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : "";
            $contact_group_id = isset($_SESSION['contact_group_id']) ? $_SESSION['contact_group_id'] : "";
            
            // If module name contact than set layout in database
            if ($module_name == "Contacts" && !isset($_SESSION['scp_user_id'])) {
                
                $modules_array = $objSCP->getPortal_accessibleModules(array("Contacts" => "Contacts"), $lang_code);
                $contact_group_id = (isset($modules_array->contact_group_id)) ? $modules_array->contact_group_id : "";
                $_SESSION['contact_group_id'] = $contact_group_id;
                
                // Insert or update contact layout ----------------
                if (!empty($modules_array->layouts)) {
                    foreach ($modules_array->layouts as $k_module => $v_module_list) {
                        foreach ($v_module_list as $k_view => $v_panel) {
                            if ($k_view != "edit") {
                                continue;
                            }
                            $panel_data = json_encode((isset($v_panel->panels)?$v_panel->panels:array()));
                            if (!is_object($v_panel)) {
                                $panel_data = $v_panel;
                            }
                            
                            $combined_fields = '';
                            if ( isset( $v_panel->combined_fields ) ) {
                                $combined_fields = json_encode($v_panel->combined_fields);
                            }
                            $conditional_statements = '';
                            if ( isset( $v_panel->conditional_statements ) ) {
                                $conditional_statements = json_encode($v_panel->conditional_statements);
                            }
                            $modified_date = '';
                            if ( isset( $v_panel->modified_date ) ) {
                                $modified_date = $v_panel->modified_date;
                            }

                            //get record exists
                            $sql_count = "SELECT id,date,COUNT(*) as count FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $k_module . "' AND layout_view='" . $k_view . "' AND contact_group='" . $contact_group_id . "' AND lang_code = '".$lang_code."'";
                            $result_count = $wpdb->get_row($sql_count);
                            $rowCount = $result_count->count;

                            if ($rowCount > 0) {
                                $db_date = $result_count->date;
                                $record_id = $result_count->id;

                                $wpdb->update(
                                    $wpdb->prefix . "module_layout",
                                    array(
                                            'version' => $sugar_crm_version,
                                            'module_name' => $k_module,
                                            'layout_view' => $k_view,
                                            'json_data' => $panel_data,
                                            'all_fields' => $combined_fields,
                                            'conditional_fields' => $conditional_statements,
                                            'date' => $modified_date,
                                            'contact_group' => $contact_group_id,
                                            'lang_code' => $lang_code,
                                    ),
                                    array( 'id' => $record_id )
                                );
                            } else {
                                $wpdb->insert($wpdb->prefix . 'module_layout', array(
                                    'version' => $sugar_crm_version,
                                    'module_name' => $k_module,
                                    'layout_view' => $k_view,
                                    'json_data' => $panel_data,
                                    'all_fields' => $combined_fields,
                                    'conditional_fields' => $conditional_statements,
                                    'date' => $modified_date,
                                    'contact_group' => $contact_group_id,
                                    'lang_code' => $lang_code,
                                ));
                            }
                        }
                    }
                }
                // End of update contact layout -----------
                
            }
            //Added by BC on 20-jun-2016 for edit view-----------------
            if($module_name == 'profile'){
                $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='$sugar_crm_version' AND module_name='Contacts' and layout_view='edit' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
            } else {
                $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
            }
            $result_count = $wpdb->get_row($sql_count);
            if (empty($result_count)) {
                echo "<lable class='errors'>".$language_array['msg_set_layout']."</label>";
                wp_die();
            }
                
            $json_data = $result_count->json_data;
            $results = json_decode($json_data);
            
            /* condition login [start] */
            $sql_conditionlogic_count = "SELECT conditional_fields FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
            $result_conditionlogic_count = $wpdb->get_row($sql_conditionlogic_count);
            if (!empty($result_conditionlogic_count->conditional_fields)) {
                $conditionlogic = json_decode($result_conditionlogic_count->conditional_fields);
                $conditionlogic = (array) $conditionlogic;
            }
            /* condition login [end] */
            
            // Check permission and Get record details for edit module
            $record_detail = array();
            $getContactInfo = '';
            if ($module_name != "Contacts") {
                if (isset($id) && !empty($id)) {
                    if($module_name == 'ProjectTask') {
                        $where_con = "project_task.id = '{$id}'";
                    } else {
                        $where_con = strtolower($module_name) . ".id = '{$id}'";
                    }
                    
                    $n_array = array();
                    if(!empty($results)){
                        foreach ($results as $k => $vals_lbl) {
                            $vals_lbl = (array) $vals_lbl;
                            foreach ($vals_lbl['rows'] as $k_lbl2 => $vals) {
                                foreach ($vals as $key_fields => $val_fields) {
                                    if (!empty($val_fields->related_module)) {
                                        if ($val_fields->type == 'relate' && (!in_array($val_fields->related_module, (array) $_SESSION['module_array']))) {
                                            continue;
                                        }
                                    }
                                    
                                    $n_array[] = (isset($val_fields->name)) ? strtolower($val_fields->name) : "";
                                }
                            }
                        }
                    }
                    array_push($n_array, 'id');
                    array_push($n_array, 'currency_id'); 
                    $custom_relationships = (isset($_SESSION['scp_custom_relationships'])) ? $_SESSION['scp_custom_relationships'] : "";
                    $relation_name = ( isset( $custom_relationships[$module_name]["Contacts"] ) && $custom_relationships[$module_name]["Contacts"] != '' ) ? $custom_relationships[$module_name]["Contacts"] : strtolower( $module_name );
                    if($_SESSION['scp_user_group_type'] == "contact_based"){
                        $record_detail = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], $relation_name, $n_array, $where_con);
                    } else {
                        $record_detail = $objSCP->get_relationships('Accounts', $_SESSION['scp_account_id'], $relation_name, $n_array, $where_con);
                    }
                    
                    if ( !empty($record_detail->entry_list) && ( ( $module_name == 'Meetings' || $module_name == 'Calls' || $module_name == 'bc_proposal' ) || (!array_key_exists($module_name, $custom_relationships[$module_name]))) ) {
                        $record_detail = $objSCP->get_entry_list($module_name, $where_con);
                        if (isset($record_detail->entry_list[0]->name_value_list->portal_visible->value) && $record_detail->entry_list[0]->name_value_list->portal_visible->value != '1') {
                            wp_die();
                        }
                    } else {
                        wp_die();
                    }
                    
                    //$record_detail = $objSCP->get_entry_list($module_name, $where_con);
                } else {
                    check_permission($module_name,'create');
                    if (isset($_REQUEST['relate_to_module']) && isset($_REQUEST['relate_to_id'])) {
                        $relate_to_module = $_REQUEST['relate_to_module'];
                        $relate_to_id = $_REQUEST['relate_to_id'];
                        check_permission($relate_to_module,'view',$relate_to_id);
                    }
                }
            } else if($module_name == 'Contacts' && isset($_SESSION['scp_user_id'])) {
                $getContactInfo = $objSCP->getPortalUserInformation($_SESSION['scp_user_id']);
                if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
                    $getContactInfo = isset( $getContactInfo->entry_list[0]->name_value_list ) ? $getContactInfo->entry_list[0]->name_value_list : '';
                }
            }

            if ( ( $module_name == 'AOS_Products' || $module_name == 'bc_proposal' ) &&
                 ( ( isset( $_SESSION['scp_all_currency'] ) && empty( $_SESSION['scp_all_currency'] ) ) ||
                   ( ! isset( $_SESSION['scp_all_currency'] ) ) ) ) {

                $all_currentcy = $objSCP->portal_getCurrencylist();
                if ( isset( $all_currentcy->currencyAdded ) && ! empty( $all_currentcy->currencyAdded ) ) {
                    $_SESSION['scp_all_currency'] = $all_currentcy->currencyAdded;
                }
            }
            
            $html = "";
            $module_without_s = isset($_SESSION['module_array_without_s'][$module_name]) ? $_SESSION['module_array_without_s'][$module_name] : "";
            if ($module_name == "Notes" && (isset($casenote) && $casenote != null)) {
                $html = "";
            } else {
                $html .= "<form action='" . site_url() . "/wp-admin/admin-post.php' method='post' id='doc_submit'>
                <input type='hidden' name='action' value='bcp_get_doc_attachment'>
                <input type='hidden' name='module_name' id='scp_module_name' value='".$module_name."'>
                <input type='hidden' name='scp_doc_id' value='' id='scp_doc_id'>
                <input type='hidden' name='scp_doc_view' value='' id='scp_doc_view'>
                <input type='hidden' name='scp_doc_field_name' value='' id='scp_doc_field_name'>
                </form>";
                // note attchment
                $html .= "<form action='" . site_url() . "/wp-admin/admin-post.php' method='post' id='download_note_id'>
                <input type='hidden' name='action' value='bcp_get_note_attachment'>
                <input type='hidden' name='scp_note_field' value='' id='scp_note_field'>
                <input type='hidden' name='scp_note_id' value='' id='scp_note_id'>
                <input type='hidden' name='scp_note_view' value='' id='scp_note_view'>
                </form>";
            }
            if (!empty($results)) {
                ob_start();
                include( TEMPLATE_PATH . 'bcp_add_page_v6.php');
                $html .= ob_get_clean();
                
            } else {
                $html.= $json_data;
            }
            
            echo $html;
            wp_die();
        } else {
            $objSCP = 0;
            $conn_err = $language_array['msg_portal_is_disabled_contact_admin'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            $redirect_url = esc_url($_REQUEST['scp_current_url']) . '?conerror=1';
            wp_redirect($redirect_url);
        }
    }
}

if( ! function_exists('scp_add_moduledata_call') ) {
    add_action('wp_ajax_bcp_add_moduledata_call', 'scp_add_moduledata_call');
    add_action('wp_ajax_nopriv_bcp_add_moduledata_call', 'scp_add_moduledata_call');
    function scp_add_moduledata_call() {
        global $objSCP, $wpdb, $sugar_crm_version, $custom_relationships, $lang_code, $language_array, $relationship_fields;
        
        $custom_relationships = (isset($_SESSION['scp_custom_relationships'])) ? $_SESSION['scp_custom_relationships'] : "";
        $relationship_fields = array();
        $current_url = esc_url($_POST['scp_current_url']);
        //20-aug-2016
        $sess_id = $objSCP->session_id;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        
        if (! isset( $_POST['sfcp_portal_nonce_field'] ) || ! wp_verify_nonce( $_POST['sfcp_portal_nonce_field'], 'sfcp_portal_nonce' ) ) {
            $response['data']['redirect_url'] = "";
            $response['data']['success'] = FALSE;
            $response['data']['msg'] = __("The form has expired due to inactivity. Please try again.");
            echo json_encode($response);
            exit();
        }
        
        if ($sess_id != '') {
            if (strpos($current_url, 'detail') !== false) {
                $current_url = explode('?', $current_url, 2);
                $current_url = $current_url[0];
                $id = null;
            } else {
                $current_url = explode('?', $current_url, 2);
                $current_url = $current_url[0];
                $id = (isset($_REQUEST['id'])) ? sanitize_text_field($_REQUEST['id']) : "";
            }
            $logged_user_id = (isset($_SESSION['scp_user_id'])) ? $_SESSION['scp_user_id'] : "";
            $module_name = sanitize_text_field($_REQUEST['module_name']);
            $view = sanitize_text_field($_REQUEST['view']);
            if (isset($_SESSION['module_array']) && is_array((array) $_SESSION['module_array'])) {//Added for getting module key OR lable added from sugar side
                $arry_lable = (array) $_SESSION['module_array'];
                $module_name_label = $arry_lable[$module_name];
                $module_name_label_Cases = (isset($_REQUEST['parent_module'])) ? $arry_lable[$_REQUEST['parent_module']] : "";
            }
            $module_with_caps = (isset($_SESSION['module_array_without_s'])) ? $_SESSION['module_array_without_s'][$module_name] : "";

            if (( $module_name == 'Meetings' || $module_name == 'Calls') && array_key_exists('date_start', $_REQUEST)) {
                if ($_REQUEST['date_start'] && (isset($_REQUEST['date_end']) && $_REQUEST['date_end'])) {
                    $date1 = new DateTime($_REQUEST['date_start']);
                    $date2 = new DateTime($_REQUEST['date_end']);
                    $diff = $date2->diff($date1);
                    $hours = $diff->h;
                    $hours = $hours + ($diff->days * 24);

                    $_REQUEST['duration_hours'] = $hours;
                    $_REQUEST['duration_minutes'] = $diff->i;
                }
            }
            //get name array
            $pass_name_arry = array();
            $deleted = 0;
            if (isset($id) && !empty($id)) {
                if (isset($_REQUEST['delete']) && !empty($_REQUEST['delete']) && $_REQUEST['delete'] == 1) {
                    check_permission($module_name,'delete',$id);
                    $deleted = 1;
                }
            }
            //Added by BC on 20-jun-2016 for edit view-----------------
            $contact_group_id = isset($_SESSION['contact_group_id']) ? $_SESSION['contact_group_id'] : "";
            
            //For Feedback module to ignore get data from database. since V3.2.0
            if ($module_name != "bc_feedback" && $deleted != 1) {
                $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
                $result_count = $wpdb->get_row($sql_count) or die(mysql_error());
                $json_data = $result_count->json_data;
                $results = json_decode($json_data);
            }
            //End by BC on 20-jun-2016----------------
            include( TEMPLATE_PATH . 'bcp_add_records_v6.php');
            
            if ($module_name == "Contacts" && !isset($_SESSION['scp_user_id'])) {
                $response = scp_user_registration($pass_name_arry);
            } else {
            //For Feedback module add data. since V3.2.0
                if ($module_name == "bc_feedback") {
                    $pass_name_arry['name'] = stripslashes_deep($_REQUEST['name']);
                    $pass_name_arry['description'] = stripslashes_deep($_REQUEST['description']);
                }
                //set sentFrom parameter for distinguished call. for v3.0
                $pass_name_arry['sentFrom'] = "portal";

                // added default status as new for case
                if ($module_name == 'Cases' && $pass_name_arry['status'] == "") {
                    if ($sugar_crm_version == 6) {
                        $pass_name_arry['status'] = "New";
                    } else {
                        $pass_name_arry['status'] = "Open_New";
                    }
                }

                // added default status as Requested for call and meeting
                if ($module_name == 'Calls' || $module_name == "Meetings") {
                    $pass_name_arry['portal_visible'] = "1";
                    if ( isset($pass_name_arry['status']) && $pass_name_arry['status'] == "") {
                        $pass_name_arry['status'] = "Requested";
                    }
                }

                if (($module_name == 'Calls' || $module_name == "Meetings")) {
                    $pass_name_arry['portal_visible'] = "1";
                }

                $pass_name_arry['id'] = $id;
                $pass_name_arry['deleted'] = $deleted;

                if (($module_name == 'Meetings' || $module_name == 'Calls') && array_key_exists('date_start', $_REQUEST)) {
                    $pass_name_arry['duration_hours'] = $_REQUEST['duration_hours'];
                    $pass_name_arry['duration_minutes'] = $_REQUEST['duration_minutes'];
                    unset($pass_name_arry['date_end']);
                }

                if ($module_name == 'bc_proposal') {

                    $pass_name_arry['line_items']['product_lineitem'] = array();
                    $pass_name_arry['line_items']['service_lineitem'] = array();

                    if ( isset( $_REQUEST['selected_product_lineitems'] ) && $_REQUEST['selected_product_lineitems'] != '' ) {

                        $selected_product_lineitems = json_decode( stripslashes( $_REQUEST['selected_product_lineitems'] ) );
                        if ( $selected_product_lineitems != '' ) {

                            $selected_product_lineitems = (array)$selected_product_lineitems;
                            sort( $selected_product_lineitems );
                            foreach( $selected_product_lineitems as $key => $item ) {
                                if ( $item->product_id != '' || $item->product_qty != '' ) {
                                    $pass_name_arry['line_items']['product_lineitem'][ $key ]['id'] = $item->product_id;
                                    $pass_name_arry['line_items']['product_lineitem'][ $key ]['qty'] = $item->product_qty;
                                }
                            }
                        }
                    }
                    if ( isset( $_REQUEST['service_lineitem'] ) && ! empty( $_REQUEST['service_lineitem'] ) ) {
                        foreach( $_REQUEST['service_lineitem'] as $index => $service_lineitem ) {
                            $_REQUEST['service_lineitem'][$index]['name'] = wp_strip_all_tags($service_lineitem['name']);
                            if ( $service_lineitem['name'] == '' || $service_lineitem['name'] == NULL ) {
                                unset($_REQUEST['service_lineitem'][$index]);
                            }
                        }
                        sort( $_REQUEST['service_lineitem'] );
                        $pass_name_arry['line_items']['service_lineitem'] = $_REQUEST['service_lineitem'];
                    }
                    $pass_name_arry['line_items'] = json_encode($pass_name_arry['line_items']);
                }

                if (( $module_name == "Tasks" || $module_name == "meetings" || $module_name == "calls" ) && isset($_REQUEST['date_start']) && !isset($pass_name_arry['date_start'])) {
                    if (empty($_SESSION['user_timezone'])) {
                        $result_timezone = $objSCP->getUserTimezone();
                    } else {
                        $result_timezone = $_SESSION['user_timezone'];
                    }
                    $offset = get_timezone_offset($result_timezone, 'UTC');
                    $GetDate = gmdate("Y-m-d H:i:s", strtotime($_REQUEST["date_start"]) + $offset);
                    $pass_name_arry['date_start'] = $GetDate;
                }

                //Delete relationship with contact
                if ($deleted == 1) {
                    if ( isset( $custom_relationships[$module_name]['Contacts'] ) && $custom_relationships[$module_name]['Contacts'] != '' ) {
                        $rel_id = $objSCP->set_relationship('Contacts', $logged_user_id, $custom_relationships[$module_name]['Contacts'], array($id), $deleted);
                    } else {
                        $rel_id = $objSCP->set_relationship('Contacts', $logged_user_id, strtolower($module_name), array($id), $deleted);
                    }
                }

                if ($module_name == "Documents") {
                    if (isset($pass_name_arry['related_doc_name']) && !empty($pass_name_arry['related_doc_name'])) {//Set Related document by revision id
                        $select_field_array = array('document_revision_id');
                        $document_result = $objSCP->get_entry('Documents', $pass_name_arry['related_doc_name'], $select_field_array);
                        $revision_id = $document_result->entry_list[0]->name_value_list->document_revision_id->value;
                        $pass_name_arry['related_doc_id'] = $pass_name_arry['related_doc_name'];
                        $pass_name_arry['related_doc_rev_id'] = $revision_id;
                    }
                    if ($deleted == 1) {
                        $new_id = $objSCP->set_entry($module_name, $pass_name_arry);
                    } else {
                        // Document Set Entry
                        $upload_file = $_FILES['filename']['name'];
                        $upload_file = str_replace(' ', '_', $upload_file);
                        $upload_path = $_FILES['filename']['tmp_name'];
                        $ftype = $_FILES['filename']['type'];
                        $new_id = $objSCP->set_Document($pass_name_arry, $upload_file, $upload_path, $ftype);
                    }
                } else if ($module_name == "Notes") {
                    if ($deleted == 1) {
                        $new_id = $objSCP->set_entry($module_name, $pass_name_arry);
                    } else {
                        // File Set Entry
                        $files = $_FILES;
                        $new_id = $objSCP->createNote($pass_name_arry, $files);
                        $rel_id = $objSCP->set_relationship($_REQUEST['parent_name'], $_REQUEST['parent_id'], strtolower($module_name), array($new_id));
                       
                    }
                } else {

                    if(isset($_FILES) && ($module_name != "Cases" && !isset($_FILES['attachment']))) {

                        $file_name = "";
                        $file_path = "";
                        foreach ($_FILES as $key => $file) {
                            $file_name = $file['name'];
                            $file_path = base64_encode(file_get_contents($file['tmp_name']));
                            $pass_name_arry[$key] = $file_name;
                            $field_name = $key;
                        }
                        $new_id = $objSCP->set_entry($module_name, $pass_name_arry);
                        if($file_name ) {
                            //set image of custom field
                            $note_id = $objSCP->custom_set_image_field($field_name, $file_path, $new_id, $action = 'add', $lang_code);
                        }
                    } else {
                        $new_id = $objSCP->set_entry($module_name, $pass_name_arry);

                    }
                }

                // Add Attachement with case add 
                if ($module_name == "Cases" && isset($_FILES['attachment'])) {
                    $removedImages = json_decode(stripslashes($_REQUEST['removed_files']));
                    $attachment = $_FILES['attachment'];
                    $countAttachement = count($attachment['name']);
                    $name = $attachment['name'];
                    $type = $attachment['type'];
                    $file_path = $attachment['tmp_name'];
                    $noteParameters = array();
                    if($countAttachement>0) {
                    for ($i = 0; $i < $countAttachement; $i++) {
                        if ($name[$i] == "" || (!empty($removedImages) && in_array($name[$i], $removedImages))) {
                            continue;
                        }
                        $AttachmentData = array();
                        $fname = explode(".", $name[$i]);
                        $ftype = $type[$i];
                        $subject = $fname[0];
                        $AttachmentData['name'] = $subject;
                        $AttachmentData['filename'] = $name[$i];
                        $AttachmentData['file_mime_type'] = $ftype;
                        $AttachmentData['parent_type'] = "Cases";
                        $AttachmentData['parent_id'] = $new_id;
                        $AttachmentData['sentFrom'] = "portal";
                        $AttachmentData['deleted'] = 0;
                        $AttachmentData['file'] = base64_encode(file_get_contents($file_path[$i]));
                        $AttachmentData['revision'] = '1';
                        $AttachmentData['module'] = "Notes";
                        array_push($noteParameters, $AttachmentData);
                    }
                    $file_id = $objSCP->createAttachment($noteParameters, $new_id, $logged_user_id);
                }
                }

                //Set other field relationship if relate field exist in edit layout
                foreach ($relationship_fields as $r_key => $r_value) {
                    $f_relate_module = $r_value['relate_module'];
                    $f_relation_name = $r_value['relation_name'];
                    $f_relation_id = $r_value['relate_id'];
                    $rel_id_other = $objSCP->set_relationship($f_relate_module, $f_relation_id, $f_relation_name, array($new_id), $deleted);
                }
              
                //Set relationship with contact
                if ($deleted != 1) {
                    if($_SESSION['scp_user_group_type'] == "contact_based"){
                        if ( isset( $custom_relationships[$module_name]['Contacts'] ) && $custom_relationships[$module_name]['Contacts'] != '' ) {
                            $rel_id = $objSCP->set_relationship('Contacts', $logged_user_id, $custom_relationships[$module_name]['Contacts'], array($new_id), $deleted);
                        } else {
                            if(array_key_exists($module_name, $custom_relationships[$module_name])) {
                                $rel_id = $objSCP->set_relationship('Contacts', $logged_user_id, strtolower($module_name), array($new_id), $deleted);
                            }
                        }
                    }else {
                        if($module_name == "Accounts" || $module_name == "Notes" || $module_name == "Calls" || $module_name == "Meetings" || $module_name == "bc_proposal" || $module_name == "bc_feedback"){
                            if ( isset( $custom_relationships[$module_name]['Contacts'] ) && $custom_relationships[$module_name]['Contacts'] != '' ) {
                                $rel_id = $objSCP->set_relationship('Contacts', $logged_user_id, $custom_relationships[$module_name]['Contacts'], array($new_id), $deleted);
                            } else {
                                if(array_key_exists($module_name, $custom_relationships[$module_name])) {
                                    $rel_id = $objSCP->set_relationship('Contacts', $logged_user_id, strtolower($module_name), array($new_id), $deleted);
                                }
                            }
                        }else {
                            if ( isset( $custom_relationships[$module_name]['Contacts'] ) && $custom_relationships[$module_name]['Contacts'] != '' ) {
                                $rel_id = $objSCP->set_relationship('Accounts', $_SESSION['scp_account_id'], $custom_relationships[$module_name]['Contacts'], array($new_id), $deleted);

                            } else {
                                if(array_key_exists($module_name, $custom_relationships[$module_name])) {
                                    $rel_id = $objSCP->set_relationship('Accounts', $_SESSION['scp_account_id'], strtolower($module_name), array($new_id), $deleted);
                                }
                            }
                        }
                    }
                }

                if (isset($rel_id->failed) && $rel_id->failed == true) {
                    $view1 = 'list';
                    $redirect_url = $current_url . '?' . $view1 . '-' . $module_name . '';
                    $conn_err = __("Counld not set relation. Please contact to administrator");
                    $_SESSION['bcp_add_error'] = $conn_err;
                } else {
                    //Set relationship with related module for v3.0
                    if (isset($_REQUEST['relate_to_id']) && isset($_REQUEST['relate_to_module']) && isset($_REQUEST['relationship'])) {
                        $relate_to_module = $_REQUEST['relate_to_module'];
                        $relate_to_id = $_REQUEST['relate_to_id'];
                        $relationship = $_REQUEST['relationship'];
                        $rel_id = $objSCP->set_relationship($relate_to_module, $relate_to_id, $relationship, array($new_id), $deleted);
                    } else {
                        if (isset($_REQUEST['relate_to_id']) && isset($_REQUEST['relate_to_module'])) {
                            $relate_to_module = $_REQUEST['relate_to_module'];
                            $relate_to_id = $_REQUEST['relate_to_id'];
                            $rel_id = $objSCP->set_relationship($relate_to_module, $relate_to_id, strtolower($module_name), array($new_id), $deleted);
                        }
                    }

                    if( ( $module_name == 'Meetings' || $module_name == 'Calls' ) && $deleted != 1) {
                        $rel_id2 = $objSCP->set_relationship($module_name, $new_id, 'users');
                    }
                    if (isset($id) && !empty($id)) {
                        if (isset($_REQUEST ['delete']) && !empty($_REQUEST ['delete']) && $_REQUEST['delete'] == 1) {
                            $view1 = 'list';
                            $redirectURL_manage = get_page_link(get_option('biztech_redirect_manange'));
                            $redirect_url = $redirectURL_manage . '/#data/'.$module_name . '/' .$view1;
                            $conn_err = $module_with_caps . " " . ( isset($_SESSION['portal_message_list']->delete_success) ? __( $_SESSION['portal_message_list']->delete_success ) : '' );
                            $_SESSION['bcp_add_record'] = $conn_err;
                            wp_redirect($redirect_url);
                            exit;
                        } else {
                            $view1 = 'list';
                            $redirect_url = $current_url . '?' . $view1 . '-' . $module_name . '';
                            $conn_err = $module_with_caps . " " . (isset($_SESSION['portal_message_list']->update_success) ? __( $_SESSION['portal_message_list']->update_success ) : '');
                            $_SESSION['bcp_add_record'] = $conn_err;
                        }
                    } else {
                        //For Feedback module to redirect to same url. since V3.2.0
                        if ($module_name == "bc_feedback") {
                            $module_with_caps = "Feedback";
                        }
                        if ($module_name == "Notes" && !empty($_REQUEST['case_id'])) {
                            $view1 = 'list';
                            $module_name1 = $_REQUEST['parent_module'];
                            $redirect_url = $current_url . '?' . $view1 . '-' . $module_name1 . '';
                            $conn_err = $module_with_caps . " " . (isset($_SESSION['portal_message_list']->added_success_in) ? __( $_SESSION['portal_message_list']->added_success_in ) : '') . " " . $_SESSION['module_array_without_s'][$module_name_label_Cases];
                            $_SESSION['bcp_add_record'] = $conn_err;
                        } else {
                            $view1 = 'list';
                            $redirect_url = $current_url . '?' . $view1 . '-' . $module_name . '';
                            if ($module_name == "bc_feedback") {
                                $conn_err = $module_with_caps . " " . (isset($_SESSION['portal_message_list']->submit_success) ? __( $_SESSION['portal_message_list']->submit_success ) : '');
                            } else {
                                $conn_err = $module_with_caps . " " . (isset($_SESSION['portal_message_list']->added_success) ? __( $_SESSION['portal_message_list']->added_success ) : '');
                            }
                            $_SESSION['bcp_add_record'] = $conn_err;
                        }
                    }
                }
            }
        } else {
            $objSCP = 0;
            $conn_err = $language_array['msg_portal_is_disabled_contact_admin'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            $redirect_url = $current_url . '?conerror=1';
            wp_redirect($redirect_url);
            exit;
        }

        if ($module_name != "Contacts" && isset($_SESSION['scp_user_id'])) {
            $response['success'] = true;
            $response['data'] = array(
                'redirect' => 1,
                'module_name' => $module_name,
                'module_action' => 'edit',
                'redirect_url' => $redirect_url,
            );
            $response['data']['msg_show'] = $conn_err;
        }
        
        //set id and module name for redirect to relate module for v3.0-----------
        if (isset($_REQUEST['relate_to_id']) && isset($_REQUEST['relate_to_module'])) {
            $response['data']['relate_to_module_name'] = $_REQUEST['relate_to_module'];
            $response['data']['id'] = $_REQUEST['relate_to_id'];
        }
        if (isset($_REQUEST['relationship'])) {
            $response['data']['relationship'] = $_REQUEST['relationship'];
        }
        //------------------------------------------------------------------------

        
        if(isset($_REQUEST['case_id']) && $_REQUEST['case_id']!=null){  //If added from Case Detail Page
             $response['data']['redirect'] = 0;
             $response['data']['case_id'] = $_REQUEST['case_id'];
             $response['data']['parent_module'] = $_REQUEST['parent_module'];
        }
        if(isset($_REQUEST['post_date']) && $_REQUEST['post_date']!=null){  //If added from calendar
            $response['data']['redirect'] = 'calendar';
        }
        echo json_encode($response);
        wp_die();
    }
}

if( ! function_exists('scp_quote_accept_decline_btn') ) {
    add_action('wp_ajax_bcp_quote_accept_decline_btn', 'scp_quote_accept_decline_btn');
    add_action('wp_ajax_nopriv_bcp_quote_accept_decline_btn', 'scp_quote_accept_decline_btn');
    function scp_quote_accept_decline_btn() {

        global $objSCP, $sugar_crm_version, $lang_code, $language_array;
        $sess_id = $objSCP->session_id;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        $current_url = esc_url($_REQUEST['current_url']);
        if ($sess_id != '') {
            $module_name = sanitize_text_field($_REQUEST['modulename']);
            $pass_name_arry['id'] = $_REQUEST['id'];
            $pass_name_arry['stage'] = (($_REQUEST['action_performed'] == 'Accept') ? 'Closed Accepted' : 'Closed Lost');
            $data = $objSCP->set_entry($module_name, $pass_name_arry);
            if( $data == $pass_name_arry['id'] ){

                if ( $_REQUEST['action_performed'] == 'Decline' ) {
                    $objSCP->addQuoteDeclineReasonValue($_REQUEST['id'], $_REQUEST['decline-reson']);
                }

                $conn_err = __('Your') . ' "' . $_REQUEST['subject'] . '" ' . __('status changed to') . ' "' . $pass_name_arry['stage'] . '" ' . __('Successfully.');
                $_SESSION['bcp_add_record'] = $conn_err;
                if($_REQUEST['action_performed'] == 'Accept'){
                    $_SESSION['bcp_accept_quote'] = $conn_err;
                } else {
                    $_SESSION['bcp_decline_quote'] = $conn_err;
                }
                $response['data']['success'] = 1;
            }
            else {
                $conn_err = __('Your') . ' "' . $_REQUEST['subject'] . '" ' . __("can't") . ' "' . $pass_name_arry['stage'] . '" ' . __('successfully. Please try again');
                $_SESSION['bcp_connection_error'] = $conn_err;
                $response['data']['success'] = 0;
            }
            $response['data']['msg_show'] = $conn_err;
            echo json_encode($response);
            wp_die();
        } else {
            $objSCP = 0;
            $conn_err = $language_array['msg_portal_is_disabled_contact_admin'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            $redirect_url = $current_url . '?conerror=1';
            wp_redirect($redirect_url);
            exit;
        }

    }
}

if( ! function_exists('scp_calendar_display') ) {
    add_action('wp_ajax_bcp_calendar_display', 'scp_calendar_display');
    add_action('wp_ajax_nopriv_bcp_calendar_display', 'scp_calendar_display');
    function scp_calendar_display() {

        global $objSCP, $sugar_crm_version, $lang_code, $language_array;
        //20-aug-2016
        $sess_id = $objSCP->session_id;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        if ($sess_id != '') {
            include( TEMPLATE_PATH . 'bcp_view_calendar.php');
            wp_die();
        } else {
            $objSCP = 0;
            $conn_err = $language_array['msg_portal_is_disabled_contact_admin'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            echo "-1";
            wp_die();
        }
    }
}

if( ! function_exists('scp_case_updates') ) {
    add_action('wp_ajax_prefix_admin_bcp_case_updates', 'scp_case_updates');
    add_action('wp_ajax_nopriv_prefix_admin_bcp_case_updates', 'scp_case_updates');
    function scp_case_updates() {

        global $objSCP, $sugar_crm_version, $lang_code, $language_array;
        //20-aug-2016
        $sess_id = $objSCP->session_id;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        if ($sess_id != '') {
            $html = '';
            if (empty($_SESSION['user_timezone'])) {
                $result_timezone = $objSCP->getUserTimezone();
            } else {
                $result_timezone = $_SESSION['user_timezone'];
            }
            date_default_timezone_set($result_timezone);
            $result_timezone = date_default_timezone_get();

            $pass_name_arry = array();
            $id = sanitize_text_field($_REQUEST['id']);
            $update_text = nl2br($_REQUEST['update_text']);
            if ($sugar_crm_version == 6) {
                $update_text = $_REQUEST['update_text'];    
            }
            $pass_name_arry['name'] = $update_text;
            $pass_name_arry['description'] = $update_text;
            $pass_name_arry['internal'] = 0;
            $pass_name_arry['assigned_user_id'] = 1;
            $pass_name_arry['contact_id'] = $_SESSION['scp_user_id'];
            $logged_user_id = $_SESSION['scp_user_id'];
            date_default_timezone_set('UTC');
            $pass_name_arry['date_entered'] = date('Y-m-d H:i:s');
            date_default_timezone_set($result_timezone);

            if ($sugar_crm_version == 6) {//custom case updates call for sugar 6
                $pass_name_arry['case_id'] = $id;
                $new_id = $objSCP->set_entry('bc_case_updates', $pass_name_arry);
                $objSCP->set_relationship('Cases', $id, "bc_case_updates_cases", array($new_id));
                $attachment = $_FILES['commentfile'];
                $countAttachement = count($attachment['name']);
                if ($countAttachement > 0) {
                    $name = $attachment['name'];
                    $type = $attachment['type'];
                    $file_path = $attachment['tmp_name'];
                    $note_parameters = array();
                    for ($i = 0; $i < $countAttachement; $i++) {
                        if ($name[$i] == "") {
                            continue;
                        }
                        $AttachmentData = array();
                        $fname = explode(".", $name[$i]);
                        $subject = $fname[0];
                        $ftype = $type[$i];
                        $AttachmentData['name'] = $subject;
                        $AttachmentData['file_mime_type'] = $ftype;
                        $AttachmentData['parent_type'] = "bc_case_updates";
                        $AttachmentData['parent_id'] = $new_id;
                        $AttachmentData['sentFrom'] = "portal";
                        $AttachmentData['deleted'] = 0;
                        $AttachmentData['file'] = base64_encode(file_get_contents($file_path[$i]));
                        $AttachmentData['filename'] = $name[$i];
                        $AttachmentData['revision'] = '1';
                        $AttachmentData['module'] = "Notes";
                        array_push($note_parameters, $AttachmentData);
                    }
                    $file_id = $objSCP->createAttachment($note_parameters, "", $logged_user_id);
                }
            }
            if ($sugar_crm_version == 5) {
                $new_id = $objSCP->set_entry('AOP_Case_Updates', $pass_name_arry);
                $objSCP->set_relationship('Cases', $id, "aop_case_updates", array($new_id));
                $attachment = $_FILES['commentfile'];
                $countAttachement = count($attachment['name']);
                if ($countAttachement > 0) {
                    $name = $attachment['name'];
                    $type = $attachment['type'];
                    $file_path = $attachment['tmp_name'];
                    $note_parameters = array();
                    for ($i = 0; $i < $countAttachement; $i++) {
                        if ($name[$i] == "") {
                            continue;
                        }
                        $AttachmentData = array();
                        $fname = explode(".", $name[$i]);
                        $subject = $fname[0];
                        $ftype = $type[$i];
                        $AttachmentData['name'] = $subject;
                        $AttachmentData['file_mime_type'] = $ftype;
                        $AttachmentData['parent_type'] = "AOP_Case_Updates";
                        $AttachmentData['parent_id'] = $new_id;
                        $AttachmentData['sentFrom'] = "portal";
                        $AttachmentData['deleted'] = 0;
                        $AttachmentData['file'] = base64_encode(file_get_contents($file_path[$i]));
                        $AttachmentData['filename'] = $name[$i];
                        $AttachmentData['revision'] = '1';
                        $AttachmentData['module'] = "Notes";
                        array_push($note_parameters, $AttachmentData);
                    }
                    $file_id = $objSCP->createAttachment($note_parameters, "", $logged_user_id);
                }
            }
            include( TEMPLATE_PATH . 'bcp_case_updates_v6.php');
            echo $html;
            wp_die();
        } else {
            $objSCP = 0;
            $conn_err = $language_array['msg_portal_is_disabled_contact_admin'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            echo "-1";
            wp_die();
        }
    }
}

if( ! function_exists('scp_global_search') ) {
    add_action('wp_ajax_bcp_global_search', 'scp_global_search');
    add_action('wp_ajax_nopriv_bcp_global_search', 'scp_global_search');
    function scp_global_search () {

        global $objSCP, $wpdb, $sugar_crm_version, $lang_code, $language_array, $custom_relationships;
        $custom_relationships = $_SESSION['scp_custom_relationships'];
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $contact_group_id = isset($_SESSION['contact_group_id']) ? $_SESSION['contact_group_id'] : "";
        $language_array = scp_get_language_data($lang_code);
        $search_string = htmlentities( urldecode( $_REQUEST['view'] ) );
        $search_string = stripslashes($search_string);
        $search_relation_name = array();
        foreach ( $_SESSION['module_array'] as $i=>$j ) {
                $search_module[] = $i;
                $search_relation_name[$i] = isset($custom_relationships[$i]["Contacts"]) ? $custom_relationships[$i]["Contacts"] : strtolower($i);
        }
        
        $max = fetch_data_option('biztech_scp_case_per_page');

        $search_data = $objSCP->globalSearch( $_SESSION['scp_user_id'], $search_module, $search_string, $max, $lang_code, $contact_group_id, $search_relation_name );
        
        $html = '<div class="search_result_content case-listing-block bg-white shadow">';
        $html .= '<h3>' . $language_array['lbl_search_result_for'] .' "'. str_replace('%', ' ', $search_string) . '"</h3>';
        if ( isset($search_data->msg) && $search_data->msg != '' ) {
            $html .= '<h4>' . $search_data->msg . '</h4>';
        } else {
            $search_data = (array) $search_data;
            if ( isset($search_data) && ! empty( $search_data ) ) {
                $search_count = count($search_data);
                foreach ( $search_data as $i=>$j ) {
                    if (!empty($j->entry_list)) {
                        if ($search_count == 1 && isset($search_data->AOK_KnowledgeBase->entry_list) && $search_data->AOK_KnowledgeBase->entry_list == null) {
                            $html .= '<h4>' . $language_array['msg_no_records'] . '</h4>';
                        } else {
                            $html .= '<h4 class="scp-' . $i . '-font scp-default-font">' . $_SESSION['module_array'][$i] . '</h4>';
                            $html .= '<table>';
                            if ( $i == 'AOK_KnowledgeBase' ) {
                                foreach ( $j->entry_list as $value ) {
                                    $kb_cat_name = (isset($value->kb_categories) ? $value->kb_categories : array($language_array['lbl_uncategorized']));
                                    $kbcat_name = implode(", ", $kb_cat_name);
                                    $html .= '<tr>
                                        <td class="scp-knowledgebase-wrapper">
                                            <div class="detail-part">
                                                <div class="question">
                                                    <h4 title="'.$language_array['lbl_click_here'].'"><i class="fa fa-plus"></i>' . $value->name->value . '</h4>
                                                    <p><i class="fa fa-key"></i><span class="scp_kb_cat">' . $kbcat_name .'</span> - '. date('M d, Y', strtotime($value->date_entered->value)) . '</p>
                                                </div>
                                                <div class="answer" style="display:none;">' . html_entity_decode( $value->description->value ) . '</div>
                                            </div>
                                        </td></tr>';
                                }
                            } else {
                                foreach ( $j->entry_list as $k => $value ) {
                                    if($k > ($max-1)) {
                                        break;
                                    }
                                    $html .= '<tr><td><a href="#data/' . $i . '/detail/' . $value->name_value_list->id->value . '/">' . $value->name_value_list->name->value . '</a></td></tr>';
                                }
                            }

                            $countdata = count($j->entry_list);
                             if ( $countdata > ($max-1) ) {
                                $html .= '<tr><td><a class="more" href="#data/' . $i . '/list/' . $search_string . '">' . $language_array['lbl_view_more'] . '</a></td></tr>';
                            }
                            $html .= '</table>';
                        }
                    }
                }
            } else {
                $html .= '<h4>' . ( isset($_SESSION['portal_message_list']->search_error) ? __( $_SESSION['portal_message_list']->search_error ) : '' ); '</h4>';
            }
        }
        $html .= "<script>jQuery(document).ready(function(){
                jQuery('.detail-part .question').on('click', function() {
                    if(!jQuery(this).hasClass('active')) {
                        jQuery('.detail-part .answer').slideUp();
                        jQuery('.detail-part .question').removeClass('active');
                        jQuery(this).find('h4').attr('title','".$language_array['lbl_click_here_to_hide']."');
                        jQuery(this).addClass('active');
                        jQuery(this).parent('.detail-part').find('.answer').slideDown();
                    } else{
                        jQuery(this).find('h4').attr('title','".$language_array['lbl_click_here']."');
                        jQuery(this).parent('.detail-part').find('.answer').slideUp();
                        jQuery(this).parent('.detail-part').find('.question').removeClass('active');
                    }
                });
            });</script>";
        $html .= '</div>';
        ob_get_clean();
        echo $html;
        wp_die();
    }
}

if( ! function_exists('scp_product_list_module') ) {
    add_action('wp_ajax_bcp_product_list_module', 'scp_product_list_module');
    add_action('wp_ajax_nopriv_bcp_product_list_module', 'scp_product_list_module');
    function scp_product_list_module(){

        global $objSCP, $wpdb, $sugar_crm_version, $custom_relationships, $lang_code, $language_array,$module_icon_content;
        $custom_relationships = $_SESSION['scp_custom_relationships'];
        $sess_id = isset( $objSCP->session_id ) && $objSCP->session_id != '' ? $objSCP->session_id : '';
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $request_type = (isset($_POST['request_type'])) ? $_POST['request_type'] : "";
        $order_by = isset($_POST['order_by']) ? trim( sanitize_text_field( $_POST['order_by'] )) : "";
        $order = isset($_POST['order']) ? trim( sanitize_text_field( $_POST['order'] )) : "";
        $language_array = scp_get_language_data($lang_code);

        if ($sess_id != '') {

            $module_name = $_REQUEST['modulename'];
            $view = 'list';
            $limit = fetch_data_option('biztech_scp_case_per_page');
            $page_no = intval($_POST['page_no']);
            $offset = ($page_no * $limit) - $limit;
            $offset = ( $offset < 0 ) ? '0' : $offset ;
            //Updated by BC on 20-jun-2016 for timezone store in session
            if (empty($_SESSION['user_timezone'])) {
                $result_timezone = $objSCP->getUserTimezone();
            } else {
                $result_timezone = $_SESSION['user_timezone'];
            }
            date_default_timezone_set($result_timezone);
            
            $order_by_query = "date_entered desc";
            if (!empty($order_by) && !empty($order)) {
                $order_by_query = "$order_by $order";
            }

            $search_name = null;
            if ( array_key_exists('search_product', $_POST) ) {
                $search_name = sanitize_text_field( str_replace( '%20', ' ', $_POST['search_product'] ) );
            }

            $where_con = 'visible_portal = 1';
            if (isset($search_name) && !empty($search_name)) {
                $where_con .= ' AND ' . strtolower($module_name) . ".name like '%{$search_name}%'";
            }

            if ( isset( $_REQUEST['selected_product_lineitems'] ) && $_REQUEST['selected_product_lineitems'] != '' ) {

                $selected_product_lineitems = json_decode( stripslashes( $_REQUEST['selected_product_lineitems'] ) );
                
                if ( $selected_product_lineitems != '' && $request_type != 'reset') {

                    $product_array = array();
                    foreach ($selected_product_lineitems as $value) {
                        array_push($product_array, $value->product_id);
                    }
                    if ( isset( $_REQUEST['request_type'] ) && $search_name == '' ) {
                        $where_con .= ' AND '. strtolower($module_name) . ".id NOT IN ('" . implode("','", $product_array). "')";
                    }
                }
            }

            $contact_group_id = isset($_SESSION['contact_group_id']) ? $_SESSION['contact_group_id'] : "";
            $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
            $result_count = $wpdb->get_row($sql_count) or die(mysql_error());

            $json_data = $result_count->json_data;
            $results = json_decode($json_data);
            //End by BC on 20-jun-2016-----------------
            if (!empty($results)) {

                foreach ($results as $key_n => $val_n) {
                    if (!empty($val_n->related_module)) {
                        if ($val_n->type == 'relate' && (!in_array($val_n->related_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array
                            continue;
                        }
                    }
                    $n_array[] = strtolower($key_n);
                }
                if (!in_array('id', $n_array)) {
                    array_push($n_array, 'id');
                }
                array_push($n_array, 'currency_id'); //Added by BC on 02-jul-2016 for getting currency id

                $pos = array_search('product_image', $n_array);
                if ( $pos != FALSE ) unset($n_array[$pos]);

                $list_result = $objSCP->get_entry_list($module_name, $where_con, $n_array, $order_by_query, 0, $limit, $offset);

                if ( ( isset( $_SESSION['scp_all_currency'] ) && empty( $_SESSION['scp_all_currency'] ) ) ||
                     ( ! isset( $_SESSION['scp_all_currency'] ) ) ) {

                    $all_currentcy = $objSCP->portal_getCurrencylist();
                    if ( isset( $all_currentcy->currencyAdded ) && ! empty( $all_currentcy->currencyAdded ) ) {
                        $_SESSION['scp_all_currency'] = $all_currentcy->currencyAdded;
                    }
                }
                $cnt = count($list_result->entry_list);
                $view = 'edit';

                if ($cnt > 0) {
                    include( TEMPLATE_PATH . 'bcp_product_list_page.php');
                    $html .= "<input type='hidden' id='record_status' value='1'>";
                } else {
                    $html .= "<strong>" . ( isset( $_SESSION['portal_message_list']->module_no_records ) ? __( $_SESSION['portal_message_list']->module_no_records ) : '' ) . "</strong>";
                    $html .="</div>";
                }
            }
            echo $html;
            wp_die();
        } else {
            $objSCP = 0;
            $conn_err = $language_array['msg_portal_is_disabled_contact_admin'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            echo "-1";
            wp_die();
        }
    }
}


if (!function_exists("scp_remove_document")) {
    add_action('wp_ajax_scp_remove_document', 'scp_remove_document');
    add_action('wp_ajax_nopriv_scp_remove_document', 'scp_remove_document');
    
    function scp_remove_document() {
        global $objSCP;
        
        $field_name = isset($_REQUEST['field_name']) ? sanitize_text_field($_REQUEST['field_name']) : 'photo';
        $id = isset($_REQUEST['id']) ? sanitize_text_field($_REQUEST['id']) : '';
        $module_name = sanitize_text_field($_REQUEST['module_name']);
        
        $test = $objSCP->delete_document_revision($id);
        echo '1';
        wp_die();
    }
}

/* custom portal menu changes */
if (!function_exists("scp_register_portal_menu")) {
    add_action( 'init', 'scp_register_portal_menu' );
    function scp_register_portal_menu() {
        register_nav_menu('portal-menu',__( 'Portal Menu' ));
        
        $locations = get_nav_menu_locations();
        $es_modules_label = get_option('es_modules_label');
        $scp_menu_modules_label = get_option("scp_menu_modules_label");
        
        if(!empty($locations) && array_key_exists( 'portal-menu', $locations ) && !empty($es_modules_label)){
            $location = $locations['portal-menu'];
            $menu = wp_get_nav_menu_object($location);
            $menuitems = wp_get_nav_menu_items( $menu->term_id);
            if(!empty($menuitems)){
                $scp_custom_menu_array = array();
                foreach ($menuitems as $key => $menuitem){
                    $scp_custom_menu_array[$key]['name'] = $menuitem->title;
                    $scp_custom_menu_array[$key]['type'] = $menuitem->type;
                    $scp_custom_menu_array[$key]['target'] = $menuitem->target;
                    $scp_custom_menu_array[$key]['slug'] = str_replace(" ", "_", $menuitem->title);
                    if($menuitem->type == 'post_type'){
                        $scp_custom_menu_array[$key]['url'] = $menuitem->object_id;
                    } else if($menuitem->type == 'custom'){
                        $scp_custom_menu_array[$key]['url'] = $menuitem->url;
                    }
                }
                update_option('scp_custom_menu',$scp_custom_menu_array);
                $scp_custom_menu = get_option("scp_custom_menu");
                if(!empty($scp_custom_menu)){
                    foreach($scp_custom_menu as $key => $value){
                        $name = str_replace(" ", "_", $value['name']);
                        if ( ! array_key_exists( $name, $es_modules_label ) ) {
                            $es_modules_label[$name] = $value['name'];
                        }
                        update_option( 'es_modules_label',  $es_modules_label );
                    }
                }
                if(!empty($es_modules_label)){
                    foreach($es_modules_label as $key => $value){
                        $separator = substr_compare($key, 'separator',0,9);
                        if($separator != 0 && ! array_key_exists( $key, $scp_menu_modules_label ) && !in_array( $key, array_column($scp_custom_menu, 'slug') )){
                            unset($es_modules_label[$key]);
                        }
                    }
                    update_option( 'es_modules_label',  $es_modules_label );
                }
            }
        } else {
            update_option("scp_custom_menu", "");
            $scp_custom_menu = get_option("scp_custom_menu");
            if(!empty($es_modules_label)){
                $esmodulesLabels = array();
                $j = 0;
                foreach($es_modules_label as $key => $value){
                    $separator = substr_compare($key, 'separator',0,9);
                    if($separator == 0){
                        $ke = 'separator-'.$j;
                        $esmodulesLabels[$ke] = $j;
                        $j++;
                    } else if ( array_key_exists( $key, $scp_menu_modules_label ) ) {
                        $esmodulesLabels[$key] = $value;
                    }
                }
                update_option( 'es_modules_label',  $esmodulesLabels );
            }
        }
    }
}

/* show hide fields based on condition */
if (!function_exists("bcp_show_hide_fields")) {
    add_action('wp_ajax_bcp_show_hide_fields', 'bcp_show_hide_fields');
    add_action('wp_ajax_nopriv_bcp_show_hide_fields', 'bcp_show_hide_fields');
    
    function bcp_show_hide_fields() {
        global $wpdb, $lang_code, $sugar_crm_version;
        
        $field_name = isset($_POST['fieldname']) ? $_POST['fieldname'] : '';
        $module_name = isset($_POST['module_name']) ? $_POST['module_name'] : '';
        $view = isset($_POST['view']) ? $_POST['view'] : '';
        $selectvalue = isset($_POST['selectvalue']) ? $_POST['selectvalue'] : '';
        $fieldName = str_replace('[]', '', $field_name);
        
        $contact_group_id = isset($_SESSION['contact_group_id']) ? $_SESSION['contact_group_id'] : "";
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        
        $sql_conditionlogic_count = "SELECT conditional_fields FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
        $result_conditionlogic_count = $wpdb->get_row($sql_conditionlogic_count);
        if (!empty($result_conditionlogic_count->conditional_fields)) {
            $conditionlogic = json_decode($result_conditionlogic_count->conditional_fields);
            $conditionlogic = (array) $conditionlogic;
        }
        
        $i = 0;
        if(!empty($conditionlogic)){
            foreach ($conditionlogic as $key => $val){
                if($key == 'modified_date'){
                    continue;
                }
                if($val->origin_field_selection == $fieldName && $val->matching_selection == 'Any'){
                    if(is_array($selectvalue) && count(array_intersect($selectvalue, $val->matching_value)) >= 1 ){
                        if($val->visibility == 0){
                            $response[$i]['FieldName'] = $key;
                            $response[$i]['Visibility'] = 'show';
                            $response[$i]['portal_required'] = $val->portal_required;
                        } else {
                            $response[$i]['FieldName'] = $key;
                            $response[$i]['Visibility'] = 'hide';
                            $response[$i]['portal_required'] = $val->portal_required;
                        }
                    } else if(in_array($selectvalue, $val->matching_value)){
                        if($val->visibility == 0){
                            $response[$i]['FieldName'] = $key;
                            $response[$i]['Visibility'] = 'show';
                            $response[$i]['portal_required'] = $val->portal_required;
                        } else {
                            $response[$i]['FieldName'] = $key;
                            $response[$i]['Visibility'] = 'hide';
                            $response[$i]['portal_required'] = $val->portal_required;
                        }
                    } else {
                        if($val->visibility == 0){
                            $response[$i]['FieldName'] = $key;
                            $response[$i]['Visibility'] = 'hide';
                            $response[$i]['portal_required'] = $val->portal_required;
                        } else {
                            $response[$i]['FieldName'] = $key;
                            $response[$i]['Visibility'] = 'show';
                            $response[$i]['portal_required'] = $val->portal_required;
                        }
                    }
                } else if($val->origin_field_selection == $fieldName && $val->matching_selection == 'All'){
                    if(is_array($selectvalue) && (count(array_intersect($selectvalue,$val->matching_value)) == count($val->matching_value))){
                        if($val->visibility == 0){
                            $response[$i]['FieldName'] = $key;
                            $response[$i]['Visibility'] = 'show';
                            $response[$i]['portal_required'] = $val->portal_required;
                        } else {
                            $response[$i]['FieldName'] = $key;
                            $response[$i]['Visibility'] = 'hide';
                            $response[$i]['portal_required'] = $val->portal_required;
                        }
                    } else {
                        if($val->visibility == 0){
                            $response[$i]['FieldName'] = $key;
                            $response[$i]['Visibility'] = 'hide';
                            $response[$i]['portal_required'] = $val->portal_required;
                        } else {
                            $response[$i]['FieldName'] = $key;
                            $response[$i]['Visibility'] = 'show';
                            $response[$i]['portal_required'] = $val->portal_required;
                        }
                    }
                }
                $i++;
            }
        }
        echo json_encode($response);
        wp_die();
    }
}


/*unlock login user */
if (!function_exists("bcp_unlock_login_user")) {
    add_action('wp_ajax_bcp_unlock_login_user', 'bcp_unlock_login_user');
    add_action('wp_ajax_nopriv_bcp_unlock_login_user', 'bcp_unlock_login_user');
    
    function bcp_unlock_login_user() {
        global $wpdb, $objSCP;
        
        $ip = bcp_getUserIpAddr();
        $record = $wpdb->get_row("SELECT * FROM " . $wpdb->base_prefix . "bcp_login_attempts WHERE ip_address='{$ip}'");
        
        $wpdb->update(
                $wpdb->base_prefix . 'bcp_login_attempts', array(
            'ip_address' => $ip,
            'login_attempt' => '0',
            'last_login_date' => date("Y-m-d H:i:s"),
            'bcp_user_login' => $record->bcp_user_login,
            'ip_status' => "active",
                ), array('id' => $record->id)
        );

        
        $objSCP->getPortalUserExists($record->bcp_user_login, 2);
        
        
        echo 1;
        wp_die();
    }
}
<table class='form-table'>
    <tr class="hide_class">
        <th scope='row'><label><?php _e('Records Per Page'); ?></label></th>
        <td>
            <input type="number" class="small-text" value="<?php echo $crmcaseperpage; ?>" min="1" step="1" name="biztech_scp_case_per_page">
            <p><strong><?php _e('Note: '); ?></strong><?php _e('Records Per Page setting will be applied to the listing page of modules and sub-panels in portal.'); ?></p>
        </td>
    </tr>
    <!--//Added by BC on 10-jun-2015-->
    <tr class="hide_class">
        <th scope="row"><label><?php _e('Portal Logo'); ?></label></th>
        <td>
            <label for="upload_image">

                <p class="sfcp-logo-img">
                    <?php
                    $crmlogo = get_option('biztech_scp_upload_image');

                    if ($crmlogo != null) {
                        $sfcp_logo_url = wp_get_attachment_image_src($crmlogo);
                        $sfcp_logo_url = $sfcp_logo_url[0];
                        if ($sfcp_logo_url) {
                            ?>
                            <img src="<?php echo $sfcp_logo_url; ?>" alt="<?php echo get_option('sfcp_name'); ?>" />
                            <?php
                        }
                    }
                    ?>
                </p>
                <p class="sfcp-logo-section">
                    <button type="button" class="button remove-sfcp-logo"<?php echo (!$crmlogo ? ' style="display: none;"' : '' ); ?>><?php _e('Remove Media'); ?></button>
                    <button type="button" class="button sfcp-logo"<?php echo ( $crmlogo ? ' style="display: none;"' : '' ); ?>><?php _e('Add Media'); ?></button>
                    <input id="sfcp_logo" type="hidden" name="biztech_scp_upload_image" value="<?php echo $crmlogo; ?>" />
                </p>

            </label>
            <script type="text/javascript">
                jQuery(function ($) {
                    var frame;

                    $('.sfcp-logo').on('click', function (event) {
                        event.preventDefault();
                        if (frame) {
                            frame.open();
                            return;
                        }

                        frame = wp.media({
                            library: {
                                type: 'image'
                            }
                        });

                        frame.on('select', function () {
                            var attachment = frame.state().get('selection').first().toJSON();
                            $('#sfcp_logo').val(attachment.id);
                            $('.sfcp-logo-img ').html('<img src="' + attachment.sizes.thumbnail.url + '" alt="<?php echo get_option('sfcp_name'); ?>" />');
                            $('.remove-sfcp-logo').show();
                            $('.sfcp-logo').hide();
                        });

                        frame.open();
                    });

                    $('.remove-sfcp-logo').on('click', function () {
                        $('.sfcp-logo-img img').remove();
                        $('#sfcp_logo').val('');
                        $('.remove-sfcp-logo').hide();
                        $('.sfcp-logo').show();
                    });
                });
            </script>
        </td>
    </tr>
    <tr class="hide_class">
        <th scope='row'><label><?php _e('Mobile Portal Menu Title'); ?></label></th>
        <td><input type="text" value="<?php echo $crmmobileportal; ?>" name="biztech_scp_portal_menu_title" /></td>
    </tr>

    <tr class="hide_class">
        <th scope='row'><label for="biztech_portal_enable_user_verification"><?php _e('Enable User Verification') ?></label></th>
        <td>
            <input type="checkbox" name="biztech_portal_enable_user_verification" id="biztech_portal_enable_user_verification" value="1" <?php echo ($biztech_portal_enable_user_verification != "") ? "checked" : ""; ?> >
            <?php _e("This feature will enable user to verify from portal."); ?>
        </td>
    </tr>
    <tr class="hide_class">
        <th scope='row'><label for="biztech_portal_user_approval"><?php _e('Enable User Approval') ?></label></th>
        <td>
            <input type="checkbox" name="biztech_portal_user_approval" id="biztech_portal_user_approval" value="1" <?php echo ($biztech_portal_user_approval != "") ? "checked" : ""; ?> >
            <?php _e("This feature will enable user approval for sign-up from portal."); ?>
        </td>
    </tr>
    <?php if (!is_network_admin()) { ?>
        <!--Added by BC on 02-aug-2016 for Single Sign-In-->
        <tr class="hide_class">
            <th scope='row'><label for="biztech_scp_single_signin"><?php _e('Single Sign-In') ?></label></th>
            <td>
                <input type='checkbox' id="biztech_scp_single_signin" value="1" name='biztech_scp_single_signin' <?php if (get_option('biztech_scp_single_signin') == 1) {
        echo 'checked="checked"';
    } ?> /><?php _e("This feature will allow admin to set single sign-in for wordpress dashboard and portal."); ?>
            </td>
        </tr>
<?php } ?>
    <tr class="hide_class">
        <th scope='row'><label for="biztech_scp_signup_link_enable"><?php _e('Enable Registration') ?></label></th>
        <td>
            <input type='hidden' value="0" name='biztech_scp_signup_link_enable' />
            <input type='checkbox' id="biztech_scp_signup_link_enable" value="1" name='biztech_scp_signup_link_enable' <?php echo checked($enable_signup_link, "1") ?> /><?php _e("This feature will allow user to register in portal."); ?>
        </td>
    </tr>
    <tr class="hide_class">
        <th scope='row'><label for="biztech_scp_otp_enable"><?php _e('Two - Step Authentication') ?></label></th>
        <td>
            <input type='hidden' value="0" name='biztech_scp_otp_enable' />
            <input type='checkbox' id="biztech_scp_otp_enable" value="1" name='biztech_scp_otp_enable' <?php echo checked($biztech_scp_otp_enable, "1") ?> /><?php _e("This feature will enable Two - Step Authentication for signin from portal."); ?>
        </td>
    </tr>
    <?php if (is_network_admin()) { ?>
    <tr class="hide_class">
        <th scope='row'><label for="biztech_multi_signin"><?php _e('Multi-site Signin'); ?></label></th>
        <td>
            <input type="checkbox" name="biztech_multi_signin" id="biztech_multi_signin" value="multi-signin-enable" <?php checked($crmmultisignin, "multi-signin-enable") ?>>
    <?php _e("This feature will allow user to login from any another subsite."); ?>
        </td>
    </tr>
        <?php
        }
        if (is_multisite() && !is_network_admin()) {
        ?>
        <tr class="hide_class">
            <th scope='row'><label for="biztech_disable_portal"><?php _e('Disable Portal'); ?></label></th>
            <td>
                <input type="checkbox" name="biztech_disable_portal" id="biztech_disable_portal" value="portal-disable" <?php checked(((get_option('biztech_disable_portal') != NULL) ? get_option('biztech_disable_portal') : ""), "portal-disable") ?>>
        <?php _e("This feature will allow admin to disable portal for this subsite."); ?>
            </td>
        </tr>
    <?php } ?>
</table>
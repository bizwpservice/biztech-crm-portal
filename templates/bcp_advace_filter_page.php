<?php
$filter_display = "style='display:none;'";
//$filter_display = "";
    if ((!empty($filter_where_condition) || !empty($filter_where_currency_condition) ) ) {
        $filter_display = "";
    }
?>
<div id="filter-panel" class="settings-panel">
    <div class="close-fliter"><em class="fas fa-times"></em></div>
    <div class="nav-item dropdown all-module">
        <form id="advance_filter_form" class="advance_filter_form" >
        <?php
        foreach ($results_data as $k => $val_fields) {
            $text_sugar_array = array('name', 'phone', 'url', 'phone_fax', 'varchar', 'datetimecombo', 'id', 'float', 'decimal', 'int','time','text');
            $datetime_options_sugar_array = array('datetime', 'date');
            $select_options_sugar_array = array('enum', 'multienum', 'dynamicenum');
            $textarea_sugar_array = array('text', 'address', 'wysiwyg');
            $check_box_sugar_array = array('bool');
            $radio_button_sugar_array = array('radioenum');
            $currency_sugar_array = array('currency');

            if($val_fields->searchable){
                $label_name = str_replace(':', '', htmlentities($val_fields->label_value));
                $val_fields_name = strtolower($val_fields->name);
                $class_name = 'input-text form-control where_field';
                $keydown = '';
                if ($val_fields->type == 'date') {
                    $class1 = ' scp_date';
                    $keydown .= 'onkeydown="return false"';
                } else if ($val_fields->type == 'time') {
                    $class1 = ' scp_time';
                    $keydown .= 'onkeydown="return false"';
                } else if ( $val_fields->type == 'datetimecombo' || $val_fields->type == 'datetime' ) {
                    $class1 = ' scp_datetime';
                    $keydown .= 'onkeydown="return false"';
                }
                $data_val = '';
                if (array_key_exists($val_fields_name,$filter_where_condition)){
                    $data_val = $filter_where_condition[$val_fields_name];
                }
            ?>
            <?php if( in_array($val_fields->type, $text_sugar_array) ) { ?>
                <div class="form-group">
                    <div class="d-flex align-items-center">
                        <label><?php echo $label_name; ?></label>
                    </div>
                    <input type="text" name="<?php echo $val_fields_name; ?>" class="<?php echo $class_name; ?>" value="<?php echo $data_val; ?>" />
            </div>
            <?php } elseif( in_array($val_fields->type, $select_options_sugar_array)) {
                $options = (array) $val_fields->options;
                $dataval = array();
                if(!empty($filter_where_condition) && !empty($filter_where_condition[$val_fields_name])){
                    if ($val_fields_name == $filter_where_condition[$val_fields_name]['key']){
                        $dataval = $filter_where_condition[$val_fields_name]['compare'];
                    }
                }
            ?>
            <div class="form-group">
                <div class="d-flex align-items-center">
                    <label><?php echo $label_name; ?></label>
                </div>
                <select id="<?php echo $val_fields_name; ?>" name="<?php echo $val_fields_name; ?>" class="<?php echo $class_name; ?> multiselectbox" multiple="multiple">
                    <?php 
                    foreach ($options as $k_op => $v_op) {
                        $checked = "";
                        if (in_array($options[$k_op]->value, $dataval)) {
                            $checked = "selected=''";
                        }
                        if ($options[$k_op]->value == "") {
                            continue;
                        }
                        ?>
                        <option <?php echo $checked; ?> value='<?php echo $options[$k_op]->value; ?>'><?php echo $options[$k_op]->name; ?></option>
                        <?php } ?>
                </select>
            </div>
            <?php } else if ( in_array($val_fields->type, $datetime_options_sugar_array) ) {
                $range = '';
                $start_range = '';
                $end_range = '';
                $first_div = '';
                $cls = '';
                $secound_div = 'style="display:none;"';
                if(!empty($filter_where_condition[$val_fields_name])){
                    if ($val_fields_name == $filter_where_condition[$val_fields_name]['key']){
                        if(isset($filter_where_condition[$val_fields_name]['name']) && $filter_where_condition[$val_fields_name]['name'] == 'single') {
                            $data_val = $filter_where_condition[$val_fields_name]['compare'];
                            $range = $filter_where_condition[$val_fields_name]['range_'.$val_fields_name];
                            $first_div = '';
                            $secound_div = 'style="display:none;"';
                        } else if(isset($filter_where_condition[$val_fields_name]['name']) && $filter_where_condition[$val_fields_name]['name'] == 'range') {
                            $data_val = 'between';
                            $start_range = $filter_where_condition[$val_fields_name]['start_range_'.$val_fields_name];
                            $end_range = $filter_where_condition[$val_fields_name]['end_range_'.$val_fields_name];
                            $first_div = 'style="display:none;"';
                            $secound_div = '';
                            $cls = 'range_between_div';
                        } else {
                            $first_div = 'style="display:none;"';
                            $secound_div = 'style="display:none;"';
                            $data_val = $filter_where_condition[$val_fields_name]['compare'];
                        }
                    }
                }
            ?>
            <div class="form-group">
                <div class="d-flex align-items-center">
                    <label><?php echo $label_name; ?></label>
                </div>
                <div class="aos_quotes_select_div <?php echo $cls; ?>">
                    <select id="<?php echo $val_fields_name; ?>" name="<?php echo $val_fields_name; ?>" class="<?php echo $class_name ?> scp_daterange">
                        <option value=''><?php _e('--None--'); ?></option>
                        <?php
                        $options = $val_fields->options;
                        foreach ($options as $k_op => $v_op) {

                            if ($options->$k_op->value == "") {
                                continue;
                            }
                            $val = $options->$k_op->value;
                            if($options->$k_op->value == '='){
                                $val = '=';
                            } else if($options->$k_op->value == 'not_equal'){
                                $val = '!=';
                            } else if($options->$k_op->value == 'greater_than'){
                                $val = '>';
                            } else if($options->$k_op->value == 'less_than'){
                                $val = '<';
                            }
                            $checked = "";
                            if ($val == $data_val) {
                                $checked = "selected='selected'";
                            }
                            ?>
                            <option <?php echo $checked; ?> value='<?php echo $val; ?>'><?php echo $options->$k_op->name; ?></option>
                        <?php } ?>
                    </select>
                    <div id="date_entered_advanced_range_div" class="date-picker" <?php echo $first_div; ?>>
                        <input type="text" name="range_<?php echo $val_fields_name; ?>" id="range_date_entered_advanced" class="input-text form-control <?php echo $class1; ?>" value="<?php echo $range; ?>" <?php echo $keydown; ?>>
                    </div>
                    <div id="date_entered_advanced_between_range_div" <?php echo $secound_div; ?>>
                        <div class="start_range_div date-picker">
                            <input type="text" name="start_range_<?php echo $val_fields_name; ?>" id="start_range_date_entered_advanced" class="input-text form-control <?php echo $class1; ?>" value="<?php echo $start_range; ?>" <?php echo $keydown; ?>>
                        </div>
                        <?php echo $language_array['lbl_and']; ?>
                        <div class="end_range_div date-picker">
                            <input type="text" name="end_range_<?php echo $val_fields_name; ?>" id="end_range_date_entered_advanced" class="input-text form-control <?php echo $class1; ?>" value="<?php echo $end_range; ?>" <?php echo $keydown; ?>>
                        </div>
                    </div>
                </div>
            </div>
            <?php 
            } else if ( in_array($val_fields->type, $currency_sugar_array) ) {
                $total_amount = '';
                $start_range = '';
                $end_range = '';
                $first = '';
                $secound = 'style="display:none;"';
                if(!empty($filter_where_currency_condition)){
                    if ($val_fields_name == $filter_where_currency_condition['key']){
                        if($filter_where_currency_condition['name'] == 'single') {
                            $data_val = $filter_where_currency_condition['compare'];
                            $total_amount = $filter_where_currency_condition['range_total_amount_advanced'];
                            $first = '';
                            $secound = 'style="display:none;"';
                        } else {
                            $data_val = 'between';
                            $start_range = $filter_where_currency_condition['start_range_total_amount_advanced'];
                            $end_range = $filter_where_currency_condition['end_range_total_amount_advanced'];
                            $first = 'style="display:none;"';
                            $secound = '';
                        }
                    }
                }
                ?>
                <div class="form-group">
                    <div class="d-flex align-items-center">
                        <label><?php echo $label_name; ?></label>
                    </div>
                    <div class="aos_quotes_select_div">
                        <select id="<?php echo $val_fields_name; ?>" name="<?php echo $val_fields_name; ?>" class="<?php echo $class_name ?> scp_currency" onchange="total_amount_advanced_range_change(this.value);">
                            <option value=''><?php _e('--None--'); ?></option>
                            <?php
                            $options = $val_fields->options;
                            foreach ($options as $k_op => $v_op) {

                                if ($options->$k_op->value == "") {
                                    continue;
                                }
                                $val = $options->$k_op->value;
                                if($options->$k_op->value == '='){
                                    $val = '=';
                                } else if($options->$k_op->value == 'not_equal'){
                                    $val = '!=';
                                } else if($options->$k_op->value == 'greater_than'){
                                    $val = '>';
                                } else if($options->$k_op->value == 'greater_than_equals'){
                                    $val = '>=';
                                } else if($options->$k_op->value == 'less_than'){
                                    $val = '<';
                                } else if($options->$k_op->value == 'less_than_equals'){
                                    $val = '<=';
                                }

                                $checked = "";
                                if ($val == $data_val) {
                                    $checked = "selected='selected'";
                                }
                                ?>
                                <option <?php echo $checked; ?> value='<?php echo $val; ?>'><?php echo $options->$k_op->name; ?></option>
                                <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div id="total_amount_advanced_range_div" <?php echo $first; ?>>
                        <input type="text" name="range_total_amount_advanced" id="range_total_amount_advanced" class="input-text form-control" value="<?php echo $total_amount; ?>">
                    </div>
                    <div id="total_amount_advanced_between_range_div" <?php echo $secound; ?>>
                        <div class="d-flex align-items-center">
                            <div class="start_range_div">
                                <input type="text" name="start_range_total_amount_advanced" id="start_range_total_amount_advanced" class="input-text form-control" value="<?php echo $start_range; ?>">
                            </div>
                            <span><?php echo $language_array['lbl_and']; ?></span>
                            <div class="end_range_div">
                                <input type="text" name="end_range_total_amount_advanced" id="end_range_total_amount_advanced" class="input-text form-control" value="<?php echo $end_range; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            } else if ( in_array($val_fields->type, $check_box_sugar_array) ) { 
                $val = '';
                if(!empty($data_val)){
                    $val = $data_val['compare'];
                }
            ?>
                <div class="form-group">
                    <div class="d-flex align-items-center">
                        <label><?php echo $label_name; ?></label>
                    </div>
                    <select id="<?php echo $val_fields_name; ?>" name="<?php echo $val_fields_name; ?>" class="<?php echo $class_name; ?> scp_boolean">
                        <option value="" ><?php _e('--None--'); ?></option>
                        <option value="1" <?php if($val == '1'){ ?>selected=""<?php } ?>><?php echo $language_array['lbl_yes']; ?></option>
                        <option value="0" <?php if($val == '0'){ ?>selected=""<?php } ?>><?php echo $language_array['lbl_no']; ?></option>
                    </select>
                </div>
            <?php } ?>
            <?php }
        } ?>
            <input type="hidden" class="scp-module-name" value="<?php echo $module_name; ?>" />
            
                <div class="preferences-button-group d-inline-block w-100">
                    <button type='submit' class='btn'><?php echo $language_array['lbl_search']; ?></button>
                    <input type="reset" class="reset_div btn border-button" value="<?php echo $language_array['lbl_reset']; ?>" />
                </div>
            
        </form>
   </div>
</div>
<script>
jQuery(document).ready(function(){
    if (jQuery('.scp_datetime').length > 0) {
        jQuery('.scp_datetime').datetimepicker({
            format: 'L',
            format: 'DD/MM/YYYY',
            focusOnShow: false,
        });
    }

    if (jQuery('.scp_time').length > 0) {
        jQuery('.scp_time').datetimepicker({
            format: 'LT',
            format: 'HH:mm',
            focusOnShow: false,
        });
    }
    if (jQuery('.scp_date').length > 0) {
        jQuery('.scp_date').datetimepicker({
            format: 'L',
            format: 'DD/MM/YYYY',
            focusOnShow: false,
        });
    } 
});

function total_amount_advanced_range_change(val) {
    //Clear the values depending on the operation
    if(val == 'between') {
       jQuery('#range_total_amount_advanced').val('');
       jQuery('#total_amount_advanced_range_div').hide();
       jQuery('#total_amount_advanced_between_range_div').show();
    } else {
       jQuery('#start_range_total_amount_advanced').val('');
       jQuery('#end_range_total_amount_advanced').val('');
       jQuery('#total_amount_advanced_range_div').show();
       jQuery('#total_amount_advanced_between_range_div').hide();
    }
}

jQuery(document).on('change', '.scp_daterange', function (e) {
    var val = jQuery(this).val();
    if(val == 'between') {
        jQuery(this).parent().addClass('range_between_div');
        jQuery(this).parent().find('#range_date_entered_advanced').val('');
        jQuery(this).parent().find('#date_entered_advanced_range_div').hide();
        jQuery(this).parent().find('#date_entered_advanced_between_range_div').show();
    } else if (val == '=' || val == '!=' || val == '>' || val == '<') {
        jQuery(this).parent().removeClass('range_between_div');
        jQuery(this).parent().find('#start_range_date_entered_advanced').val('');
        jQuery(this).parent().find('#end_range_date_entered_advanced').val('');
        jQuery(this).parent().find('#date_entered_advanced_range_div').show();
        jQuery(this).parent().find('#date_entered_advanced_between_range_div').hide();
    } else {
        jQuery(this).parent().removeClass('range_between_div');
        jQuery(this).parent().find('#start_range_date_entered_advanced').val('');
        jQuery(this).parent().find('#end_range_date_entered_advanced').val('');
        jQuery(this).parent().find('#range_date_entered_advanced').val('');
        jQuery(this).parent().find('#date_entered_advanced_range_div').hide();
        jQuery(this).parent().find('#date_entered_advanced_between_range_div').hide();
    }
});
jQuery(document).ready(function() {       
    jQuery('.multiselectbox').multiselect({
        selectAll: true,
        placeholder : "<?php _e('--None--'); ?>",
    });
});

</script>
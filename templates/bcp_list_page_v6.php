<div class='table-responsive table-content scp-table-responsive custom-scrollbar'>
    <div class="sfcp-validation">
        <?php if ( isset($_SESSION['bcp_pdf_error']) && $_SESSION['bcp_pdf_error'] != '') { ?>
            <div class='alert alert-danger alert-dismissible fade show login_error' id='setting-error-settings_updated'>
                <span><?php echo $_SESSION['bcp_pdf_error']; ?></span>
                <button type='button' class='btn-close' data-bs-dismiss='alert'></button>
            </div>
            <?php
            unset($_SESSION['bcp_pdf_error']);
        }
        if ($relate_to_module == "") {
            if (isset($_SESSION['bcp_add_record']) && $_SESSION['bcp_add_record'] != '') {
            ?>
                <div class='alert alert-success alert-dismissible fade show bcp-success'>
                    <span id='succid'><?php echo $_SESSION['bcp_add_record']; ?></span>
                    <button type='button' class='btn-close' data-bs-dismiss='alert'></button>
                </div>
                <?php
                unset($_SESSION['bcp_add_record']);
            }
            if (isset($_SESSION['bcp_add_error']) && $_SESSION['bcp_add_error'] != '') {
                ?>
                <div class='alert alert-danger alert-dismissible fade show login_error' id='setting-error-settings_updated'>
                    <span><?php echo $_SESSION['bcp_add_error']; ?></span>
                    <button type='button' class='btn-close' data-bs-dismiss='alert'></button>
                </div>
                <?php
                unset($_SESSION['bcp_add_error']);
            }
        }
        ?>
    </div>
    <?php if ($cnt > 0) { ?>
    <table>
        <thead>
            <tr>
            <?php
            if ($relate_to_module == "" && $module_name != 'Accounts') {
                $select_all_th_class = "";
                if (isset($_REQUEST['select_all']) && $_REQUEST['select_all'] == '1') {
                    $select_all_th_class = "scp_select_th_active";
                }
                ?>
                <th>
                    <div class='scp-th-checkbox-div'>
                        <label class="checkbox-button">
                            <input type='checkbox' class='scp-check-all checkbox-button__input <?php echo $select_all_th_class; ?>' id='select_all' >
                            <span class="checkbox-button__control"></span>
                            <em class="fas fa-chevron-down" aria-hidden="true" ></em>
                        </label>
                    </div>
                    <ul class='scp-th-checkbox-ul' style='display:none;'>
                        <li id='scp-select-th-page'><?php echo $language_array['lbl_select_this_page']." (".$cnt.")"; ?></li>
                        <li id='scp-select-th-all' data-count='<?php echo $countCases; ?>'><?php echo $language_array['lbl_select_all']." (".$countCases.")"; ?></li>
                        <li id='scp-deselect-th-all'><?php echo $language_array['lbl_deselect_all']; ?></li>
                    </ul>
                </th>
            <?php
            }
            foreach ($results as $key_name => $val_array) {
                if ($key_name == "SET_COMPLETE") {
                    continue;
                }
                if (!empty($val_array->related_module)) {
                    if ($val_array->type == 'relate' && (!array_key_exists($val_array->related_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array
                        continue;
                    }
                }
                $ar_val = strtolower($key_name);
                $name_arry[] = $ar_val;
                //make type array
                $type_array[$ar_val] = $val_array->type;
                
                    if ( $ar_val == "modified_user_id" || $ar_val == "created_by" || ($ar_val == "id" && $id_show == false) || ( $module_name == 'AOS_Quotes' && $ar_val == 'rejection_history' ) ) {
                        continue;
                    }
                    // changes to set label in v3.0
                    $field_label = str_replace(':', '', htmlentities($val_array->label_value));

                    ?>
                    <th class="text-left">
                    <?php
                    if ($val_array->type == 'file' || $val_array->type == 'relate' ) {//Front Side: 'Notes' Module: No need to place sorting on 'Attachment'
                        ?>
                        <a><?php echo $field_label; ?></a>
                        <?php
                    } else {
                        if ($order_by == $ar_val) {
                            if ($order == 'asc') {
                                    $or = 'desc';
                                } else {
                                    $or = 'asc';
                                }
                            ?>
                            <a href='javascript:void(0);' onclick='bcp_module_order_by(0,"<?php echo $module_name; ?>","<?php echo $ar_val; ?>","<?php echo $or; ?>","<?php echo $view ?>","<?php echo $current_url; ?>");' >
                                <?php 
                                echo $field_label;
                                if ($order == 'asc') {
                                    echo $module_icon_content['arrow-up'];
                                } else {
                                    echo $module_icon_content['arrow-down'];
                                }
                                ?>
                            </a>
                            <?php } else { ?>
                            <a href='javascript:void(0);' onclick='bcp_module_order_by(0,"<?php echo $module_name; ?>","<?php echo $ar_val; ?>","asc","<?php echo $view ?>","<?php echo $current_url; ?>");' >
                                <?php echo $field_label; ?>
                                <?php echo $module_icon_content['arrow']; ?>
                            </a>
                            <?php
                        }
                    }
                    ?>
                    </th>
                    <?php } ?>
            <th class="action-th"><?php echo $language_array['lbl_action']; ?></th>
            </tr>
        </thead>
        <tbody>
        <?php
        $currency_symbol = isset($currency_symbol) ? $currency_symbol : '';
        if(!empty($product_array)){

        }else{
            $product_array = array();
        }
        foreach ($list_result->entry_list as $list_result_s) {

            $proposal_status = '';
            $call_status = "";
            $meeting_status = "";
            $id = $list_result_s->name_value_list->id->value;
            if ($module_name == 'AOS_Products') {
                array_push($product_array, $id);
            }
            if (isset($list_result_s->name_value_list->parent_type->value)) {
                $parent_type = $list_result_s->name_value_list->parent_type->value;
            }
            if ($module_name == 'AOS_Quotes' || $module_name == 'AOS_Invoices' || $module_name == 'AOS_Contracts' || $module_name == 'AOS_Products' ) { // Added by BC on 02-july-2016 for currency symbol
                $curreny_id = $list_result_s->name_value_list->currency_id->value;
                if (!empty($curreny_id)) {
                    $currency_symbol = $_SESSION['Currencies']->$curreny_id->symbol;
                }
            }
            ?>
            <tr>
            <?php

            if ($relate_to_module == "" && $module_name != 'Accounts') {
            // checkbox in first td. Since v3.2.0
                $delete_class = "";
                if ($module_name == "bc_proposal" && $list_result_s->name_value_list->status->value != "pending") {
                    $delete_class = "bc_proposal_not_pending";
                }
                ?>
                <td>
                    <label class="checkbox-button">
                        <input type='checkbox' class='scp-checkbox checkbox-button__input <?php echo $delete_class; ?>' data-id='<?php echo $id; ?>'>
                        <span class="checkbox-button__control"></span>
                    </label>
                </td>
                <?php
            }

            foreach ($name_arry as $nkey => $nval) {
                switch ($nval) {
                    case 'assigned_user_id':
                        $get_entrty_module = "Users";
                        break;
                    case 'modified_user_id':
                        $get_entrty_module = "Users";
                        break;
                    case 'created_by':
                        $get_entrty_module = "Users";
                        break;
                    case 'parent_id':
                        if ($parent_type) {
                            $get_entrty_module = $parent_type;
                        } else {
                            $get_entrty_module = "";
                        }
                        break;
                    default:
                        $get_entrty_module = "";
                        break;
                }
                $hlink_st = $hlink_en = $cls = '';

                $value = $list_result_s->name_value_list->$nval->value;
                $c_nval = strtoupper($nval);
                
                //if($fields_meta[$nval] == 'name' || $fields_meta[$nval] == 'text' || $fields_meta[$nval] == 'parent'){
                if($results->$c_nval == 'name' || $results->$c_nval == 'phone' || $results->$c_nval == 'text' || $results->$c_nval == 'parent'){
                    $value = htmlentities($value);
                }

                $value = strip_tags( html_entity_decode( $value ) );

                if ( $module_name == 'AOS_Quotes' && $nval == 'rejection_history' ) {
                    continue;
                }

                if ($nval == 'status') {
                    if (isset($InvoiceStatus->$id->payment_status) && $InvoiceStatus->$id->payment_status) {
                        if (isset($InvoiceStatus->$id->status)) {
                            $value = $InvoiceStatus->$id->status;
                        }
                    }
                }

                if ($nval == "filename") {
                    $download_file = 0;
                    if (!empty($value)) {//Added by BC on 08-jul-2016
                        $cls = "general-link-btn scp-download-btn";
                        if ($module_name == "Notes") {
                            $hlink_st = "<a title='" . $language_array['lbl_download'] . "' href='javascript:void(0);' onclick='form_submit_note_document(\"$id\",\"filename\");' class='kb-attachment $cls scp-$module_name-font'>";
                            $hlink_en = "</a>";
                            $download_file = 1;
                        }
                        if ($module_name == "Documents") {
                            $hlink_st = "<a title='" . $language_array['lbl_download'] . "' href='javascript:void(0);' onclick='form_submit_document(\"$id\");' class='kb-attachment $cls scp-$module_name-font'>";
                            $hlink_en = "</a>";
                            $download_file = 1;
                        }
                    }
                }
                if ($nval == "date_start" || $nval == "date_end" || $nval == "date_due") {
                    if ($value != '') {
                        if ( $_SESSION['browser_timezone'] != NULL ) {
                            $value = scp_user_time_convert( $value );
                        } else {
                            $UTC = new DateTimeZone("UTC");
                            $newTZ = new DateTimeZone($result_timezone);
                            $date = new DateTime($value, $UTC);
                            $date->setTimezone($newTZ);
                            $value = $date->format($objSCP->date_format . " " . $objSCP->time_format);
                        }
                    }
                }
                if ($nval == "date_modified"|| $nval == "date_entered") {
                    if ($value != '') { // aaded to display blank date instead of default date
                        $UTC = new DateTimeZone("UTC");
                        $newTZ = new DateTimeZone($result_timezone);
                        $date = new DateTime($value, $UTC);
                        $date->setTimezone($newTZ);
                        $value = $date->format($objSCP->date_format . " " . $objSCP->time_format);
                    }
                }
                if ($type_array[$nval] == 'date') {
                    $value = (!empty($value)) ? date($objSCP->date_format, strtotime($value)) : '';
                }
                if ($get_entrty_module) {
                    $select_fields = array('name');
                    $record_detail = $objSCP->get_entry($get_entrty_module, $value, $select_fields);
                    if (isset($record_detail) && !empty($record_detail)) {
                        $value = $record_detail->entry_list[0]->name_value_list->name->value;
                    }
                }
               
                if ($results->$c_nval->type == 'enum' || $results->$c_nval->type == 'multienum' || $results->$c_nval->type == 'dynamicenum' || $results->$c_nval->type == 'radioenum') {
                    if ( $results->$c_nval->type == 'multienum' ) {
                        $tmp_multi_val = array();
                        $tmp_multi_vals = $objSCP->unencodeMultienum( $value );
                        foreach( $tmp_multi_vals as $tmp_multi_val_ ) {
                            $resultoption = $results->$c_nval->options;
                            $array = json_decode(json_encode($resultoption), true);
                            $key = array_search($tmp_multi_val_, array_column($array, 'value'));
                            $keys = array_keys($array);
                            $tmp_multi_val[] = (!empty($tmp_multi_val_)) ? $keys[$key] : '';
                            
                        }
                        $value = implode(', ', $tmp_multi_val);
                    } else {
                        $resultoption = $results->$c_nval->options;
                        $array = json_decode(json_encode($resultoption), true);
                        $key = array_search($value, array_column($array, 'value'));
                        $keys = array_keys($array);
                        
                        //$value = (!empty($value)) ? $fields_meta[$nval]->module_fields->$nval->options->$value->value : '';
                        $value = (!empty($value)) ? $keys[$key] : '';
                        
                    }
                }
                //if ($fields_meta[$nval]->module_fields->$nval->type == 'currency' && $value != '') {//Added by BC on 02-jul-2016 for currency
                if ($results->$c_nval->type == 'currency' && $value != '') {//Added by BC on 02-jul-2016 for currency
                    $value = $currency_symbol . number_format($value, 2);
                }
                if ( $type_array[$nval] == 'bool' ) {
                    if ($value == "1") {
                        $value = $language_array['lbl_yes'];
                    } else {
                        $value = $language_array['lbl_no'];
                    }
                }
                if ($type_array[$nval] == 'url' && !empty($value)) {//for url
                    if (strpos($value, 'http') === false) {
                        $value = 'http://' . $value;
                    }
                    $hlink_st = "<a title='" . $language_array['lbl_download'] . "' href='" . $value . "' target='_blank'>";
                    $hlink_en = "</a>";
                }
                if ( $nval == 'currency_id' ) {
                    $value = $_SESSION['scp_all_currency']->$value->currency_symbol;
                }
                if ( $module_name == 'bc_proposal' && $nval == 'status' && strtolower( $value ) == 'pending' ) {
                    $proposal_status = $value;
                }

                //For Call Status
                if ( $module_name == 'Calls' && $nval == 'status' ) {
                    $call_status = $value;
                }

                //For Meeting Status
                if ( $module_name == 'Meetings' && $nval == 'status' ) {
                    $meeting_status = $value;
                }

                if ($value == '' || empty($value) && !$type_array[$nval] == 'int') {
                    $value = "-";
                }
                if ($nval == 'status' && $module_name == "AOS_Invoices") {
                    if (isset($InvoiceStatus->$id->payment_status) && $InvoiceStatus->$id->payment_status) {
                        if (isset($InvoiceStatus->$id->status)) {
                            $value = $InvoiceStatus->$id->status;
                        }
                    }
                }
                
                $status_value = '';
                $status_td = '';
                if($module_name == 'Cases' && $nval == 'priority') {
                    $status_td = 'status-td';
                    $status_value = str_replace(' ', '-', strtolower($value));
                } else if($module_name == 'AOS_Quotes' && $nval == 'stage') {
                    $status_td = 'status-td';
                    $status_value = str_replace(' ', '-', strtolower($value));
                } else if(($module_name == 'Meetings' || $module_name == 'Calls' || $module_name == 'bc_proposal') && $nval == 'status') {
                    $status_td = 'status-td';
                    $status_value = str_replace(' ', '-', strtolower($value));
                }
                
                if ($nval != 'link') {
                    ?>
                    <td class="<?php echo $status_td; ?>">
                        <?php if($nkey == 0) { ?>
                        <a href="#data/<?php echo $module_name; ?>/detail/<?php echo $id; ?>/">
                            <?php echo ( ( strlen( $value ) > 60 ) ? substr( $value, 0, 60).'...' : $value ); ?>
                        </a>
                        <?php } else { ?>
                            <p class="<?php echo $status_value; ?>"><?php echo ( ( strlen( $value ) > 60 ) ? substr( $value, 0, 60 ).'...' : $value ); ?></p>
                        <?php } ?>
                    </td>
                    <?php
                }
                //Invoice addon for v3.2.0
                if (strtolower($nval) == get_option('biztech_amount_field')) {
                    $payment_price = $value;
                }
            }
            $view = 'edit';
            $deleted = 1;

            ?>
            <td class="action-list">
                
                <?php
                if ($relate_to_module == '') {
                    $edit_flag = 0;
                    if ($_SESSION['module_action_array'][$module_name]['edit'] == 'true') {

                        if ($module_name == 'bc_proposal' && strtolower( $proposal_status ) == 'pending') {
                            $edit_flag = 1;
                        }

                        if ($module_name == 'Calls' && ( strtolower( $call_status ) == 'requested' || strtolower( $call_status ) == 'not held')) {
                            $edit_flag = 1;
                        }

                        if ($module_name == 'Meetings' && ( strtolower( $meeting_status ) == 'requested' || strtolower( $meeting_status ) == 'not held' )) {
                            $edit_flag = 1;
                        }

                        if ($module_name != 'bc_proposal' && $module_name != 'Calls' && $module_name != 'Meetings') {
                            $edit_flag = 1;
                        }

                        if ($edit_flag) {
                            ?>
                            <a class="dropdown-item" href="#data/<?php echo $module_name; ?>/edit/<?php echo $id; ?>/" title="<?php echo $language_array['lbl_edit']; ?>">
                                <?php echo $module_icon_content['edit']; ?>
                            </a>
                            <?php
                        }
                    }
                }
                ?>
                <a class="dropdown-item" href='#data/<?php echo $module_name; ?>/detail/<?php echo $id; ?>/' title="<?php echo $language_array['lbl_view']; ?>">
                    <?php echo $module_icon_content['view']; ?>
                </a>
                <?php if ($_SESSION['module_action_array'][$module_name]['delete'] == 'true') { ?>
                <a class="dropdown-item" href="javascript:void(0);" title="<?php echo $language_array['lbl_delete']; ?>" onclick="form_submit('<?php echo $id; ?>');">
                    <?php echo $module_icon_content['delete']; ?>
                </a>
                <?php } ?>
                <?php if ($module_name == 'AOS_Quotes' || $module_name == "AOS_Invoices" || $module_name == "AOS_Contracts") { //to download pdf while showing quotes ?>
                <a class="dropdown-item" href='javascript:void(0);' onclick='pdf_data_download("<?php echo $id; ?>", "<?php echo $module_name; ?>");' title="<?php echo $language_array['lbl_download']; ?>">
                        <?php echo $module_icon_content['download']; ?>
                    </a>
                    <?php
                    // For v3.2.0 Invoice Addon
                    if ($module_name == "AOS_Invoices" && get_option('biztech_payment_status') == 1){
                        global $InvoiceStatus;
                        if (!isset($list_result_s->name_value_list->name->value) || !isset($list_result_s->name_value_list->number->value) || !isset($list_result_s->name_value_list->status->value)) {
                            $select_fields = array('name', 'number', 'status');
                            $product_details = $objSCP->get_entry($module_name, $id, $select_fields);
                            $product_name = $product_details->entry_list[0]->name_value_list->name->value;
                            $product_number = $product_details->entry_list[0]->name_value_list->number->value;
                            $product_status = $product_details->entry_list[0]->name_value_list->status->value;
                        } else {
                            $product_name = $list_result_s->name_value_list->name->value;
                            $product_number = $list_result_s->name_value_list->number->value;
                            $product_status = $list_result_s->name_value_list->status->value;
                        }
                        $currency_id = $list_result_s->name_value_list->currency_id->value;
                        echo (function_exists("scp_display_payment_button")) ? scp_display_payment_button($id, $payment_price, $product_name, $product_number, $product_status, $currency_id) : "";
                    }
                }
                if ( $module_name == 'Documents' && isset( $download_file ) && $download_file ) {
                    ?>
                    <a class="dropdown-item" href='javascript:void(0);' onclick='form_submit_document("<?php echo $id; ?>");' title="<?php echo $language_array['lbl_download']; ?>">
                        <?php echo $module_icon_content['download']; ?>
                    </a>
                    <a class="dropdown-item" href='<?php echo admin_url()."admin-post.php?action=bcp_get_doc_attachment&scp_doc_id=$id&module_name=$module_name&scp_doc_view=1"; ?>' target='__blank' title="<?php echo $language_array['lbl_preview']; ?>">
                        <?php echo $module_icon_content['view']; ?>
                    </a>
                    <?php
                }
                if ( $module_name == 'Notes' && isset( $download_file ) && $download_file ) {
                    ?>
                    <a class="dropdown-item" href='javascript:void(0);' onclick='form_submit_note_document("<?php echo $id; ?>", "filename");' title="<?php echo $language_array['lbl_download']; ?>">
                        <?php echo $module_icon_content['download']; ?>
                    </a>
                    <a class="dropdown-item" href='<?php echo admin_url()."admin-post.php?action=bcp_get_note_attachment&scp_note_id=$id&scp_note_field=filename&scp_note_view=1"; ?>' target='__blank' title="<?php echo $language_array['lbl_preview']; ?>">
                        <?php echo $module_icon_content['view']; ?>
                    </a>
                    <?php } ?>
                
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <?php } else { ?>
        <strong><?php echo ( isset( $_SESSION['portal_message_list']->module_no_records ) ? $_SESSION['portal_message_list']->module_no_records : '' ); ?></strong>
    <?php } ?>
</div>
<?php

global $woocommerce;
if ($module_name == "AOS_Invoices" && $woocommerce != null) {
    $invoice_path = plugin_dir_url(ABSPATH . '/wp-content/plugins/portal-invoice-addon');
    $invoice_path = $invoice_path.'portal-invoice-addon';
    ?>
    <script type="text/javascript">
        function add_prouct(invoice_id, invoice_name, invoice_val)
        {
            var filepath = "<?php echo $invoice_path; ?>";
            if (invoice_val == "-1111") {
                $("#curmsg").show();
                return false;
            } else {
                var el = jQuery("#example");
                App.blockUI(el);
                jQuery.ajax({
                    url: filepath + "/invoice.php",
                    data: "invoice_val=" + invoice_val + "&invoice_name=" + invoice_name + "&invoice_id=" + invoice_id,
                    success: function (result) {
                        App.unblockUI(el);
                        window.location = "<?php echo $woocommerce->cart->get_checkout_url(); ?>";
                    }});
            }
        }
    </script>
    <?php
}
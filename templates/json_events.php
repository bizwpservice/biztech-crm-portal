<?php

include_once("../../../../wp-config.php");
include_once("../../../../wp-load.php");
include_once("../../../../wp-includes/wp-db.php");

global $wpdb, $sugar_crm_version, $custom_relationships;
$custom_relationships = $_SESSION['scp_custom_relationships'];
if ( isset( $_REQUEST['calendar'] ) && ( $_REQUEST['calendar'] == 'view_calendar' ) ) {
    $scp_sugar_rest_url = get_option('biztech_scp_rest_url');
    $scp_sugar_username = get_option('biztech_scp_username');
    $scp_sugar_password = scp_decrypt_string(get_option('biztech_scp_password'));
    $scp_user_id = $_SESSION['scp_user_id'];
    $today_date = ( isset($_REQUEST['today_date']) && $_REQUEST['today_date'] != '' ) ? $_REQUEST['today_date'] : '';
    $today_time = ( isset($_REQUEST['today_time']) && $_REQUEST['today_time'] != '' ) ? $_REQUEST['today_time'] : '';

    $objSCP = new SugarRestApiCall($scp_sugar_rest_url, $scp_sugar_username, $scp_sugar_password);
    if (!is_object($_SESSION['module_array'])) {//check session value in module array
        $modules_array2 = $objSCP->getPortal_accessibleModules();
        $modulesAry1 = array();
        foreach ($modules_array2->accessible_modules as $key_1 => $value_1) {
            $modulesAry1[$key_1] = $value_1->module_name;
        }
        $accessble_modules = $modulesAry1;
    } else {
        $accessble_modules = (array) $_SESSION['module_array'];
    }
    $arry_event_modules = array('Meetings', 'Calls', "Tasks");
    $t = 0;
    // set default timezone from sugar side*******
    //Updated by BC on 17-jun-2016 for timezone store in session
    if (empty($_SESSION['user_timezone'])) {
        $result_timezone = $objSCP->getUserTimezone();
        if ($sugar_crm_version == 7) {
            $result_timezone = $result_timezone->timezone;
        }
    } else {
        $result_timezone = $_SESSION['user_timezone'];
        if ($sugar_crm_version == 7) {
            $result_timezone = $_SESSION['user_timezone']->timezone;
        }
    }
    date_default_timezone_set($result_timezone); //updated by BC on 20-jun-2016
    $result_timezone = date_default_timezone_get();
    $browser_timezone = $_SESSION['browser_timezone'];
    $temp_timezone = (-1) * (int) $browser_timezone / 60;
    $dot_after = explode( ".", $temp_timezone );
    if ( $dot_after[1] != NULL && $dot_after[1] > 0 ) {
        $offset = floor( $dot_after[0] ) . ':30';
    } else {
        $offset = $dot_after[0];
    }
    list($hours, $minutes) = explode(':', $offset);
    $seconds = $hours * 60 * 60 + $minutes * 60;
    // Get timezone name from seconds
    $brow_timezone = timezone_name_from_abbr('', $seconds, 1);
    if($brow_timezone === false) $brow_timezone = timezone_name_from_abbr('', $seconds, 0);
    //default timezone******
    foreach ($arry_event_modules as $module_name) {
        if (array_key_exists($module_name, $accessble_modules)) {

            $arry = array('id', 'name', 'date_start');
            if ($module_name == "Calls") {
                array_push($arry, "duration_hours", "duration_minutes");
            }
            if ($module_name == "Meetings") {
                array_push($arry, "date_end");
            }
            if ($module_name == "Tasks") {
                array_push($arry, "date_due");
            }
            // version 6 or 5
            if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
                $relation_name = isset($custom_relationships[$module_name]["Contacts"]) ? $custom_relationships[$module_name]["Contacts"] : strtolower($module_name);

                if($_SESSION['scp_user_group_type'] == "contact_based"){
                    $list_result = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], $relation_name, $arry);
                }else{
                    $list_result = $objSCP->get_relationships('Accounts', $_SESSION['scp_account_id'], $relation_name, $arry);

                    if($module_name == "Calls" || $module_name == "Meetings"){
                            $list_result = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], $relation_name, $arry);     
                    }
                       
                }


                $cnt = count($list_result->entry_list);
                if ($cnt > 0) {
                    for ($m = 0; $m < $cnt; $m++) {//for loop starts
                        $id = $list_result->entry_list[$m]->name_value_list->id->value;
                        $name = $list_result->entry_list[$m]->name_value_list->name->value;
                        $date_start = $list_result->entry_list[$m]->name_value_list->date_start->value;
                        if ($date_start == "") {
                            continue;
                        }
                        if (isset($list_result->entry_list[$m]->name_value_list->date_end->value)) {
                            $date_end = $list_result->entry_list[$m]->name_value_list->date_end->value;
                        }
                        if (isset($list_result->entry_list[$m]->name_value_list->date_due->value)) {
                            $date_end = $list_result->entry_list[$m]->name_value_list->date_due->value;
                        }
                        //for convert utc timezone format
                        $UTC = new DateTimeZone($brow_timezone);
                        $newTZ = new DateTimeZone("UTC");
                        $date = new DateTime($date_start, $newTZ);
                        $date2 = new DateTime($date_end, $newTZ);
                        $date->setTimezone($UTC);
                        $date_start = $date->format("Y-m-d H:i:s");
                        $date2->setTimezone($UTC);
                        $date_end = $date2->format("Y-m-d H:i:s");
                        $date_diff = $date2->diff($date);
                        if ( ( isset($today_date) && $today_date != '' ) && ( isset($today_time) && $today_time != '' ) ) {    //dashboard today schedule check
                            $date_start_2 = new DateTime($date_start);
                            $date_to_compare = $date_start_2->format('Y-m-d');
                            $time_to_compare = $date_start_2->format('H:i:s');
                            if ( $today_date != $date_to_compare ) {
                                continue;
                            }
                            if ( $today_time > $time_to_compare ) {
                                continue;
                            }
                        }
                        ////////////////////////////
                        $explode_date = explode(" ", $date_start);

                        $ary[$t]['id'] = $id;
                        if ( ( isset($today_date) && $today_date != '' ) && ( isset($today_time) && $today_time != '' ) ) {
                            $ary[$t]['title'] = preg_replace("/[^A-Za-z0-9\-: ']/", '', $name);
                        } else {
                            $ary[$t]['title'] = date('h:i a',strtotime($date_start))." - ".preg_replace("/[^A-Za-z0-9\-: ']/", '', $name);
                        }

                        $ary[$t]['start'] = $date_start;
                        $ary[$t]['allDay'] = false;

                        if ($module_name == "Calls") {
                            $duration_hours = $list_result->entry_list[$m]->name_value_list->duration_hours->value;
                            $duration_minutes = $list_result->entry_list[$m]->name_value_list->duration_minutes->value;
                            if (!empty($duration_hours)) {
                                $time_hr_min = "+" . $duration_hours . " hour ";
                            }
                            if (!empty($duration_minutes)) {
                                $time_hr_min .= "+" . $duration_minutes . " minutes";
                            }
                            if (!empty($time_hr_min)) {
                                $end_date_calls = date("Y-m-d H:i:s", strtotime($time_hr_min, strtotime($date_start)));
                                $ary[$t]['end'] = $end_date_calls;
                            }
                            $ary[$t]['hours'] = $duration_hours.".".$duration_minutes;
                            $ary[$t]['backgroundColor'] = fetch_data_option('biztech_scp_calendar_calls_color');
                            $view1 = 'detail';
                            $ary[$t]['url'] = '#data/'.$module_name.'/'.$view1.'/'.$id.'/';
                        }
                        if ($module_name == "Meetings") {
                            $ary[$t]['end'] = $date_end;
                            $h = $date_diff->h;
                            $min = $date_diff->i;
                            $ary[$t]['hours'] = $h.".".$min;
                            $ary[$t]['backgroundColor'] = fetch_data_option('biztech_scp_calendar_meetings_color');
                            $view1 = 'detail';
                            $ary[$t]['url'] = '#data/'.$module_name.'/'.$view1.'/'.$id.'/';
                        }
                        if ($module_name == "Tasks") {
                            $ary[$t]['end'] = $date_end;
                            $h = $date_diff->h;
                            $min = $date_diff->i;
                            $ary[$t]['hours'] = $h.".".$min;
                            $ary[$t]['backgroundColor'] = fetch_data_option('biztech_scp_calendar_tasks_color');
                            $view1 = 'detail';
                            $ary[$t]['url'] = '#data/'.$module_name.'/'.$view1.'/'.$id.'/';
                        }
                        if (!isset($_REQUEST['today_schedule'])) {
                            $ary[$t]['title'] .= " (".$ary[$t]['hours']." Hrs)";
                        }
                        $t++;
                    }//for loop ends
                }
            }
            //version 7
            $final_str = implode(',', $arry);
            if ($sugar_crm_version == 7) {
                $relation_name = isset($custom_relationships[$module_name]["Contacts"]) ? $custom_relationships[$module_name]["Contacts"] : strtolower($module_name);
                $list_result = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], $relation_name, $final_str, array(), '', '', 'id:ASC');
                $cnt = count($list_result->records);
                if ($cnt > 0) {
                    for ($m = 0; $m < $cnt; $m++) {//for loop starts
                        $id = $list_result->records[$m]->id;
                        $name = $list_result->records[$m]->name;
                        $date_start = $list_result->records[$m]->date_start;
                        if (isset($list_result->records[$m]->date_end)) {
                            $date_end = $list_result->records[$m]->date_end;
                        }
                        if (isset($list_result->records[$m]->date_due)) {
                            $date_end = $list_result->records[$m]->date_due;
                        }
                        //for convert browser timezone format
                        $UTC = new DateTimeZone($brow_timezone);
                        $newTZ = new DateTimeZone($result_timezone);
                        $date = new DateTime($date_start, $newTZ);
                        $date2 = new DateTime($date_end, $newTZ);
                        $date->setTimezone($UTC);
                        $date_start = $date->format("Y-m-d H:i:s");
                        $date2->setTimezone($UTC);
                        $date_end = $date2->format("Y-m-d H:i:s");
                        $date_diff = $date2->diff($date);
                        if ( ( isset($today_date) && $today_date != '' ) && ( isset($today_time) && $today_time != '' ) ) {    //dashboard today schedule check
                            $date_start_2 = new DateTime($date_start);
                            $date_to_compare = $date_start_2->format('Y-m-d');
                            $time_to_compare = $date_start_2->format('H:i:s');
                            if ( $today_date != $date_to_compare ) {
                                continue;
                            }
                            if ( $today_time > $time_to_compare ) {
                                continue;
                            }
                        }
                        ////////////////////////////
                        $explode_date = explode(" ", $date_start);

                        $ary[$t]['id'] = $id;
                        if ( ( isset($today_date) && $today_date != '' ) && ( isset($today_time) && $today_time != '' ) ) {
                            $ary[$t]['title'] = preg_replace("/[^A-Za-z0-9\-: ']/", '', $name);
                        } else {
                        $ary[$t]['title'] = date('h:i a',strtotime($date_start))." - ".preg_replace("/[^A-Za-z0-9\-: ']/", '', $name);
                        }
                        $ary[$t]['start'] = $date_start;
                        $ary[$t]['allDay'] = false;

                        if ($module_name == "Calls") {
                            $duration_hours = $list_result->records[$m]->duration_hours;
                            $duration_minutes = $list_result->records[$m]->duration_minutes;
                            if (!empty($duration_hours)) {
                                $time_hr_min = "+" . $duration_hours . " hour ";
                            }
                            if (!empty($duration_minutes)) {
                                $time_hr_min .= "+" . $duration_minutes . " minutes";
                            }
                            if (!empty($time_hr_min)) {
                                $end_date_calls = date("Y-m-d H:i:s", strtotime($time_hr_min, strtotime($date_start)));
                                $ary[$t]['end'] = $end_date_calls;
                            }
                            $ary[$t]['hours'] = $duration_hours.".".$duration_minutes;
                            $ary[$t]['backgroundColor'] = fetch_data_option('biztech_scp_calendar_calls_color');
                            $view1 = 'detail';
                            $ary[$t]['url'] = '#data/'.$module_name.'/'.$view1.'/'.$id.'/';
                        }
                        if ($module_name == "Meetings") {
                            
                            $ary[$t]['end'] = $date_end;
                            $h = $date_diff->h;
                            $min = $date_diff->i;
                            $ary[$t]['hours'] = $h.".".$min;
                            $ary[$t]['backgroundColor'] = fetch_data_option('biztech_scp_calendar_meetings_color');
                            $view1 = 'detail';
                            $ary[$t]['url'] = '#data/'.$module_name.'/'.$view1.'/'.$id.'/';
                        }
                        if ($module_name == "Tasks") {
                            $ary[$t]['end'] = $date_end;
                            $h = $date_diff->h;
                            $min = $date_diff->i;
                            $ary[$t]['hours'] = $h.".".$min;
                            $ary[$t]['backgroundColor'] = fetch_data_option('biztech_scp_calendar_tasks_color');
                            $view1 = 'detail';
                            $ary[$t]['url'] = '#data/'.$module_name.'/'.$view1.'/'.$id.'/';
                        }
                        if (!isset($_REQUEST['today_schedule'])) {
                            $ary[$t]['title'] .= " (".$ary[$t]['hours']." Hrs)";
                        }
                        $t++;
                    }//for loop ends
                }
            }
        }
    }
    if ( ( isset($today_date) && $today_date != '' ) && ( isset($today_time) && $today_time != '' ) ) {
        function date_compare($a, $b) {
            $t1 = strtotime($a['start']);
            $t2 = strtotime($b['start']);
            return $t1 - $t2;
        }
        if ( $ary == null ) {
            $ary = array();
        }

        usort($ary, 'date_compare');
        foreach ( $ary as $key=>$values ){
            $ary[$key]['start'] = date('h:i A', strtotime($values['start']));
        }
        $ary = array_slice($ary, 0, 5, true);
    }
    echo json_encode($ary);
}
<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if( ! function_exists('scp_sign_up') ) {
    add_shortcode('bcp-sign-up', 'scp_sign_up');
    function scp_sign_up() {

        if (isset($_REQUEST['action']) && $_REQUEST['action'] == "edit") {
            return;
        }
        
        global $objSCP, $sugar_crm_version, $lang_code, $language_array, $all_language;
        $language_array = scp_get_language_data($lang_code);
        if ($objSCP == NULL) {
            return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                <strong>".__('CRM Authentication failed')."</strong>
            </div>";
        }
        
        if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
            $sess_id = $objSCP->session_id;
        }
        if ($sugar_crm_version == 7) {
            $sess_id = $objSCP->access_token;
        }
        if ($sess_id != '') {
            ob_start();
            if (empty($_SESSION['scp_user_id']) && is_object( $objSCP )) {

                $enable_signup_link = fetch_data_option('biztech_scp_signup_link_enable');
                if ( $enable_signup_link ) {
                    include( TEMPLATE_PATH . 'signup.php');
                } else {
                    ob_clean();
                    wp_die( $language_array['msg_contact_admin'] );
                }
                return ob_get_clean();
            } else {
                
                if (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'] != '') {
                    $bcp_connection_error = $_SESSION['bcp_connection_error'];
                    unset($_SESSION['bcp_connection_error']);
                    return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                        <strong>".$bcp_connection_error."</strong>
                    </div>";
                }
            }
        } else {
            if (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'] != '') {
                $bcp_connection_error = $_SESSION['bcp_connection_error'];
                unset($_SESSION['bcp_connection_error']);
                return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                    <strong>".$bcp_connection_error."</strong>
                </div>";
            }
        }

    }
}

if ( ! function_exists('scp_login_shortcode') ) {

    add_shortcode('bcp-login', 'scp_login_shortcode');
    function scp_login_shortcode() {

        if (isset($_REQUEST['action']) && $_REQUEST['action'] == "edit") {
            return;
        }
        
        global $objSCP, $wpdb, $sugar_crm_version, $all_language, $lang_code, $language_array;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        if ($objSCP == NULL) {
            return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                <strong>".__('CRM Authentication failed')."</strong>
            </div>";
        }
        
        if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
            $sess_id = $objSCP->session_id;
        }
        if ($sugar_crm_version == 7) {
            $sess_id = $objSCP->access_token;
        }
        if ($sess_id != '' && is_object( $objSCP )) {
            ob_start();
            if (get_option('biztech_scp_single_signin') != '' && is_user_logged_in()) {//Added by BC on 02-aug-2016
                ob_clean();
                do_action('admin_post_bcp_login');
            }

            if (empty($_SESSION['scp_user_id'])) {
                
                $bcp_portal_login_min = fetch_data_option("bcp_lockout_effective_period");
                $bcp_maximum_login_attempts = fetch_data_option( 'bcp_maximum_login_attempts' );
                $ip = bcp_getUserIpAddr();
                $record = $wpdb->get_row("SELECT * FROM " . $wpdb->base_prefix . "bcp_login_attempts WHERE ip_address='{$ip}'");
                $login_attempt = isset($record->login_attempt) ? $record->login_attempt : 0;
                $last_login_date = isset($record->last_login_date) ? $record->last_login_date : date("Y-m-d H:i:s");
                $disabled = "";
                $bcp_last_login_date = new DateTime($last_login_date);
                $since_start = $bcp_last_login_date->diff( new DateTime() );
                $min_diff = $since_start->i;
                $sec_diff = $since_start->s;
                $bcp_login_password_enable = fetch_data_option('bcp_login_password_enable');
                
                if ( $bcp_login_password_enable && $login_attempt >= $bcp_maximum_login_attempts && $min_diff < $bcp_portal_login_min ) {
                    $disabled = "disabled='disabled'";
                    $bcp_portal_login_min -= 1;
                    $min_diff = $bcp_portal_login_min - $min_diff;
                    $sec_diff = 60 - $sec_diff;
                    if ($min_diff < 10) {
                        $min_diff = "0".$min_diff;
                    }
                } else {
                    if ( (!$bcp_login_password_enable || $login_attempt >= $bcp_maximum_login_attempts) && !empty($record) ) {
                        $wpdb->update(
                            $wpdb->base_prefix . 'bcp_login_attempts', array(
                                'ip_address' => $ip,
                                'login_attempt' => "0",
                                'last_login_date' => date("Y-m-d H:i:s"),
                                'ip_status' => "active",
                            ), array('id' => $record->id)
                        );
                    }
                }
                
                include( TEMPLATE_PATH . 'login.php');
                return ob_get_clean();
            }
        } else {
            if (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'] != '') {
                $bcp_connection_error = $_SESSION['bcp_connection_error'];
                unset($_SESSION['bcp_connection_error']);
                return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                    <strong>".$bcp_connection_error."</strong>
                </div>";
            }
        }
    }
}

if( ! function_exists('scp_profile_shortcode') ) {
    add_shortcode('bcp-profile', 'scp_profile_shortcode');
    function scp_profile_shortcode() {

        if (isset($_REQUEST['action']) && $_REQUEST['action'] == "edit") {
            return;
        }
        
        global $objSCP, $sugar_crm_version, $getContactInfo, $wpdb, $lang_code, $language_array;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
            $sess_id = $objSCP->session_id;
        }
        if ($sugar_crm_version == 7) {
            $sess_id = $objSCP->access_token;
        }
        if ($sess_id != '') {
            if (isset($_SESSION['scp_user_id']) && is_object($objSCP)) {

                $getContactInfo = $objSCP->getPortalUserInformation($_SESSION['scp_user_id']);
                if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
                    $getContactInfo = isset( $getContactInfo->entry_list[0]->name_value_list ) ? $getContactInfo->entry_list[0]->name_value_list : '';
                }
                ob_start();
                include( TEMPLATE_PATH . 'profile.php');
                return ob_get_clean();
            } else {
                if (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'] != '') {
                    $bcp_connection_error = $_SESSION['bcp_connection_error'];
                    unset($_SESSION['bcp_connection_error']);
                    return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                        <strong>".$bcp_connection_error."</strong>
                    </div>";
                }
            }
        } else {
            if (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'] != '') {
                $bcp_connection_error = $_SESSION['bcp_connection_error'];
                unset($_SESSION['bcp_connection_error']);
                return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                    <strong>".$bcp_connection_error."</strong>
                </div>";
            }
        }
    }
}

if( ! function_exists('scp_forgot_password') ) {
    add_shortcode('bcp-forgot-password', 'scp_forgot_password');
    function scp_forgot_password() {

        if (isset($_REQUEST['action']) && $_REQUEST['action'] == "edit") {
            return;
        }
        
        global $objSCP, $sugar_crm_version, $all_language, $lang_code, $language_array;
        
        if ($objSCP == NULL) {
            return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                <strong>".__('CRM Authentication failed')."</strong>
            </div>";
        }
                        
        if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
            $sess_id = $objSCP->session_id;
        }
        if ($sugar_crm_version == 7) {
            $sess_id = $objSCP->access_token;
        }
        if ($sess_id != '') {                  
            ob_start();
            if (empty($_SESSION['scp_user_id']) && is_object($objSCP)) {
                include( TEMPLATE_PATH . 'forgotpassword.php');
                return ob_get_clean();
            } else {
                if (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'] != '') {
                    $bcp_connection_error = $_SESSION['bcp_connection_error'];
                    unset($_SESSION['bcp_connection_error']);
                    return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                        <strong>".$bcp_connection_error."</strong>
                    </div>";
                }
            }
        } else {
            if (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'] != '') {
                $bcp_connection_error = $_SESSION['bcp_connection_error'];
                unset($_SESSION['bcp_connection_error']);
                return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                    <strong>".$bcp_connection_error."</strong>
                </div>";
            }
        }
    }
}

if( ! function_exists('scp_manage_page_shortcode') ) {
    add_shortcode('bcp-manage-page', 'scp_manage_page_shortcode');
    function scp_manage_page_shortcode() {

        if (isset($_REQUEST['action']) && $_REQUEST['action'] == "edit") {
            return;
        }
        
        global $objSCP, $sugar_crm_version, $wpdb, $lang_code, $language_array;
        
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        
        $sess_id = "";
        if ( $objSCP && ( $sugar_crm_version == 6 || $sugar_crm_version == 5 ) ) {
            $sess_id = $objSCP->session_id;
        }
        if ( $objSCP && $sugar_crm_version == 7 ) {
            $sess_id = $objSCP->access_token;
        }
        if ($sess_id != '') {
            if (is_object($objSCP)) {
                $language_array = scp_get_language_data($lang_code);
                if (empty($_SESSION['module_array'])) {//check session value in module array
                    $modules_array2 = $objSCP->getPortal_accessibleModules();
                    $modulesAry = array();
                    if ( isset( $modules_array2->accessible_modules ) ) {
                        foreach ($modules_array2->accessible_modules as $key_1 => $value_1) {
                            if ( isset( $value_1->module_name ) && $value_1->module_name != '' ) {
                                $modulesAry[$key_1] = $value_1->module_name;
                            }
                        }
                    }
                    $modules = $modulesAry;
                } else {
                    $modules = $_SESSION['module_array'];
                }
                include( TEMPLATE_PATH . 'bcp_manage_page.php');
                return ob_get_clean();
            } else {
                if (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'] != '') {
                    $bcp_connection_error = $_SESSION['bcp_connection_error'];
                    unset($_SESSION['bcp_connection_error']);
                    return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                        <strong>".$bcp_connection_error."</strong>
                    </div>";
                }
            }
        } else {
            if (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'] != '') {
                $bcp_connection_error = $_SESSION['bcp_connection_error'];
                unset($_SESSION['bcp_connection_error']);
                return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                    <strong>".$bcp_connection_error."</strong>
                </div>";
            }
        }
    }
}

if( ! function_exists('scp_dashboard_gui_shortcode') ) {
    add_shortcode('bcp-dashboard-gui', 'scp_dashboard_gui_shortcode');
    function scp_dashboard_gui_shortcode() {

        if (isset($_REQUEST['action']) && $_REQUEST['action'] == "edit") {
            return;
        }
        
        global $objSCP, $sugar_crm_version, $lang_code, $language_array;
        if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
            $sess_id = $objSCP->session_id;
        } else if ($sugar_crm_version == 7) {
            $sess_id = $objSCP->access_token;
        }
        if ($sess_id != '') {
            include( TEMPLATE_PATH . 'bcp_dashboard_gui.php');
            return ob_get_clean();
        } else {
            if (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'] != '') {
                $bcp_connection_error = $_SESSION['bcp_connection_error'];
                unset($_SESSION['bcp_connection_error']);
                return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                    <strong>".$bcp_connection_error."</strong>
                </div>";
            }
        }
    }
}

if( ! function_exists('scp_reset_password') ) {
    add_shortcode('bcp-reset-password', 'scp_reset_password_shortcode');
    function scp_reset_password_shortcode(){
        
        if (isset($_REQUEST['action']) && $_REQUEST['action'] == "edit") {
            return;
        }
        
        global $objSCP, $sugar_crm_version, $all_language, $lang_code, $language_array;
        
        if ($objSCP == NULL) {
            return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                <strong>".__('CRM Authentication failed')."</strong>
            </div>";
        }
        
        if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
            $sess_id = $objSCP->session_id;
        }
        if ($sugar_crm_version == 7) {
            $sess_id = $objSCP->access_token;
        }
        if ($sess_id != '') {
            ob_start();
            $token_1 = isset($_REQUEST['scp_token']) ? urlencode($_REQUEST['scp_token']) : "";
            $token_2 = rawurldecode($token_1);
            $json_token = decryptPass($token_2);
            $final_id = json_decode($json_token);
            if (!empty($objSCP) && is_object($objSCP)) {
                include( TEMPLATE_PATH . 'resetpassword.php');
                return ob_get_clean();
            } else {
                if (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'] != '') {
                    $bcp_connection_error = $_SESSION['bcp_connection_error'];
                    unset($_SESSION['bcp_connection_error']);
                    return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                        <strong>".$bcp_connection_error."</strong>
                    </div>";
                }
            }
        } else {
            if (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'] != '') {
                $bcp_connection_error = $_SESSION['bcp_connection_error'];
                unset($_SESSION['bcp_connection_error']);
                return "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                    <strong>".$bcp_connection_error."</strong>
                </div>";
            }
        }
    }
}
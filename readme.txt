=== SugarCRM/SuiteCRM Customer Portal ===
author: CRMJetty
Requires at least: 3.6.1
Tested up to: 5.2.3
Stable tag: 3.4.0

== Description ==

Reduce operational costs and improve customer satisfaction by empowering customers to get online support and get their complains and queries addressed through Customer Portal V2 that integrates the user friendly front end interface of Wordpress and the back end data sourcing from SugarCRM.


== Installation ==

= From your WordPress dashboard =

1. Visit 'Plugins > Add New'
2. Search for 'SugarCRM/SuiteCRM Customer Portal'
3. Activate SugarCRM/SuiteCRM Customer Portal from your Plugin's page.

<table class='form-table'>
    <tr>
        <th scope='row'><label><?php _e('Portal Name'); ?></label></th>
        <td><input type="text"  class="regular-text" id="txtPortalName" value="<?php echo $crmscpname; ?>" name='biztech_scp_name'></td>
    </tr>
    <input type="hidden" name="biztech_scp_sugar_crm_version" value="5">
    <tr>
        <th scope='row'><label><?php _e('CRM URL'); ?> *</label></th>
        <td><input type='text'  class='regular-text' value="<?php echo $crmresturl; ?>" name='biztech_scp_rest_url' id="sugar_crm_url">
            <br /><?php _e('Enter your URL as mentioned in User Manual'); ?>
        </td>
    </tr>

    <tr>
        <th scope='row'><label><?php _e('Username'); ?> *</label></th>
        <td><input type='text' value="<?php echo $crmusername; ?>" name='biztech_scp_username' id="sugar_username"></td>
    </tr>

    <tr>
        <th scope='row'><label><?php _e('Password'); ?> *</label></th>
        <td><input type='password'  name='biztech_scp_password' id="sugar_password"><p class="description"><strong><?php _e( 'Note: ' ); ?></strong><?php _e( 'If password is blank then authentication will not check.'); ?></p></td>
    </tr>
</table>
<?php
/**
 * Template Name : CRM Fullwidth
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 */
get_header();
sugar_crm_portal_style_and_script();
?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php
$fullwidth_class_gen = '';
global $post;
while (have_posts()) : the_post();
    $theme_name = wp_get_theme();
    if ($theme_name == "Twenty Fourteen") {
        $fullwidth_class_gen = "fullwidth-fourteen-theme";
    }
    if ($theme_name == "Twenty Fifteen") {
        $fullwidth_class_gen = "fullwidth-fifteen-theme";
    }
    if ($theme_name == "Twenty Sixteen") {
        $fullwidth_class_gen = "fullwidth-sixteen-theme";
    }
    if ($theme_name == "Twenty Seventeen") {
        $fullwidth_class_gen = "fullwidth-seventeen-theme";
    }
    if ($theme_name == "Twenty Nineteen") {
        $fullwidth_class_gen = "fullwidth-nineteen-theme";
    }
    if ($theme_name == "Twenty Twenty") {
        $fullwidth_class_gen = "fullwidth-twenty-theme";
    }
    if ($theme_name == "Twenty Twenty-One") {
        $fullwidth_class_gen = "fullwidth-twentyone-theme";
    }
    if (isset($_SESSION['bcp_auth_error']) && $_SESSION['bcp_auth_error'] != '' ) {
    ?>
        <div id="fullwidth-wrapper" class="crm_standalone sfcp-manage-page <?php echo $fullwidth_class_gen; ?>">
            <div class='alert alert-danger' id='setting-error-settings_updated'>
                <p><strong><?php _e( 'Portal is disabled. Please contact your administrator.' ); ?></strong></p>
            </div>
        </div>
        <?php
        exit();
    } else if ( get_option("biztech_redirect_manange") == $post->ID || get_option("biztech_redirect_profile") == $post->ID) {
        ?>
        <div id="fullwidth-wrapper" class="crm_fullwidth <?php echo $fullwidth_class_gen; ?>">
            <?php
            the_content();
            ?>
        </div>
        <?php
    } else {
        $sec_class = "";
        if (get_option("biztech_redirect_signup") == $post->ID) {
            $sec_class = "signup-new-section";
        }
        if ( has_post_thumbnail( $post->ID ) ){
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        }
        ?>
        <div id="fullwidth-wrapper" class="dcp_page_login crm_fullwidth <?php echo $fullwidth_class_gen; ?>">
            <div class="login-main-section" >
                <?php if(isset($image)) { ?> 
                <img src="<?php echo $image[0]; ?>" alt="<?php echo $post->ID; ?>" />
                <?php } ?>
                <section class="login-section <?php echo $sec_class; ?>">
                    <div class="container">
                        <div class="row">                                 
                            <?php
                            // Include the page content.
                            the_content();
                            ?>
                        </div>
                    </div>
                </section>	
            </div>
        </div>
        <?php
    }
    endwhile; 
    ?>
<?php get_footer();
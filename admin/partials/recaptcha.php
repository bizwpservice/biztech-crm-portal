<table class='form-table'>
    <!--Google Recptcha-->
    <tr class="hide_class">
        <th scope='row'><label><?php _e('reCaptcha Site Key'); ?></label></th>
        <td><input type="text" class="regular-text" value="<?php echo $biztech_scp_recaptcha_site_key; ?>" name="biztech_scp_recaptcha_site_key" /></td>
    </tr>
    <tr class="hide_class">
        <th scope='row'><label><?php _e('reCaptcha Secret Key'); ?></label></th>
        <td><input type="text" class="regular-text" value="<?php echo $biztech_scp_recaptcha_secret_key; ?>" name="biztech_scp_recaptcha_secret_key" /></td>
    </tr>
    <tr class="hide_class">
        <th scope='row'><label><?php _e('reCaptcha Visible') ?></label></th>
        <td>
            <select id="biztech_portal_visible_reCAPTCHA" name="biztech_portal_visible_reCAPTCHA[]" multiple="multiple" size="4">
                <?php
                $reCAPTECHA_pages = [
                    'portal-sign-up' => __("Sign up Page"),
                    'portal-login' => __("Login Page"),
                    'portal-forgot-password' => __("Forgot Password Page"),
                ];
                foreach ($reCAPTECHA_pages as $pagg_key => $pagg_val) {
                    $option = '<option value="' . ($pagg_key) . '" ';
                    if (in_array($pagg_key, $biztech_portal_visible_reCAPTCHA)) {
                        $option .= ' selected="selected"';
                    }
                    $option .= '>';
                    $option .= $pagg_val;
                    $option .= '</option>';
                    echo $option;
                }
                ?>
            </select>
        </td>
    </tr>
</table>
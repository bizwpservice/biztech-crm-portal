<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

    $calendarcallcolor = fetch_data_option('biztech_scp_calendar_calls_color');
    $calendarmeetingcolor = fetch_data_option('biztech_scp_calendar_meetings_color');
    $calendartaskscolor = fetch_data_option('biztech_scp_calendar_tasks_color');
    $cal_checked = (isset($_SESSION['scp_additional_setting']->calender) ? $_SESSION['scp_additional_setting']->calender : 0);
?>
<!-- BEGIN PAGE -->
<div class="">
    <div class="row ">
        <div class="col-md-12 col-sm-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet box blue calendar">
                <div class="portlet-title scp-action-header">
                    <div class="scp-action-header-left">
                        <h3 class="side-icon-wrapper scp-calendar-font"><em class="fa fa-calendar"></em><?php echo $language_array['lbl_calendar']; ?></h3>
                    </div>
                    

                </div>
                <?php
                if (isset($_SESSION['bcp_add_record']) && $_SESSION['bcp_add_record'] != '') {
                    echo "<span class='success' id='succid'>" . $_SESSION['bcp_add_record'] . "</span>";
                    unset($_SESSION['bcp_add_record']);
                }
                ?>
                <div class="portlet-body light-grey">
                    <div id="calendar"></div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <?php
                            if (isset($_SESSION['module_array']['Calls'])) {
                            ?>
                            <label><?php echo $_SESSION['module_array']['Calls']; ?> : </label><span class="badge" style="<?php echo 'background:' .$calendarcallcolor ; ?>"></span>
                            <?php } 
                            if (isset($_SESSION['module_array']['Meetings'])) {
                            ?>
                            <label><?php echo $_SESSION['module_array']['Meetings']; ?> : </label><span class="badge" style="<?php echo 'background:' . $calendarmeetingcolor; ?>"></span>
                            <?php } 
                            if (isset($_SESSION['module_array']['Tasks'])) {
                            ?>
                            <label><?php echo $_SESSION['module_array']['Tasks']; ?> : </label><span class="badge" style="<?php echo 'background:' . $calendartaskscolor; ?>"></span>
                            <?php } ?>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>
    </div>
</div>
<!-- END PAGE -->
<div id="eventContent" style="display:none;"><div id="eventInfo"></div></div>

<script type='text/javascript'>
    var ajax = '<?php echo admin_url('admin-ajax.php'); ?>';
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    var h = {};

    if ($('#calendar').width() <= 400) {
        $('#calendar').addClass("mobile");
        h = {
            left: 'title, prev, next',
            center: '',
            right: 'today,month,agendaWeek,agendaDay'
        };
    } else {
        $('#calendar').removeClass("mobile");
        if (App.isRTL()) {
            h = {
                right: 'title',
                center: '',
                left: 'prev,next,today,month,agendaWeek,agendaDay'
            };
        } else {
            h = {
                left: 'title',
                center: '',
                right: 'prev,next,today,month,agendaWeek,agendaDay'
            };
        }
    }
    //var a=new Date(y, m, d + 1, 19, 0) ;

    var el2 = jQuery("#calendar");
    App.blockUI(el2);

    jQuery('#calendar').fullCalendar('destroy'); // destroy the calendar
    jQuery('#calendar').fullCalendar({  //re-initialize the calendar
        disableDragging: true,
        disableResizing: true,
        defaultView: 'month',
        slotMinutes: 15,
        minTime: 8,
        header: h,
        editable: true,
        lazyFetching: true,
        axisFormat: 'h(:mm) tt',
        //timeFormat: 'h(:mm) "a" - ', // uppercase H for 24-hour clock,lowercase h for 12-hour clock, 't' for a, p while 'a' for am, pm
        buttonText: {
            today: "<?php echo $language_array['lbl_cal_today']; ?>",
            month: "<?php echo $language_array['lbl_cal_month']; ?>",
            week: "<?php echo $language_array['lbl_cal_week']; ?>",
            day: "<?php echo $language_array['lbl_cal_day']; ?>",
        },
        viewDisplay: function (view) {
            //App.blockUI(el2);
        },
        loading: function( isLoading, view ) {
            if(isLoading) { // isLoading gives boolean value
                App.blockUI(el2);
            } else {
                App.unblockUI(el2);
            }
        },
        events: {
            url: '<?php echo plugin_dir_url(__FILE__); ?>' + "json_events.php?calendar=view_calendar",
            success: function (data) {
                App.unblockUI(el2);
            }
        },
        dayRender: function(date, cell){
                nowDate=moment();
                if (date < nowDate){
                    jQuery(cell).addClass('disabled');
                }
        },
        dayClick: function(date, jsEvent, view) {
            var d1 = new Date();
            var clicked_date = (("0" + (date.getMonth() + 1)).slice(-2))+"/"+(("0" + date.getDate()).slice(-2))+"/"+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes();
            if (date.getHours() == 0 && date.getMinutes() == 0) {
                var clicked_date = (("0" + (date.getMonth() + 1)).slice(-2))+"/"+(("0" + date.getDate()).slice(-2))+"/"+date.getFullYear()+" "+d1.getHours()+":"+d1.getMinutes();
            }
            var dis_date = date.getDate();
            var dis_mon = (date.getMonth()+1);
            if (dis_date < 10) {
                dis_date = "0"+dis_date;
            }
            if (dis_mon < 10) {
                dis_mon = "0"+dis_mon;
            }
            var clicked_date_str = dis_date+"-"+dis_mon+"-"+date.getFullYear();
            //d1.setHours(0, 0, 0, 0);   //get today date and remove hours
            var d2 = new Date(clicked_date); //d2.setHours(0, 0, 0, 0);   //get selected date and remove hours
            if (date.getHours() == 0 && date.getMinutes() == 0) {
                d2.setHours(d1.getHours(), d1.getMinutes(), d1.getSeconds(), d1.getMilliseconds());
            }
            if(d1 <= d2){   //if clicked date is grater than current date
                <?php
                    if ( ( (array_key_exists( 'Calls', $_SESSION['module_array'] ) ) && ($_SESSION['module_action_array']['Calls']['create'] == 'true') ) || ( (array_key_exists( 'Meetings', $_SESSION['module_array'] ) ) && ($_SESSION['module_action_array']['Meetings']['create'] == 'true') )  || ( (array_key_exists( 'Tasks', $_SESSION['module_array'] ) ) && ($_SESSION['module_action_array']['Tasks']['create'] == 'true') )  ) {
                        ?>
                        var msg = '<span class="date"><?php echo $language_array['lbl_date'] ?>: '+clicked_date_str+'</span><select class="module_list" name="module"><?php if( (array_key_exists( 'Calls', $_SESSION['module_array'] ) ) && ($_SESSION['module_action_array']['Calls']['create'] == 'true') ) {?><option value="Calls"><?php echo $_SESSION['module_array_without_s']['Calls']; ?></option><?php } if( (array_key_exists( 'Meetings', $_SESSION['module_array'] ) ) && ($_SESSION['module_action_array']['Meetings']['create'] == 'true') ) {?><option value="Meetings"><?php echo $_SESSION['module_array_without_s']['Meetings']; ?></option><?php } if( (array_key_exists( 'Tasks', $_SESSION['module_array'] ) ) && ($_SESSION['module_action_array']['Tasks']['create'] == 'true') ) {?><option value="Tasks"><?php echo $_SESSION['module_array_without_s']['Tasks']; ?></option><?php } ?></select><input type="hidden" id="select_date_module" value="'+clicked_date+'" /><a href="javascript:void(0);" onclick="add_module_calendar();"><?php echo $language_array['lbl_add']; ?></a>';
                        <?php
                    } else {
                        ?>
                        var msg = "<?php echo $language_array['msg_no_permission_data']; ?>";
                        <?php
                    }
                ?>
                var data = '<div id="add_module_calendar">'+msg+'</div>';                
                jQuery("#eventInfo").html(data);
                jQuery("#eventContent").dialog({ modal: true, title: '<?php echo $language_array['lbl_select_activity']; ?>', width: 350, height: 150, draggable: false, resizable: false, position: { my: "center", at: "center", of: '#responsedata' }, });
                jQuery('#eventContent').parent().css({position:"fixed", top:"45%"});
                jQuery('body').addClass('scp-calander-popup-for-all');
            } else {
                
            }
        },
    });
    //for opening event in new tab
    jQuery('#calendar').on('click', '.fc-event', function (e) {
        e.preventDefault();
        //window.open(jQuery(this).attr('href'), '_blank');
        window.open(jQuery(this).attr('href'), '_self');
    });
    
    function add_module_calendar(){
        jQuery('body').removeClass('scp-calander-popup-for-all');
        jQuery('.ui-widget-overlay.ui-front').remove();
        jQuery('.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front').remove();
        var module = jQuery('select.module_list').val();
        var date = document.getElementById("select_date_module").value;
        bcp_module_call_add('0',module,"edit",'','','',date,'Calendar');
    }

</script>

<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

$current_url = explode('?', $_SERVER['REQUEST_URI'], 2);
$current_url = $current_url[0];
$biztech_redirect_login = get_page_link(get_option('biztech_redirect_login'));
if ($biztech_redirect_login != NULL) {
    $redirect_url_login = $biztech_redirect_login;
} else {
    $redirect_url_login = home_url() . "/portal-login/";
}
//to get name portal name
$biztech_scp_name = fetch_data_option('biztech_scp_name');
//to get upload logo
$biztech_upload_image = fetch_data_option('biztech_scp_upload_image');
?>

<div class=" border-0 rounded-lg w-100">
    <?php if ($biztech_upload_image != NULL) { 
        $sfcp_logo_url = wp_get_attachment_image_src( $biztech_upload_image, 'full' );
    ?>
        <div class="d-block mb-5 text-center">
            <img src="<?php echo $sfcp_logo_url[0]; ?>" alt="<?php echo $biztech_scp_name; ?>">
        </div>
    <?php } ?>
    <form action="<?php echo site_url(); ?>/wp-admin/admin-post.php" method="post" id="scpResetPwdForm">
    <h1><?php echo $language_array['lbl_portal_reset_pwd']; ?></h1>
    <?php
    if ($final_id != NULL) {
    if ($all_language != null && !empty($all_language) && count($all_language) > 1 && !isset($_SESSION['scp_otp_generated'])) {
    ?>
    <div class="dropdown">
        <button class="country-dropdown dropdown-toggle dropdown dcp_lang_dropdown" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
            <?php
            $lang_selected = "English (US)";
            $flag_selected_class = "flag-us";
            foreach ($all_language as $key => $value) {
                $country_code = $value['module'];    
                $c_code_array = explode("_", $country_code);
                $c_code = strtolower($c_code_array[1]);
                $c_class = "flag-".$c_code;
                $c_label = str_replace(" - ".$country_code, "", $value['label']);
                if ($value['module'] == "en_UK") {
                    $c_class = "flag-england";
                }
                if ($value['module'] == "el_EL") {
                    $c_class = "flag-sv";
                }
                if ($value['module'] == $lang_code) {
                    $lang_selected = $c_label;
                    $flag_selected_class = $c_class;
                }
            }
            ?>
            <span class="flag <?php echo $flag_selected_class; ?>"></span>
            <?php echo $lang_selected; ?>
            <em class="fas fa-chevron-down"></em>
        </button>
        <ul class="dropdown-menu scrollbar scrollbar-primary" aria-labelledby="dropdownMenuButton1">
            <?php
            foreach ($all_language as $key => $value) {
            $country_code = $value['module'];    
            $c_code_array = explode("_", $country_code);
            $c_code = strtolower($c_code_array[1]);
            $c_class = "flag-".$c_code;
            $lang_active = "";
            if ($value['module'] == "en_UK") {
                $c_class = "flag-england";
            }
            if ($value['module'] == "el_EL") {
                $c_class = "flag-sv";
            }
            if ($value['module'] == $lang_code) {
                $lang_active = " active_lang";
            }
            $c_label = str_replace(" - ".$country_code, "", $value['label']);
            ?>
            <li class="<?php echo $lang_active; ?>">
                <a href="javascript:void(0);" onclick="bcp_change_language('<?php echo $country_code ?>');" >
                    <span class="flag <?php echo $c_class; ?>" ></span>
                    <?php echo $c_label; ?>
                </a>
            </li>
            <?php } ?>
        </ul>
    </div>
    <?php } ?>
    
        <div class="login-input">
            <input class="form-control" type="password" name="scp_new_password" id="scp_new_password" required placeholder="<?php echo $language_array['lbl_new_pwd']; ?>*">
        </div>
        <div class="login-input">
            <input class="form-control" type="password" name="scp_cfm_password" id="scp_cfm_password" required placeholder="<?php echo $language_array['lbl_cnfm_pwd']; ?>*">
        </div>

        <div class="login-btn-group d-flex justify-content-between align-items-center">
            <input type="hidden" name="action" value="bcp_reset_password">
            <input type="hidden" name="contact_id" value="<?php echo $token_2; ?>">
            <input type="hidden" name="scp_redirect_url" value="<?php echo $biztech_redirect_login; ?>">
            <button type="submit" class="btn btn-primary"><?php echo $language_array['lbl_reset'] ?></button>
        </div>
    </form>
    <?php } else { ?>
    <div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
        <strong><?php echo $language_array['msg_url_not_valid']; ?></strong>
    </div>
    <?php } ?>
</div>

<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if( ! function_exists('twak_api_call') ) {
    add_action('wp_footer', 'twak_api_call', 99);
    function twak_api_call() {

        if (isset($_SESSION['scp_user_account_name'])) {
            ?>
            <script type="text/javascript">
                if ( typeof $_Tawk_API !== 'undefined' ) {
                    $_Tawk_API.visitor = {
                        name: '<?php echo $_SESSION['scp_user_account_name']; ?>',
                        email: '<?php echo $_SESSION['scp_user_mailid']; ?>',
                        hash: '<?php echo hash_hmac("sha256", $_SESSION['scp_user_mailid'], get_option('chatapikey')); ?>'
                    };
                }
            </script>
            <?php
        }
    }
}

if( ! function_exists('call_chat_feature') ) {
    add_action('admin_init', 'call_chat_feature');
    function call_chat_feature() {

        global $objSCP;
        //check chat enable/disable and get api key
        if ($objSCP != '' && $objSCP != NULL) {
            //date_default_timezone_set('Asia/Calcutta');
            $dateenable = date('Y-m-d H:i:s');
            if (get_option('chatenabletime') != NULL) { // if alredy option set then check time diff not grater then 24 hours
                $chatenabledate = get_option('chatenabletime');
                $timediff = strtotime($dateenable) - strtotime($chatenabledate);
                if ($timediff > 86400) { //if time diff > 24 then call again
                    if (!is_object($objSCP)) {                        
                        $objSCP = unserialize($objSCP);
                    }
                    $getchatdata = $objSCP->getChatConfiguration();
                    if ($getchatdata != NULL) {
                    update_option('chatapikey', $getchatdata->TalkApiKey);
                    update_option('chatenableflg', $getchatdata->ChatEnable);
                    update_option('chatenabletime', $dateenable);
                    }
                }
            } else {
                if (!is_object($objSCP)) {                        
                    $objSCP = unserialize($objSCP);
                }
                $getchatdata = $objSCP->getChatConfiguration();
                add_option('chatapikey', $getchatdata->TalkApiKey);
                add_option('chatenableflg', $getchatdata->ChatEnable);
                add_option('chatenabletime', $dateenable);
            }
        }
    }
}

if( ! function_exists('admin_notice_chat_error') ) {
    add_action('admin_notices', 'admin_notice_chat_error');
    function admin_notice_chat_error() {

        $checkchatflg = get_option('chatenableflg');
        //from crm chat enable and plugin not activated then only display msg
        if (($checkchatflg == 1) && (!is_plugin_active('tawkto-live-chat/tawkto.php'))) {
            ?><div class = "error notice is-dismissible">
                <p> <?php _e('To use chat feature,please install and activate ');
                        echo '<a href="https://wordpress.org/plugins/tawkto-live-chat/"> ';
                        _e('Tawk.to Live Chats');
                        echo '</a>';
                        _e('Plugin');
            ?></p>
            </div><?php
        }
    }
}

if( ! function_exists('logincall_chat_feature') ) {
    function logincall_chat_feature() {

        global $objSCP;
        //check chat enable/disable and get api key
        if ($objSCP != '' && $objSCP != NULL) {
    //        date_default_timezone_set('Asia/Calcutta');
            $dateenable = date('Y-m-d H:i:s');
            if (get_option('chatenabletime') != NULL) { // if alredy option set then check time diff not grater then 24 hours
                $chatenabledate = get_option('chatenabletime');
                $timediff = strtotime($dateenable) - strtotime($chatenabledate);
                $getchatdata = $objSCP->getChatConfiguration();
                update_option('chatapikey', $getchatdata->TalkApiKey);
                update_option('chatenableflg', $getchatdata->ChatEnable);
                update_option('chatenabletime', $dateenable);
            } else {
                $getchatdata = $objSCP->getChatConfiguration();
                add_option('chatapikey', $getchatdata->TalkApiKey);
                add_option('chatenableflg', $getchatdata->ChatEnable);
                add_option('chatenabletime', $dateenable);
            }
        }
    }
}

<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

global $lang_code;

$biztech_scp_name = fetch_data_option('biztech_scp_name');
if($biztech_scp_name != NULL){
    $biztech_scp_name = '<h1>' . $biztech_scp_name . '</h1>';
}
//to get upload logo
$biztech_upload_image = fetch_data_option('biztech_scp_upload_image');
if($biztech_upload_image != NULL){
    $sfcp_logo_url = wp_get_attachment_image_src( $biztech_upload_image, 'full' );
    $biztech_upload_image = '<img src="' . $sfcp_logo_url[0] . '" alt="'.$biztech_scp_name.'" />';
}
$lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
?>
<div class="registration-inner">
    <!--row start -->
    <div class="row">
        <div class="col-12 d-flex align-items-center justify-content-between registration-header">
            <?php if($biztech_upload_image){
                    echo $biztech_upload_image;
                } else {
                    echo $biztech_scp_name;
                }
            ?>
            <?php if ($all_language != null && !empty($all_language) && count($all_language) > 1) { ?>
            <div class="dropdown">
                <button class="country-dropdown dropdown-toggle dropdown dcp_lang_dropdown" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                    <?php
                    $lang_selected = "";
                    $flag_selected_class = "";
                    $selected_code = "";
                    foreach ($all_language as $key => $value) {
                        $country_code = $value['module'];
                        $c_code_array = explode("_", $country_code);
                        $c_code = strtolower($c_code_array[1]);
                        $c_class = "flag-".$c_code;
                        if ($value['module'] == "en_UK") {
                            $c_class = "flag-england";
                        }
                        if ($value['module'] == "el_EL") {
                            $c_class = "flag-sv";
                        }
                        $c_label = str_replace(" - ".$country_code, "", $value['label']);
                        if ($value['module'] == $lang_code) {
                            $lang_selected = $c_label;
                            $flag_selected_class = $c_class;
                            $selected_code = $c_code;
                        }
                    }
                    ?>
                    <span class="flag <?php echo $flag_selected_class; ?>"></span>
                    <em class="fas fa-chevron-down"></em>
                </button>
                <ul class="dropdown-menu scrollbar scrollbar-primary" aria-labelledby="dropdownMenuButton1">
                    <?php
                    foreach ($all_language as $key => $value) {
                    $country_code = $value['module'];    
                    $c_code_array = explode("_", $country_code);
                    $c_code = strtolower($c_code_array[1]);
                    $c_class = "flag-".$c_code;
                    $lang_active = "";
                    if ($value['module'] == "en_UK") {
                        $c_class = "flag-england";
                    }
                    if ($value['module'] == "el_EL") {
                        $c_class = "flag-sv";
                    }
                    if ($value['module'] == $lang_code) {
                        $lang_active = " active_lang";
                    }
                    $c_label = str_replace(" - ".$country_code, "", $value['label']);
                    ?>
                    <li class="<?php echo $lang_active; ?>">
                        <a href="javascript:void(0);" onclick="bcp_change_language('<?php echo $country_code ?>');" >
                        <span class="flag <?php echo $c_class; ?>" ></span>
                        <?php echo $c_label; ?>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <?php } ?>
        </div>
        <div class="col-12 registration-header-text">
            <h2 class=""><?php echo $language_array['lbl_portal_singup']; ?></h2>
        </div>
    </div>
    <!--row end -->
    <!--signup form start -->
    <div class="scp-signup-form">
        <div id="responsedata" class="scp-entry-header">
            <script>
                scp_signup_form('Contacts', 'edit');
            </script>
        </div>
        <div id="otherdata" style="display:none;"></div>
    </div>
    <!--signup form end -->
    
</div>
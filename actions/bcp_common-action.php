<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if( ! function_exists('scp_update_profile') ) {
    add_action('admin_post_bcp_update_profile', 'scp_update_profile');
    add_action('admin_post_nopriv_bcp_update_profile', 'scp_update_profile');
    function scp_update_profile() {

        global $objSCP, $sugar_crm_version,$wpdb, $lang_code, $language_array;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        if ($objSCP != NULL) {
            $username_c = sanitize_text_field($_REQUEST['username_c']);
            $email1 = sanitize_email($_REQUEST['email1']);
            $first_name = sanitize_text_field($_REQUEST['first_name']);
            $last_name = sanitize_text_field($_REQUEST['last_name']);
            $checkUserExists = $objSCP->getPortalUserExists($username_c);
            $checkEmailExists = $objSCP->getPortalEmailExists($email1);

            if (($checkUserExists) && ($checkEmailExists)) {    //if username and mail exists
                $contact_group_id = isset($_SESSION['contact_group_id']) ? $_SESSION['contact_group_id'] : "";
                $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='$sugar_crm_version' AND module_name='Contacts' and layout_view='edit' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
                $result_count = $wpdb->get_row($sql_count) or die(mysql_error());

                $json_data = $result_count->json_data;
                $results = json_decode($json_data);

                $pass_name_arry = $_REQUEST;
                $module_name = "Contacts";
                if ( $sugar_crm_version == 7 ) {
                    include( TEMPLATE_PATH . 'bcp_add_records_v7.php');
                } else {
                    include( TEMPLATE_PATH . 'bcp_add_records_v6.php');
                }
                unset($pass_name_arry['contact_photo']);
                unset($pass_name_arry['password_c']);
                $updateUserInfo = $pass_name_arry;
                unset($updateUserInfo['action']);
                unset($updateUserInfo['remove_pro_pic']);
                unset($updateUserInfo['scp_current_url']);
                unset($updateUserInfo['update-profile']);

                $updateUserInfo['id'] = $_SESSION['scp_user_id'];

                $isUpdate = $objSCP->set_entry('Contacts', $updateUserInfo);

                if($_REQUEST['remove_pro_pic'] == 'true'){
                    $filename = $_FILES['contact_photo']['name'];
                    if ( $sugar_crm_version == 7 ) {
                        $set_entry_dataArray['id'] = $_SESSION['scp_user_id'];
                        $set_entry_dataArray['deleted'] = 1;
                        $objSCP->set_entry('contacts', $set_entry_dataArray);
                        $file_path = $_FILES['contact_photo']['tmp_name'];
                    } else {
                        $file_path = base64_encode(file_get_contents($_FILES['contact_photo']['tmp_name']));
                        $isUpdate2 = $objSCP->upload_profile_photo($filename, $file_path, $_SESSION['scp_user_id'], 'remove', $lang_code);
                    }
                    $filename = plugin_dir_path( __FILE__ ).'../assets/images/user_profile_pic/'.basename($_SESSION['scp_user_profile_pic']);
                    unlink($filename);
                    $_SESSION['scp_user_profile_pic'] = '';
                }

                if($_FILES["contact_photo"]['size'] != 0){
                    $img_error = '';
                    $check = getimagesize($_FILES["contact_photo"]["tmp_name"]);
                    if($check !== false) {  //check if uploaded file is image or not
                        if ( ($check['mime'] == 'image/png') || ($check['mime'] == 'image/jpg') || ($check['mime'] == 'image/jpeg') ) {  //check uploaded file jpg,png or not
                            $image_size = $_FILES["contact_photo"]["size"];
                            if($image_size < 2000000){     //check uploaded image-file's size
                                $filename = $_FILES['contact_photo']['name'];
                                $up_action = "add";
                                if ( $sugar_crm_version == 7 ) {
                                    $file_path = $_FILES['contact_photo']['tmp_name'];
                                    $up_action = "POST";
                                } else {
                                    $file_path = base64_encode(file_get_contents($_FILES['contact_photo']['tmp_name']));
                                    $up_action = "add";
                                }
                                $isUpdate2 = $objSCP->upload_profile_photo($filename, $file_path, $_SESSION['scp_user_id'], $up_action, $lang_code);
                            } else {
                                $img_error = isset( $_SESSION['scp_admin_messages']->invalid_image_size ) ? __( $_SESSION['scp_admin_messages']->invalid_image_size ) : '';
                            }
                        } else {
                            $img_error = isset( $_SESSION['scp_admin_messages']->invalid_image_format ) ? __( $_SESSION['scp_admin_messages']->invalid_image_format ) : '';
                        }
                    } else {
                        $img_error = isset( $_SESSION['scp_admin_messages']->invalid_image_type ) ? __( $_SESSION['scp_admin_messages']->invalid_image_type ) : '';
                    }
                }

                if ($isUpdate != NULL) {
                    $_SESSION['scp_user_account_name'] = $username_c;
                    $user_id = username_exists($username_c);
                    $wp_email_exist = email_exists($email1);
                    if( $user_id && $wp_email_exist ) {
                        $user_id = wp_update_user(array(
                            'ID' => $user_id,
                            'first_name' => $first_name,
                            'last_name' => $last_name,
                        ));
                    }

                    $succmsg = isset( $_SESSION['portal_message_list']->profile_update_success ) ? __( $_SESSION['portal_message_list']->profile_update_success ) : '';
                    $redirect_url = $_REQUEST['scp_current_url'] . "?success=1";
                    $check_for_7 = (array)($isUpdate2);
                    if( ( isset($isUpdate2) && $isUpdate2->success != 1 && $sugar_crm_version != 7 ) || //for sugar6 and suitecrm
                         (isset($img_error) && $img_error != '') ||     //if any error not relate to sugar
                         (isset($isUpdate2) && (empty($check_for_7)) && $sugar_crm_version == 7 ) ){   //for sugar7
                        $succmsg = ( isset( $_SESSION['portal_message_list']->profile_image_error ) ? __( $_SESSION['portal_message_list']->profile_image_error ) : '' );
                        $redirect_url = esc_url($_REQUEST['scp_current_url']) . "?error=1";
                        $_SESSION['bcp_profile_error'] = $succmsg;
                    } else {
                        if($_FILES["contact_photo"]['size'] != 0){  //if any file uploaded
                            if( isset($_SESSION['scp_user_profile_pic']) && $_SESSION['scp_user_profile_pic'] != '' ){  //remove old file if exists
                                $filename = plugin_dir_path( __FILE__ ).'../assets/images/user_profile_pic/'.basename($_SESSION['scp_user_profile_pic']);
                                unlink($filename);
                            }
                            $profile_pic = $objSCP->get_profile_picture($_SESSION['scp_user_id'], $lang_code);  //fetch image file
                            if( (isset($profile_pic)) &&
                                    ($profile_pic->success) &&
                                    ($profile_pic->profile_photo != "") &&
                                    ( $sugar_crm_version == 6 || $sugar_crm_version == 5 )
                               ) {
                                $_SESSION['scp_user_profile_pic'] = base64_to_image($profile_pic->profile_photo, 'user_profile_pic', $_SESSION['scp_user_id']);   //set image file
                            } else if( (isset($profile_pic)) &&
                                       ($profile_pic != '') &&
                                       ($sugar_crm_version == 7)
                                     ){
                                $_SESSION['scp_user_profile_pic'] = base64_to_image($profile_pic, 'user_profile_pic', $_SESSION['scp_user_id']);  //set image file
                            } else {
                                $_SESSION['scp_user_profile_pic'] = ''; //set blank image file
                            }
                        }
                        $_SESSION['bcp_profile_succ'] = $succmsg;
                    }
                } else {
                    $succmsg = isset( $_SESSION['scp_admin_messages']->signup_error_profile_upload ) ? __( $_SESSION['scp_admin_messages']->signup_error_profile_upload ) : '';
                    $redirect_url = esc_url($_REQUEST['scp_current_url']) . "?error=1";
                    $_SESSION['bcp_profile_error'] = $succmsg;
                }
            } else {
                $succmsg = isset( $_SESSION['scp_admin_messages']->signup_error_profile_upload ) ? __( $_SESSION['scp_admin_messages']->signup_error_profile_upload ) : '';
                $redirect_url = esc_url($_REQUEST['scp_current_url']) . "?error=1";
                $_SESSION['bcp_profile_error'] = $succmsg;
            }
            wp_redirect($redirect_url);
            exit;
        } else {
            $objSCP = 0;
            $conn_err = $language_array['msg_portal_is_disabled_contact_admin'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            $redirect_url = esc_url($_REQUEST['scp_current_url']) . '?conerror=1';
            wp_redirect($redirect_url);
        }
    }
}

if( ! function_exists('scp_pagination') ) {
    function scp_pagination($total_record, $per_page = 10, $page = 1, $url = '?', $module_name='', $order_by='', $order='', $view='', $current_url='',$advance_filter='') {

        $total = $total_record;
        $adjacents = "1";

        $prevlabel = "<em class='fas fa-chevron-left'>".$language_array['lbl_prev']."</em>";
        $nextlabel = $language_array['lbl_next']."<em class='fas fa-chevron-right'></em>";
        
        $page = ($page == 0 ? 1 : $page);
        $start = ($page - 1) * $per_page;

        $prev = $page - 1;
        $next = $page + 1;

        $lastpage = ceil($total / $per_page);
        
        $total_show = $per_page + $start;
        if($lastpage == $page) {
            $total_show = $total;
        }

        $lpm1 = $lastpage - 1; // //last page minus 1
        

        $pagination = "";
        if ($lastpage > 1) {
            $pagination .= '<div class="case-footer d-flex justify-content-between flex-column flex-md-row align-items-md-center">';
            $pagination .= "<p class='m-0'>Display ".$start + 1 ." to ".$total_show." of ".$total." ".$module_name."</p>";
            $pagination .= "<nav aria-label='Page navigation example'>";
            $pagination .= "<ul class='pagination justify-content-center m-0'>";
            
            if ($page == 1) {
                $pagination.= "<li class='page-item disabled'><a class='previous-item' href='javascript:void(0);'>{$prevlabel}</a></li>";
            } else {
                if (($order_by != NULL) && ($order != NULL)) {
                    $pagination.= "<li class='page-prev page-item'><a class='previous-item' href='javascript:void(0);' onclick='bcp_module_paging($prev,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$prevlabel}</a></li>";
                } else {
                    $pagination.= "<li class='page-prev page-item'><a class='previous-item' href='javascript:void(0);' onclick='bcp_module_paging($prev,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$prevlabel}</a></li>";
                }
            }
            

            if ($lastpage < 7 + ($adjacents * 2)) {
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    if ($counter == $page) {
                        $pagination.= "<li class='page-item active'><a class='page-link current'>{$counter}</a></li>";
                    } else {
                        if (($order_by != NULL) && ($order != NULL)) {
                            $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$counter}</a></li>";
                        } else {
                            $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$counter}</a></li>";
                        }
                    }
                }
            } elseif ($lastpage > 5 + ($adjacents * 2)) {

                if ($page < 1 + ($adjacents * 2)) {

                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page) {
                            $pagination.= "<li class='page-item active'><a class='page-link current'>{$counter}</a></li>";
                        } else {
                            if (($order_by != NULL) && ($order != NULL)) {
                                $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$counter}</a></li>";
                            } else {
                                $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$counter}</a></li>";
                            }
                        }
                    }

                    if (($order_by != NULL) && ($order != NULL)) {
                        $pagination.= "<li class='page-item dot d-flex align-items-center'>...</li>";
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($lpm1,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$lpm1}</a></li>";
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($lastpage,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$lastpage}</a></li>";
                    } else {
                        $pagination.= "<li class='page-item dot d-flex align-items-center'>...</li>";
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($lpm1,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$lpm1}</a></li>";
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($lastpage,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$lastpage}</a></li>";
                    }
                } elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {

                    if (($order_by != NULL) && ($order != NULL)) {
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging(1,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>1</a></li>";
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging(2,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>2</a></li>";
                        $pagination.= "<li class='page-item dot d-flex align-items-center'>...</li>";
                    } else {
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging(1,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>1</a></li>";
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging(2,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>2</a></li>";
                        $pagination.= "<li class='page-item dot d-flex align-items-center'>...</li>";
                    }

                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                        if ($counter == $page) {
                            $pagination.= "<li class='page-item active'><a class='page-link current'>{$counter}</a></li>";
                        } else {
                            if (($order_by != NULL) && ($order != NULL)) {
                                $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$counter}</a></li>";
                            } else {
                                $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$counter}</a></li>";
                            }
                        }
                    }

                    if (($order_by != NULL) && ($order != NULL)) {
                        $pagination.= "<li class='page-item dot d-flex align-items-center'>..</li>";
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($lpm1,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$lpm1}</a></li>";
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($lastpage,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$lastpage}</a></li>";
                    } else {
                        $pagination.= "<li class='page-item dot d-flex align-items-center'>..</li>";
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($lpm1,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$lpm1}</a></li>";
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($lastpage,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$lastpage}</a></li>";
                    }
                } else {

                    if (($order_by != NULL) && ($order != NULL)) {
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging(1,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>1</a></li>";
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging(2,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>2</a></li>";
                        $pagination.= "<li class='page-item dot d-flex align-items-center'>..</li>";
                    } else {
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging(1,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>1</a></li>";
                        $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging(2,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>2</a></li>";
                        $pagination.= "<li class='page-item dot d-flex align-items-center'>..</li>";
                    }
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                        if ($counter == $page) {
                            $pagination.= "<li class='page-item active'><a class='page-link current'>{$counter}</a></li>";
                        } else {
                            if (($order_by != NULL) && ($order != NULL)) {
                                $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$counter}</a></li>";
                            } else {
                                $pagination.= "<li class='page-item'><a class='page-link' href='javascript:void(0);' onclick='bcp_module_paging($counter,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$counter}</a></li>";
                            }
                        }
                    }
                }
            }

            if ($page == $counter - 1) {
                $pagination.= "<li class='page-item disabled'><a class='next-item' href='javascript:void(0);'>{$nextlabel}</a></li>";
            } else {
                if (($order_by != NULL) && ($order != NULL)) {
                    $pagination.= "<li class='page-item page-next'><a class='next-item' href='javascript:void(0);' onclick='bcp_module_paging($next,\"" . $module_name . "\",0,\"$order_by\",\"$order\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$nextlabel}</a></li>";
                } else {
                    $pagination.= "<li class='page-item page-next'><a class='next-item' href='javascript:void(0);' onclick='bcp_module_paging($next,\"" . $module_name . "\",\"\",\"\",\"\",\"$view\",\"$current_url\",\"$advance_filter\");'>{$nextlabel}</a></li>";
                }
            }

            $pagination.= "</ul>";
            $pagination.= "</nav>";
            $pagination.= "</div>";
        }

        return $pagination;
    }
}

if( ! function_exists('scp_get_note_attachment') ) {
    add_action('admin_post_bcp_get_note_attachment', 'scp_get_note_attachment');
    add_action('admin_post_nopriv_bcp_get_note_attachment', 'scp_get_note_attachment');
    function scp_get_note_attachment() {

        global $objSCP, $sugar_crm_version;
        $redirectURL_manage = get_page_link(get_option('biztech_redirect_manange'));
        if ( $objSCP != NULL || $objSCP != 0 ) {

            if ( !isset($_REQUEST['scp_note_field']) || !isset($_REQUEST['scp_note_id']) || !isset($_SESSION['scp_user_id']) ) {
                wp_redirect($redirectURL_manage);
                exit();
            }

            $note_id = sanitize_text_field($_REQUEST['scp_note_id']);
            $note_field = sanitize_text_field($_REQUEST['scp_note_field']);
            $view = (isset($_REQUEST['scp_note_view'])) ? $_REQUEST['scp_note_view'] : "";
            $case_id = (isset($_REQUEST['scp_note_case_id'])) ? $_REQUEST['scp_note_case_id'] : "";
            if($sugar_crm_version == 6 || $sugar_crm_version == 5){
                $objSCP->getNoteAttachment($note_id,$view,$case_id);
            }
            if($sugar_crm_version == 7){    // New call for note module and kbdocument download
                $module_name = "Notes";
                $relation_name = strtolower($module_name);
                $where_con = array(
                    "id" => array(
                        '$equals' => $note_id,
                    ),
                );
                if ($view != "kb") {
                    if ($case_id != "") {
                        $module_name = "Cases";
                        $relation_name = strtolower($module_name);
                        $where_con = array(
                            "id" => array(
                                '$equals' => $case_id,
                            ),
                        );
                        $list_result = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], $relation_name, 'id', $where_con, '', '', 'date_entered:DESC');
                    } else {
                        $list_result = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], $relation_name, 'id', $where_con, '', '', 'date_entered:DESC');
                    }
                    if ($list_result->records == NULL) {
                        wp_redirect($redirectURL_manage."#access_denied");
                        exit();
                    }
                }
                $data = $objSCP->getfile($note_id,'Notes',$note_field,$view);
            }
        } else {
            wp_redirect($redirectURL_manage);
            exit();
        }
    }
}

if( ! function_exists('portal_password_reset') ) {
    add_action('after_password_reset', 'portal_password_reset', 1, 2);
    function portal_password_reset($user, $new_pass) {

        global $objSCP,$sugar_crm_version;
        if (isset($_REQUEST['pass1']) && !empty($_REQUEST['pass1'])) {
            $user_ID = $user->ID;
            $user_email = $user->user_email;
            $user_name = $user->user_login;
            $new_pass_post =  sanitize_text_field($_REQUEST['pass1']);

            $isLogin = $objSCP->PortalLogin($user_name, $user_email, 1);
            if($sugar_crm_version == 6 || $sugar_crm_version == 5){
                $Record_obj = $isLogin->entry_list[0];
            }
            if($sugar_crm_version == 7){
               $Record_obj = $isLogin->records[0];
            }
            if ($Record_obj != NULL) {
                $scp_user_id = $Record_obj->id;
                $updateUserInfo = array(
                    'id' => $scp_user_id,
                    'password_c' => $new_pass_post,
                );
                $isUpdate = $objSCP->set_entry('Contacts', $updateUserInfo);
            }
        }
    }
}

if( ! function_exists('portal_profile_update') ) {
    add_action('profile_update', 'portal_profile_update', 999, 2);
    function portal_profile_update($user_id, $old_user_data) {

        global $objSCP,$sugar_crm_version,$wpdb;
        $user = get_userdata($user_id);
        $user_first_name = $user->first_name;
        $user_last_name = $user->last_name;
        $new_user_email = $user->user_email;
        $new_pass = isset( $_REQUEST['pass1'] ) ? sanitize_text_field($_REQUEST['pass1']) : '';
        $user_name = $user->user_login;
        $old_user_email = $old_user_data->user_email;
        if( isset($_REQUEST['password_1']) && !empty($_REQUEST['password_1']) ){    //if user update password using woocommerce
            $new_pass = sanitize_text_field($_REQUEST['password_1']);
        }
        // Check email exist in wp and portal
        //check portal mail
        if($objSCP){
            $checkEmailExists = $objSCP->getPortalEmailExists($new_user_email,$user_name);

            if ($checkEmailExists){
                $wpdb->query("update ".$wpdb->prefix."users set user_email='".$old_user_email."' where ID='".$user_id."'");
                if(current_user_can('administrator')){
                    $append_url = "&email_error=1";
                }else{
                    $append_url = "?email_error=1";
                }
                $redirct = $_SERVER['HTTP_REFERER'].$append_url;
                wp_redirect($redirct);
                die();
            } else {
                $isLogin = $objSCP->PortalLogin($user_name, $old_user_email, 1);
                if($sugar_crm_version == 6 || $sugar_crm_version == 5){
                    $Record_obj = $isLogin->entry_list[0];
                }
                if($sugar_crm_version == 7){
                   $Record_obj = $isLogin->records[0];
                }
                if ($Record_obj != NULL) {
                    $scp_user_id = $Record_obj->id;
                    $updateUserInfo = array(
                        'id' => $scp_user_id,
                        'first_name' => $user_first_name,
                        'last_name' => $user_last_name,
                        'email1' => $new_user_email,
                    );
                    if (isset($new_pass) && !empty($new_pass)) {
                        $updateUserInfo['password_c'] = $new_pass;
                    }
                    $isUpdate = $objSCP->set_entry('Contacts', $updateUserInfo);
                }
            }
        }
    }
}

if(isset($_REQUEST['email_error']) && $_REQUEST['email_error']!='') {
   add_action( 'admin_notices', 'bcp_admin_notice__success' );
}

if( ! function_exists('bcp_admin_notice__success') ) {
    function bcp_admin_notice__success() {
        ?>
        <div class="notice notice-error">
            <p><strong><?php _e( 'ERROR' ); ?></strong>: <?php echo ( isset( $_SESSION['portal_message_list']->email_exists_error ) ? __( $_SESSION['portal_message_list']->email_exists_error ) : '' ); ?></p>
        </div>
        <?php
    }

}

if( ! function_exists('base64_to_image') ) {
    function base64_to_image($data, $image_type, $image_name) { // row data, image type = user_image or product_image, image name

        $data = str_replace(' ', '+', $data);
        $data = base64_decode($data);
        $path = plugin_dir_path( __FILE__ );
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $data, FILEINFO_MIME_TYPE);
        $extension = str_replace('image/','.',$mime_type);
        $file = $path.'../assets/images/' . $image_type . '/' . $image_name . $extension;
        $success = file_put_contents($file, $data);
        if($success != 0){  //we will get diffent differnt id each time, only get 0 if image can't set or get
            $url = IMAGES_URL . $image_type . '/' . $image_name .$extension;
        } else {
            $url = '';
        }
        return $url;
    }
}

if( ! function_exists('is_pro_img_exist') ) {
    function is_pro_img_exist($url){

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($code == 200){
           $status = true;
        }else{
          $status = false;
        }
        curl_close($ch);
        return $status;
    }
}

if ( !function_exists('check_permission')) {

    function check_permission($module_name, $view, $id=""){
        global $objSCP, $wpdb, $sugar_crm_version, $custom_relationships;
        $custom_relationships = $_SESSION['scp_custom_relationships'];
            if (isset($_SESSION['module_action_array'][$module_name])) {
                if ($view != "list") {
                    if ((isset($_SESSION['module_action_array'][$module_name][$view]) && $_SESSION['module_action_array'][$module_name][$view] == 'true') || $view == 'view') {
                        if ($id != "") {
                            $relation_name = ( isset( $custom_relationships[$module_name]["Contacts"] ) && $custom_relationships[$module_name]["Contacts"] != '' ) ? $custom_relationships[$module_name]["Contacts"] : strtolower( $module_name );

                            if ($sugar_crm_version != '7') {
                                $where_con = strtolower($module_name) . ".id = '{$id}'";
                                $fields = array();
                                if ($module_name=="bc_proposal" || $module_name=="Calls" || $module_name=="Meetings") {
                                    $fields = array("status","name");
                                }

                                    $record_detail = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], $relation_name, $fields, $where_con);

                                if ( $record_detail->entry_list == NULL ) {
                                    if ( ( $module_name == 'Meetings' || $module_name == 'Calls' ) && $view == "view" || (!array_key_exists($module_name, $custom_relationships[$module_name]))) {
                                        $record_detail = $objSCP->get_entry_list($module_name, $where_con);
                                        if (isset($record_detail->entry_list[0]->name_value_list->portal_visible->value) && $record_detail->entry_list[0]->name_value_list->portal_visible->value != '1') {
                                            wp_die();
                                        }
                                    } else {
                                        wp_die();
                                    }
                                } else {
                                    $status = "";
                                    if (isset( $record_detail->entry_list[0]->name_value_list->status->value ) && ($module_name=="bc_proposal" || $module_name=="Calls" || $module_name=="Meetings")) {
                                        $status = strtolower($record_detail->entry_list[0]->name_value_list->status->value);
                                        if ($status != "pending" && $status != "requested" && $status != "not held" && $view != "view") {
                                            wp_die();
                                        }
                                    }
                                }
                            } else {
                                $where_con = array(
                                    "id" => array(
                                        '$equals' => $id,
                                    ),
                                );
                                $fields = "id";
                                if ($module_name=="Calls" || $module_name=="Meetings") {
                                    $fields .= ",status";
                                }
                                if ($module_name=="bc_proposal") {
                                    $fields .= ",proposal_status";
                                }
                                $list_result = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], $relation_name, $fields, $where_con, '', '', 'date_entered:DESC');
                                if ($list_result->records == NULL) {
                                    
                                    $record_detail = $objSCP->get_entry_list_custom($module_name, $where_con);
                                    if (isset($record_detail->entry_list[0]->name_value_list->portal_visible->value) && $record_detail->entry_list[0]->name_value_list->portal_visible->value != '1') {
                                            wp_die();
                                        }
                                } else {
                                    $status = "";
                                    if ($module_name=="bc_proposal" || $module_name=="Calls" || $module_name=="Meetings") {
                                        if ($module_name=="bc_proposal") {
                                            $status = strtolower($list_result->records[0]->proposal_status);
                                        } else {
                                            $status = strtolower($list_result->records[0]->status);
                                        }
                                        if ($status != "pending" && $status != "requested" && $status != "not held" && $view != "view") {
                                            wp_die();
                                        }
                                    }
                                }
                            }
                        }
                        
                        if ((strtolower($module_name) == 'accounts' && $view == "create") || ($custom_relationships[$module_name]['Type'] == 'many-to-one' && $view == "create")) {
                            
                            $relation_name = ( isset( $custom_relationships[$module_name]["Contacts"] ) && $custom_relationships[$module_name]["Contacts"] != '' ) ? $custom_relationships[$module_name]["Contacts"] : strtolower( $module_name );
                            if ($sugar_crm_version != '7') {
                                $record_detail = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], $relation_name, '');                                
                                if ( $record_detail->entry_list != NULL ) {
                                    wp_die();
                                }
                            } else {
                                $list_result = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], $relation_name, 'id', array(), '', '', 'date_entered:DESC');
                                if ($list_result->records != NULL) {
                                    wp_die();
                                }
                            }

                        }
                    } else {
                        wp_die();
                    }
                }
            } else {
                wp_die();
            }
    }
}
// function created by akshay for changing password
if( ! function_exists('scp_update_pwd') ) {

    add_action('wp_ajax_bcp_update_pwd', 'scp_update_pwd');
    add_action('wp_ajax_nopriv_bcp_update_pwd', 'scp_update_pwd');
    function scp_update_pwd(){
        global $objSCP, $sugar_crm_version;
        if ($objSCP != NULL) {

            $new_pwd = $_REQUEST['new_password'];
            $old_pwd = $_REQUEST['old_password'];
            $user_id = $_SESSION['scp_user_id'];
            $email = "";
            
            if ($sugar_crm_version == 7){
                $data = $objSCP->getRecordDetail('Contacts',$user_id);
                $current_pwd = decryptPass($data->password_c);
                $email = $data->email1;
            } else {
                $data = $objSCP->get_entry('Contacts',$user_id);
                $current_pwd = decryptPass($data->entry_list[0]->name_value_list->password_c->value);
                $email = $data->entry_list[0]->name_value_list->email1->value;
            }
            if ($current_pwd == $old_pwd) {
                $set_entry_dataArray['id'] = $_SESSION['scp_user_id'];
                $set_entry_dataArray['password_c'] = encryptPass($new_pwd);
                $result = $objSCP->set_entry('Contacts',$set_entry_dataArray);
                $user = get_user_by('email',$email);
                $userid = $user->ID;
                if( $userid ) {
                    $userid = wp_update_user(array(
                        'ID' => $userid,
                        'user_pass' => $new_pwd,
                    ));
                }
                echo "1";
            } else {
                echo "-1";
            }
            wp_die();
        } else {
            echo "0";
            wp_die();
        }
    }
}

// function created by dharti for changing profile image
if( ! function_exists('scp_update_profile_img') ) {

//    add_action('wp_ajax_bcp_update_profile_img', 'scp_update_profile_img');
//    add_action('wp_ajax_nopriv_bcp_update_profile_img', 'scp_update_profile_img');
    add_action('admin_post_bcp_update_profile_img', 'scp_update_profile_img');
    add_action('admin_post_nopriv_bcp_update_profile_img', 'scp_update_profile_img');
    function scp_update_profile_img(){
        global $objSCP, $lang_code,$sugar_crm_version;
        if ($objSCP != NULL) {

            $user_id = $_SESSION['scp_user_id'];
            
            if($_REQUEST['remove_profile_pic'] == 'true'){
                $filename = $_FILES['contact_photo']['name'];
                if ( $sugar_crm_version == 7 ) {
                    $set_entry_dataArray['id'] = $_SESSION['scp_user_id'];
                    $set_entry_dataArray['deleted'] = 1;
                    $objSCP->set_entry('contacts', $set_entry_dataArray);
                    $file_path = $_FILES['contact_photo']['tmp_name'];
                } else {
                    $file_path = base64_encode(file_get_contents($_FILES['contact_photo']['tmp_name']));
                    $isUpdate2 = $objSCP->upload_profile_photo($filename, $file_path, $_SESSION['scp_user_id'], 'remove', $lang_code);
                }
                $filename = plugin_dir_path( __FILE__ ).'../assets/images/user_profile_pic/'.basename($_SESSION['scp_user_profile_pic']);
                unlink($filename);
                $_SESSION['scp_user_profile_pic'] = '';
            }
            
            if($_FILES["contact_photo"]['size'] != 0){
                $img_error = '';
                $check = getimagesize($_FILES["contact_photo"]["tmp_name"]);
                if($check !== false) {  //check if uploaded file is image or not
                    if ( ($check['mime'] == 'image/png') || ($check['mime'] == 'image/jpg') || ($check['mime'] == 'image/jpeg') ) {  //check uploaded file jpg,png or not
                        $image_size = $_FILES["contact_photo"]["size"];
                        if($image_size < 2000000){     //check uploaded image-file's size
                            $filename = $_FILES['contact_photo']['name'];
                            $up_action = "add";
                            if ( $sugar_crm_version == 7 ) {
                                $file_path = $_FILES['contact_photo']['tmp_name'];
                                $up_action = "POST";
                            } else {
                                $file_path = base64_encode(file_get_contents($_FILES['contact_photo']['tmp_name']));
                                $up_action = "add";
                            }
                            $isUpdate2 = $objSCP->upload_profile_photo($filename, $file_path, $_SESSION['scp_user_id'], $up_action, $lang_code);
                        } else {
                            $img_error = isset( $_SESSION['scp_admin_messages']->invalid_image_size ) ? __( $_SESSION['scp_admin_messages']->invalid_image_size ) : '';
                        }
                    } else {
                        $img_error = isset( $_SESSION['scp_admin_messages']->invalid_image_format ) ? __( $_SESSION['scp_admin_messages']->invalid_image_format ) : '';
                    }
                } else {
                    $img_error = isset( $_SESSION['scp_admin_messages']->invalid_image_type ) ? __( $_SESSION['scp_admin_messages']->invalid_image_type ) : '';
                }
            }
            
                    $succmsg = isset( $_SESSION['portal_message_list']->profile_update_success ) ? __( $_SESSION['portal_message_list']->profile_update_success ) : '';
                    $redirect_url = $_REQUEST['scp_current_url'] . "?success=1";
                    $check_for_7 = (array)($isUpdate2);
                    if( ( isset($isUpdate2) && $isUpdate2->success != 1 && $sugar_crm_version != 7 ) || //for sugar6 and suitecrm
                         (isset($img_error) && $img_error != '') ||     //if any error not relate to sugar
                         (isset($isUpdate2) && (empty($check_for_7)) && $sugar_crm_version == 7 ) ){   //for sugar7
                        $succmsg = ( isset( $_SESSION['portal_message_list']->profile_image_error ) ? __( $_SESSION['portal_message_list']->profile_image_error ) : '' );
                        $redirect_url = esc_url($_REQUEST['scp_current_url']) . "?error=1";
                        $_SESSION['bcp_profile_img_error'] = $succmsg;
                    } else {
                        if($_FILES["contact_photo"]['size'] != 0){  //if any file uploaded
                            if( isset($_SESSION['scp_user_profile_pic']) && $_SESSION['scp_user_profile_pic'] != '' ){  //remove old file if exists
                                $filename = plugin_dir_path( __FILE__ ).'../assets/images/user_profile_pic/'.basename($_SESSION['scp_user_profile_pic']);
                                unlink($filename);
                            }
                            $profile_pic = $objSCP->get_profile_picture($_SESSION['scp_user_id'], $lang_code);  //fetch image file
                            if( (isset($profile_pic)) &&
                                    ($profile_pic->success) &&
                                    ($profile_pic->profile_photo != "") &&
                                    ( $sugar_crm_version == 6 || $sugar_crm_version == 5 )
                               ) {
                                $_SESSION['scp_user_profile_pic'] = base64_to_image($profile_pic->profile_photo, 'user_profile_pic', $_SESSION['scp_user_id']);   //set image file
                            } else if( (isset($profile_pic)) &&
                                       ($profile_pic != '') &&
                                       ($sugar_crm_version == 7)
                                     ){
                                $_SESSION['scp_user_profile_pic'] = base64_to_image($profile_pic, 'user_profile_pic', $_SESSION['scp_user_id']);  //set image file
                            } else {
                                $_SESSION['scp_user_profile_pic'] = ''; //set blank image file
                            }
                        }
                        $_SESSION['bcp_profile_img_succ'] = $succmsg;
                    }
            
            wp_redirect($redirect_url);
            exit;
        } else {
            echo "0";
            wp_die();
        }
    }
}


/* metod to encrypt password for crm using "aes-256-cbc" method. * @param type $password password * @return type */
if( ! function_exists('encryptPass') ) {
    function encryptPass($password)
    {
        $sSalt = '20adeb83e85f03cfc84d0fb7e5f4d290';
        $sSalt = substr(hash('sha256', $sSalt, true), 0, 32);
        $method = 'aes-256-cbc';

        $iv = chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0);

        $encrypted = base64_encode(openssl_encrypt($password, $method, $sSalt,
                OPENSSL_RAW_DATA, $iv));
        return $encrypted;
    }
}

/*
 *  Password Decryption Method
 */
if( ! function_exists('decryptPass') ) {
    function decryptPass($password)
    {
        $sSalt = '20adeb83e85f03cfc84d0fb7e5f4d290';
        $sSalt = substr(hash('sha256', $sSalt, true), 0, 32);
        $method = 'aes-256-cbc';

        $iv = chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0).chr(0x0);

        $decrypted = openssl_decrypt(base64_decode($password), $method, $sSalt,
            OPENSSL_RAW_DATA, $iv);
        return $decrypted;
    }
}

/**
 * Setting page for notification
 */
if ( !function_exists('scp_settings_page') ) {
    add_action('wp_ajax_scp_settings_page', 'scp_settings_page');
    add_action('wp_ajax_nopriv_scp_settings_page', 'scp_settings_page');

    function scp_settings_page(){
        global $wpdb,$sugar_crm_version, $lang_code, $language_array;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        $contact_id = $_SESSION['scp_user_id'];
        $sql = "SELECT * FROM ".$wpdb->prefix . "scp_portal_contact_info WHERE contact_id = '".$contact_id."'";
        $result = $wpdb->get_row($sql);
        $notification_permission = json_decode($result->notification_permission);
        $dashboard_pref = json_decode($result->dashboard_preferences);
        $notification_action = array(
            'new_record' => $language_array['lbl_create'],
            'status_change' => $language_array['lbl_status_change'],
        );
        include( TEMPLATE_PATH . 'scp_contact_settings.php');
        wp_die();
    }
}

// function created by akshay for updating notification preferences
if( ! function_exists('scp_update_notification_settings') ) {

    add_action('wp_ajax_bcp_update_notification_settings', 'scp_update_notification_settings');
    add_action('wp_ajax_nopriv_bcp_update_notification_settings', 'scp_update_notification_settings');

    function scp_update_notification_settings(){
        global $wpdb,$sugar_crm_version,$objSCP, $lang_code, $language_array;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);

        /* Dashboard preferences added */
        $dashboard_pref = array();
        //Counter blocks
        if (isset($_REQUEST['scp_contact_counter_blocks_modules'])) {
            $dashboard_pref['scp_contact_counter_blocks_modules'] = $_REQUEST['scp_contact_counter_blocks_modules'];
        }

        //Recent blocks
        if (isset($_REQUEST['scp_contact_recent_blocks_modules'])) {
            $dashboard_pref['scp_contact_recent_blocks_modules'] = $_REQUEST['scp_contact_recent_blocks_modules'];
        }

        //Charts blocks
        if (isset($_REQUEST['scp_contact_charts_blocks_modules'])) {
            $dashboard_pref['scp_contact_charts_blocks_modules'] = $_REQUEST['scp_contact_charts_blocks_modules'];
        }

        //Schedule blocks
        if (isset($_REQUEST['scp_contact_enable_schedule_blocks'])) {
            $dashboard_pref['scp_contact_enable_schedule_blocks'] = "1";
        }


        $notification = $_REQUEST['scp_notification_permission'];
        $contact_id = $_SESSION['scp_user_id'];
        $noti_table = $wpdb->prefix . "scp_portal_contact_info";
        $sql = "SELECT * FROM ".$noti_table . " WHERE contact_id = '".$contact_id."'";
        $result = $wpdb->get_row($sql);

        $noti_result = $objSCP->save_user_preference_notification($notification);

        if ($result == NULL) {
            $wpdb->insert($noti_table, array(
                'contact_id' => $contact_id,
                'notification_permission' => json_encode($notification),
                'dashboard_preferences' => json_encode($dashboard_pref),
            ));
        } else {
            $wpdb->update(
                    $noti_table,
                    array(
                        'contact_id' => $contact_id,
                        'notification_permission' => json_encode($notification),	// integer (number)
                        'dashboard_preferences' => json_encode($dashboard_pref),
                    ),
                    array( 'contact_id' => $contact_id ),
                    array(
                        '%s',	// value1
                        '%s'	// value2
                    ),
                    array( '%s' )
            );
        }
        $_SESSION['preferences_success'] = isset( $_SESSION['scp_admin_messages']->preferences_success ) ? __( $_SESSION['scp_admin_messages']->preferences_success ) : 'Preferences update successfully';
        echo 1;
        exit();
    }
}

/**
 * Notifications page for v3.0
 */
if ( !function_exists('scp_notifications_page') ) {
    add_action('wp_ajax_scp_notifications_page', 'scp_notifications_page');
    add_action('wp_ajax_nopriv_scp_notifications_page', 'scp_notifications_page');

    function scp_notifications_page(){
        global $wpdb,$sugar_crm_version,$objSCP, $lang_code, $language_array;
        if (!isset($_SESSION['scp_user_id'])) {
            echo "-1";
            wp_die();
        }
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        $notification_action = array(
            'new_record'    => $language_array['lbl_create'],
            'status_change' => $language_array['lbl_status_change'],
            'case_update'   => $language_array['lbl_case_update'],
        );
        $scp_modules = $_SESSION['module_array'];
        $notification_modules = $scp_modules;
        if ($sugar_crm_version == 7) {
            unset($notification_modules['Accounts']);
            unset($notification_modules['KBContents']);
            unset($notification_modules['Notes']);
            unset($notification_modules['ProductTemplates']);
        } else {
            unset($notification_modules['Accounts']);
            unset($notification_modules['AOS_Contracts']);
            unset($notification_modules['AOK_KnowledgeBase']);
            unset($notification_modules['Notes']);
            unset($notification_modules['AOS_Products']);
        }
        
        if (!isset($notification_modules['Cases'])) {
            unset($notification_action['case_update']);
        }

        $module_name = $_REQUEST['modulename'];
        $limit = fetch_data_option('biztech_scp_case_per_page');
        $page_no = isset($_POST['page_no']) ? intval( $_POST['page_no'] ) : 1;
        $offset = ($page_no * $limit) - $limit;
        $offset = ( $offset < 0 ) ? '0' : $offset ;
        $current_url = isset($_POST['current_url']) ? esc_url( $_POST['current_url'] ) : '';
        $pagination_url = "?";
        $order_by = 'date';
        $order = isset($_REQUEST['order']) ? $_REQUEST['order'] : '';
        $view1 = "";
        $contact_id = $_SESSION['scp_user_id'];
        $noti_table = $wpdb->prefix . "scp_portal_contact_info";

        //Get Notification count and display in popup
        $sql = "SELECT * FROM ".$noti_table . " WHERE contact_id = '".$contact_id."'";
        $contact_noti_db = $wpdb->get_row($sql);
        $contact_noti_count = (!empty($contact_noti_db)) ? $contact_noti_db->total_notification : "0";
        if ($contact_noti_count == "0" && isset($_REQUEST['view']) && $_REQUEST['view'] == 'popup') {
                echo "0";
                wp_die();
        }
        $where_args = array();
        if (isset($_REQUEST['noti_type']) && $_REQUEST['noti_type'] != '0') {
            $where_args['notification_type'] = $_REQUEST['noti_type'];
        }
        if (isset($_REQUEST['noti_module']) && $_REQUEST['noti_module'] != '0') {
            $where_args['module'] = $_REQUEST['noti_module'];
        }
        if (isset($_REQUEST['noti_from_date']) && $_REQUEST['noti_from_date'] != '') {
            $where_args['date_entered']['start_date'] = $_REQUEST['noti_from_date'];
        }
        if (isset($_REQUEST['noti_to_date']) && $_REQUEST['noti_to_date'] != '') {
            $where_args['date_entered']['end_date'] = $_REQUEST['noti_to_date'];
        }
        if (isset($_REQUEST['notification_status']) && $_REQUEST['notification_status'] != '') {
            $where_args['notification_status'] = $_REQUEST['notification_status'];
        }
        if (isset($_REQUEST['view']) && $_REQUEST['view'] == 'popup') {
            $limit = ($contact_noti_count > 5) ? "5" : $contact_noti_count;
            $where_args['notification_status'] = "unread";
        }

        //24-09-2018 - change to get only accessible notification list
        $scp_key_modules = array_keys($scp_modules);

        $wpdb->update(
                $noti_table,
                array(
                        'total_notification' => 0,	// integer (number)
                ),
                array( 'contact_id' => $contact_id ),
                array(
                        '%d',	// value1
                ),
                array( '%s' )
        );
        //get notifications lists from crm
        $notification_result = $objSCP->get_notification_list($offset,$limit, $order, $where_args, $scp_key_modules, $lang_code);
        $notificationlist = isset($notification_result->notification_records) ? $notification_result->notification_records : '';
        $countCases = $notification_result->total_notification_count;

        if (isset($_REQUEST['view']) && $_REQUEST['view'] == 'popup') {
            if ($notificationlist == "") {
                echo "0";
                wp_die();
            } else {
                $html = "";
                $noti_count = count($notificationlist);
                $redirectURL_manage = "";
                if (get_page_link(get_option('biztech_redirect_manange')) != NULL) {
                    $redirectURL_manage = get_page_link(get_option('biztech_redirect_manange'));
                }
                else {
                    $redirectURL_manage = home_url() . "/portal-manage-page/";
                }
                $onclick = "";
                if (isset($_REQUEST['current_url']) ) {
                    $redirectURL_manage = "javascript:void(0);";
                    $onclick = "onclick='window.location.reload(true);'";
                }
                 $html .= "<li class='scp-popup-notification-count'>
                            <span><strong>".$contact_noti_count." ".$language_array['lbl_pending']."</strong> ".$language_array['lbl_notifications']."</span>
                            <a href='".$redirectURL_manage."#data/Notifications' $onclick >".$language_array['lbl_view_all']."</a>
                           </li>";
                 $html .= "<ul class='scp-standalone-drop-down-notification'>";
                if(!empty($notificationlist)){
                    foreach ($notificationlist as $value) {
                        $onclick = "";
                        $today = new DateTime();
                        $val_date = $value->date_entered;
                        $UTC = new DateTimeZone("UTC");
                        $scp_noti_date = new DateTime($val_date, $UTC);
                        $scp_noti_date_diff = $today->diff($scp_noti_date);
                        $diff_y = $scp_noti_date_diff->y;
                        $diff_m = $scp_noti_date_diff->m;
                        $diff_d = $scp_noti_date_diff->d;
                        $diff_h = $scp_noti_date_diff->h;
                        $diff_i = $scp_noti_date_diff->i;
                        $noti_date_string = "";
                        if ($diff_y != 0) {
                            $noti_date_string .= $diff_y." years";
                        } else if ($diff_m != 0) {
                            $noti_date_string .= $diff_m." months";
                        } else if ($diff_d != 0) {
                            $noti_date_string .= $diff_d." days";
                        } else if ($diff_h != 0) {
                            $noti_date_string .= $diff_h." hrs";
                        } else if ($diff_i != 0) {
                            $noti_date_string .= $diff_i." mins";
                        } else {
                            $noti_date_string = "just now";
                        }
                        $noti_url = "";
                        if (isset($scp_modules[$value->module])) {
                            $noti_url = "#data/".$value->module ."/detail/". $value->record_id;
                        }
                        if (isset($_REQUEST['current_url']) && $_REQUEST['current_url'] == $noti_url) {
                            $noti_url = "javascript:void(0);";
                            $onclick = "onclick='window.location.reload(true);'";
                        }
                        $noti_message = (strlen($value->notification_subject) > 30) ? substr($value->notification_subject, 0, 30) . "..." : $value->notification_subject;
                        $html .= "<li class='scp-popup-notification'>
                                    <a href='$noti_url' $onclick>
                                        <span class='time'>".$noti_date_string."</span>
                                        <span class='details'>".$noti_message."</span>
                                    </a>
                                  </li>";
                    }
                }
                $html .= "</ul>";
                echo $html;
            }
        } else {

            include( TEMPLATE_PATH . 'scp_notification_list.php');

            if ($countCases != 0) {
                echo scp_pagination($countCases, $limit, $page_no, $pagination_url, $module_name, $order_by, $order, $view1, $current_url);
            }
        }
        //updating unread notifications
        $noti_ids = array();
        if(!empty($notificationlist)){
            foreach ($notificationlist as $value){
                if ($value->notification_status == 'unread') {
                    array_push($noti_ids, $value->id);
                }
            }
        }
        if (!empty($noti_ids)) {
            if ($sugar_crm_version == 7) {
                $entrydata = array(
                    'notification_status' => 'read',
                );
                $notificationflag = $objSCP->mass_update($noti_ids,$entrydata);
            } else {
                $entrydata = array();
                $params = array(
                    'name' => 'notification_status',
                    'value'=> 'read',
                );
                array_push($entrydata, $params);
                $where_cond = "id IN ('".implode("','", $noti_ids)."')";
                $notificationflag = $objSCP->mass_update_records('bc_wp_portal_notification', $entrydata, $where_cond);
            }
        }
        wp_die();
    }
}

/**
 * Delete notifications call
 */
if (!function_exists('scp_delete_multiple')) {
    add_action('wp_ajax_scp_delete_multiple','scp_delete_multiple');
    add_action('wp_ajax_nopriv_scp_delete_multiple','scp_delete_multiple');

    function scp_delete_multiple(){
        global $sugar_crm_version,$objSCP, $custom_relationships, $language_array, $lang_code;
        $custom_relationships = $_SESSION['scp_custom_relationships'];
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        $ids = $_REQUEST['ids'];
        $delete_all = $_REQUEST['delete_all'];
        $module_name = $_REQUEST['module_name'];
        if ($module_name == "bc_wp_portal_notification") {
            $module_with_caps = $language_array['lbl_notifications'];
        } else {
            $module_with_caps = $_SESSION['module_array_without_s'][$module_name];
            if ( $delete_all != '0' || count($ids) > 1) {
            $module_with_caps = $_SESSION['module_array'][$module_name];
            }
        }
        if ($sugar_crm_version == 7) {
            if ($module_name != "bc_wp_portal_notification") {
                $related_ids = array();
                if ( $delete_all == '0' ) {
                    $related_ids = $ids;
                }
                $contact_id = $_SESSION['scp_user_id'];
                $relationship_name = (isset($custom_relationships[$module_name]['Contacts'])) ? $custom_relationships[$module_name]['Contacts'] : strtolower($module_name);
                $notificationflag = $objSCP->portal_massdelete("Contacts", $contact_id, $relationship_name, $module_name, $related_ids);
                if (isset($notificationflag->success) && $notificationflag->success) {
                    echo '1';
                    $conn_err = $module_with_caps . " " . ( isset($_SESSION['portal_message_list']->delete_success) ? __( $_SESSION['portal_message_list']->delete_success ) : '' );
                    $_SESSION['bcp_add_record'] = $conn_err;
                } else {
                    echo '0';
                }
            } else {
                if ($delete_all == '0') {
                    $notificationflag = $objSCP->mass_delete($ids);
                    if ($notificationflag->failed) {
                        echo '0';
                    } else {
                        echo '1';
                        $conn_err = $module_with_caps . " " . ( isset($_SESSION['portal_message_list']->delete_success) ? __( $_SESSION['portal_message_list']->delete_success ) : '' );
                        $_SESSION['bcp_add_record'] = $conn_err;
                    }
                } else {
                    $notificationflag = $objSCP->delete_module_records($module_name);
                    if ($notificationflag) {
                        echo '1';
                        $conn_err = $module_with_caps . " " . ( isset($_SESSION['portal_message_list']->delete_success) ? __( $_SESSION['portal_message_list']->delete_success ) : '' );
                        $_SESSION['bcp_add_record'] = $conn_err;
                    } else {
                        echo '0';
                    }
                }
            }
        }else{
            if ($module_name != "bc_wp_portal_notification") {
                $related_ids = array();
                if ( $delete_all == '0' ) {
                    $related_ids = $ids;
                }
                $contact_id = $_SESSION['scp_user_id'];
                $relationship_name = (isset($custom_relationships[$module_name]['Contacts'])) ? $custom_relationships[$module_name]['Contacts'] : strtolower($module_name);
                $notificationflag = $objSCP->portal_massdelete("Contacts", $contact_id, $relationship_name, $module_name, $related_ids);
            } else {
                $entrydata = array();
                $params = array(
                    'name' => 'deleted',
                    'value'=> '1',
                );
                array_push($entrydata, $params);
                $where_cond = "";
                if ($delete_all == '0') {
                    $noti_ids = $ids;
                    $where_cond = "id IN ('".implode("','", $noti_ids)."')";
                }
                $notificationflag = $objSCP->mass_update_records($module_name, $entrydata, $where_cond);
            }
            if ($notificationflag) {

                $conn_err = $module_with_caps . " " . ( isset($_SESSION['portal_message_list']->delete_success) ? __( $_SESSION['portal_message_list']->delete_success ) : '' );
                $_SESSION['bcp_add_record'] = $conn_err;
                echo '1';
            } else {
                echo '0';
            }
        }
        wp_die();
    }
}

/**
 * function to update notification counter
 */
if (!function_exists('scp_update_notifications_counter')) {
    add_action('wp_ajax_scp_update_notifications_counter','scp_update_notifications_counter');
    add_action('wp_ajax_nopriv_scp_update_notifications_counter','scp_update_notifications_counter');

    function scp_update_notifications_counter(){
        global $wpdb, $objSCP;
        if (!isset($_SESSION['scp_user_id']) || $_SESSION['scp_user_id'] == "") {
            $notification_response['total_notification'] = -1;
        }
        
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        
        $scp_modules = (isset($_SESSION['module_array'])) ? $_SESSION['module_array'] : "";
        $limit = fetch_data_option('biztech_scp_case_per_page');
        $order = 'desc';
        $contact_id = (isset($_SESSION['scp_user_id'])) ? $_SESSION['scp_user_id'] : "";
 
        $scp_key_modules = array_keys((array)$scp_modules);

        //get notifications lists from crm
        $notification_result = $objSCP->get_notification_list('0',10, $order, array(), $scp_key_modules, $lang_code);
        
        $notyCount = array();
        $notificount = 0;
        if(!empty($notification_result->notification_records)) {
            $notificationlist = $notification_result->notification_records; 
           
            foreach ($notificationlist as $notification) {
                if($notification->notification_status == 'unread') {
                    $notyCount[] = $notification;
                     $notificount++;
                }
            }
        }

        $noti_table = $wpdb->prefix . "scp_portal_contact_info";
        $wpdb->update(
                $noti_table,
                array(
                        'total_notification' => $notificount,	// integer (number)
                ),
                array( 'contact_id' => $contact_id ),
                array(
                        '%d',	// value1
                ),
                array( '%s' )
        );
        
        $sql = "SELECT * FROM ".$noti_table . " WHERE contact_id = '".$contact_id."'";
        $result = $wpdb->get_row($sql);
        $notification_response = [];
        if ($result == NULL) {
           
        } else {
          
            $total_notification = $result->total_notification;
            $notification_response['total_notification'] = $total_notification;
            if($result->total_notification > 0){ 
                $op = '';
                $op .=  '<h6 class="dropdown-header">Message Center</h6>'; ?>
                <?php foreach($notyCount as $value) { 
                    $noti_url = "javascript:void(0);";
                    if (isset($scp_modules[$value->module])) {
                        $noti_url = "#data/".$value->module ."/detail/". $value->record_id;
                    }
                ?>
                <?php

                $op .=  '<a class="dropdown-item d-flex align-items-center" href="'.$noti_url.'">';
                $op .=   '<div class="font-weight-bold">';
                $op .=   '<div class="text-truncate">'.$value->notification_subject.'</div>';
                $op .=   '<div class="small text-gray-500">';
                
                $val_date = $value->date_entered;
                if ( $_SESSION['browser_timezone'] != NULL ) {
                    $val_date = scp_user_time_convert( $val_date );
                } else {
                    /****change****/
                    if (empty($_SESSION['user_timezone'])) {
                        $result_timezone = $objSCP->getUserTimezone();
                    }
                    else {
                        $result_timezone = $_SESSION['user_timezone'];
                    }
                    $UTC = new DateTimeZone("UTC");
                    $newTZ = new DateTimeZone($result_timezone);
                    $date = new DateTime($val_date, $UTC);
                    $date->setTimezone($newTZ);
                    //Updated by BC on 16-nov-2015
                    $date_format = $_SESSION['user_date_format'];
                    $time_format = $_SESSION['user_time_format'];
                    $val_date = $date->format($date_format . " " . $time_format);
                }
                $op .=  $val_date;
                
                $op .=   '</div></div>';
                $op .=    '</a>';
               } 
                
                $notification_response['noti_data'] = $op;
            }
        }
        echo json_encode($notification_response);
        exit;
        wp_die();
    }
}

/**
 * function to reset password
 */
if( ! function_exists('scp_reset_password_callback') ) {
    add_action('admin_post_bcp_reset_password', 'scp_reset_password_callback');
    add_action('admin_post_nopriv_bcp_reset_password', 'scp_reset_password_callback');
    function scp_reset_password_callback() {
        global $sugar_crm_version,$objSCP, $lang_code, $language_array;
        if ($objSCP == "") {
            scp_create_connection();
        }
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        $token_2 = $_REQUEST['contact_id'];
        $json_token = decryptPass($token_2);
        $id = json_decode($json_token);
        $redirect_url = $_REQUEST['scp_redirect_url'];
        $username = $email = "";
        $module_name = "Contacts";
        $pass_array['id'] = $id;
        $new_pwd = $_REQUEST['scp_new_password'];
        $encryptPass = encryptPass($new_pwd);
        $pass_array['password_c'] = $encryptPass;
        $pass_array['forgot_pass_access_token_c'] = "";
        $flag = $objSCP->set_entry($module_name, $pass_array);
        if ($sugar_crm_version == 7) {
            $where_con = array(
                "id" => array(
                    '$contains' => $id,
                ),
            );
            $userDetail = $objSCP->get_entry_list_custom($module_name, '', $where_con);
            $username = $userDetail->entry_list[0]->name_value_list->username_c->value;
            $email = $userDetail->entry_list[0]->name_value_list->email1->value;
            $user = get_user_by('email',$email);
        }else{
            $where_con = strtolower($module_name) . ".id = '{$id}'";
            $userDetail = $objSCP->get_entry_list($module_name, $where_con);
            $username = $userDetail->entry_list[0]->name_value_list->username_c->value;
            $email = $userDetail->entry_list[0]->name_value_list->email1->value;
            $user = get_user_by('email',$email);
        }
        //update wordpress user
        if ($user != NULL) {
            $user_login = $user->data->user_login;
            if ($user_login == $username) {
                $userid = $user->ID;
                $userid = wp_update_user(array(
                    'ID' => $userid,
                    'user_pass' => $new_pwd,
                ));
            }
        }
        $msg = $language_array['msg_pwd_updated_successfully'];
        $_SESSION['bcp_login_suc'] = $msg;
        wp_redirect($redirect_url);
    }
}

/**
 * Function for search solutions.
 *
 * @since 3.2.0
 *
 * @global array $scp_modules Accessible modules to access in template function
 */
if (!function_exists("scp_search_solutions")) {

    add_action('wp_ajax_bcp_search_solutions','scp_search_solutions');
    add_action('wp_ajax_nopriv_bcp_search_solutions','scp_search_solutions');
    function scp_search_solutions() {
        global $custom_relationships, $scp_modules, $lang_code, $language_array, $relate_to_module, $relate_to_id;
        $custom_relationships = $_SESSION['scp_custom_relationships'];
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        if ( isset( $_REQUEST['relate_to_module'] ) && $_REQUEST['relate_to_module'] != '' ) {  // Currently only for proposal's under quote list
            $relate_to_module = $_REQUEST['relate_to_module'];
            $relate_to_id = $_REQUEST['relate_to_id'];
        }
        $case_deflection = 0;
        if (isset($_SESSION['scp_additional_setting']) && isset($_SESSION['scp_additional_setting']->case_deflection) && $_SESSION['scp_additional_setting']->case_deflection) {
            $case_deflection = 1;
        }
        if (!$case_deflection) {
            echo "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                <strong>".$language_array['msg_no_permission_data']."</strong>
            </div>";
            wp_die();
        }
        $scp_modules = $_SESSION['module_array'];
        $view = $_REQUEST['view'];
        $module_name = $_REQUEST['modulename'];
        $request_module_file = TEMPLATE_PATH . 'bcp_solutions.php';
        include( $request_module_file );
        $request_function = 'bcp_solutions_searchpage';
        echo $request_function();
        wp_die();
    }
}

/**
 * Function for add static language data to local database
 *
 * @since 3.2.0
 */
if (!function_exists("scp_change_language")) {

    add_action('wp_ajax_bcp_change_language','scp_change_language');
    add_action('wp_ajax_nopriv_bcp_change_language','scp_change_language');

    function scp_change_language() {
        $lang_code = isset($_REQUEST['lang_code']) ? $_REQUEST['lang_code'] : "en_us";
        
        setcookie("scp_lang_code", '', time() - (60*60*24*30), "/");
        setcookie("scp_lang_code", $lang_code, time() + (60*60*24*30), "/");
        echo $lang_code;
        wp_die();
    }
}

/**
 * Function for get static language data
 *
 * @since 3.2.0
 */
if (!function_exists("scp_get_language_data")) {

    function scp_get_language_data($lang_code = "en_us") {
        global $wpdb;

        $table_name = $wpdb->prefix . 'crm_language_messages';
        $sql = "SELECT * FROM $table_name where lang_code='" . $lang_code . "'";
        $getlayout = $wpdb->get_results($sql);
        $getcount = $wpdb->num_rows;
        $lang_data = isset($getlayout[0]->response_data) ? json_decode($getlayout[0]->response_data) : array();
        
        if ( isset($_SESSION['scp_user_id']) && $getcount > 0 ) {
            $lang_data = json_decode($getlayout[0]->response_data);
        } else {
            $lang_data = scp_set_language_data($lang_code);
        }
        $_COOKIE['scp_lang_code'] = $lang_code;
        $lang_data = (array) $lang_data;
        return $lang_data;
    }
}

/**
 * scp set language data
 * @global type $objSCP
 * @global type $wpdb
 * @global type $sugar_crm_version
 * @param type $lang_code
 * @return type
 */
if (!function_exists("scp_set_language_data")) {

    function scp_set_language_data($lang_code = "en_us") {
        global $objSCP, $wpdb;

        $table_name = $wpdb->prefix . 'crm_language_messages';
        $sql = "SELECT * FROM $table_name where lang_code='" . $lang_code . "'";
        $getlayout = $wpdb->get_results($sql);
        $getcount = $wpdb->num_rows;
        $lang_data = '';
        if($objSCP) {
            $lang_data = $objSCP->get_language_data($lang_code);

            if (!empty($lang_data)) {
                $responseLayout = json_encode($lang_data);
                if ($getcount > 0) {
                    foreach ($getlayout as $singlepost) {
                        $layoutid = $singlepost->id;
                            $wpdb->update(
                                $table_name,
                                array(
                                    'lang_code' => $lang_code,
                                    'response_data' => $responseLayout,
                                    'modified_date' => date("Y-m-d H:i:s"),
                                ),
                                array('id' => $layoutid)
                            );
                    }
                } else {
                    $wpdb->insert(
                        $table_name, array(
                            'lang_code' => $lang_code,
                            'response_data' => $responseLayout,
                            'modified_date' => date("Y-m-d H:i:s"),
                            'created_date' => date("Y-m-d H:i:s"),
                        )
                    );
                }
            }
        }
        return $lang_data;
    }
}

/**
 *
 */
if (!function_exists("scp_feedback_page")) {

    add_action('wp_ajax_scp_feedback_page','scp_feedback_page');
    add_action('wp_ajax_nopriv_scp_feedback_page','scp_feedback_page');
    function scp_feedback_page() {
        global $lang_code, $language_array;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        include( TEMPLATE_PATH . 'scp_add_feedback.php');
        wp_die();
    }
}

if (!function_exists("scp_export_data")) {
    add_action('admin_post_scp_export_data','scp_export_data');
    add_action('admin_post_nopriv_scp_export_data','scp_export_data');
    function scp_export_data() {
        global $objSCP, $sugar_crm_version, $lang_code, $wpdb, $custom_relationships;
        $custom_relationships = $_SESSION['scp_custom_relationships'];
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        $ids = $_REQUEST['scp_csv_module_id'];
        $module_name = trim($_REQUEST['module_name']);
        $contact_group_id = isset($_SESSION['contact_group_id']) ? $_SESSION['contact_group_id'] : "";

        $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='list' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
        $result_count = $wpdb->get_row($sql_count) or die(mysql_error());
        $json_data = $result_count->json_data;
        $results = json_decode($json_data);
        $n_array = array();
        foreach ($results as $key_n => $val_n) {
            if ($key_n == 'ID') {
                $id_show = true;
            }
            if (!empty($val_n->related_module)) {
                if ($val_n->type == 'relate' && (!in_array($val_n->related_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array
                    continue;
                }
            }
            $n_array[] = strtolower($key_n);
        }
        if (!in_array('id', $n_array)) {
            array_push($n_array, 'id');
        }
        if ($module_name == 'Quotes' || $sugar_crm_version != 7) {
            array_push($n_array, 'currency_id'); //Added by BC on 02-jul-2016 for getting currency id
        }
        $relate_to_module_name = 'Contacts';
        $relate_to_id = $_SESSION['scp_user_id'];
        
        $order_by = (isset($_POST['order_by']) && $_POST['order_by'] != "") ? trim( sanitize_text_field( $_POST['order_by'] )) : "date_entered";
            $order = (isset($_POST['order']) && $_POST['order'] != "") ? trim( sanitize_text_field( $_POST['order'] )) : "desc";
            
        $relationship_name = ( isset( $custom_relationships[$module_name][$relate_to_module_name] ) && $custom_relationships[$module_name][$relate_to_module_name] != '' ) ? $custom_relationships[$module_name][$relate_to_module_name] : strtolower( $module_name );

        if ($sugar_crm_version == 7) {
            $where_con = array();
            if (!empty($ids)) {
                $where_con = array(
                    "id" => array(
                        '$in' => $ids,
                    ),
                );
            }
            $order_by_query = "$order_by:$order";
            $limit = "";
            $offset = "";
            
            $n_array_str = implode(",", $n_array);
            $list_result = $objSCP->getRelationship($relate_to_module_name, $relate_to_id, $relationship_name, $n_array_str, $where_con, $limit, $offset, $order_by_query);
            include( TEMPLATE_PATH . 'bcp_export_csv_page7.php');
        } else {
            $where_con = '';
            $order_by_query = "$order_by $order";
            $limit = count($ids);
            if (!empty($ids)) {
                $where_con = strtolower($module_name).".id IN ('".implode("','", $ids)."')";
            }
            if ($module_name == 'AOS_Products') {
                $pos = array_search('product_image', $n_array);
                if ( $pos != FALSE ) unset($n_array[$pos]);

                $product_where_con = 'visible_portal = 1';
                $where_con = ( $where_con != '' ) ? $where_con . ' AND ' . $product_where_con : $product_where_con;
                $list_result = $objSCP->get_entry_list($module_name, $where_con, $n_array, $order_by_query, 0);
            } else {
                $list_result = $objSCP->get_relationships($relate_to_module_name, $relate_to_id, $relationship_name, $n_array, $where_con, $order_by_query, 0, 0, 5);
            }
            
            include( TEMPLATE_PATH . 'bcp_export_csv_page6.php');
        }
        exit();
        wp_die();
    }
}


if ( !function_exists("bcp_get_child_options") ) {
    add_action('wp_ajax_bcp_get_child_options', 'bcp_get_child_options');
    add_action('wp_ajax_nopriv_bcp_get_child_options', 'bcp_get_child_options');
    function bcp_get_child_options(){

        global $objSCP, $wpdb, $sugar_crm_version, $custom_relationships, $lang_code, $language_array;
        $contact_group_id = isset($_SESSION['contact_group_id']) ? $_SESSION['contact_group_id'] : "";
        $module_name = $_REQUEST['module_name'];
        $parent_value = $_REQUEST['parent_value'];
        $child_field = $_REQUEST['child_field'];
        $id = $_REQUEST['id'];
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $language_array = scp_get_language_data($lang_code);
        $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='edit' AND contact_group='" . $contact_group_id . "' AND lang_code='".$lang_code."'";
        $result_count = $wpdb->get_row($sql_count);
        $response = array();
        if (empty($result_count)) {
            $response['success'] = FALSE;
        } else {
            $response['success'] = TRUE;
            $json_data = $result_count->json_data;
            $results = json_decode($json_data);
            $child_options_tmp = array();
            $child_options = "<option disabled='' selected=''>".__('--None--')."</option>";
            foreach ($results as $vals_lbl) {
                foreach ($vals_lbl->rows as $vals) {
                    foreach ($vals as $val_fields) {
                        if ($val_fields->name == $child_field) {
                            $child_options_tmp = isset($val_fields->options) ? $val_fields->options : array();
                        }
                    }
                }
            }
            $dynamicvalue = '';
            if(!empty($id)){
                $where_con = strtolower($module_name) . ".id = '{$id}'";
                $record_detail = $objSCP->get_entry_list($module_name, $where_con);
                $dynamicvalue = isset($record_detail->entry_list[0]->name_value_list->$child_field->value) ? $record_detail->entry_list[0]->name_value_list->$child_field->value : '';
            }
            foreach ($child_options_tmp as $key => $value) {
                if ( strpos($child_options_tmp->$key->value, $parent_value) !== false ) {
                    $sel = '';
                    if($dynamicvalue && $dynamicvalue == $value->value){
                        $sel = "selected=''";
                    }
                    $child_options .= "<option value='".$value->value."' ".$sel.">".$value->name."</option>";
                }
            }
            $response['data'] = $child_options;
        }
        echo json_encode($response);
        wp_die();
    }
}
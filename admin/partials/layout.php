<table class='form-table'>
    <!-- TR Start -->
    <tr class="hide_class">
        <th scope='row'><label><?php _e('Portal Template') ?></label></th>
        <td>
            <select name = "biztech_portal_template" id="biztech_portal_template">
                <?php
                if (is_multisite() && !is_network_admin()) {
                    echo '<option value="100">' . __('-- Select any one --') . '</option>';
                }
                $template_ary = array(1 => __('CRM Standalone Page'), 0 => __('CRM Full Width Page'));
                $option = '';
                foreach ($template_ary as $key11 => $value11) {
                    $option .= '<option value="' . $key11 . '"';
                    if ($crmtemplate == $key11) {
                        $option .= ' selected="selected"';
                    }
                    $option .= '>';
                    $option .= $value11;
                    $option .= '</option>';
                }
                echo $option;
                ?>
            </select>
        </td>
    </tr>
    <!-- TR End -->
    <!-- TR Start -->
    <?php
        $cls = 'hide';
        if($crmtemplate == 0){
            $cls = 'show';
        }
        $locations = get_nav_menu_locations();
        $locations = array_filter($locations);
        if (!empty($locations)) {
    ?>
        <tr class="menu-location <?php echo $cls; ?>">
            <th scope='row'><label><?php _e('Menu Location', 'bcp_portal'); ?></label></th>
            <td>
                <?php
                if (fetch_data_option('bcp_portal_menuLocation') != NULL) {
                    $crm_menuLocation = fetch_data_option('bcp_portal_menuLocation');
                } else {
                    $crm_menuLocation = '';
                }
                $portal_menus_locations = get_registered_nav_menus();
                ?>
                <select name="bcp_portal_menuLocation" id="bcp_portal_menuLocation">
                    <option value="" ><?php _e('select'); ?></option>
                    <?php
                    foreach ($portal_menus_locations as $key => $value) {
                        if ($key == $crm_menuLocation) {
                            $selected = 'selected';
                        } else {
                            $selected = '';
                        }
                        ?>
                        <option value="<?php echo $key; ?>" <?php echo $selected; ?> ><?php echo $value; ?></option>
                    <?php } ?>
                </select>
            </td>
        </tr>
        <?php } else {
            echo "<p>" . sprintf(__("Please select at least one menu location in menu %s here."), home_url('wp-admin/nav-menus.php?action=locations')) . "</p>"; 
        } ?>
    <!-- TR End -->
    <!-- TR Start -->
    <tr class="hide_class">
        <th scope='row'><label><?php _e('Theme Color'); ?></label></th>
        <td>
            <input type="text" value="<?php echo $crmthemecolor; ?>" class="my-color-field" data-default-color="" name="biztech_scp_theme_color" />
            <br /><?php _e('Clear theme color to restore default'); ?>
        </td>
    </tr>
    <!-- TR End -->
    <!-- TR Start -->
    <tr class="hide_class" id="calandar_call_color">
        <th scope='row'><label><?php _e('Calendar Calls Color'); ?></label></th>
        <td><input type="text" value="<?php echo $crmcalendarcall; ?>" class="my-color-field" <?php if (is_network_admin() || !is_multisite()) {
                    echo 'data-default-color="#7d9e12" ';
                } ?>name="biztech_scp_calendar_calls_color" /></td>
    </tr>
    <!-- TR End -->
    <!-- TR Start -->
    <tr class="hide_class" id="calandar_meeting_color">
        <th scope='row'><label><?php _e('Calendar Meetings Color'); ?></label></th>
        <td><input type="text" value="<?php echo $crmcalendarmetting; ?>" class="my-color-field" <?php if (is_network_admin() || !is_multisite()) {
                    echo 'data-default-color="#00585e" ';
                } ?>name="biztech_scp_calendar_meetings_color" /></td>
    </tr>
    <!-- TR End -->
    <!-- TR Start -->
    <tr class="hide_class" id="calandar_task_color">
        <th scope='row'><label><?php _e('Calendar Tasks Color'); ?></label></th>
        <td><input type="text" value="<?php echo $crmcalendartask; ?>" class="my-color-field" <?php if (is_network_admin() || !is_multisite()) {
                    echo 'data-default-color="#3a87ad" ';
                } ?>name="biztech_scp_calendar_tasks_color" /></td>
    </tr>
    <!-- TR End -->
    <!-- TR Start -->
    <tr class="hide_class">
        <th scope='row'><label><?php _e('Custom Css'); ?></label></th>
        <td>
            <textarea style="max-height: 200px;" cols="50" rows="10" name="biztech_scp_custom_css"><?php echo $crmcustomcss; ?></textarea>
            <br /><?php _e('Leave blank to restore to default'); ?>
        </td>
    </tr>
    <!-- TR End -->
    <!-- TR Start -->
    <tr class="hide_class">
        <th scope="row"><?php _e('Counter On Dashboard'); ?></th>
        <td>

            <select id="biztech_scp_counter" name="biztech_scp_counter[]" multiple="multiple" size="4">
                <?php
                if (!empty((array) $modules_array)) {
                    foreach ($modules_array as $key => $value) {
                        if ($key == 'KBContents' || $key == 'AOK_KnowledgeBase') {
                            continue;
                        }
                        $selected = '';
                        if ($biztechcounter != "" && in_array($key, $biztechcounter)) {
                            $selected = ' selected';
                        }
                        ?>
                        <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </td>
    </tr>
    <!-- TR End -->
    <!-- TR Start -->
    <tr class="hide_class">
        <th scope="row"><?php _e('Recent Activities On Dashboard'); ?></th>
        <td>

            <select id="biztech_scp_recent_activity" name="biztech_scp_recent_activity[]" multiple="multiple" size="4">
                <?php
                if (!empty((array) $modules_array)) {
                    foreach ($modules_array as $key => $value) {
                        $selected = '';
                        if ($recentactivity != "" && in_array($key, $recentactivity)) {
                            $selected = ' selected';
                        }
                        ?>
                        <option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value; ?></option>
                        <?php
                    }
                }
                ?>
            </select>
        </td>
    </tr>
    <!-- TR End -->
</table>
<style>
    .menu-location.show {display: table-row;}
    .menu-location.hide {display: none;}
</style>
<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if(is_multisite()){
    add_action( 'network_admin_menu', 'scp_import_user_csv_create_menu');    //add menu in network side
}
else{
    add_action('admin_menu', 'scp_import_user_csv_create_menu');
}
if (!function_exists('scp_import_user_csv_create_menu')) {
    function scp_import_user_csv_create_menu() {
        add_submenu_page('biztech-crm-portal', __('Import Users'), __('Import Users'), 'manage_options', 'scp-import-users-csv', 'scp_import_users_csv_callback');
    }
}

if (!function_exists('scp_import_users_csv_callback')) {
    function scp_import_users_csv_callback() {

        global $objSCP;
        $crmresturl = get_option('biztech_scp_rest_url');
        $crmversionkey = get_option('biztech_scp_sugar_crm_version');
        $crmusername = get_option('biztech_scp_username');
        $scp_sugar_password = scp_decrypt_string(get_option('biztech_scp_password'));
        $objSCP = new SugarRestApiCall($crmresturl, $crmusername, $scp_sugar_password);

        if (isset($_REQUEST['scp_user_mail_message'])) {
            (is_multisite()) ? update_site_option('scp_user_mail_message', $_REQUEST['scp_user_mail_message']) : update_option('scp_user_mail_message', $_REQUEST['scp_user_mail_message']);
        }
        $sucflag = 0;
        $not_inserted = array();
        if ($_FILES != null) {
            if ($_FILES['scp_upload_csv_file']['name'] != null) {
                $file = fopen($_FILES['scp_upload_csv_file']['tmp_name'], 'r');
                $count = 0;

                if(is_multisite()){
                    global $wpdb;
                    $blogids = $wpdb->get_results("SELECT blog_id FROM {$wpdb->blogs} WHERE site_id = '{$wpdb->siteid}'");  //get all sites id
                    foreach($blogids as $id => $blog){  //get all website urls
                        $blogsiteurls[$blog->blog_id] = get_site_url($blog->blog_id);
                    }
                    $multisiteenable = get_site_option('biztech_multi_signin');

                    while (!feof($file)) {
                        $file_data = fgetcsv($file);
                        if ($file_data != null) {
                            if ($count != 0) {
                                for($i=0; $i<=2;$i++){
                                    if($file_data[$i] == NULL){
                                        $import_checker[] = __('Cell') . ' = ' . chr(65+$i).($count+1) . ' ' . __('Value') . ' = ' . $file_data[$i];
                                    }
                                }
                                $toaccesssite = explode('|',( isset( $file_data[5]) ? $file_data[5] : '' ) );
                                if(!empty($toaccesssite) && $toaccesssite[0]!=null){
                                    foreach($toaccesssite as $i=>$j){
                                        if (!(false !== $tempkey = array_search(trim($j), $blogsiteurls))) {
                                            $import_checker[] = __('Cell') . ' = ' . chr(65+5) . ($count+1) . ' , ' . __('Value') . ' = '.$j;
                                        }
                                    }
                                }
                                if( isset( $file_data[6] ) && $file_data[6] != NULL){
                                    if (!(false !== $tempkey = array_search(trim($file_data[6]), $blogsiteurls))) {
                                        $import_checker[] = 'Cell = ' . chr(65+6) . ($count+1) . ' , ' . __('Value') . ' = ' . $file_data[6];
                                    }
                                }
                            }
                        }
                        $count++;
                    }
                }
                $count = 0;
                if(empty($import_checker)){
                    $file = fopen($_FILES['scp_upload_csv_file']['tmp_name'], 'r');
                    while (!feof($file)) {
                        $file_data = fgetcsv($file);
                        if ($file_data != null) {
                            //echo '<pre>'; print_r( $file_data); echo '</pre>';
                            if ($count != 0) {
                                $userdata = array(
                                    'user_login' => $file_data[0],
                                    'user_pass' => decryptPass($file_data[1]),
                                    'user_email' => $file_data[2],
                                    'first_name' => $file_data[3],
                                    'last_name' => $file_data[4],
                                    'role' => 'subscriber',
                                );
                                $user_id = wp_insert_user($userdata);

                                if (!is_wp_error($user_id)) {

                                    if(is_multisite()){
                                        remove_user_from_blog($user_id,get_current_blog_id());  //remove user from main site, by default it added
                                        $toaccesssite = explode( '|', ( isset( $file_data[5]) ? $file_data[5] : '' ) ); //get all entered sites in csv
                                        if(($multisiteenable == 'multi-signin-enable') && (empty($toaccesssite) || $toaccesssite[0]==NULL)){  //give access to user to all sites if acccess sites are null and multi-signin is enable
                                            foreach ($blogids as $id => $blog){
                                                add_user_to_blog($blog->blog_id, $user_id, 'subscriber');
                                            }
                                        }
                                        else{
                                            if(empty($toaccesssite) || $toaccesssite[0]==NULL){ //if user has not any site access give him main site access
                                                add_user_to_blog(get_current_blog_id(), $user_id, 'subscriber');
                                            }
                                            else{
                                                foreach($toaccesssite as $i=>$j){
                                                    if (false !== $tempkey = array_search(trim($j), $blogsiteurls)) {
                                                        add_user_to_blog($tempkey , $user_id, 'subscriber');
                                                    }
                                                }
                                            }
                                        }

                                        $getblogs = get_blogs_of_user( $user_id );
                                        if(empty($getblogs)){
                                            add_user_to_blog(get_current_blog_id(), $user_id, 'subscriber');
                                        }

                                        $toaccesssite = ( isset( $file_data[6] ) ? $file_data[6] : '' ); //get redirectoin url from csv

                                        if (false !== $tempkey = array_search(trim($toaccesssite), $blogsiteurls)) {
                                            update_user_meta( $user_id, 'biztech_user_redirection_url', 'blog_id_'.$tempkey );
                                            $toaccesssiteconf = $toaccesssite;
                                        }
                                    }

                                    if ($_REQUEST['scp_send_user_mail']) {

                                        if(isset($toaccesssiteconf)){   //if its redirected
                                            $login_link = $toaccesssiteconf;
                                            unset($toaccesssiteconf);
                                        }
                                        else{
                                            $login_link = get_permalink(get_option('biztech_redirect_login'));
                                        }

                                        $to = $file_data[2];

                                        $subject = __("Your account created at") . " " . get_option('blogname');

                                        $body = __('Hi') . " " . $file_data[3] . ' ' . $file_data[4] . ",<br><br>";

                                        if ($_REQUEST['scp_user_mail_message'] != null) {
                                            $body .= $_REQUEST['scp_user_mail_message'] . '<br><br>';
                                        }
                                        $body .= __("Use below your credentials to access portal") . ':';
                                        $body.="<br>" . __('Username'). ': ' . $file_data[0] . "<br> " . __('Password') . ': ' . decryptPass($file_data[1]) . "<br><br>" . __('Log in at:') . ' <a href="'.$login_link.'">' . $login_link . '</a> ' . __('to get started');

                                        if (get_option('biztech_scp_upload_image') != NULL) {
                                            $crm_logo = "<img src='" . get_option('biztech_scp_upload_image') . "' title='" . get_bloginfo('name') . "' alt='" . get_bloginfo('name') . "' style='border:none' width='100'>";
                                        } else {
                                            $crm_logo = get_bloginfo('name');
                                        }
                                        $body_full = "<table width='100%' cellspacing='0' cellpadding='0' border='0' style='background:#e4e4e4; padding:30px 0;'>
      <tbody>
        <tr>
          <td><center>
                <table width='800' cellspacing='0' cellpadding='0' border='0' style='font-family: arial,sans-serif; font-size:14px; border-top:4px solid #35b0bf; background:#fff; line-height: 22px;'>
                    <tr>
                        <td style='padding: 30px 40px 0;'>
                            <table width='100%' cellspacing='0' cellpadding='0' border='0' style='border-bottom:2px solid #ddd;'>
                                <tr>
                                    <td style='padding-bottom:20px;'>
                                        <a href='".get_site_url()."'>
                                            $crm_logo
                                        </a>
                                    </td>
                                    <td align='right' valign='top' style='padding-bottom:20px;'>
                                       <h2 style='margin:0; padding:0; color:#35b0bf; font-size:22px;'>" . get_option('biztech_scp_name') . "</h2>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                    <tr>
                        <td style='padding:30px 40px 0; color: #484848;' align='left' valign='top'>
                             <p>" . $body . "</p>
                            <p>Regards, <br />
                            " . get_bloginfo('name') . "</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width='100%' cellspacing='0' cellpadding='0' border='0' style='background:#f7f7f7; padding:15px 40px;'>
                                <tr>
                                    <td align='left' valign='middle'>
                                        <a target='_blank' style='color: #35b0bf;text-decoration: none; font-size:14px;' href='" . get_site_url() . "'>" . get_site_url() . "</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center></td>
        </tr>
    </tbody>
    </table>";

                                        add_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
                                        wp_mail($to, $subject, $body_full);
                                        remove_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
                                    }$sucflag = 1;
                                } else {
                                    if(email_exists($file_data[2])){
                                        $not_inserted[] = $file_data[2];
                                    }
                                    else{
                                        $not_inserted[] = $file_data[3] . ' ' . $file_data[4];
                                    }
                                }
                            }
                            $count ++;
                        }
                    }
                }
                fclose($file);
            }
        }
        ?>
        <div class="wrap">
            <?php
            if(!(empty($import_checker))){
            ?>
            <div class="notice notice-error is-dismissible">
                <p><?php _e('We found Error(s) in following cell(s):'); ?> </p>
                <ul>
                    <?php
                        foreach($import_checker as $i=>$j){
                            echo '<li>'.$j.'</li>';
                        }
                    ?>
                </ul>
            </div>
            <?php
            }
            if ($not_inserted != null) {
                ?>
                <div class="notice notice-error is-dismissible">
                    <p><?php _e('Following users are already exist:'); ?> </p>
                    <ul>
                        <?php
                        foreach ($not_inserted as $value) {
                            ?><li><?php echo $value; ?></li><?php
                        }
                        ?>
                    </ul>
                </div>
            <?php } else if ($sucflag == 1) {
                ?>
                <div class="notice notice-success is-dismissible">
                    <p> <?php _e('Users imported successfully.'); ?></p>
                </div>
            <?php }
            ?>
            <h1><?php _e('Import Users'); ?></h1>
            <?php

            if (is_object( $objSCP ) ) {
                ?>
                <form method="post" enctype="multipart/form-data">
                    <table class="form-table">
                        <tbody>
                            <tr>
                                <th scope="row"><label for="scp_send_user_mail"><?php _e('Send User Mail?'); ?></label></th>
                                <td>
                                    <input type="hidden" name="scp_send_user_mail" value="0"/>
                                    <input type="checkbox" id="scp_send_user_mail" name="scp_send_user_mail" value="1"/>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"><label><?php _e('User Mail Message'); ?></label></th>
                                <td>
                                    <textarea name="scp_user_mail_message" class="large-text" rows="8"><?php echo ( get_option('scp_user_mail_message') ? get_option('scp_user_mail_message') : __('This mail is regarding your account created in portal.') ); ?></textarea>
                                    <p class="description"><?php _e('This text will be appended in mail body before your portal credentials.'); ?></p>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row"><label><?php _e('Upload CSV File'); ?></label></th>
                                <td>
                                    <input type="file" name="scp_upload_csv_file" id="scp_upload_csv_file"/><br>
                                    <?php
                                        $csvfilename = (is_multisite()) ? '/users-multisite.csv' : '/users.csv';
                                    ?>
                                    <a href="<?php echo plugins_url('', __FILE__) . $csvfilename ; ?>" target="_blank"><?php _e('Download sample CSV file.'); ?></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p><input type="submit" class="button-primary" name="submit" value="<?php _e('Import'); ?>" id="submitdata" /></p>
                    <script type="text/javascript">
                        jQuery('#submitdata').click(function (e) {
                            var scp_upload_csv_file = jQuery('#scp_upload_csv_file').val();
                            var extension = scp_upload_csv_file.replace(/^.*\./, '');
                            if (extension == scp_upload_csv_file) {
                                extension = '';
                            } else {
                                extension = extension.toLowerCase();
                            }
                            if (scp_upload_csv_file == '') {
                                alert("<?php _e('Please upload csv file'); ?>");
                                return false;
                            } else if (extension != 'csv' && extension != 'CSV') {
                                alert("<?php _e('Please upload csv file only'); ?>");
                                return false;
                            }
                        });
                    </script>
                </form>
            <?php
            } else {
                global $bcp_framework;
                $crmresturl = get_option('biztech_scp_rest_url');
                $crmversionkey = get_option('biztech_scp_sugar_crm_version');
                $crmusername = get_option('biztech_scp_username');
                $scp_sugar_password = scp_decrypt_string(get_option('biztech_scp_password'));
                $objSCP = new SugarRestApiCall($crmresturl, $crmusername, $scp_sugar_password);
                $ismenable = $objSCP->check_enable_modules();
                if( $ismenable->success != 1 ) {
                    $error_msg = isset( $_SESSION['scp_admin_messages']->conn_problem ) ? __( $_SESSION['scp_admin_messages']->conn_problem ) : '';
                } elseif ( $ismenable->fronend_framework != $bcp_framework ) {
                    $backend_error = ( $crmversionkey == 6 ) ? 'SugarCRM' : 'SuiteCRM';
                    $error_msg = ( isset( $_SESSION['scp_admin_messages']->combination_mismatch ) ? __( $_SESSION['scp_admin_messages']->combination_mismatch ) : '' ) . ' (' . $backend_error . ' + ' . $ismenable->fronend_framework . ')';
                }
                ?>
                <div class="error settings-error" id="setting-error-settings_updated">
                    <p><strong><?php echo $error_msg; ?></strong></p>
                </div>
            <?php
            }
            ?>
            </div>
            <?php
        }
    }
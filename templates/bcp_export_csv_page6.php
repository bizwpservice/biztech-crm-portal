<?php
if ($module_name == "AOS_Invoices") {    
    $invoiceIds = array();
    foreach ($list_result->entry_list as $key => $value) {
        array_push($invoiceIds, $value->id);
    }
    $InvoiceStatus = $objSCP->getInvoiceTransactionDetails($invoiceIds);
}
$header = array();
$fields_meta = array();
$product_array = array();
$type_array = array();
$module_with_caps = $_SESSION['module_array'][$module_name];

$filename = $module_with_caps.".csv";
ob_clean();
$fp = fopen('php://output', 'w');
header("Content-Type: text/csv");
header("Content-Disposition: attachment; filename=\"$filename\"");
foreach ($results as $key_name => $val_array) {
    if ($key_name == "SET_COMPLETE") {
        continue;
    }
    if (!empty($val_array->related_module)) {
        if ($val_array->type == 'relate' && (!array_key_exists($val_array->related_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array
            continue;
        }
    }                
    $field_label = str_replace(':', '', htmlentities($val_array->label_value));
    $ar_val = strtolower($key_name);
    $type_array[$ar_val] = $val_array->type;
    $res2 = $objSCP->get_module_fields($module_name, array($ar_val));
    $fields_meta[$ar_val] = $res2;
    $name_arry[] = $ar_val;
    $header[] = $field_label;
}
fputcsv($fp, $header);
foreach ($list_result->entry_list as $list_result_s) {
    $first_field = TRUE;
    $id = $list_result_s->name_value_list->id->value;
    if ($module_name == 'AOS_Products') {
        array_push($product_array, $id);
    }
    if (isset($list_result_s->name_value_list->parent_type->value)) {
        $parent_type = $list_result_s->name_value_list->parent_type->value;
    }
    if ($module_name == 'AOS_Quotes' || $module_name == 'AOS_Invoices' || $module_name == 'AOS_Contracts' || $module_name == 'AOS_Products' ) { // Added by BC on 02-july-2016 for currency symbol
        $curreny_id = $list_result_s->name_value_list->currency_id->value;
        if (!empty($curreny_id)) {
            $currency_symbol = $_SESSION['Currencies']->$curreny_id->symbol;
        }
    }
    $row = array();
    foreach ($name_arry as $nkey => $nval) {
        switch ($nval) {
            case 'assigned_user_id':
                $get_entrty_module = "Users";
                break;
            case 'modified_user_id':
                $get_entrty_module = "Users";
                break;
            case 'created_by':
                $get_entrty_module = "Users";
                break;
            case 'parent_id':
                if ($parent_type) {
                    $get_entrty_module = $parent_type;
                } else {
                    $get_entrty_module = "";
                }
                break;
            default:
                $get_entrty_module = "";
                break;
        }
        $value = $list_result_s->name_value_list->$nval->value;
        if($fields_meta[$nval] == 'name' || $fields_meta[$nval] == 'text' || $fields_meta[$nval] == 'parent'){
            $value = htmlentities($value);
        }

        $value = strip_tags( html_entity_decode( $value ) );

        if ( $module_name == 'AOS_Quotes' && $nval == 'rejection_history' ) {
            continue;
        }

        if ($nval == 'status') {
            if (isset($InvoiceStatus->$id->payment_status) && $InvoiceStatus->$id->payment_status) {
                if (isset($InvoiceStatus->$id->status)) {
                    $value = $InvoiceStatus->$id->status;
                }
            }
        }
        if ($nval == "date_modified" || $nval == "date_start" || $nval == "date_end" || $nval == "date_entered" || $nval == "date_due") {
        if ($value != '') { // aaded to display blank date instead of default date
                if ( $_SESSION['browser_timezone'] != NULL ) {
                    $value = scp_user_time_convert( $value );
                } else {
                    $UTC = new DateTimeZone("UTC");
                    $newTZ = new DateTimeZone($result_timezone);
                    $date = new DateTime($value, $UTC);
                    $date->setTimezone($newTZ);
                    $value = $date->format($objSCP->date_format . " " . $objSCP->time_format);
                }
            }
        }
        if ($type_array[$nval] == 'date') {
            $value = (!empty($value)) ? date($objSCP->date_format, strtotime($value)) : '';
        }
        if ($get_entrty_module) {
            $select_fields = array('name');
            $record_detail = $objSCP->get_entry($get_entrty_module, $value, $select_fields);
            if (isset($record_detail) && !empty($record_detail)) {
                $value = $record_detail->entry_list[0]->name_value_list->name->value;
            }
        }

        if ($fields_meta[$nval]->module_fields->$nval->type == 'enum' || $fields_meta[$nval]->module_fields->$nval->type == 'multienum' || $fields_meta[$nval]->module_fields->$nval->type == 'dynamicenum') {
            if ( $fields_meta[$nval]->module_fields->$nval->type == 'multienum' ) {
                $tmp_multi_val = array();
                $tmp_multi_vals = $objSCP->unencodeMultienum( $value );
                foreach( $tmp_multi_vals as $tmp_multi_val_ ) {
                    $tmp_multi_val[] = (!empty($tmp_multi_val_)) ? $fileds->options->$tmp_multi_val_->value : '';
                }
                $value = implode(', ', $tmp_multi_val);
            } else {
                $value = (!empty($value)) ? $fields_meta[$nval]->module_fields->$nval->options->$value->value : '';
            }
        }
        if ($fields_meta[$nval]->module_fields->$nval->type == 'currency' && $value != '') {//Added by BC on 02-jul-2016 for currency
            $value = $currency_symbol . number_format($value, 2);
        }
        if ( $type_array[$nval] == 'bool' ) {
            if ($value == "1") {
                $value = $language_array['lbl_yes'];
            } else {
                $value = $language_array['lbl_no'];
            }
        }
        if ( $nval == 'currency_id' ) {
            $value = $_SESSION['scp_all_currency']->$value->currency_symbol;
        }
        if ($value == '' || empty($value) && !$type_array[$nval] == 'int') {
            $value = "-";
        }
        if ($nval == 'status' && $module_name == "AOS_Invoices") {
            if (isset($InvoiceStatus->$id->payment_status) && $InvoiceStatus->$id->payment_status) {
                if (isset($InvoiceStatus->$id->status)) {
                    $value = $InvoiceStatus->$id->status;
                }
            }
        }
        $row[] = $value;
    }
    fputcsv($fp, $row);
}


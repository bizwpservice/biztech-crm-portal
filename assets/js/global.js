$(document).ready(function(){
    $(".scp-login-body, .scp-signup-body").height($('body').height());

    // sidebar toggle
    $(".sidebartoggler").click(function(){
        $("body").toggleClass("sidebar-icon-only");
    });

    $(".three-dots").click(function(){
        $(".settings-panel").toggleClass("right-panel-open");
    });
    $("body").on('click','.btn-filter', function(){
        $("#filter-panel").addClass("filter-open");
        $("body").addClass("filter-body-open");
    });
    $("body").on('click','.close-fliter', function(){
        $("#filter-panel").removeClass("filter-open");
        $("body").removeClass("filter-body-open");
    });

   $("body").on('change','.dropzone', function(){
       readFile(this);
   });
   
   $("body").on('change','.case-upload', function(event){
       Uploadmultipleimage(this);
   });
   
   
});
$(window).resize(function(){
    $(".scp-login-body, .scp-signup-body").height($('body').height());
});

window.newFileList = [];
jQuery(document).on('click', 'a.remove-image-preview', function(){
    var remove_element = $(this);
    var id = remove_element.attr('data-id');
    remove_element.closest('.img-preview').remove();
    var input = document.getElementById('uploadBtn');
    var files = input.files;
    if (files.length) {
       if (typeof files[id] !== 'undefined') {
         window.newFileList.push(files[id].name)
       }
    }
    document.getElementById('removed_files').value = JSON.stringify(window.newFileList);
    
});

function readFile(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
 
        reader.onload = function(e) {
            var htmlPreview =
              '<img width="200" src="' + e.target.result + '" />';
            var wrapperZone = $(input).parent();
            var previewZone = $(input).parent().parent().find('.preview-zone');
            var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');
            jQuery('input[name="remove_profile_pic"]').val('true');

            wrapperZone.removeClass('dragover');
            previewZone.removeClass('hidden');
            boxZone.empty();
            boxZone.append(htmlPreview);
        };
        reader.readAsDataURL(input.files[0]);
    }
}


function Uploadmultipleimage(input) {
    if (input.files && input.files[0]) {
        jQuery.each( input.files, function( i, val ) {
            var name = val.name;
            var fileType = val.type;
            var reader = new FileReader();
            reader.onload = function(e) {
                if (fileType.search('image') >= 0) {
                    var res = e.target.result;
                } else {
                    var res = my_ajax_object.homeurl+'wp-content/plugins/biztech-crm-portal/assets/images/file.jpg';
                }
                var htmlPreview =
                  '<div class="image-div img-preview"><img width="200" src="' + res + '" /><p><a class="remove-image-preview" href="javascript:void(0);" data-id="'+i+'"><i class="fas fa-times"></i></a></p></div>';
                var wrapperZone = $(input).parent();
                var previewZone = $(input).parent().parent().find('.preview-zone');
                var boxZone = $(input).parent().parent().find('.preview-zone').find('.box').find('.box-body');

                wrapperZone.removeClass('dragover');
                previewZone.removeClass('hidden');
                boxZone.append(htmlPreview);
            };
            reader.readAsDataURL(val);
        });
    }
}
 

 

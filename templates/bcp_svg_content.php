<?php
global $module_icon_content;
$module_icon_content = array();


/* dashboard */
$module_icon_content['dashboard'] = '<svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path fill-rule="evenodd" clip-rule="evenodd" d="M1 3.52778C1 1.63179 1.0203 1 3.52778 1C6.03525 1 6.05556 1.63179 6.05556 3.52778C6.05556 5.42377 6.06355 6.05556 3.52778 6.05556C0.992003 6.05556 1 5.42377 1 3.52778Z" stroke="#333333" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd" d="M8.94434 3.52778C8.94434 1.63179 8.96464 1 11.4721 1C13.9796 1 13.9999 1.63179 13.9999 3.52778C13.9999 5.42377 14.0079 6.05556 11.4721 6.05556C8.93634 6.05556 8.94434 5.42377 8.94434 3.52778Z" stroke="#333333" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M1 11.4722C1 9.57623 1.0203 8.94444 3.52778 8.94444C6.03525 8.94444 6.05556 9.57623 6.05556 11.4722C6.05556 13.3682 6.06355 14 3.52778 14C0.992003 14 1 13.3682 1 11.4722Z" stroke="#333333" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M8.94434 11.4722C8.94434 9.57623 8.96464 8.94444 11.4721 8.94444C13.9796 8.94444 13.9999 9.57623 13.9999 11.4722C13.9999 13.3682 14.0079 14 11.4721 14C8.93634 14 8.94434 13.3682 8.94434 11.4722Z" stroke="#333333" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
</svg>';

/* accounts */
$module_icon_content['accounts'] = '<svg width="15" height="17" viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path fill-rule="evenodd" clip-rule="evenodd" d="M7.50043 11.2892C3.99422 11.2892 1 11.7791 1 13.7415C1 15.7038 3.97523 16.2113 7.50043 16.2113C11.0066 16.2113 14 15.7205 14 13.759C14 11.7975 11.0256 11.2892 7.50043 11.2892Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M7.50074 8.4903C9.80166 8.4903 11.6666 6.76579 11.6666 4.63909C11.6666 2.51238 9.80166 0.788673 7.50074 0.788673C5.19981 0.788673 3.33401 2.51238 3.33401 4.63909C3.32626 6.75861 5.17909 8.48312 7.47138 8.4903H7.50074Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
</svg>';


/* calls */
$module_icon_content['calls'] = '<svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path fill-rule="evenodd" clip-rule="evenodd" d="M7.17961 7.82321C9.90899 10.5518 10.5282 7.39512 12.266 9.13171C13.9413 10.8066 14.9043 11.1422 12.7816 13.2643C12.5157 13.478 10.8264 16.0487 4.88947 10.1135C-1.04817 4.17747 1.52108 2.48641 1.73482 2.2206C3.86265 0.0926313 4.19243 1.06116 5.86781 2.73607C7.60562 4.47339 4.45024 5.0946 7.17961 7.82321Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M9.10986 3.42413C10.3216 3.6595 11.2685 4.60713 11.5046 5.81887" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
</svg>';


/* cases */
$module_icon_content['cases'] = '<svg width="15" height="17" viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path fill-rule="evenodd" clip-rule="evenodd" d="M9.7653 0.834211H4.20486C2.4829 0.834211 1 2.22934 1 3.9513V13.02C1 14.839 2.38593 16.1756 4.20486 16.1756H10.8821C12.604 16.1756 14 14.742 14 13.02V5.24445L9.7653 0.834211Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M9.54541 0.824425V3.25607C9.54541 4.44306 10.5059 5.40602 11.6929 5.40853C12.7929 5.41104 13.9189 5.41187 13.9949 5.40686" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
</svg>';


/* aos_contracts */
$module_icon_content['aos_contracts'] = '<svg width="15" height="17" viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path fill-rule="evenodd" clip-rule="evenodd" d="M9.76614 0.83421H4.20486C2.48373 0.83421 1 2.22934 1 3.9513V12.9064C1 14.7253 2.38677 16.1756 4.20486 16.1756H10.8829C12.6049 16.1756 14 14.6292 14 12.9064V5.24445L9.76614 0.83421Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M9.54639 0.824425V3.25607C9.54639 4.44306 10.5068 5.40602 11.693 5.40853C12.7939 5.41104 13.9198 5.41187 13.9959 5.40686" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M9.38833 11.5304H4.87695" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M7.68176 7.39088H4.87646" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
</svg>';


/* documents */
$module_icon_content['documents'] = '<svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path fill-rule="evenodd" clip-rule="evenodd" d="M14.4807 10.2411C14.4807 12.7575 12.9979 14.2404 10.4815 14.2404H5.00571C2.48288 14.2404 1 12.7575 1 10.2411V4.75891C1 2.24251 1.92439 0.759628 4.4408 0.759628H5.84665C6.35179 0.759628 6.82744 0.997458 7.13053 1.40157L7.77247 2.25535C8.07621 2.65867 8.55145 2.89628 9.05635 2.89729H11.0464C13.5692 2.89729 14.5 4.18117 14.5 6.74893L14.4807 10.2411Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M4.36328 9.34874H11.1101" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
</svg>';


/* aos_invoices */
$module_icon_content['aos_invoices'] = '<svg width="15" height="17" viewBox="0 0 15 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path fill-rule="evenodd" clip-rule="evenodd" d="M9.7653 0.834229H4.20486C2.4829 0.834229 1 2.22935 1 3.95132V13.0201C1 14.839 2.38593 16.1756 4.20486 16.1756H10.8821C12.604 16.1756 14 14.742 14 13.0201V5.24446L9.7653 0.834229Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M9.54541 0.824432V3.25608C9.54541 4.44307 10.5059 5.40603 11.6929 5.40854C12.7929 5.41104 13.9189 5.41188 13.9949 5.40687" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M8.744 10.368C8.744 10.6614 8.66667 10.9334 8.512 11.184C8.36267 11.4294 8.13867 11.632 7.84 11.792C7.54667 11.9467 7.19733 12.032 6.792 12.048V12.696H6.28V12.04C5.71467 11.992 5.25867 11.824 4.912 11.536C4.56533 11.2427 4.38133 10.8454 4.36 10.344H5.816C5.848 10.664 6.00267 10.8694 6.28 10.96V9.69603C5.864 9.58936 5.53067 9.48536 5.28 9.38403C5.03467 9.2827 4.81867 9.12003 4.632 8.89603C4.44533 8.67203 4.352 8.36536 4.352 7.97603C4.352 7.4907 4.53067 7.10136 4.888 6.80803C5.25067 6.5147 5.71467 6.34936 6.28 6.31203V5.66403H6.792V6.31203C7.352 6.3547 7.79467 6.51736 8.12 6.80003C8.44533 7.0827 8.62667 7.4747 8.664 7.97603H7.2C7.168 7.68803 7.032 7.50136 6.792 7.41603V8.65603C7.23467 8.7787 7.576 8.88803 7.816 8.98403C8.056 9.08003 8.26933 9.24003 8.456 9.46403C8.648 9.6827 8.744 9.98403 8.744 10.368ZM5.8 7.91203C5.8 8.04536 5.84 8.15736 5.92 8.24803C6.00533 8.3387 6.12533 8.4187 6.28 8.48803V7.37603C6.13067 7.4027 6.01333 7.46136 5.928 7.55203C5.84267 7.63736 5.8 7.75736 5.8 7.91203ZM6.792 10.984C6.952 10.9574 7.07733 10.8934 7.168 10.792C7.264 10.6907 7.312 10.5654 7.312 10.416C7.312 10.2774 7.26667 10.1654 7.176 10.08C7.09067 9.98936 6.96267 9.91203 6.792 9.84803V10.984Z" fill="black"/>
</g>
</svg>';


/* aok_knowledgebase */
$module_icon_content['aok_knowledgebase'] = '<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path fill-rule="evenodd" clip-rule="evenodd" d="M8 1.5C11.8663 1.5 15 4.63449 15 8.5C15 12.3655 11.8663 15.5 8 15.5C4.13449 15.5 1 12.3655 1 8.5C1 4.63449 4.13449 1.5 8 1.5Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M7.99609 5.62744V8.97155" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M7.99609 11.3726H8.00443" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
</svg>';


/* meetings */
$module_icon_content['meetings'] = '<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path d="M1.04102 11.2319C1.04102 11.2319 1.1485 12.5475 1.17348 12.9623C1.20679 13.5187 1.42177 14.1402 1.78057 14.5716C2.28697 15.1832 2.88346 15.399 3.67978 15.4005C4.61614 15.402 11.425 15.402 12.3614 15.4005C13.1577 15.399 13.7542 15.1832 14.2606 14.5716C14.6194 14.1402 14.8343 13.5187 14.8684 12.9623C14.8926 12.5475 15.0001 11.2319 15.0001 11.2319" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M5.34961 3.55062V3.26979C5.34961 2.3463 6.09749 1.59842 7.02098 1.59842H8.97545C9.89818 1.59842 10.6468 2.3463 10.6468 3.26979L10.6476 3.55062" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M7.99805 12.1412V11.1617" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M1 5.86659V8.49097C2.45185 9.44852 4.19134 10.1192 6.10039 10.3849C6.329 9.55147 7.08066 8.9406 7.99431 8.9406C8.89358 8.9406 9.66038 9.55147 9.87384 10.3925C11.7905 10.1268 13.5368 9.45609 14.9962 8.49097V5.86659C14.9962 4.5843 13.9644 3.5518 12.6822 3.5518H3.3216C2.03931 3.5518 1 4.5843 1 5.86659Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
</svg>';


/* notes */
$module_icon_content['notes'] = '<svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path d="M8.76221 13.9267H14.0003" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M8.06797 1.65289C8.67335 0.881584 9.65164 0.921833 10.4238 1.52721L11.5655 2.42254C12.3376 3.02792 12.6112 3.96679 12.0058 4.73973L5.19715 13.4261C4.96962 13.7169 4.62217 13.8886 4.25254 13.8927L1.6265 13.9263L1.0318 11.3677C0.948018 11.0087 1.0318 10.6309 1.25933 10.3393L8.06797 1.65289Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M6.79297 3.27953L10.7308 6.36637" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
</svg>';


/* aos_products */
$module_icon_content['aos_products'] = '<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path d="M8 16.0276V7.79562" stroke="black" stroke-width="1.8" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M4.5 2.54565L11.5 6.04565" stroke="black" stroke-width="1.8" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M1.22412 4.40765L8.00011 7.79565L14.7761 4.40765" stroke="black" stroke-width="1.8" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M8.62302 1.11064L14.223 3.91064C14.4562 4.02654 14.6525 4.20521 14.7898 4.42657C14.9271 4.64792 14.9999 4.90317 15 5.16363V11.8346C14.9999 12.0951 14.9271 12.3503 14.7898 12.5717C14.6525 12.793 14.4562 12.9717 14.223 13.0876L8.62302 15.8876C8.42851 15.9849 8.21401 16.0356 7.99652 16.0356C7.77902 16.0356 7.56452 15.9849 7.37002 15.8876L1.77003 13.0876C1.53702 12.9702 1.34146 12.79 1.2054 12.5674C1.06935 12.3448 0.998218 12.0885 1.00003 11.8276V5.16363C1.00017 4.90317 1.07297 4.64792 1.21023 4.42657C1.34749 4.20521 1.54378 4.02654 1.77703 3.91064L7.37702 1.11064C7.57061 1.01445 7.78384 0.964386 8.00002 0.964386C8.21619 0.964386 8.42943 1.01445 8.62302 1.11064V1.11064Z" stroke="black" stroke-width="1.8" stroke-linecap="round" stroke-linejoin="round"/>
</g>
</svg>';


/* bc_proposal */
$module_icon_content['bc_proposal'] = '<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path d="M11.0517 12.086H4.98633" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M11.0517 8.56897H4.98633" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M7.30076 5.06015H4.98633" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M11.2137 0.767029C11.2137 0.767029 4.76442 0.770389 4.75434 0.770389C2.4357 0.784671 1 2.31026 1 4.6373V12.3627C1 14.7015 2.44662 16.233 4.78542 16.233C4.78542 16.233 11.2339 16.2305 11.2448 16.2305C13.5635 16.2162 15 14.6897 15 12.3627V4.6373C15 2.2985 13.5525 0.767029 11.2137 0.767029Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
</svg>';


/* aos_quotes */
$module_icon_content['aos_quotes'] = '<svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path fill-rule="evenodd" clip-rule="evenodd" d="M13.3767 13.6257C11.161 15.8416 7.88009 16.3204 5.19515 15.0787C4.79879 14.9191 4.47383 14.7901 4.16489 14.7901C3.3044 14.7952 2.23335 15.6296 1.67669 15.0736C1.12003 14.5168 1.95502 13.4449 1.95502 12.5792C1.95502 12.2703 1.83115 11.9511 1.67159 11.554C0.429302 8.86948 0.908729 5.58745 3.12444 3.37222C5.95291 0.542713 10.5483 0.542713 13.3767 3.3715C16.2103 6.20538 16.2052 10.7969 13.3767 13.6257Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M11.1055 8.79938H11.1122" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M8.19922 8.79938H8.20597" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M5.29297 8.7995H5.29972" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
</g>
</svg>';


/* print */
$module_icon_content['print'] = '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M5 7.50002V1.66669H15V7.50002" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M4.99984 15H3.33317C2.89114 15 2.46722 14.8244 2.15466 14.5118C1.8421 14.1993 1.6665 13.7754 1.6665 13.3333V9.16667C1.6665 8.72464 1.8421 8.30072 2.15466 7.98816C2.46722 7.6756 2.89114 7.5 3.33317 7.5H16.6665C17.1085 7.5 17.5325 7.6756 17.845 7.98816C18.1576 8.30072 18.3332 8.72464 18.3332 9.16667V13.3333C18.3332 13.7754 18.1576 14.1993 17.845 14.5118C17.5325 14.8244 17.1085 15 16.6665 15H14.9998" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M15 11.6667H5V18.3334H15V11.6667Z" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* download */
$module_icon_content['download'] = '<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M10.1021 12.8634L10.1021 2.82922" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M12.5322 10.4236L10.1022 12.8636L7.67223 10.4236" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path opacity="0.4" d="M13.9626 6.77332H14.7401C16.4359 6.77332 17.8101 8.14748 17.8101 9.84415V13.9141C17.8101 15.6058 16.4392 16.9766 14.7476 16.9766H5.46423C3.76839 16.9766 2.39339 15.6016 2.39339 13.9058L2.39339 9.83498C2.39339 8.14415 3.76506 6.77332 5.45589 6.77332L6.24089 6.77332" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* calendar */
$module_icon_content['calendar'] = '<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<g opacity="0.4">
<path d="M1.06982 6.55315H14.4378" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M4.41895 12.3971H4.42589M11.0821 9.48219H11.089H11.0821ZM7.75398 9.48219H7.76093H7.75398ZM4.41895 9.48219H4.42589H4.41895ZM11.0821 12.3971H11.089H11.0821ZM7.75398 12.3971H7.76093H7.75398Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M10.7827 0.999939V3.46802" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M4.72363 0.999939V3.46802" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M10.9287 2.18443H4.57822C2.3757 2.18443 1 3.41138 1 5.6667V12.4539C1 14.7447 2.3757 16 4.57822 16H10.9217C13.1312 16 14.5 14.766 14.5 12.5107V5.6667C14.5069 3.41138 13.1381 2.18443 10.9287 2.18443Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
</svg>';


/* default */
$module_icon_content['default'] = '<svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M5.25001 1.00001H8.75002C9.02617 1.00001 9.25002 1.22386 9.25002 1.50001V5.00002C9.25002 5.27617 9.02617 5.50002 8.75002 5.50002H5.25001C4.97386 5.50002 4.75001 5.27617 4.75001 5.00002V1.49998C4.75001 1.22386 4.97386 1.00001 5.25001 1.00001ZM5.74999 1.99999V4.49998H8.24998V1.99999H5.74999ZM13 0H16.5C16.7761 0 17 0.223855 17 0.500005V4.00001C17 4.27616 16.7761 4.50001 16.5 4.50001H13C12.7238 4.50001 12.5 4.27616 12.5 4.00001V0.500005C12.5 0.223855 12.7239 0 13 0ZM13.5 3.5H16V1.00001H13.5V3.5ZM11.25 6.74998H14.75C15.0262 6.74998 15.25 6.97384 15.25 7.24999V10.75C15.25 11.0261 15.0262 11.25 14.75 11.25H11.25C10.9739 11.25 10.75 11.0261 10.75 10.75V7.24999C10.75 6.97384 10.9739 6.74998 11.25 6.74998ZM11.75 10.25H14.25V7.74999H11.75V10.25ZM11 12.5C11.2761 12.5 11.5 12.7238 11.5 13V16.5C11.5 16.7761 11.2761 17 11 17H0.500007C0.223856 17 0 16.7761 0 16.5V5.99999C0 5.72384 0.223856 5.49999 0.500007 5.49999H4.00001C4.27616 5.49999 4.50002 5.72384 4.50002 5.99999V8.99999H7.50002C7.77617 8.99999 8.00003 9.22384 8.00003 9.49999V12.5H11ZM1.00001 6.5V16H10.5V13.5H7.50002C7.22387 13.5 7.00001 13.2761 7.00001 13V10H4.00001C3.72386 10 3.50001 9.77614 3.50001 9.49999V6.5H1.00001Z" fill="#8D8D8D"/>
</svg>';


/* tick-square */
$module_icon_content['tick-square'] = '<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M11.2798 1.5H4.71946C2.4333 1.5 1 3.1187 1 5.40941V11.5906C1 13.8813 2.42649 15.5 4.71946 15.5H11.279C13.5728 15.5 15 13.8813 15 11.5906V5.40941C15 3.1187 13.5728 1.5 11.2798 1.5Z" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path opacity="0.4" d="M5.30469 8.50001L7.10123 10.2958L10.6928 6.70422" stroke="#333333" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* create */
$module_icon_content['create'] = '<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<path opacity="0.4" d="M10.5669 8.4933H5.43359M8.00026 5.92908V11.0575V5.92908Z" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M11.28 1.5H4.72C2.43333 1.5 1 3.11846 1 5.40961V11.5904C1 13.8815 2.42667 15.5 4.72 15.5H11.28C13.5733 15.5 15 13.8815 15 11.5904V5.40961C15 3.11846 13.5733 1.5 11.28 1.5Z" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* status-change */
$module_icon_content['status-change'] = '<svg width="16" height="19" viewBox="0 0 16 19" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M11.8887 0.944458L14.9998 4.05557L11.8887 7.16668" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M1 8.72221V7.16665C1 6.34153 1.32778 5.55021 1.91122 4.96677C2.49467 4.38332 3.28599 4.05554 4.11111 4.05554H15" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M4.11111 18.0556L1 14.9445L4.11111 11.8334" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M15 10.2778V11.8334C15 12.6585 14.6722 13.4498 14.0888 14.0333C13.5053 14.6167 12.714 14.9445 11.8889 14.9445H1" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* export */
$module_icon_content['export'] = '<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M4.4947 6.14103H3.78865C2.24865 6.14103 1 7.38968 1 8.92968L1 12.6189C1 14.1581 2.24865 15.4068 3.78865 15.4068H12.2114C13.7514 15.4068 15 14.1581 15 12.6189V8.92211C15 7.38665 13.7551 6.14103 12.2197 6.14103H11.5061" stroke="#8D8D8D" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M8 0.999998V10.1121" stroke="#8D8D8D" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M5.79395 3.21601L7.99989 1.00023L10.2066 3.21601" stroke="#8D8D8D" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* delete */
$module_icon_content['delete'] = '<svg width="15" height="16" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M12.9394 6.14964C12.9394 6.14964 12.5231 11.3127 12.2816 13.4875C12.1666 14.5263 11.525 15.135 10.474 15.1541C8.47392 15.1901 6.47156 15.1924 4.47227 15.1503C3.46112 15.1296 2.83021 14.5132 2.71752 13.4929C2.47451 11.2989 2.06055 6.14964 2.06055 6.14964" stroke="#8D8D8D" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M14 3.67467H1" stroke="#8D8D8D" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M11.4952 3.67467C10.8934 3.67467 10.3752 3.2492 10.2571 2.65969L10.0708 1.7275C9.95582 1.29744 9.56639 1 9.12253 1H5.87751C5.43365 1 5.04422 1.29744 4.92923 1.7275L4.74294 2.65969C4.62488 3.2492 4.10666 3.67467 3.50488 3.67467" stroke="#8D8D8D" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* edit */
$module_icon_content['edit'] = '<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M8.35857 1.03249H5.21127C2.62289 1.03249 1 2.86497 1 5.45924V12.4575C1 15.0518 2.61532 16.8843 5.21127 16.8843H12.6389C15.2357 16.8843 16.851 15.0518 16.851 12.4575V9.06698" stroke="#8D8D8D" stroke-linecap="round" stroke-linejoin="round"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M6.11461 7.87751L12.405 1.58712C13.1887 0.804293 14.4589 0.804293 15.2425 1.58712L16.2669 2.61153C17.0506 3.3952 17.0506 4.66624 16.2669 5.44906L9.94625 11.7698C9.60366 12.1123 9.13902 12.3051 8.65417 12.3051H5.50098L5.5801 9.1233C5.59189 8.65528 5.78296 8.20916 6.11461 7.87751Z" stroke="#8D8D8D" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M11.4502 2.55901L15.2936 6.40243" stroke="#8D8D8D" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* view */
$module_icon_content['view'] = '<svg width="22" height="18" viewBox="0 0 22 18" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M14.4177 8.89427C14.4177 10.7818 12.8869 12.3116 10.9993 12.3116C9.11176 12.3116 7.58203 10.7818 7.58203 8.89427C7.58203 7.00562 9.11176 5.47589 10.9993 5.47589C12.8869 5.47589 14.4177 7.00562 14.4177 8.89427Z" stroke="#8D8D8D" stroke-linecap="round" stroke-linejoin="round"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M10.9978 16.7881C15.1146 16.7881 18.88 13.8281 21 8.89405C18.88 3.96 15.1146 1 10.9978 1H11.0022C6.8854 1 3.12 3.96 1 8.89405C3.12 13.8281 6.8854 16.7881 11.0022 16.7881H10.9978Z" stroke="#8D8D8D" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* arrow-down */
$module_icon_content['arrow-down'] = '<svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M4.02734 10.8997V0.977554" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M7.0009 7.91523L4.02974 10.9001L1.05859 7.91523" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* arrow-up */
$module_icon_content['arrow-up'] = '<svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M3.97266 1.0004V10.9226" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M1 3.98491L3.97115 1L6.9423 3.98491" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* arrow */
$module_icon_content['arrow'] = '<svg width="16" height="14" viewBox="0 0 16 14" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M12.0273 12.8997V2.97755" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M15.0009 9.91523L12.0297 12.9001L9.05859 9.91523" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M3.97266 1.0004V10.9226" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M1 3.98491L3.97115 1L6.9423 3.98491" stroke="#8D8D8D" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* image-upload */
$module_icon_content['image-upload'] = '<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M15.6858 1.21939H6.31064C3.04689 1.21939 1 3.48646 1 6.69477V15.3487C1 18.557 3.03822 20.8241 6.31064 20.8241H15.6804C18.9582 20.8241 20.9954 18.557 20.9954 15.3487V6.69477C20.9997 3.48646 18.9615 1.21939 15.6858 1.21939Z" stroke="#1045F9" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path opacity="0.4" fill-rule="evenodd" clip-rule="evenodd" d="M9.6178 7.63093C9.6178 8.71453 8.72275 9.59204 7.61749 9.59204C6.51332 9.59204 5.61719 8.71453 5.61719 7.63093C5.61719 6.54732 6.51332 5.66982 7.61749 5.66982C8.72167 5.67088 9.61671 6.54838 9.6178 7.63093Z" stroke="#1045F9" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path opacity="0.4" d="M21.0001 14.1814C19.9999 13.1722 18.0766 11.1335 15.9852 11.1335C13.8929 11.1335 12.6868 15.6305 10.6746 15.6305C8.66241 15.6305 6.8344 13.5971 5.22204 14.9006C3.60967 16.2031 2.08398 18.8664 2.08398 18.8664" stroke="#1045F9" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* search */
$module_icon_content['search'] = '<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M7.82491 14.5664C11.5481 14.5664 14.5663 11.5481 14.5663 7.82494C14.5663 4.10176 11.5481 1.08353 7.82491 1.08353C4.10173 1.08353 1.0835 4.10176 1.0835 7.82494C1.0835 11.5481 4.10173 14.5664 7.82491 14.5664Z" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M12.5137 12.8638L15.1567 15.5" stroke="black" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';


/* close */
$module_icon_content['close'] = '<svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M13.5911 13.5833L8.40625 8.3974M13.5895 8.39967L8.40895 13.5802L13.5895 8.39967Z" stroke="#8D8D8D" stroke-linecap="round" stroke-linejoin="round"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M15.6854 1H6.31351C3.04757 1 1 3.31243 1 6.58487V15.4151C1 18.6876 3.03784 21 6.31351 21H15.6843C18.9611 21 21 18.6876 21 15.4151V6.58487C21 3.31243 18.9611 1 15.6854 1Z" stroke="#8D8D8D" stroke-linecap="round" stroke-linejoin="round"/>
</svg>';
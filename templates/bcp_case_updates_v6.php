<?php defined('ABSPATH') or die('No script kiddies please!'); ?>
<div class='updates-details comments-section'>
    <h3><?php echo $language_array['lbl_case_update']; ?></h3>
    <?php
    $sel_update_fields = array('id', 'date_entered', 'contact_id', 'internal', 'description');
    $getCaseCommetsResult = $objSCP->getCaseCommentDetails($id);
    if (isset($getCaseCommetsResult->list_data) && $getCaseCommetsResult->list_data != NULL) {
        $cntcomts = 0;
        $countCaseCommets = 0;
        foreach ($getCaseCommetsResult->list_data as $setCaseCommetsObj) {
            $get_internal = $setCaseCommetsObj->internal;
            if ($get_internal == 1) {
                continue;
            }
            $countCaseCommets++;
        }
        $countCaseCommets = $countCaseCommets - 1;
        
        ?>
        <div class='updates-details-div user-details-1'>
        <?php
            $ctn = 0;
            $bgcolor = '';
            foreach ($getCaseCommetsResult->list_data as $setCaseCommets) {
                $lastcomt = '';
                //get internal
                $get_internal = $setCaseCommets->internal;
                if ($get_internal == 1) {
                    continue;
                }
                //get date entered
                $get_date = $setCaseCommets->date_entered;
                if ($_SESSION['browser_timezone'] != NULL) {
                    $date_entered = scp_user_time_convert($get_date);
                } else {
                    $UTC = new DateTimeZone("UTC");
                    $newTZ = new DateTimeZone($result_timezone);
                    $date = new DateTime($get_date, $UTC);
                    $date->setTimezone($newTZ);
                    $date_entered = $date->format($objSCP->date_format . " " . $objSCP->time_format);
                }

                //get contact user name
                $contact_iid = $setCaseCommets->contact_id;

                if (isset($contact_iid) && !empty($contact_iid)) {
                    $where_con_cc = "contacts.id = '{$contact_iid}'";
                    $record_detail_contact_cc = $objSCP->get_entry_list('Contacts', $where_con_cc);
                    $salutation = $record_detail_contact_cc->entry_list[0]->name_value_list->salutation->value;
                    $contact_user_name = $record_detail_contact_cc->entry_list[0]->name_value_list->name->value;
                    if (isset($salutation) && !empty($salutation)) {
                        $con_name = "- " . $salutation . " " . $contact_user_name;
                    } else {
                        $con_name = $contact_user_name;
                    }
                } else {
                    $con_name = 'Administrator';
                }

                //get comment description, nl2br and exclude tags
                $comment_name = html_entity_decode($setCaseCommets->description);
                $comment_name = nl2br($comment_name);
                $comment_name = strip_tags($comment_name, '<br /><br><br/>');

                //get comment id
                $comment_id = $setCaseCommets->id;

                if ($countCaseCommets == $cntcomts) {
                    $lastcomt = 'last';
                }
                if ($ctn == 0) {
                    $ctn = 1;
                    $bgcolor = '';
                } else {
                    $ctn = 0;
                    $bgcolor = 'dark-gray-right';
                }
                $auth_names = explode(" ", $con_name);
                $Auth_letter = strtoupper($auth_names[0])[0];
                $Auth_letter .= (isset($auth_names[1])) ? strtoupper($auth_names[1])[0] : "";
                ?>
                <div class="user-comment-list <?php echo $bgcolor; ?>">
                    <div class="user-comment-list-name">
                        <div class="user-description">
                            <?php echo $comment_name; ?>
                        </div>
                        <?php if (isset($setCaseCommets->notes_data) && !empty($setCaseCommets->notes_data)) { ?>
                            <div class="attachment-div">
                            <?php
                            foreach ($setCaseCommets->notes_data as $notes) {
                                $note_name = $notes->filename;
                                $noteid = $notes->id;
                                ?>
                                <div class="attachment-details">
                                    <a title="<?php echo $language_array['lbl_download']; ?>" href="javascript:void(0);" onclick='form_submit_note_document("<?php echo $noteid; ?>", "filename", "0", "<?php echo $id; ?>");' class='scp-Notes-Attachment-font' >
                                        <?php echo $note_name; ?>
                                        <?php echo $module_icon_content['download']; ?>
                                    </a>
                                </div>
                            <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="post-comment-user d-flex align-items-center">
                        
                        <div class="user-image"><?php echo $Auth_letter; ?></div>
                        <div class="user-name"><?php echo $con_name; ?></div>
                        <div class="post-date">
                            <p><?php echo $date_entered; ?></p>
                        </div>
                    </div>
                </div>
                <?php
                $cntcomts++;
            }
            ?>
        </div>
    <?php } ?>

    <div class='scp-case-form'>
        <form action = '<?php echo site_url(); ?>/wp-admin/admin-post.php' method = 'post' enctype = 'multipart/form-data' id = 'case_updates' class='scp-form'>
            <?php wp_nonce_field('sfcp_portal_nonce', 'sfcp_portal_nonce_field'); ?>
            <input type = 'hidden' name = 'action' value = 'prefix_admin_bcp_case_updates'>
            <input type = 'hidden' value = '<?php echo $id; ?>' name='id'>
            
            <div class='form-group'>
                <textarea rows="2" class="form-control" cols="95" name="update_text" id="update_text" placeholder="<?php echo $language_array['msg_please_enter_comment'] . "...."; ?>"></textarea>
            </div>
            <div class='form-group'>
                <div class='file-field'>
                    <div>
                        <label class="custom-file">No file Chosen</label>
                        <input type='file' name='commentfile[]' class='upload_btn commentfile form-control' multiple>
                    </div>
                </div>
                <span class='scp-information'><?php echo $language_array['lbl_maximum_upload_size'] . ': 2MB.'; ?></span>
            </div>
            <div class='form-group'>
                <div class='scp-form-actions'>
                    <span>
                        <input value='<?php echo $language_array['lbl_save']; ?>' class='common-btn action-form-btn scp-Cases-update btn' type='submit'>
                    </span>
                </div>
            </div>
        </form>
    </div>
</div>
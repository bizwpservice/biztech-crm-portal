<?php

// If class exits check start
if (!class_exists('SugarRestApiCall')) {

    // Class Starts
    Class SugarRestApiCall {

        var $username;
        var $password;
        var $url;
        var $session_id;
        //Added by BC on 05-aug-2015 for login with admin user
        var $user_id;
        var $date_format;
        var $time_format;

        function __construct($url, $username, $password) {
            $custom_api_url = "/custom/service/v4_1_custom/rest.php"; //Added on 05-dec-2015 appended rest url
            $this->url = rtrim($url, '/') . $custom_api_url; //remove back slash if any in url
            $this->username = $username;
            $this->password = $password;
            $login_response = $this->login();

            if (( $login_response != null ) && $login_response != 'portal-disable' && (!isset($login_response->name) || $login_response->name != 'Invalid Login' )) {
                $this->session_id = $login_response->id;
                $this->user_id = $login_response->name_value_list->user_id->value;
                $this->date_format = $login_response->name_value_list->user_default_dateformat->value;
                $this->time_format = $login_response->name_value_list->user_default_timeformat->value;
            }
        }

        public function call($method, $parameters, $url) {
            if (is_multisite() && get_option('biztech_disable_portal') == 'portal-disable' && !is_admin()) {   //if it's multisite and portal is disabled
                if (isset($_SESSION['scp_user_id']) && ($_SESSION['scp_use_blog_id'] == get_current_blog_id())) {   //only logout if user logged in
                    sugar_crm_portal_logout();
                }
                return 'portal-disable';
            }
            ob_start();
            $curl_request = curl_init();

            curl_setopt($curl_request, CURLOPT_URL, $url);
            curl_setopt($curl_request, CURLOPT_POST, 1);
            curl_setopt($curl_request, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
            curl_setopt($curl_request, CURLOPT_HEADER, 1);
            curl_setopt($curl_request, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($curl_request, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl_request, CURLOPT_FOLLOWLOCATION, 0);
            curl_setopt($curl_request, CURLOPT_TIMEOUT, 10);

            $jsonEncodedData = json_encode($parameters);

            $post = array(
                "method" => $method,
                "input_type" => "JSON",
                "response_type" => "JSON",
                "rest_data" => $jsonEncodedData
            );
          
            curl_setopt($curl_request, CURLOPT_POSTFIELDS, $post);
            $result = curl_exec($curl_request);
            curl_close($curl_request);

            $result = explode("\r\n\r\n", $result, 2);
            $response = isset($result[1]) ? json_decode($result[1]) : '';
           
            if ((isset($response->number) && $response->number == 11 ) && isset($_SESSION['scp_user_id'])) {
                $msg = "msg_lbl_session_timed_out";
                setcookie('bcp_login_session_error', $msg, time() + 3600, '/');
                unset($_SESSION['scp_user_id']);
            }
            if ( ( $method != 'login' ) && 
                    $method != 'IsActivate' && 
                    ( isset($response->error) && $response->error != '' ) ) {

                global $objSCP;
                unset($_SESSION['scp_object']);
                bcp_suger_connection();
                if ((!is_object($objSCP) && $objSCP != 0 ) || is_object($objSCP)) {
                    $this->url = $objSCP->url;
                    $this->username = $objSCP->username;
                    $this->password = $objSCP->password;
                    $this->session_id = $objSCP->session_id;
                    $response = $this->call($method, $parameters, $url);
                }
            }
            ob_end_flush();

            return $response;
        }

        //Added by BC on 05-aug-2015 for login with admin user
        public function login() {
            $login_parameters = array(
                "user_auth" => array(
                    "user_name" => $this->username,
                    "password" => md5($this->password),
                ),
            );
            $login_response = $this->call('login', $login_parameters, $this->url);
            return $login_response;
        }

        // login into Portal (login call in Users module, it give all information about contact)
        public function PortalLogin($username, $password, $login_by_email = 0, $fields = array(), $lang = 'en_us') {
            /* $username and $password are passed from login page */
            $response = $this->call("IsActivate", array(), $this->url);

            if ($response->success) {
                if ($login_by_email == 1) {//Added by BC on 03-aug-2016
                    $query_pass = "contacts_cstm.username_c ='{$username}' AND contacts.id in (
                    SELECT eabr.bean_id
                    FROM email_addr_bean_rel eabr JOIN email_addresses ea
                    ON (ea.id = eabr.email_address_id)
                    WHERE eabr.deleted=0 AND ea.email_address = '{$password}')";
                    ;
                } else {
                    $query_pass = "contacts_cstm.username_c = '{$username}' AND  contacts_cstm.password_c = '{$password}'";
                }
                $get_entry_list = array(
                    'session' => $this->session_id,
                    'module_name' => 'Contacts',
                    'query' => $query_pass,
                    'order_by' => '',
                    'offset' => 0,
                    'select_fields' => $fields,
                    'link_name_to_fields_array' => array(),
                    'max_results' => 0,
                    'deleted' => 0,
                );
                $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);
                return $get_entry_list_result;
            } else {
                return $response;
            }
        }
        
        /**
         * Function to get Portal Message Lists
         * 
         * @return array
         */
        public function getPortalMessagesLists($lang) {
            
            if ($this->session_id) {
                // Portal Validation and Other messages
                $lang_list = array(
                    'session' => $this->session_id,
                    'lang' => $lang,
                );
                $lang_list = $this->call("getPortalLoginMessageList", $lang_list, $this->url);
                return $lang_list;
            }
        }
        
        function verifyOTP($contactId,  $otp){
            if ($this->session_id) {
                $set_entry_parameters = array(
                    "session" => $this->session_id,
                    'args' => array(
                        'contact_id' => $contactId,
                        'otp_number' => $otp,
                    ),
                );
                $set_entry_result = $this->call("verifyOTP", $set_entry_parameters, $this->url);
                return $set_entry_result;
            }
        }

        public function PortalGenerateOTP($contactId) {
            
            if ($this->session_id) {
                $set_entry_parameters = array(
                    "session" => $this->session_id,
                    'args' => array(
                        'contact_id' => $contactId,
                    ),
                );
                $set_entry_result = $this->call("generateOTP", $set_entry_parameters, $this->url);
                return $set_entry_result;
            }
        }
        
        public function set_entry($module_name, $set_entry_dataArray) { // create a new record
            if ($this->session_id) {

                $set_entry_dataArray['assigned_user_id'] = $this->user_id;
                $set_entry_dataArray['assigned_user_name'] = $this->username;

                $nameValueListArray = array();
                $i = 0;
                foreach ($set_entry_dataArray as $field => $value) {
                    if ($field == NULL && $value == NULL) {
                        continue;
                    }
                    $nameValueListArray[$i]['name'] = $field;
                    $nameValueListArray[$i]['value'] = $value;
                    $i++;
                }
                $v = $i + 1;
                $v1 = $v + 1;
                // added condition for not send email on reset password for v3.0.0
                if (!isset($set_entry_dataArray['forgot_pass_access_token_c'])) {
                    $nameValueListArray[$v]['name'] = 'created_from_c';
                    $nameValueListArray[$v]['value'] = 1;

                    $nameValueListArray[$v1]['name'] = 'enable_portal_c';
                    $nameValueListArray[$v1]['value'] = 1;
                }
                $set_entry_parameters = array(
                    "session" => $this->session_id,
                    "module_name" => $module_name,
                    "name_value_list" => $nameValueListArray
                );
                
                $set_entry_result = $this->call("set_entry", $set_entry_parameters, $this->url);
                
                $recordID = $set_entry_result->id;
                return $recordID;
            }
        }

        //Set Profile Pic
        public function upload_profile_photo($filename, $file_path, $contact_id, $action = 'add', $lang_key='en_us') {
            $content = array('id' => $contact_id,
                'file' => $file_path,
                'filename' => $filename,
                'action' => $action);
            $get_parameters = array('session' => $this->session_id, 'content' => $content, 'lang_key' => $lang_key);
            $set_entry_result = $this->call("set_profile_picture", $get_parameters, $this->url);
            return $set_entry_result;
        }

        //Get Profile Pic
        public function get_profile_picture($contact_id, $lang_key = "en_us") {
            $content = array('id' => $contact_id);
            $get_parameters = array('session' => $this->session_id, 'content' => $content, 'lang_key' => $lang_key);
            $set_entry_result = $this->call("get_profile_picture", $get_parameters, $this->url);
            return $set_entry_result;
        }

        function get_module_fields($module_name, $fields = array()) {
            if ($this->session_id) {
                $parameters = array(
                    'session' => $this->session_id,
                    'module_name' => $module_name,
                    'fields' => $fields
                );
                $result = $this->call("get_module_fields", $parameters, $this->url);
                return $result;
            }
        }

        //Added by BC on 03-jul-2015 updated function
        function createNote($Note_set_entry_dataArray, $upload_file = "", $upload_path = "") {
            $files = $_FILES;
            if ($this->session_id) {

                foreach ($files as $key => $value) {
                    if ($value['name'] != "") {
                        $value['name'] = str_replace(' ', '_', $value['name']);
                        $upload_file = $value['name'];
                        $fileMimeType = $value['type'];
                        $Note_set_entry_dataArray[$key] = $upload_file;
                        $Note_set_entry_dataArray["file_mime_type"] = $fileMimeType;
                    } else {
                        unset($Note_set_entry_dataArray[$key]);
                    }
                }
                
                $NoteId = $this->set_entry('Notes', $Note_set_entry_dataArray);
                foreach ($files as $filename => $value) {
                    if ($value['name'] != "") {
                        $upload_file = $value['name'];
                        $upload_file = str_replace(' ', '_', $upload_file);
                        $upload_path = $value['tmp_name'];
                        $fileMimeType = $value['type'];
                        $Note_set_entry_dataArray['id'] = $NoteId;
                        $Note_set_entry_dataArray[$filename] = $upload_file;
                        $Note_set_entry_dataArray['file'] = base64_encode(file_get_contents($upload_path));
                        $Note_set_entry_dataArray['module'] = "Notes";
                        //set attachment call
                        $note_attachment = array(
                            'session' => $this->session_id,
                            'attachmentDetails' => array(
                                $Note_set_entry_dataArray,
                            ),
                            'case_id' => "",
                            'contact_id' => "",
                        );
                        $attachment = $this->call('custom_set_attachment', $note_attachment, $this->url);
                    }
                }
                return $NoteId;
            }
        }

        /**
         * To Add Attachment With Case Add
         * 
         * @param type $Note_set_entry_dataArray
         * @param type $case_id
         * @param type $contact_id
         * @return type
         */
        function createAttachment($Note_set_entry_dataArray, $case_id, $contact_id) {
            if ($this->session_id) {
                
                $parameters = array(
                    'session' => $this->session_id,
                    'attachmentDetails' => $Note_set_entry_dataArray,
                    'case_id' => $case_id,
                    'contact_id' => $contact_id,
                );
                $attachment = $this->call('custom_set_attachment', $parameters, $this->url);
                return $attachment;
            }
        }

        /**
         * Get Entry List to get multiple records of single module
         * 
         * @param type $module_name
         * @param type $where_condition
         * @param type $select_fields_array
         * @param type $order_by
         * @param type $deleted
         * @param type $limit
         * @param type $offset
         * @return type
         */
        public function get_entry_list($module_name, $where_condition = '', $select_fields_array = array(), $order_by = '', $deleted = 0, $limit = '', $offset = '') {
            if ($this->session_id) {
                $get_entry_list_parameters = array(
                    'session' => $this->session_id,
                    'module_name' => $module_name,
                    'query' => $where_condition,
                    'order_by' => $order_by,
                    "offset" => $offset,
                    'select_fields' => $select_fields_array,
                    'link_name_to_fields_array' => array(),
                    'max_results' => $limit,
                    'deleted' => $deleted,
                );
                $get_entry_list_result = $this->call("get_entry_list", $get_entry_list_parameters, $this->url);
                return $get_entry_list_result;
            }
        }

        /**
         * Get Single Record of Module
         * 
         * @param type $module_name
         * @param type $record_id
         * @param type $select_fields_array
         * @return type
         */
        public function get_entry($module_name, $record_id, $select_fields_array = array()) {
            if ($this->session_id) {
                $get_entry_parameters = array(
                    'session' => $this->session_id,
                    'module_name' => $module_name,
                    'id' => $record_id,
                    'select_fields' => $select_fields_array,
                    'link_name_to_fields_array' => array(),
                );
                $get_entry_result = $this->call("get_entry", $get_entry_parameters, $this->url);
                return $get_entry_result;
            }
        }

        /**
         * Set Relationship with module and record
         * 
         * @param type $module_name
         * @param type $module_id
         * @param type $relationship_name
         * @param type $related_ids_array
         * @param type $deleted
         * @return type
         */
        public function set_relationship($module_name, $module_id, $relationship_name, $related_ids_array = array(), $deleted = 0) {// set reletion between task and contact
            if ('users' == $relationship_name) {
                $related_ids_array = array($this->user_id);
            }
            if ($this->session_id) {
                $set_relationships_parameters = array(
                    'session' => $this->session_id,
                    'module_name' => $module_name,
                    'module_id' => $module_id,
                    'link_field_name' => $relationship_name,
                    'related_ids' => $related_ids_array,
                    'name_value_list' => '',
                    'deleted' => $deleted,
                );
                $set_relationships_result = $this->call("set_relationship", $set_relationships_parameters, $this->url);
                return $set_relationships_result;
            }
        }
        
        /**
         * function to get related records with contacts and with total count for v3.0.0
         *
         * @param type $module_name
         * @param type $module_id
         * @param type $relationship_name
         * @param type $related_fields_array
         * @param type $where_condition
         * @param type $order_By
         * @param type $deleted
         * @param type $offset
         * @param type $limit
         * @return type
         */
        public function get_relationships($module_name, $module_id, $relationship_name, $related_fields_array = array(), $where_condition = '', $order_By = '', $deleted = 0, $offset = 0, $limit = '',$advanced_filter = array()) {
            if ($this->session_id) {
                $get_relationships_parameters = array(
                    'data' => array(
                        'session' => $this->session_id,
                        'module_name' => $module_name,
                        'module_id' => $module_id,
                        'link_field_name' => $relationship_name, // relationship name
                        'related_module_query' => $where_condition, // where condition
                        'related_fields' => $related_fields_array,
                        'related_module_link_name_to_fields_array' => array(),
                        'deleted' => $deleted,
                        'order_by' => $order_By,
                        'offset' => $offset,
                        'limit' => $limit,
                        'advanced_filter' => $advanced_filter,
                    ),
                );
                $get_relationships_result = $this->call("get_custom_list_records", $get_relationships_parameters, $this->url);
                
               
                return $get_relationships_result;
            }
        }

        /**
         * Download Or View File Attached in Note Module
         * 
         * @param type $note_id
         * @param type $doc_view
         * @param type $case_id
         */
        function getNoteAttachment($note_id, $doc_view = "", $case_id = "") {

            $note_parameters = array(
                'session' => $this->session_id,
                'module_name' => 'Notes',
                'id' => $note_id,
                'select_fields' => array('file_mime_type', 'contact_id'),
                'link_name_to_fields_array' => array(),
            );
            $get_entry_result = $this->call("get_entry", $note_parameters, $this->url);
            if ($doc_view != "kb" && $case_id == "" && isset($get_entry_result->entry_list[0]->name_value_list->contact_id->value) && $get_entry_result->entry_list[0]->name_value_list->contact_id->value != $_SESSION['scp_user_id']) {
                $redirectURL_manage = get_page_link(get_option('biztech_redirect_manange'));
                wp_redirect($redirectURL_manage . "#access_denied");
                exit();
            }
            if ($case_id != "") {
                $relation_name = strtolower("Cases");
                $where_con = $relation_name . ".id = '{$case_id}'";
                $record_detail = $this->get_relationships('Contacts', $_SESSION['scp_user_id'], $relation_name, '', $where_con);
                if ( $record_detail->entry_list == NULL ) {
                    wp_redirect($redirectURL_manage."#access_denied");
                    exit();
                }
            }
            $note_params = array(
                'session' => $this->session_id,
                'id' => $note_id,
            );
            $file_mime_type = $get_entry_result->entry_list[0]->name_value_list->file_mime_type->value;
            $noteResult = $this->call('get_note_attachment', $note_params, $this->url);
            $filename = $noteResult->note_attachment->filename;
            $content = base64_decode($noteResult->note_attachment->file);
            header("Content-type: {$file_mime_type}");
            if ($doc_view) {
                header('Content-Disposition: inline; filename=' . basename($filename));
            } else {
                header('Content-Disposition: attachment; filename=' . basename($filename));
            }
            header('Content-Description: File Transfer');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            echo $content;
            exit;
        }

        /**
         * Add File For Document Module
         * 
         * @param string $Document_set_entry_dataArray
         * @param type $upload_file
         * @param type $upload_path
         * @param type $fileMimeType
         * @return type
         */
        function set_Document($Document_set_entry_dataArray, $upload_file, $upload_path, $fileMimeType = "") {

            if ($this->session_id) {
                //set created_from_c parameter
                $Document_set_entry_dataArray['created_from_c'] = '1';
                if (empty($upload_file)) {//Added by BC on 07-jul-2016
                    unset($Document_set_entry_dataArray['filename']);
                }
                $DocId = $this->set_entry('Documents', $Document_set_entry_dataArray);
                if (!empty($upload_file)) {
                    $set_document_revision_parameters = array(
                        array('id' => $DocId,
                            'document_name' => $upload_file,
                            'filename' => $upload_file,
                            'file' => base64_encode(file_get_contents($upload_path)),
                            'revision' => '1',
                            'file_mime_type' => $fileMimeType,
                            'module' => "Documents",
                        )
                    );
                    //set attachment call
                    $note_attachment = array(
                    'session' => $this->session_id,
                    'attachmentDetails' => $set_document_revision_parameters,
                    'case_id' => '',
                    'contact_id' => '',
                    );
                    $set_document_revision_result = $this->call('custom_set_attachment', $note_attachment, $this->url);
                }
                return $DocId;
            }
        }

        /**
         * Get File For Document Module
         * 
         * @param type $document_id
         * @param type $doc_view
         */
        function getDocument($document_id, $doc_view = "") {
            if ($this->session_id) {
                $select_field_array = array('document_revision_id');
                $document_result = $this->get_entry('Documents', $document_id, $select_field_array);
                $revision_id = $document_result->entry_list[0]->name_value_list->document_revision_id->value;
                $revision_params = array(
                    'session' => $this->session_id,
                    'id' => $revision_id,
                );
                $select_revision_field = array('file_mime_type');
                $revision_result = $this->get_entry('DocumentRevisions', $revision_id, $select_revision_field);
                
                $file_mime_type = $revision_result->entry_list[0]->name_value_list->file_mime_type->value;
                
                $DocRevisionResult = $this->call('get_document_revision', $revision_params, $this->url);
                $filename = $DocRevisionResult->document_revision->filename;
                $content = base64_decode($DocRevisionResult->document_revision->file);
                header("Content-type: {$file_mime_type}");
                if ($doc_view) {
                    header('Content-Disposition: inline; filename="' . basename($filename) . '"');
                } else {
                    header('Content-Disposition: attachment; filename="' . basename($filename) . '"');
                }
                header('Content-Description: File Transfer');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                echo $content;
                exit;
            }
        }

        /**
         * Download PDF For Quote Module And Invoice Module
         * 
         * @param type $download_pdf_id
         * @param type $modulename
         * @return type
         */
        function getModuleDataPDF($download_pdf_id, $modulename) {
            if ($this->session_id) {
                $get_parameters = array(
                    'session' => $this->session_id,
                    'quote_id' => $download_pdf_id,
                    'module_name' => $modulename,
                );
                $set_entry_result = $this->call("generatePDF", $get_parameters, $this->url);
                if (!isset($set_entry_result->msg) && !$set_entry_result->msg) {
                    $content = base64_decode($set_entry_result->file_content);
                    header('Content-Transfer-Encoding: binary');
                    header('Cache-Control: public, must-revalidate, max-age=0');
                    header('Pragma: public');
                    header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
                    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
                    header('Content-Type: application/force-download');
                    header('Content-Type: application/octet-stream', false);
                    header('Content-Type: application/download', false);
                    header('Content-Type: application/pdf', false);
                    header('Content-disposition: attachment; filename=' . $set_entry_result->file_name);
                    echo $content;
                    exit;
                } else {
                    return $set_entry_result->msg;
                }
            }
        }
        
        /**
        * Check user exists or not
        *
        * @param string $username
        * @param int $lock_update - 0 Do nothing
        *                           1 Update lock flag as 1
        *                           2 Update lock flag as 0
         * @return boolean
         * 
         * @deprecated since version 3.0
         */
         public function getPortalUserExists($username, $lock_update = 0) {//Updated by BC on 11-aug-2015
            $get_entry_list = array(
                'session' => $this->session_id,
                'module_name' => 'Contacts',
                'query' => "contacts_cstm.username_c = '{$username}'",
                'order_by' => '',
                'offset' => 0,
                'select_fields' => array('id', 'username_c'),
                'link_name_to_fields_array' => array(),
                'max_results' => 0,
                'deleted' => 0,
            );
            $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);
            $isUser = $get_entry_list_result->entry_list[0]->name_value_list->username_c->value;
            $contact_id = $get_entry_list_result->entry_list[0]->name_value_list->id->value;

            if ($isUser == $username) {
                if ($lock_update == 1) {
                    $nameValueListarray = array(
                        array(
                            'name' => 'id',
                            'value' => $contact_id,
                        ),
                        array(
                            'name' => 'lock_portal_user',
                            'value' => 1,
                        ),
                        array(
                            'name' => 'from_portal',
                            'value' => 1,
                        )
                    );
                    $set_entry_parameters = array(
                        "session" => $this->session_id,
                        "module_name" => 'Contacts',
                        "name_value_list" => $nameValueListarray
                    );
                    $set_entry_result = $this->call("set_entry", $set_entry_parameters, $this->url);
                } else if ($lock_update == 2) {
                    $nameValueListarray = array(
                        array(
                            'name' => 'id',
                            'value' => $contact_id,
                        ),
                        array(
                            'name' => 'lock_portal_user',
                            'value' => 0,
                        ),
                        array(
                            'name' => 'from_portal',
                            'value' => 1,
                        )
                    );
                    $set_entry_parameters = array(
                        "session" => $this->session_id,
                        "module_name" => 'Contacts',
                        "name_value_list" => $nameValueListarray
                    );
                    $set_entry_result = $this->call("set_entry", $set_entry_parameters, $this->url);
                }
                return true;
            } else {
                return false;
            }
        }

        /**
         * Get user information by email address //Added by BC on 11-sep-2015
         * 
         * @param type $email1
         * @param type $portal_username
         * @return boolean
         */
        public function getPortalEmailExists($email1, $portal_username = '') {//Updated by BC on 11-aug-2015
            if ($portal_username != '') {
                $query_pass = "(contacts_cstm.username_c != '{$portal_username}' OR contacts_cstm.username_c IS NULL )AND contacts.id in (
                SELECT eabr.bean_id
                    FROM email_addr_bean_rel eabr JOIN email_addresses ea
                        ON (ea.id = eabr.email_address_id)
                    WHERE eabr.deleted=0 AND ea.email_address ='{$email1}')";
            } else {
                $query_pass = "contacts.id in (
                    SELECT eabr.bean_id
                    FROM email_addr_bean_rel eabr JOIN email_addresses ea
                    ON (ea.id = eabr.email_address_id)
                    WHERE eabr.deleted=0 AND ea.email_address = '{$email1}')";
            }

            $get_entry_list = array(
                'session' => $this->session_id,
                'module_name' => 'Contacts',
                'query' => $query_pass,
                'order_by' => '',
                'offset' => 0,
                'select_fields' => array('id', 'email1'),
                //A list of link names and the fields to be returned for each link name.
                'link_name_to_fields_array' => array(
                ),
                //The maximum number of results to return.
                'max_results' => '',
                //If deleted records should be included in results.
                'deleted' => 0,
                //If only records marked as favorites should be returned.
                'favorites' => false,
            );

            $get_entry_list_result = $this->call("get_entry_list", $get_entry_list, $this->url);

            $isEmailExist = $get_entry_list_result->entry_list[0]->name_value_list->email1->value;
            if ($isEmailExist) {
                return true;
            } else {
                return false;
            }
        }

        /**
         * Get User Information
         * 
         * @param type $user_id
         * @return type
         */
        public function getPortalUserInformation($user_id) {
            $get_entry_parameters = array(
                'session' => $this->session_id,
                'module_name' => 'Contacts',
                'id' => ( isset($_SESSION['scp_user_id']) ? $_SESSION['scp_user_id'] : '' ),
                'select_fields' => array(),
                'link_name_to_fields_array' => array(),
            );
            $get_entry_result = $this->call("get_entry", $get_entry_parameters, $this->url);
            return $get_entry_result;
        }
        

        /**
         * Added by BC on 20-jul-2015 for getting fields
         * 
         * @param type $module_name
         * @param type $view
         * @return type
         * 
         * @deprecated since version 3.0
         */
        public function get_module_layout($module_name, $view) {
            //Session id
            $params = array('session' => $this->session_id,
                'modules' => $module_name,
                'types' => array('default'),
                'views' => $view,
                'acl_check' => false,
                'md5' => false,
            );
            $layout = $this->call("get_module_layout", $params, $this->url);
            return $layout;
        }

        //Added by BC on 03-aug-2015 for custom function made in sugar to change layout
        public function get_customListlayout($module_name, $view) {
            //Session id
            $params = array(
                'session' => $this->session_id,
                'module_name' => $module_name,
                'view' => $view,
            );
            $layout = $this->call("get_customListlayout", $params, $this->url);
            return $layout;
        }

        //Added by BC on 03-aug-2015 for store scp_key at sugarside
        public function store_wpkey($key) {
            //Session id
            $params = array(
                'session' => $this->session_id,
                'key' => $key
            );
            $message = $this->call("store_wpkey", $params, $this->url);
            return $message;
        }

        //Added by BC on 11-aug-2015 for get user timezone
        public function getUserTimezone() {
            if ($this->session_id) {
                $parameters = array(
                    'session' => $this->session_id,
                    'module_name' => 'Users',
                    'user_id' => $this->user_id,
                );
                if ($this->session_id) {
                    $get_entry_result = $this->call("getUserTimezone", $parameters, $this->url);
                }
                return $get_entry_result;
            }
        }

        //Added by BC on 05-dec-2015 for uncode string like ^module1^,^module2^...
        function unencodeMultienum($string) {
            if (is_array($string)) {
                return $string;
            }
            if (substr($string, 0, 1) == "^" && substr($string, -1) == "^") {
                $string = substr(substr($string, 1), 0, strlen($string) - 2);
            }

            return explode('^,^', $string);
        }

        //Added by BC on 05-dec-2015 get Portal user's accessible Modules.
        function getPortal_accessibleModules($modules = array(), $lang_code = "") {
            if ($this->session_id) {

                $parameters = array(
                    'session' => $this->session_id,
                    'contact_id' => (isset($_SESSION['scp_user_id']) ? $_SESSION['scp_user_id'] : ''),
                    'modules_array' => $modules,
                    'lang_code' => $lang_code,
                );

                if ($this->session_id) {
                    $get_entry_result = $this->call("getPortal_accessibleModules", $parameters, $this->url);
                }
                return $get_entry_result;
            }
        }

        //Added by BC on 23-may-2016
        //For get entry quotes & invoice
        public function get_entry_quotes($module_name, $record_id) {
            if ($this->session_id) {
                $get_entry_parameters = array(
                    'session' => $this->session_id,
                    'module_name' => $module_name,
                    'id' => $record_id,
                );
                $get_entry_result = $this->call("get_entry_quotes", $get_entry_parameters, $this->url);
                return $get_entry_result;
            }
        }

        //Added for default group lable
        public function get_all_modules() {

            $get_available_modules_parameters = array(
                //Session id
                "session" => $this->session_id,
                //Module filter. Possible values are 'default', 'mobile', 'all'.
                "filter" => 'all',
            );

            $set_entry_result = $this->call('get_available_modules', $get_available_modules_parameters, $this->url);

            return $set_entry_result;
        }

        //check chat enabled or not
        public function getChatConfiguration() {
            $login_parameters = array(
                "user_auth" => array(
                    "user_name" => $this->username,
                    "password" => md5($this->password),
                ),
            );
            $login_response = $this->call('getChatConfiguration', $login_parameters, $this->url);
            return $login_response;
        }

        public function check_enable_modules($lang = 'en_us') {
            /* $username and $password are passed from login page */
            $response = $this->call("IsActivate", array(), $this->url);

            // Portal Validation and Other messages
            $lang_list = array(
                'session' => $this->session_id,
                'lang' => $lang,
            );
            $lang_list = $this->call("getPortalConnectionMessageList", $lang_list, $this->url);
            ( $response != NULL && $response != "portal-disable" ) ? $response->portal_connection_message_list = $lang_list : '';

            return $response;
        }

        public function forgotpassword($forgot_email, $lang_key="en_us") {
            $get_parameters = array(
                //'user_name'     => $forgot_user,
                'user_email' => $forgot_email,
                'lang_key'  => $lang_key,
            );
            $set_entry_result = $this->call("forgotPassword", $get_parameters, $this->url);
            return $set_entry_result;
        }

        public function globalSearch($user_id, $module_list, $search_string, $max, $lang_key = "en_us", $c_group_id = "", $relationship_name = array()) {
            $set_entry_parameters = array(
                "session" => $this->session_id,
                "contact_id" => $user_id,
                "searchDetail" => array(
                    'module_list' => $module_list,
                    'search_string' => $search_string . '%',
                    'max_result' => $max - 1,
                ),
                'lang_key' => $lang_key,
                "extra_params" => array(
                    'contact_group_id' => $c_group_id
                ),
                "relationship_name" => $relationship_name,
            );
            
            $set_entry_result = $this->call("globalSearch", $set_entry_parameters, $this->url);
            return $set_entry_result;
        }

        //Added on 10-jan-2017 get Portal user's all accessible Modules from Default Group.
        function getAllAccessibleModules() {

            if ($this->session_id) {

                $parameters = array(
                    'session' => $this->session_id,
                );
                if ($this->session_id) {
                    $get_entry_result = $this->call("getAllAccessibleModules", $parameters, $this->url);
                }
                return $get_entry_result;
            }
        }

        // Get all visible products
        function GetProductsListCall($modulename) {
            if ($this->session_id) {

                $parameters = array(
                    'session' => $this->session_id,
                );
                $get_entry_result = $this->call("getPortalVisibleProducts", $parameters, $this->url);
                return $get_entry_result;
            }
        }

        // Get all currencies
        function portal_getCurrencylist() {
            if ($this->session_id) {

                $parameters = array(
                    'session' => $this->session_id,
                );
                $get_entry_result = $this->call("getCurrencylist", $parameters, $this->url);
                return $get_entry_result;
            }
        }

        function addQuoteDeclineReasonValue($quote_id, $reason) {
            if ($this->session_id) {

                $parameters = array(
                    'session' => $this->session_id,
                    'quote_id' => $quote_id,
                    'reason' => $reason,
                );
                $get_entry_result = $this->call("addQuoteDeclineReasonValue", $parameters, $this->url);
                return $get_entry_result;
            }
        }

        function getProductsImages($productId) {

            if ($this->session_id) {

                $parameters = array(
                    'session' => $this->session_id,
                    'productId' => $productId,
                );
                $get_entry_result = $this->call("getProductsImages", $parameters, $this->url);
                return $get_entry_result;
            }
        }

        function getInvoiceTransactionDetails($invoiceIds = array()) {
            if ($this->session_id) {

                $parameters = array(
                    'invoiceIds' => $invoiceIds,
                );
                $get_entry_result = $this->call("getInvoiceTransactionDetails", $parameters, $this->url);
                return $get_entry_result;
            }
        }

        /**
         * get file
         * 
         * @param type $module
         * @param type $id
         * @param type $fieldname
         * 
         * @deprecated since version 3.0
         */
        function getfile($module, $id, $fieldname) {
            if ($this->session_id) {
                $layout_arguments = array(
                    //Session id
                    "session" => $this->session_id,
                    //Module filter. Possible values are 'default', 'mobile', 'all'.
                    "id" => $id,
                    "field" => $fieldname,
                );

                $noteResult = $this->call("custom_get_note_attachment", $layout_arguments, $this->url);
                $content = base64_decode($noteResult->note_attachment->file);
                $file_mime_type = substr_compare($noteResult->note_attachment->filename, '.', '-1');
                $filename = $noteResult->note_attachment->filename;
                header("Content-type: {$file_mime_type}");
                header('Content-Disposition: attachment; filename=' . basename($filename));
                header('Content-Description: File Transfer');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                echo $content;
                exit;
            }
        }

        //set notification preference call
        function save_user_preference_notification($permission) {
            if ($this->session_id) {

                $parameters = array(
                    'session' => $this->session_id,
                    'contact_id' => (isset($_SESSION['scp_user_id']) ? $_SESSION['scp_user_id'] : ''),
                    'notification_preferences' => json_encode($permission),
                );
                $get_entry_result = $this->call("save_user_preference_notification", $parameters, $this->url);
                return $get_entry_result;
            }
        }

        /**
         * get notification list of contact
         *
         * @param varchar $contact_id - id of contact
         */
        function get_notification_list($offset = 0, $limit = 20, $order = 'asc', $where_args = array(), $access_modules = array(), $lang_key="en_us" ) {
            if ($this->session_id) {
                $parameters = array(
                    'session' => $this->session_id,
                    'arguments' => array(
                        'contact_id' => (isset($_SESSION['scp_user_id']) ? $_SESSION['scp_user_id'] : ''),
                        'offset' => $offset,
                        'limit' => $limit,
                        'order_by' => $order,
                        'access_modules' => $access_modules,
                        'where_args' => $where_args,
                        'lang_key' => $lang_key,
                    ),
                );
            }
            $response = $this->call("get_notification_list", $parameters, $this->url);
            return $response;
        }

        /**
         * delete all records
         *
         * @param $module_name varchar - Module name
         */
        function delete_module_records($module_name, $ids) {

            if ($this->session_id) {

                $parameters = array(
                    'session' => $this->session_id,
                    'args' => array(
                        'contact_id' => (isset($_SESSION['scp_user_id']) ? $_SESSION['scp_user_id'] : ''),
                        'module_name' => $module_name,
                        'notification_ids' => $ids,
                    ),
                );
                $get_entry_result = $this->call("delete_module_records", $parameters, $this->url);
                return $get_entry_result;
            }
        }

        /**
         * Update All Records in one Call. Use to delete multiple records
         * 
         * @param type $module_name
         * @param type $entry_data
         * @param type $where_cond
         * @return type
         */
        function mass_update_records($module_name, $entry_data, $where_cond) {
            if ($this->session_id) {
                if ($module_name == "bc_wp_portal_notification") {
                    if ($where_cond != "") {
                        $where_cond .= " AND ";
                    }
                    $where_cond .= "contact_id = '" . (isset($_SESSION['scp_user_id']) ? $_SESSION['scp_user_id'] : '') . "'";
                }
                $parameters = array(
                    'session' => $this->session_id,
                    'args' => array(
                        'module_name' => $module_name,
                        'where_query' => $where_cond,
                        'entry_data' => $entry_data,
                    ),
                );
                $get_entry_result = $this->call("mass_update_records", $parameters, $this->url);
                return $get_entry_result;
            }
        }
        
        /**
         * 
         * @param type $module_name
         * @param type $module_id
         * @param type $link_field_name
         * @param type $link_field_ids
         */
        function portal_massdelete($module_name, $module_id, $relationship_name, $related_module, $related_ids_array = array()) {
            if ($this->session_id) {
                $set_relationships_parameters = array(
                    'session' => $this->session_id,
                    'module_name' => $module_name,
                    'module_id' => $module_id,
                    'link_field_name' => $relationship_name,
                    'releted_module' => $related_module,
                    'related_ids' => $related_ids_array,
                );
                $set_relationships_result = $this->call("portal_massdelete", $set_relationships_parameters, $this->url);
                return $set_relationships_result;
            }
        }

        /**
         * get subpanel details functions
         *
         * @param type $module_name
         * @param type $record_id
         */
        function getRecordSubpanelsDetail($module_name, $record_id, $lang = 'en_us') {
            if ($this->session_id) {

                $parameters = array(
                    'session' => $this->session_id,
                    'module_name' => $module_name,
                    'record_id' => $record_id,
                );
                $get_entry_result = $this->call("getRecordSubpanelsDetail", $parameters, $this->url);
                return $get_entry_result;
            }
        }

        /**
         * This function is used to get records of related module
         * that is filtered by contact
         * get_relationship does not provide such functionality in suite
         *
         * @param varchar $module_name
         * @param varchar $module_id
         * @param varchar $relationship_name
         * @param varchar $related_fields_array
         * @param varchar $where_condition
         * @param varchar $order_By
         * @param boolean $deleted
         * @param int $offset
         * @param int $limit
         * @return array
         */
        public function get_contact_filtered_relationships($module_name, $module_id, $relationship_name, $related_fields_array = array(), $where_condition = '', $order_By = '', $deleted = 0, $offset = 0, $limit = '', $relate_third_id = '', $contact_relationship_name = "",$accountrelate_third_id = "") {
            if ($this->session_id) {
                $get_relationships_parameters = array(
                    'session' => $this->session_id,
                    'module_name' => $module_name,
                    'module_id' => $module_id,
                    'contact_id' => ($relate_third_id != "" ? $relate_third_id : $_SESSION['scp_user_id']),
                    'account_id' => ($accountrelate_third_id != "" ? $accountrelate_third_id : ''),
                    'link_field_name' => $relationship_name, // relationship name
                    'cntc_link_field_name' => $contact_relationship_name,
                    'related_module_query' => $where_condition, // where condition
                    'related_fields' => $related_fields_array,
                    'related_module_link_name_to_fields_array' => array(),
                    'deleted' => $deleted,
                    'order_by' => $order_By,
                    'offset' => $offset,
                    'limit' => $limit,
                );
                
                $get_relationships_result = $this->call("get_contact_filtered_relationships", $get_relationships_parameters, $this->url);
                return $get_relationships_result;
            }
        }

        /**
         * verify user email for v3.0.0
         *
         * @param type $scp_access_token
         * @return type
         */
        function verifyPortalUser($scp_access_token, $lang_key="en_us") {
            if ($this->session_id) {
                $parameters = array(
                    'session' => $this->session_id,
                    'scp_access_token' => $scp_access_token,
                    'lang_key' => $lang_key,
                );
                $get_entry_result = $this->call("verifyPortalUser", $parameters, $this->url);
                return $get_entry_result;
            }
        }

        /**
         * getCaseCommentDetails
         * 
         * @param type $caseid
         * @return type
         */
        function getCaseCommentDetails($caseid) {
            if ($this->session_id) {
                $parameters = array(
                    'session' => $this->session_id,
                    'args' => array('case_id' => $caseid
                    ),
                );
                $get_entry_result = $this->call("getCaseCommentDetails", $parameters, $this->url);
                return $get_entry_result;
            }
        }
        
        /**
         * Get Language lists
         */
        function get_all_language_lists() {
            if ($this->session_id) {
                $parameters = array(
                    'session' => $this->session_id,
                );
                $get_entry_result = $this->call("get_enabled_languages", $parameters, $this->url);
                return $get_entry_result;
            }
        }
        
        /**
         * Function to get custom language data
         * 
         * @param type $lang_code
         */
        function get_language_data($lang_code){
            if ($this->session_id) {
                $parameters = array(
                    'session' => $this->session_id,
                    'selected_lang' => $lang_code,
                    'msg_key' => "customer_portal_app_msgs",
                );
                $get_entry_result = $this->call("get_language_data", $parameters, $this->url);
                return $get_entry_result;
            }
        }
        
        /**
         * 
         * @param type $category_id
         * @param type $offset
         * @param type $limit
         */
        function get_knowledgebase_categories_wise($category_id, $search_name, $offset = 0, $limit = 5) {
            if ($this->session_id) {
                $parameters = array(
                    'session' => $this->session_id,
                    'category_id' => $category_id,
                    'search_val' => $search_name,
                    'offset' => $offset,
                    'limit' => $limit,
                );
                $get_entry_result = $this->call("get_knowledgebase_categories_wise", $parameters, $this->url);
                return $get_entry_result;
            }
        }
        
        /**
         * Function to get KB related to group
         * @param type $group_id
         * @return type
         */
        function get_kb_categories_group_wise($group_id) {
            if ($this->session_id) {
                $parameters = array(
                    'session' => $this->session_id,
                    'group_id' => $group_id,
                );
                $get_entry_result = $this->call("get_kb_categories_group_wise", $parameters, $this->url);
                return $get_entry_result;
            }
        }
        
        /**
         * Function to get Image of Custom module of custom field
         * 
         * @param type $module_name
         * @param type $id
         * @param type $field_type
         * @param type $field_name
         * @return type
         */
        function biz_getAttachmentData($module_name, $id, $field_type = "image", $field_name = "image_c", $doc_view = 0){
            if ($this->session_id) {
                $parameters = array(
                    'session' => $this->session_id,
                    "moduleName" => $module_name,
                    "recordID" => $id,
                    "userID" => $_SESSION['scp_user_id'],
                    "fieldType" => $field_type,
                    "fieldName" => $field_name,
                );
                $get_entry_result = $this->call("biz_getAttachmentData", $parameters, $this->url);
                
                $filename = $get_entry_result->fileName;
                $content = base64_decode($get_entry_result->imageContent);
                $file_mime_type = $get_entry_result->mime_content_type;
                header("Content-type: {$file_mime_type}");
                if ($doc_view) {
                    header('Content-Disposition: inline; filename="' . basename($filename) . '"');
                } else {
                    header('Content-Disposition: attachment; filename="' . basename($filename) . '"');
                }
                header('Content-Description: File Transfer');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                echo $content;
                exit;
            }
        }
        
        /**
         * Function to set Image of Custom module of custom field
         * 
         * @param type $field_name
         * @param type $file_path
         * @param type $record_id
         * @param type $action
         * @param type $lang_key
         * @return type
         */
        public function custom_set_image_field($field_name, $file_path, $record_id, $action = 'add', $lang_key='en_us') {
            $content = array('id' => $record_id,
                'file' => $file_path,
                'field_name' => $field_name,
                'action' => $action);
            $get_parameters = array('session' => $this->session_id, 'content' => $content, 'lang_key' => $lang_key);
            $set_entry_result = $this->call("custom_set_image_field", $get_parameters, $this->url);
            return $set_entry_result;
        }
        
        /**
         * Function to get Dashboard Data from CRM
         * 
         * @param type $modules
         * @param type $limit
         * @param type $lang_key
         * @return type
         */
        public function get_dashboard_data($modules = array(), $limit = "5", $lang_key='en_us') {
            if ($this->session_id) {
                $get_parameters['args'] = array(
                    'session'   => $this->session_id,
                    'language'  => $lang_key,
                    'modules'   => $modules,
                    "deleted"   => "0",
                    "order_by"  => "date_entered DESC",
                    "offset"    => "0",
                    "limit"     => $limit,
                );
              
                $set_entry_result = $this->call("get_dashboard_data", $get_parameters, $this->url);
                return $set_entry_result;
            }
        }
        
        /**

        * Function to delete document revision
        * 
        * @param type $document_id
        * @return type bool
        */
        public function delete_document_revision($document_id = '') {
            if ($this->session_id) {
                $get_parameters['args'] = array(
                    'session' => $this->session_id,
                    'document_id' => $document_id,
                );
                $set_entry_result = $this->call("delete_document_revision", $get_parameters, $this->url);
                return $set_entry_result;
            }
        }

    }
    // Class End
}
// If class exits check end

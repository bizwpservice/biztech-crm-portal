<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

$pro_budle_cnt = count($quote_entry->Result->ProductBundle) - 1;

?>
<div class="col-lg-8">
    <div class="bg-white shadow border-radius-5 margin-btn-30">
        <div class="case-listing-block invoice-listing">
            <?php foreach ($quote_entry->Result->ProductBundle as $k_pro => $v_pro) { ?>
                <div class="table-responsive custom-scrollbar">
                <table class="table" id="LBL_LINE_ITEMS">
                <?php if ( empty( $v_pro->Product ) ) {
                        continue;
                    } 
                    if($v_pro->name != ''){ ?>
                        <tr class="group-quotes-wrapper">
                            <th scope="row" style="text-align: left;" class="tabDetailViewDL" colspan="2"><?php echo $language_array['lbl_group_name']; ?>:</th>
                            <td style="text-align: left;" colspan="7" class="tabDetailViewDL"><?php echo $v_pro->name; ?></td>
                        </tr>
                    <?php } 
                    $ct = 1;
                    $ct_qty = 1;

                    foreach ($v_pro->Product as $k_line => $v_line) {
                        if ($ct_qty == 1) {
                            ?>
                        <thead>
                            <tr>
                                <th class="tabDetailViewDL">#</th>
                                <th class="tabDetailViewDL"><?php echo $language_array['lbl_quantity']; ?></th>
                                <th class="tabDetailViewDL"><?php echo $language_array['lbl_porduct_or_service']; ?></th>
                                <th class="tabDetailViewDL"><?php echo $language_array['lbl_list']; ?></th>
                                <th class="tabDetailViewDL"><?php echo $language_array['lbl_discount']; ?></th>
                                <th class="tabDetailViewDL"><?php echo $language_array['lbl_sales_price']; ?></th>
                                <th class="tabDetailViewDL"><?php echo $language_array['lbl_tax']; ?></th>
                                <th class="tabDetailViewDL"><?php echo $language_array['lbl_tax_amount']; ?></th>
                                <th class="tabDetailViewDL"><?php echo $language_array['lbl_total']; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        }
                        $ct_qty++;
                        $v_line->product_list_price = ( $v_line->product_list_price != NULL ) ? $v_line->product_list_price : '0';
                        $v_line->product_discount = ( $v_line->product_discount != NULL ) ? $v_line->product_discount : '0';
                        $v_line->product_discount_amount = ( $v_line->product_discount_amount != NULL ) ? $v_line->product_discount_amount : '0';
                        ?>
                            <tr>
                                <td class="tabDetailViewDF"><?php echo $ct; ?></td>
                                <?php 
                                if ($v_line->product_qty == '' || $v_line->product_qty == 0) { ?>
                                    <td class="tabDetailViewDF">-</td>
                                            <td class="tabDetailViewDF"><?php echo $v_line->name; ?></td>
                                <?php } else { ?>
                                    <td class="tabDetailViewDF"><?php echo intval($v_line->product_qty); ?></td>
                                            <td class="tabDetailViewDF"><?php echo $v_line->name; ?></td>
                                <?php } ?>
                                        <td class="tabDetailViewDF"><?php echo $currency_symbol . number_format($v_line->product_list_price, 2); ?></td>
<?php if ($v_line->discount == 'Amount') { ?>
    <td class="tabDetailViewDF"><?php echo $currency_symbol . number_format($v_line->product_discount, 2); ?></td>
<?php } else { ?>
    <td class="tabDetailViewDF"><?php echo number_format($v_line->product_discount, 2); ?>%</td>
<?php } ?>
    <td class="tabDetailViewDF"><?php echo $currency_symbol . number_format($v_line->product_unit_price, 2); ?></td>
            <td class="tabDetailViewDF"><?php echo number_format($v_line->vat, 2); ?>%</td>
            <td class="tabDetailViewDF"><?php echo $currency_symbol . number_format($v_line->vat_amt, 2); ?></td>
            <td class="tabDetailViewDF"><?php echo $currency_symbol . number_format($v_line->product_total_price, 2); ?></td>
        </tr>
        <?php   $ct++; 
            } 
        ?>
                        </tbody>
                </table>
                </div>
                <?php if($quote_entry->Result->enableGroups == 1){ 
                    $vpro_tot_amt = ( $v_pro->total_amt == "" ) ? "0" : $v_pro->total_amt;
                    $vpro_discount_amount = ( $v_pro->discount_amount == "" ) ? "0" : $v_pro->discount_amount;
                    $vpro_subtotal_amount = ( $v_pro->subtotal_amount == "" ) ? "0" : $v_pro->subtotal_amount;
                    $vpro_tax_amount = ( $v_pro->tax_amount == "" ) ? "0" : $v_pro->tax_amount;
                    $vpro_total_amount = ( $v_pro->total_amount == "" ) ? "0" : $v_pro->total_amount;
                ?>
        <div class="subtotal-block">
            <div class="d-flex">
                <label><?php echo $language_array['lbl_total']; ?>:</label>
                <p><?php echo $currency_symbol . number_format($vpro_tot_amt, 2); ?></p>
            </div>
            <div class="d-flex">
                <label><?php echo $language_array['lbl_discount']; ?>:</label>
                <p><?php echo $currency_symbol . number_format($vpro_discount_amount, 2); ?></p>
            </div>
            <div class="d-flex">
                <label><?php echo $language_array['lbl_subtotal']; ?>:</label>
                <p><?php echo $currency_symbol . number_format($vpro_subtotal_amount, 2); ?></p>
            </div>
            <div class="d-flex">
                <label><?php echo $language_array['lbl_tax']; ?>:</label>
                <p><?php echo $currency_symbol . number_format($vpro_tax_amount, 2); ?></p>
            </div>
            <div class="d-flex">
                <label><?php echo $language_array['lbl_grand_total']; ?>:</label>
                <p><?php echo $currency_symbol . number_format($vpro_total_amount, 2); ?></p>
            </div>
        </div>
                <?php } ?>
            <?php } ?>
             <?php
                $scp_total_amt = ($quote_entry->Result->name_value_list->total_amt->value == "") ? "0" : $quote_entry->Result->name_value_list->total_amt->value;
                $scp_discount_amount = ($quote_entry->Result->name_value_list->discount_amount->value == "") ? "0" : $quote_entry->Result->name_value_list->discount_amount->value;
                $scp_subtotal_amount = ($quote_entry->Result->name_value_list->subtotal_amount->value == "") ? "0" : $quote_entry->Result->name_value_list->subtotal_amount->value;
                $scp_shipping_amount = ($quote_entry->Result->name_value_list->shipping_amount->value == "") ? "0" : $quote_entry->Result->name_value_list->shipping_amount->value;
                $scp_shipping_tax_amt = ($quote_entry->Result->name_value_list->shipping_tax_amt->value == "") ? "0" : $quote_entry->Result->name_value_list->shipping_tax_amt->value;
                $scp_tax_amount = ($quote_entry->Result->name_value_list->tax_amount->value == "") ? "0" : $quote_entry->Result->name_value_list->tax_amount->value;
                $scp_total_amount = ($quote_entry->Result->name_value_list->total_amount->value == "") ? "0" : $quote_entry->Result->name_value_list->total_amount->value;
?>
            <div class="main-grand-total">
                <div class="subtotal-block">
                    <div class="d-flex">
                        <label><?php echo $language_array['lbl_total']; ?>:</label>
                        <p><?php echo $currency_symbol . number_format($scp_total_amt, 2); ?></p>
                    </div>
                    <div class="d-flex">
                        <label><?php echo $language_array['lbl_discount']; ?>:</label>
                        <p><?php echo $currency_symbol . number_format($scp_discount_amount, 2); ?></p>
                    </div>
                    <div class="d-flex">
                        <label><?php echo $language_array['lbl_subtotal']; ?>:</label>
                        <p><?php echo $currency_symbol . number_format($scp_subtotal_amount, 2); ?></p>
                    </div>
                    <div class="d-flex">
                        <label><?php echo $language_array['lbl_shipping']; ?>:</label>
                        <p><?php echo $currency_symbol . number_format($scp_shipping_amount, 2); ?></p>
                    </div>
                    <div class="d-flex">
                        <label><?php echo $language_array['lbl_shipping_tax']; ?>:</label>
                        <p><?php echo $currency_symbol . number_format($scp_shipping_tax_amt, 2); ?></p>
                    </div>
                    <div class="d-flex">
                        <label><?php echo $language_array['lbl_tax']; ?>:</label>
                        <p><?php echo $currency_symbol . number_format($scp_tax_amount, 2); ?></p>
                    </div>
                    <div class="d-flex">
                        <label><?php echo $language_array['lbl_grand_total']; ?>:</label>
                        <p><?php echo $currency_symbol . number_format($scp_total_amount, 2); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
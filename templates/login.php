<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if(get_option('biztech_scp_single_signin') != '' && is_user_logged_in()){
    if (isset($_SESSION['bcp_auth_error']) && $_SESSION['bcp_auth_error'] != '') {
        echo "<div class='error settings-error error-line error-msg' id='setting-error-settings_updated'>
                <strong>".$_SESSION['bcp_auth_error']."</strong>
            </div>";
        unset($_SESSION['bcp_auth_error']);
        exit();
    }
}

$lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";

//to get name portal name
$biztech_scp_name = fetch_data_option('biztech_scp_name');
if($biztech_scp_name != NULL){
    $biztech_scp_name = '<h1>' . $biztech_scp_name . '</h1>';
}
//to get upload logo
$biztech_upload_image = fetch_data_option('biztech_scp_upload_image');
if($biztech_upload_image != NULL){
    $sfcp_logo_url = wp_get_attachment_image_src( $biztech_upload_image, 'full' );
    $biztech_upload_image = '<div class="d-block mb-5 text-center"><img src="' . $sfcp_logo_url[0] . '" alt="'.$biztech_scp_name.'" /></div>';
}

$biztech_portal_visible_reCAPTCHA = (fetch_data_option("biztech_portal_visible_reCAPTCHA") != NULL) ? fetch_data_option("biztech_portal_visible_reCAPTCHA") : array();
$captch_form_css = "";
if ( in_array("portal-login", $biztech_portal_visible_reCAPTCHA) && get_option('biztech_scp_recaptcha_site_key') != "" && get_option('biztech_scp_recaptcha_secret_key') != "" ) {
    $captch_form_css = "width: 365px;";
}

//get option to redirect to which page
$currentURL = explode('?', $_SERVER['REQUEST_URI'], 2);
$currentURL = $currentURL[0];
//get option to redirect to which page for sign up
if (get_page_link(get_option('biztech_redirect_signup')) != NULL) {
    $redirectURL_signup = get_page_link(get_option('biztech_redirect_signup'));
} else {
    $redirectURL_signup = home_url() . "/portal-sign-up/";
}
//get option to redirect to which page for forgot pwd
if (get_page_link(get_option('biztech_redirect_forgotpwd')) != NULL) {
    $redirectURL_forgot_pwd = get_page_link(get_option('biztech_redirect_forgotpwd'));
} else {
    $redirectURL_forgot_pwd = home_url() . "/portal-forgot-password/";
}

?>
<div class=" border-0 rounded-lg w-100">
    <?php echo $biztech_upload_image; ?>
    <form name="scp-login-form" id="scp-login-form" action="<?php echo site_url() ?>/wp-admin/admin-post.php" method="post" class="scp-login-form">
    <?php
    if ((isset($_REQUEST['signup']) || isset($_REQUEST['success'])) && (isset($_SESSION['bcp_signup_suc']) && $_SESSION['bcp_signup_suc'] != '')) {
        echo "<div class='alert alert-success alert-dismissible fade show bcp-success'><span>" . $_SESSION['bcp_signup_suc'] . "</span><button type='button' class='btn-close' data-bs-dismiss='alert'></button></div>";
        unset($_SESSION['bcp_signup_suc']);
    }
    if (isset($_SESSION['bcp_login_suc']) && $_SESSION['bcp_login_suc'] != '') {
        echo "<div class='alert alert-success alert-dismissible fade show bcp-success'><span>" . $_SESSION['bcp_login_suc'] . "</span><button type='button' class='btn-close' data-bs-dismiss='alert'></button></div>";
        unset($_SESSION['bcp_login_suc']);
    }
    if (isset($_SESSION['bcp_login_otp_suc']) && $_SESSION['bcp_login_otp_suc'] != '') {
        echo "<div class='alert alert-success alert-dismissible fade show bcp-success'>" . $_SESSION['bcp_login_otp_suc'] . "</span><button type='button' class='btn-close' data-bs-dismiss='alert'></button></div>";
        unset($_SESSION['bcp_login_otp_suc']);
    }
    if (isset($_REQUEST['error']) && !empty($_REQUEST['error'])) {
        if (isset($_SESSION['bcp_login_error']) && $_SESSION['bcp_login_error']!='') {
            echo "<div class='alert alert-danger alert-dismissible fade show login_error'><span class='error'>" . $_SESSION['bcp_login_error'] . "</span>";
            if (isset($_SESSION['bcp_login_attempt_error']) && $_SESSION['bcp_login_attempt_error']!='') {
                echo "<span class='error'>" . $_SESSION['bcp_login_attempt_error'] . "</span>";
                unset($_SESSION['bcp_login_attempt_error']);
            }
            echo '<button type="button" class="btn-close" data-bs-dismiss="alert"></button></div>';
            unset($_SESSION['bcp_login_error']);
        }
    }
    
    /* get cookie value for rember me */
    $scp_username = '';
    $scp_password = '';
    $remember_me = '';
    if (isset($_COOKIE['bcp_remember_password']) && !empty($_COOKIE['bcp_remember_password'])) {
        $remember_me = 'checked';
        foreach ($_COOKIE['bcp_remember_password'] as $name => $value) {
            if($name == 'username'){
                $scp_username = $value;
            }
            if($name == 'password'){
                $scp_password = decryptPass($value);
            }
        }
    }
    
    if (!empty($disabled)) {
        ?>
        <div id="login-attempt-error-div" class="login-attempt-error-div">
            <p>
                <?php echo $language_array['lbl_wrong_attempt_label']; ?>
                <strong id="bcp-login-attempt-min"><?php echo $min_diff; ?></strong>:
                <strong id="bcp-login-attempt-second"><?php echo $sec_diff; ?></strong>
            </p>
        </div>
        <?php } ?>
    
        <?php 
            if($biztech_upload_image == '') {
                echo $biztech_scp_name;
            }
        ?>
        <?php if ($all_language != null && !empty($all_language) && count($all_language) > 1 && !isset($_SESSION['scp_otp_generated'])) { ?>
        <div class="dropdown">
            <button class="country-dropdown dropdown-toggle dropdown dcp_lang_dropdown" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                <?php
                $lang_selected = "English (US)";
                $flag_selected_class = "flag-us";
                foreach ($all_language as $key => $value) {
                    $country_code = $value['module'];
                    $c_code_array = explode("_", $country_code);
                    $c_code = strtolower($c_code_array[1]);
                    $c_class = "flag-".$c_code;
                    $c_label = str_replace(" - ".$country_code, "", $value['label']);
                    if ($value['module'] == "en_UK") {
                        $c_class = "flag-england";
                    }
                    if ($value['module'] == "el_EL") {
                        $c_class = "flag-sv";
                    }
                    if ($value['module'] == $lang_code) {
                        $lang_selected = $c_label;
                        $flag_selected_class = $c_class;
                    }
                }
                ?>
                <span class="flag <?php echo $flag_selected_class; ?>"></span>
                <?php echo $lang_selected; ?>
                <em class="fas fa-chevron-down"></em>
            </button>
            <ul class="dropdown-menu scrollbar scrollbar-primary" aria-labelledby="dropdownMenuButton1">
                <?php
                foreach ($all_language as $key => $value) {
                    $country_code = $value['module'];    
                    $c_code_array = explode("_", $country_code);
                    $c_code = strtolower($c_code_array[1]);
                    $c_class = "flag-".$c_code;
                    $lang_active = "";
                    if ($value['module'] == "en_UK") {
                        $c_class = "flag-england";
                    }
                    if ($value['module'] == "el_EL") {
                        $c_class = "flag-sv";
                    }
                    if ($value['module'] == $lang_code) {
                        $lang_active = " active_lang";
                    }
                    $c_label = str_replace(" - ".$country_code, "", $value['label']);
                    ?>
                    <li class="<?php echo $lang_active; ?>">
                        <a href="javascript:void(0);" onclick="bcp_change_language('<?php echo $country_code ?>');" >
                        <span class="flag <?php echo $c_class; ?>" ></span>
                        <?php echo $c_label; ?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <?php } ?>
        <?php if (isset($_SESSION['scp_otp_generated']) && isset($_SESSION['scp_otp_contact_id']) && $_SESSION['scp_otp_generated'] == "1") { ?>
            <div class="login-input">
                <input type="text" name="scp_otp" id="scp_otp" class="form-control" required="" placeholder="<?php echo $language_array['lbl_otp_placeholder']; ?>" />
                <a href="<?php echo site_url() ?>/wp-admin/admin-post.php?action=scp_resend_otp" class="forgot-psw float-right"><?php echo $language_array['lbl_resend_otp_label']; ?></a>
            </div>
        <?php } else { ?>
            <!--username field start -->
            <div class="login-input">
                <input type="text" name="scp_username" id="scp_username" class="form-control" required="" placeholder="<?php echo $language_array['lbl_username_placeholder']; ?>" value="<?php echo $scp_username; ?>" />
            </div>
            <!--username field end -->
            <!--password field start -->
            <div class="password-input password">
                <input type="password" name="scp_password" id="scp_password" class="form-control" required="" placeholder="<?php echo $language_array['lbl_password_placeholder']; ?>" value="<?php echo $scp_password; ?>" />
                <div class="eye-password">
                    <em class="far fa-eye" id="togglePassword"></em>
                </div>
            </div>
            <!--password field end -->
            <div class="d-flex align-items-center justify-content-between">
                <div class="form-check">
                    <input class="styled-checkbox form-check-input" id="inputRememberPassword" type="checkbox" value="1" name="remember_password" <?php echo $remember_me; ?>>
                    <label class="form-check-label" for="inputRememberPassword"> <?php echo $language_array['lbl_remember_me_label']; ?></label>
                </div>
                <a href="<?php echo $redirectURL_forgot_pwd; ?>" class="forgot-psw float-right"><?php echo $language_array['lbl_forgot_password_link'] ?></a>
            </div>
            <?php if ( in_array("portal-login", $biztech_portal_visible_reCAPTCHA) && get_option('biztech_scp_recaptcha_site_key') != "" && get_option('biztech_scp_recaptcha_secret_key') != "" ) { ?>
            <div class="re-capcha">
                <div id="scp-recaptcha"></div>
                <label id="scp-recaptcha-error-msg" style="display:none;">
                    <?php echo $language_array['msg_captcha_validate']; ?>
                </label>
            </div>
            <?php 
            } 
        }
        ?>
        <div class="login-btn-group mt-4 mb-0 text-center">
            <?php
            $action = "bcp_login";
            if (isset($_SESSION['scp_otp_generated']) && isset($_SESSION['scp_otp_contact_id']) && $_SESSION['scp_otp_generated'] == "1") {
                $action = "bcp_verify_otp";
            }
            if (!empty($disabled)) {
                $disabled = 'disabled="disabled"';
            }
            ?>
            <input type="hidden" name="action" value="<?php echo $action; ?>">
            <input type="hidden" name="scp_current_url" value="<?php echo $currentURL; ?>">
            <input type="hidden" class="browser_timezone" name="browser_timezone" value="" />
            <button type="submit" class="btn btn-primary" <?php echo $disabled; ?>><?php echo $language_array['lbl_login']; ?></button>
        </div>
    </form>
    <?php
    $enable_signup_link = fetch_data_option('biztech_scp_signup_link_enable');
    if ( $enable_signup_link && !isset($_SESSION['scp_otp_generated']) ) {
        ?>
        <div class="card-footer card-footer-registration text-center">
            <?php echo $language_array['lbl_signup_link_label'] ?>
            <a href="<?php echo $redirectURL_signup; ?>"><?php echo $language_array['lbl_signup_now'] ?></a>
        </div>
        <?php } ?>
</div>
<?php
if ( in_array("portal-login", $biztech_portal_visible_reCAPTCHA) && get_option('biztech_scp_recaptcha_site_key') != "" && get_option('biztech_scp_recaptcha_secret_key') != "" ) {
?>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
    async defer>
</script>
<script type="text/javascript">
  var onloadCallback = function() {
    widget1 = grecaptcha.render('scp-recaptcha', {
      'sitekey' : '<?php echo get_option("biztech_scp_recaptcha_site_key"); ?>',
    });
  };
</script>
<?php } ?>

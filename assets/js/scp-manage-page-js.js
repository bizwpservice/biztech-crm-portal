/* global App */

//mange page script
var ajaxurl = my_ajax_object.ajaxurl;
var homeurl = my_ajax_object.homeurl;
var language_array = my_ajax_object.language_array;
//var session = my_ajax_object.session;

jQuery(document).ready(function () {

    //show / hide side bar menu
    jQuery("#toggle").click(function () {
        jQuery(".scp-leftpanel").toggle();
        jQuery(this).toggleClass('toggle-icon-wrapper');
    });
    
    /* login validate */
    jQuery("#scp-login-form").validate();
    jQuery.extend(jQuery.validator.messages, {
        required: language_array.msg_field_required,
        email: language_array.msg_enter_valid_email,
    });
    
    /* password show hide */
    if (jQuery('#togglePassword').length > 0) {
        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#scp_password');

        togglePassword.addEventListener('click', function () {
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            this.classList.toggle('fa-eye-slash');
        });
    }
    
//    setTimeout(function(){ jQuery( '.login_error' ).hide(); }, 4000);
//    setTimeout(function(){ jQuery( '.success' ).hide(); }, 4000);

    if (jQuery('#scp-login-form').hasClass('scp-login-form')) {
        var d, n;
        d = new Date();
        n = d.getTimezoneOffset();
        jQuery('.browser_timezone').val(n);
        
        if (jQuery('#login-attempt-error-div').hasClass('login-attempt-error-div')) {
            var timeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            jQuery('.browser_timezone').val(timeZone);
            var time_min = parseInt(jQuery("#bcp-login-attempt-min").text());
            var time_sec = parseInt(jQuery("#bcp-login-attempt-second").text());
            //Login attempt counter script
            setInterval(function(){
                time_min = parseInt(jQuery("#bcp-login-attempt-min").text());
                time_sec = parseInt(jQuery("#bcp-login-attempt-second").text());
                if (time_sec == 0) {
                    time_min -= 1;
                    time_sec = 60;
                } else {
                    time_sec -= 1;
                }

                if ( time_min == 0 && time_sec == 0 ) {

                    jQuery('.login-attempt-error-div').hide();
                    var data = {
                        action: 'bcp_unlock_login_user',
                        login_attempt: '1',
                    };
                    jQuery.post(ajaxurl, data, function (response) {
                        window.location.reload();
                    });
                } else {
                    if (time_min < 10) {
                        time_min = "0" + time_min;
                    }
                    if (time_sec < 10) {
                        time_sec = "0" + time_sec;
                    }
                    jQuery("#bcp-login-attempt-min").text(time_min);
                    jQuery("#bcp-login-attempt-second").text(time_sec);
                }
            }, 1000);
        }
    }
    
    jQuery("#scp-login-form").submit(function(){
        if (jQuery('#scp-recaptcha').length > 0 && jQuery('#scp-recaptcha').html() != "") {
            var msg = grecaptcha.getResponse(widget1);
            if (jQuery.trim(msg) == "") {
                jQuery('#scp-recaptcha-error-msg').css("display","block");
                return false;
            } else {
                jQuery('#scp-recaptcha-error-msg').css("display","none");
            }
        }
    });
    
    
    /* forgot password js */
    jQuery("#commentForm").submit(function(){
        jQuery("input").each(function(){
            jQuery(this).val(jQuery.trim(jQuery(this).val()));
        });
        if ($('#scp-recaptcha').length > 0 && $('#scp-recaptcha').html() != "") {
            var msg = grecaptcha.getResponse(widget1);
            if ($.trim(msg) == "") {
                $('#scp-recaptcha-error-msg').css("display","block");
                return false;
            } else {
                $('#scp-recaptcha-error-msg').css("display","none");
            }
        }
    });
    jQuery("#commentForm").validate();
    
    
    


//    jQuery('.scp-menu-dashboard').click(function () {
//        jQuery('.scp-open-dashboard-menu').slideToggle();
//        jQuery(this).toggleClass('menu-active');
//
//    });
//    jQuery('.scp-quick-add-menu').click(function () {
//        jQuery('.scp-quick-add-menu-list').slideToggle();
//        jQuery(this).toggleClass('menu-active');
//
//    });
//    jQuery('.scp-menu-manage').click(function () {
//        jQuery('.scp-open-manage-menu').slideToggle();
//        jQuery(this).toggleClass('menu-active');
//    });
//    jQuery('.scp-menu-profile').click(function () {
//        jQuery('.scp-open-profile-menu').slideToggle();
//        jQuery(this).toggleClass('menu-active');
//    });
//    jQuery('.scp-menu-modules').click(function () {
//        jQuery('.scp-open-modules-menu').slideToggle();
//        jQuery(this).toggleClass('menu-active');
//    });

    /*jQuery(window).click(function (e) {
        var scp_menu_manage = jQuery('.scp-menu-manage');
        var scp_menu_modules = jQuery('.scp-menu-modules');
        var scp_menu_dashboard = jQuery('.scp-menu-dashboard');
        var scp_quick_add_part = jQuery('.scp-quick-add-part');
        var scp_menu_profile = jQuery('.scp-menu-profile');

        if (!scp_menu_manage.is(e.target) // if the target of the click isn't the container...
                && scp_menu_manage.has(e.target).length === 0) // ... nor a descendant of the container
        {
            if (jQuery('.scp-open-manage-menu').is(":visible")) {
                jQuery('.scp-open-manage-menu').hide();
                jQuery(scp_menu_manage).toggleClass('menu-active');
            }
        }
        if (!scp_menu_modules.is(e.target) // if the target of the click isn't the container...
                && scp_menu_modules.has(e.target).length === 0) // ... nor a descendant of the container
        {
            if (jQuery(window).width() > 1024) {
                if (jQuery('.scp-open-modules-menu').is(":visible")) {
                    jQuery( '.scp-fullwidth-menu' ).removeClass('menu-active');
                    jQuery('.scp-open-modules-menu').hide();
                }
            }
        }

        if (!scp_menu_dashboard.is(e.target) // if the target of the click isn't the container...
                && scp_menu_dashboard.has(e.target).length === 0) // ... nor a descendant of the container
        {
            if (jQuery('.scp-open-dashboard-menu').is(":visible")) {
                jQuery('.scp-open-dashboard-menu').hide();
                jQuery(scp_menu_dashboard).toggleClass('menu-active');
            }
        }
        if (!scp_quick_add_part.is(e.target) // if the target of the click isn't the container...
                && scp_quick_add_part.has(e.target).length === 0) // ... nor a descendant of the container
        {
            if (jQuery('.scp-quick-add-menu-list').is(":visible")) {
                jQuery('.scp-quick-add-menu-list').hide();
                jQuery(scp_quick_add_part).toggleClass('menu-active');
            }
        }

        if (!scp_menu_profile.is(e.target) // if the target of the click isn't the container...
                && scp_menu_profile.has(e.target).length === 0) // ... nor a descendant of the container
        {
            if (jQuery('.scp-open-profile-menu').is(":visible")) {
                jQuery('.scp-open-profile-menu').hide();
                jQuery(scp_menu_profile).toggleClass('menu-active');
            }
        }
    });*/

    //jQuery(window).resize(function () {
    /*jQuery(window).click(function (e) {
        if (jQuery(window).width() < 1025) {

            jQuery('.scp-sidebar-class').click(function () {


                var getid = jQuery(this).attr('id');
                jQuery(".inner_ul").not("#dropdown_" + getid).slideUp();
                jQuery('.scp-active-menu').not("#" + getid + " a.label").removeClass('scp-active-menu').addClass('scp-sidemenu');
                jQuery('#dropdown_' + getid).show();
                jQuery('.scp-menu-modules').removeClass('menu-active');
                jQuery('#dropdown_' + getid).click(function () {
                    jQuery('.scp-open-modules-menu').hide();
                });
                jQuery('#dashboard_id a').addClass('scp-active-menu');
                jQuery("#" + getid + " a.label").addClass('scp-active-menu');
            });
             jQuery(".inner_ul> li").click(function ()
                {
                    var divId = jQuery(this).attr('id');
                    jQuery('.no-toggle').removeAttr('style');
                    jQuery('a').removeClass('scp-active-submenu');
                    jQuery('#' + divId + ' a').addClass('scp-active-submenu');
                });
        }
    });*/
    
    jQuery('#g_search_name').on('keyup', function () {
        var serach_term;
        serach_term = document.getElementById('g_search_name').value;
        serach_term = serach_term.trim();
        if (serach_term.length < 3) {
            document.getElementById("g_search_name").style.border = "1px solid #b7b7b7";
        }
    });
    
    jQuery('#global_search_form_part').on('submit', '#global_search_form', function () {

        var serach_term, e, module_name, redirect_url;
        serach_term = document.getElementById('g_search_name').value.trim();
        document.getElementById('g_search_name').value = serach_term;

        if (serach_term.length >= 3) {

            e = document.getElementById("select_module_option");
            module_name = e.options[e.selectedIndex].value;

            if (module_name == 0) {
                redirect_url = homeurl+'portal-manage-page#data/SearchResult/' + serach_term;
            } else {
                redirect_url = homeurl+'portal-manage-page#data/' + module_name + '/list/' + serach_term;
            }
            window.location.href = redirect_url;
        } else {
            document.getElementById("g_search_name").style.border = "1px solid red";
            alert(language_array.msg_search_min_keyword);
            if (serach_term.length < 3) {
            } else {
                document.getElementById("g_search_name").value = "";
            }
        }
        return false;
    });
    
    jQuery(document).on('click', '#btn_global_search', function() {
        search_gloabal();
    });
    jQuery(document).on('keypress', '#g_search_name', function(e) {
        if (e.which == 13) {
            search_gloabal();
        }
    });
    
    jQuery("#responsedata").on("click", '.scp-bulk-action-btn', function (evt) {
        jQuery(".bulk-action-ul").slideToggle();
    });

    jQuery("#responsedata").on("click", '.scp-th-checkbox-div', function (e) {
        var scp_select_all = jQuery('#select_all');
        if (!scp_select_all.is(e.target)) {
            jQuery(".scp-th-checkbox-ul").slideToggle();
        }
    });
    
    //Submit case comment form
    jQuery("#responsedata").on("submit", "#case_updates", function (e) {
        e.preventDefault();
        var update_text = $.trim(jQuery('#update_text').val());
        var formData = new FormData(this);
        if (!(update_text)) {
            alert(language_array.msg_please_enter_comment);
            return false;
        } else {
            jQuery("#case-updates").addClass('bcp-loader');
            jQuery.ajax({
                url: ajaxurl,
                type: 'POST',
                data: formData,
                success: function (response, status) {
                    jQuery("#case-updates").removeClass('bcp-loader');
                    if ($.trim(response) != '-1') {
                        jQuery('#case-updates').html(response);
                    } else
                    {
                        var redirect_url = $(location).attr('href') + "?conerror=1";
                        window.location.href = redirect_url;
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
            return false;
        }
    });
    
    jQuery("#responsedata").on("change", '#select_all', function (evt) { //"select all" change

        jQuery(".scp-checkbox").prop('checked', jQuery(this).prop("checked")); //change all ".checkbox" checked status
        var check_count = jQuery('.scp-checkbox:checked').length;
        if (check_count != 0) {
            jQuery("#scp-checkbox-selected-span").text(language_array.lbl_selected +": "+check_count);
        } else {
            jQuery("#scp-checkbox-selected-span").text("");
        }
        if (jQuery('.scp-checkbox:checked').length > 0) {
            jQuery('#delete-export-btn').removeClass('disabled');
        } else {
            jQuery('#delete-export-btn').addClass('disabled');
        }
    });
    
    jQuery("#responsedata").on("click", '#scp-select-th-page', function (evt) { //"select all" change

        jQuery(".scp-checkbox").prop('checked', "checked"); //change all ".checkbox" checked status
        jQuery("#select_all").prop('checked', "checked");
        jQuery("#select_all").removeClass("scp_select_th_active");
        jQuery(".scp-checkbox").removeAttr('disabled');
        var check_count = jQuery('.scp-checkbox:checked').length;
        jQuery("#scp-checkbox-selected-span").text(language_array.lbl_selected+": "+check_count);
        if (jQuery('.scp-checkbox:checked').length > 0) {
            jQuery('#delete-export-btn').removeClass('disabled');
        } else {
            jQuery('#delete-export-btn').addClass('disabled');
        }
    });
    
    jQuery("#responsedata").on("click", '#scp-select-th-all', function (evt) { //"select all" change

        jQuery(".scp-checkbox").prop('checked', "checked"); //change all ".checkbox" checked status
        jQuery(".scp-checkbox").attr('disabled', "disabled");
        jQuery("#select_all").prop('checked', "checked");
        jQuery("#select_all").addClass("scp_select_th_active");
        
        var check_count = jQuery(this).attr("data-count");
        jQuery("#scp-checkbox-selected-span").text(language_array.lbl_selected+": "+check_count);
        
        if (jQuery('.scp-checkbox:checked').length > 0) {
            jQuery('#delete-export-btn').removeClass('disabled');
        } else {
            jQuery('#delete-export-btn').addClass('disabled');
        }
    });
    
    jQuery("#responsedata").on("click", '#scp-deselect-th-all', function (evt) { //"select all" change
        jQuery(".scp-checkbox").removeAttr('checked'); //change all ".checkbox" checked status
        jQuery(".scp-checkbox").removeAttr('disabled');
        jQuery("#select_all").removeAttr('checked');
        jQuery(".scp-checkbox, #select_all").prop('checked', false);
        jQuery("#select_all").removeClass("scp_select_th_active");
        jQuery("#scp-checkbox-selected-span").text("");
        if (jQuery('.scp-checkbox:checked').length > 0) {
            jQuery('#delete-export-btn').removeClass('disabled');
        } else {
            jQuery('#delete-export-btn').addClass('disabled');
        }
    });
    
    //".checkbox" change
    jQuery("#responsedata").on("change", '.scp-checkbox', function (evt) {

        var check_count = jQuery('.scp-checkbox:checked').length;
        if (check_count != 0) {
            jQuery("#scp-checkbox-selected-span").text(language_array.lbl_selected+": "+check_count);
        } else {
            jQuery("#scp-checkbox-selected-span").text("");
        }
        
        if (jQuery('.scp-checkbox:checked').length > 0) {
            jQuery('#delete-export-btn').removeClass('disabled');
        } else {
            jQuery('#delete-export-btn').addClass('disabled');
        }
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if (false == jQuery(this).prop("checked")) { //if this item is unchecked
            jQuery("#select_all").prop('checked', false); //change "select all" checked status to false
        }
        //check "select all" if all checkbox items are checked
        if (jQuery('.scp-checkbox:checked').length == jQuery('.scp-checkbox').length) {
            jQuery("#select_all").prop('checked', true);
        }
    });
    
    //File type validation
    //jQuery("#responsedata").on("change", "input[type='file']", function (e) {
    jQuery("#responsedata").on("change", ".file-field, input[type='file']", function (e) {
        var selection = jQuery(this)[0];
        var allowedFiles = ['jpg', 'JPG', 'PNG', 'JPEG', 'jpeg', 'png', 'gif', 'doc', 'docx', 'txt', 'pdf', 'csv', 'xls', 'xlsx', 'zip'];
        for (var i = 0; i < selection.files.length; i++) {
            var ext = selection.files[i].name.split('.').pop();
            var name = selection.files[i].name;
            var size = selection.files[i].size;
            jQuery(".error-line").remove();
            if (size > 2097152) {
                jQuery(this).val('');
                jQuery('<label class="error error-line file-upload">Maximum file upload size limit is 2 MB.</label>').insertAfter(jQuery('.scp-information'));
                return false;
            }
            if (allowedFiles.indexOf(ext) == '-1') {
                jQuery(this).val("");
                jQuery("<label class='error-line'>"+language_array.msg_filetype_allowed +": jpg, jpeg, png, gif, doc, docx, pdf, txt, csv, xls, xlsx, zip.</label>").insertAfter(jQuery('.scp-information'));
                alert(language_array.msg_filetype_error_start+' ' + name +' '+ language_array.msg_filetype_error_end);
                return false;
            }
        }
        if (jQuery('.custom-file').prop('tagName') == 'LABEL') {
            jQuery('.custom-file').text(e.target.files[0].name); //to handle in case > attachment file upload
        } else {
            jQuery('.custom-file').val(e.target.files[0].name);
        }
    });
    
    jQuery("#responsedata").on("change", ".scp_image", function (e) {
        var selection = jQuery(this)[0];
        var allowedFiles = ['jpg', 'JPG', 'jpeg', 'JPEG', 'png', 'PNG', 'gif'];
        for (var i = 0; i < selection.files.length; i++) {
            var ext = selection.files[i].name.split('.').pop();
            var name = selection.files[i].name;
            var size = selection.files[i].size;
            jQuery('.scp_image ').next(".error-line").remove();
            if (size > 1048576) {
                jQuery(this).val('');
                jQuery('<label class="error error-line file-upload">Maximum file upload size limit is 1 MB.</label>').insertAfter('.scp_image ');
                return false;
            }
            if (allowedFiles.indexOf(ext) == '-1') {
                jQuery(this).val("");
                jQuery("<label class='error-line'>"+language_array.msg_filetype_allowed +": jpg, jpeg, png, gif.</label>").insertAfter('.scp_image ');
                alert(language_array.msg_filetype_error_start+' ' + name +' '+ language_array.msg_filetype_error_end);
                return false;
            }
        }
    });
    
    
    jQuery('body').on('change', '.custom-file-input', function(e){
        jQuery('.error-line.file-upload').remove();
        var selection = jQuery(this)[0];
        var allowedFiles = ['jpg', 'jpeg', 'JPG', 'JPEG', 'PNG', 'png', 'gif', 'doc', 'docx', 'pdf', 'txt', 'csv', 'xls', 'xlsx', 'zip'];
        for (var i = 0; i < selection.files.length; i++) {
            var ext = selection.files[i].name.split('.').pop();
            var name = selection.files[i].name;
            var size = selection.files[i].size;

            var insertAfter = '.custom-file-input';
            if (jQuery(this).parent().hasClass('fileUpload')) {
                var insertAfter = '.custom-file-label';
            }
            if (jQuery(this).parent().parent().hasClass('bcp-flex')) {
                var insertAfter = '#error-msg-after-flex-div'; //to handle in case > attachment file upload
            }
            if (size > 2097152) {
                jQuery(this).val('');
                jQuery('<label class="error error-line file-upload">Maximum file upload size limit is 2 MB.</label>').insertAfter(jQuery(this));
                return false;
            }
            if (allowedFiles.indexOf(ext) == '-1') {
                jQuery(this).val('');
                jQuery('<label class="error error-line file-upload">Only [jpg, jpeg, JPG, JPEG, PNG,  png, gif, doc, docx, pdf, txt, csv, xls, xlsx, zip] file formats allowed.</label>').insertAfter(jQuery(this));
                return false;
            }
        }
        if (jQuery('.custom-file-label').prop('tagName') == 'LABEL') {
            jQuery(this).parent().find('.custom-file-label').text(e.target.files[0].name); //to handle in case > attachment file upload
        } else {
            jQuery(this).parent().find('.custom-file-label').val(e.target.files[0].name);
        }
    });
    jQuery('body').on("change", ".scp-enum", function(){
        var fieldname = jQuery(this).attr("name");
        if (jQuery("select[data-parent='"+fieldname+"']").length > 0) {
            var child_field = jQuery("select[data-parent='"+fieldname+"']").attr("name");
            var parent_value = jQuery(this).val();
            var el = jQuery('#general_form_id');
            var module_name = jQuery("input[name='module_name']").val();
            var id = jQuery("input[name='id']").val();
            jQuery.ajax({
                url: ajaxurl + '?action=bcp_get_child_options',
                type: 'POST',
                data: {
                    'child_field': child_field,
                    'parent_value': parent_value,
                    'module_name': module_name,
                    'id' :id,
                },
                beforeSend: function() {
//                    App.blockUI(el);
                },
                success : function(response){
//                    App.unblockUI(el);
                    var res = JSON.parse(response);
                    if (res.success) {
                        var res_data = res.data;
                        jQuery("select[data-parent='"+fieldname+"']").html(res_data);
                    }
                }
            });
        }
    });
    
});

jQuery(document).ajaxComplete(function () {
    if (!jQuery('#otherdata').is(':empty'))
    {
        jQuery("#responsedata").after('<div id="otherdata" style="display:none;"></div>');
    }
});

function quick_show_add_new() {
    jQuery('#quick_show_add_new').slideToggle();
}

//function to change language of page 
// since v3.2.0
function bcp_change_language(lang_code='en_us') {
    
    var el = jQuery('#fullwidth-wrapper');
    if (lang_code == "0") {
        alert("please select language.");
        return false;
    }
    jQuery.ajax({
        type : "POST",
        url : ajaxurl,
        data: {
            action : 'bcp_change_language',
            lang_code: lang_code,
        },
        beforeSend: function() {
//            App.blockUI(el);
        },
        success : function(response){
//            App.unblockUI(el);
//            if(response) {
                window.location.reload();
//            }
        },
    });
}

/*jQuery(document).click(function(evt){

    if (evt.target.id != "steven" && evt.target.parentNode.className != "mb-1 mt-1" && evt.target.id != 'accordion_children' && evt.target.className != "collapsed") {
        jQuery(".steven-dropdown").find(".dropdown-menu").slideUp();
    }
    if (evt.target.id != "moduleDropdown") {
        jQuery(".casec-dropdown .dropdown-list").slideUp();
    }
    if (jQuery(".dcp_lang_dropdown").length > 0 && evt.target.id != "drop1" && evt.target.className != "scp-lang-dropdown-toggle dropdown-toggle" && evt.target.className != "fa fa-angle-down" && evt.target.className != "flag flag-us" ) {
        jQuery(".dcp_lang_dropdown").find(".dropdown-menu").slideUp();
    }
    if (jQuery("#accordionSidebar").length > 0 && evt.target.className != "fa fa-bars dcp_nav_icon" && evt.target.className != "d-flex justify-content-between align-items-center") {
        if (screen.width <= 800) {
        jQuery("#accordionSidebar").removeClass("active");
        jQuery("nav.topbar").removeClass("active");
        }
    }
});*/

function PrintElem(elem)
{
    jQuery('#responsedata').printThis({
        debug: false,               // show the iframe for debugging
        importCSS: true,            // import parent page css
        importStyle: false,         // import style tags
        printContainer: true,       // print outer container/$.selector
        loadCSS: "",                // path to additional css file - use an array [] for multiple
        pageTitle: "",              // add title to print page
        removeInline: false,        // remove inline styles from print elements
        removeInlineSelector: "*",  // custom selectors to filter inline styles. removeInline must be true
        printDelay: 333,            // variable print delay
        header: jQuery(".print-header").html(),               // prefix to html
        footer: null,               // postfix to html
        base: false,                // preserve the BASE tag or accept a string for the URL
        formValues: true,           // preserve input/form values
        canvas: false,              // copy canvas content
        doctypeString: ' ',       // enter a different doctype for older markup
        removeScripts: false,       // remove script tags from print content
        copyTagClasses: false,      // copy classes from the html & body tag
        beforePrintEvent: null,     // function for printEvent in iframe
        beforePrint: null,          // function called before iframe is filled
        afterPrint: null            // function called before iframe is removed
    });
}



jQuery(document).on('submit', '#general_form_id', function (e) {
    var module_name, selected_product_lineitems, products, product_error, serviceSelected;
    product_error = false;
    serviceSelected = false;
    jQuery("#error_msg").text("");
    if ($('#scp-recaptcha').length > 0 && $('#scp-recaptcha').html() != "") {
        var msg = grecaptcha.getResponse(widget1);
        if ($.trim(msg) == "") {
            $('#scp-recaptcha-error-msg').css("display","block");
            return false;
        } else {
            $('#scp-recaptcha-error-msg').css("display","none");
        }
    }
    module_name = jQuery('.scp-module-name').val();

    // If this is proposal module than clear search result and validate product qty before submit
    if (module_name == 'bc_proposal') {
        check_blank_product_qty();
        selected_product_lineitems = jQuery(".selected_product_lineitems").val();

        if (selected_product_lineitems != null && selected_product_lineitems != '') {

            products = jQuery.parseJSON(selected_product_lineitems);

            for (var i = 0; i < products.length; i++) {
                if (products[i]['product_qty'] == '0' || products[i]['product_qty'] == '') {
                    jQuery('.product_search_clear').click();
                    product_error = true;
                    break;
                }
            }
            if (products.length == 0) {
                product_error = true;
            }
            if (product_error == true) {
                return false;
            }
        } else {
            jQuery('.scp_proposal_input_service_name').each(function () {
                if (jQuery.trim(jQuery(this).val()) != "") {
                    serviceSelected = true;
                }
            });
            if (serviceSelected == false) {
                jQuery('.line-item-error').remove();
                jQuery('.error-line-item').html("<label class='error line-item-error'>"+language_array.msg_select_product_or_service+"</label>");
                return false;
            }
        }
    }

    jQuery('form#general_form_id textarea').each(function () { // Trim all inputbox and again validate form before submit
        this.value = jQuery(this).val().trim();
    });

    jQuery('form#general_form_id input[type="text"]').each(function () { // Trim all inputbox and again validate form before submit
        this.value = jQuery(this).val().trim();
    });

    e.preventDefault();
    var $form = $(this);
    if (!$form.valid())
        return false;

    var form_data = new FormData(jQuery(this)[0]);
    jQuery.ajax({
        url: ajaxurl + '?action=bcp_add_moduledata_call',
        type: 'POST',
        data: form_data,
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        beforeSend: function () {
            jQuery("#responsedata").addClass('bcp-loader');
        },
        success: function (response) {
            var response = jQuery.parseJSON(response);
            if (response['success']) {
                if (response['data']['redirect'] == '1') {
                    if(response['data']['module_name'] == 'Contacts'){
                        window.location.href = response['data']['redirect_url'];
                    } else {
                        if ("id" in response['data']) {
                            if (response['data']['relationship']) {
                                window.location.hash = '#data/' + response['data']['relate_to_module_name'] + '/detail/' + response['data']['id'] + '/relationship/'+response['data']['relationship']+'/response/' + response['data']['module_name'];
                            } else {
                                window.location.hash = '#data/' + response['data']['relate_to_module_name'] + '/detail/' + response['data']['id'] + '/response/' + response['data']['module_name'];
                            }
                        } else {
                            if (response['data']['module_name'] == 'Accounts') {
                                jQuery("ul#dropdown_Accounts_id li#edit-Accounts").remove();
                            }
                            if (response['data']['module_name'] == 'bc_feedback') {
                                window.location.reload();
//                                setTimeout(function(){ jQuery('.sfcp-validation, .success, .error, .login_error').hide(); }, 2000);
                            } else {
                                window.location.hash = '#data/' + response['data']['module_name'] + '/list/';
                            }
                        }
                    }
                } else if (response['data']['redirect'] == 'calendar') {
                    jQuery.ajax({
                        url: ajaxurl + '?action=bcp_calendar_display',
                        type: 'POST',
                        success: function (response2) {
                            jQuery("#responsedata").removeClass('bcp-loader');
                            if (jQuery.trim(response2) != '-1') {
                                jQuery('#responsedata').html(response2);
                            } else {
                                var redirect_url = $(location).attr('href') + "?conerror=1";
                                window.location.href = redirect_url;
                            }
                        }
                    });
                } else {
                    if (response['data']['module_name'] == 'Notes') {
                        var data = {
                            'id': response['data']['case_id'],
                            'parent_module': response['data']['parent_module'],
                        }
                        jQuery.ajax({
                            url: ajaxurl + '?action=get_notes_in_case_after_add',
                            type: 'POST',
                            data: data,
                            success: function (response3) {
                                jQuery("#responsedata").removeClass('bcp-loader');
                                jQuery(".all-notes.scp-section-heading .scp-Notes-list").html(response3);
                                jQuery(".all-notes.scp-section-heading.list_notes_in_case #succid").html(response['data']['msg_show']).fadeIn();
                                jQuery('#otherdata #general_form_id')[0].reset();
//                                setTimeout(function () {
//                                    jQuery('.all-notes.scp-section-heading.list_notes_in_case #succid').fadeOut();
//                                }, 2000);
                            }
                        });   
                    }
                }
            } else {
                jQuery("#responsedata").removeClass('bcp-loader');
                jQuery('.login_error, .error').show();
                jQuery('.login_error .error').html(response['data']['msg']);
                jQuery("html, body").animate({
                scrollTop: jQuery(".login-section").offset().top,
            }, 100);
            }
//            setTimeout(function(){ jQuery('.sfcp-validation, .success, .error, .login_error').hide(); }, 2000);
        }
    });
    return false;
});

/* advance filter */

/*jQuery(document).ready(function() {
    var modal = document.getElementById("filter-modal");
    window.onclick = function(event) {
        if (event.target == modal) {
            jQuery('#advance_filter_form').removeClass("modal-open");
        }
    }
    jQuery(document).on("click",'.btn-filter', function() {
        jQuery('#advance_filter_form').addClass("modal-open");
    });
    jQuery(document).on("click",'.filter-close', function() {
        // e.preventDefault();
        jQuery('#advance_filter_form').removeClass("modal-open");
    });
});

jQuery(document).on("click", ".btn-filter", function(){
    jQuery("#advance_filter_form").slideToggle();
});*/

jQuery(document).on('submit', '#advance_filter_form', function (e) {
    var module_name, show;
    var where_condition = {};
    var currency = {};
    var boolean = {};

    module_name = jQuery('.scp-module-name').val();
    e.preventDefault();
    jQuery("body").removeClass("filter-body-open");
    
    show = 0;
    jQuery(".where_field").each(function() {
        var key = jQuery(this).attr("name");
        var where_field_val = jQuery(this).val();
        if(where_field_val != '' && where_field_val != null) {
            var date = {};
            var select = {};
            show = 1;
            if(jQuery(this).hasClass("scp_currency")){
                if(where_field_val == 'between'){
                    var start_range = jQuery('#start_range_total_amount_advanced').val();
                    var end_range = jQuery('#end_range_total_amount_advanced').val();
                    if ((start_range.length >0 && end_range.length == 0) ||(end_range.length >0 && start_range.length == 0) ){
                        alert('Please choose both starting and ending range entries');
                        if (start_range.length == 0) {
                            jQuery('#start_range_total_amount_advanced').focus();
                        } else {
                            jQuery('#end_range_total_amount_advanced').focus();
                        }
                        show = 0;
                        return false;
                    } else {
                        currency['key'] = key;
                        currency['compare'] = where_field_val;
                        currency['name'] = 'range';
                        currency['start_range_total_amount_advanced'] = start_range;
                        currency['end_range_total_amount_advanced'] = end_range;
                    }
                } else {
                    var total_amount = jQuery('#range_total_amount_advanced').val();
                    currency['key'] = jQuery(this).attr("name");
                    currency['compare'] = where_field_val;
                    currency['name'] = 'single';
                    currency['range_total_amount_advanced'] = total_amount;
                }
            } else if (jQuery(this).hasClass("scp_date")) {
                where_condition['date'][key] = where_field_val;
            } else if (jQuery(this).hasClass("multiselectbox")) {
                select['key'] = key;
                select['name'] = 'selectbox';
                select['compare'] = where_field_val;
                where_condition[key] = select;
            } else if (jQuery(this).hasClass("scp_boolean")) {
                boolean['name'] = 'boolean';
                boolean['compare'] = where_field_val;
                where_condition[key] = boolean;
            } else if (jQuery(this).hasClass("scp_daterange")) {
                if(where_field_val == 'between'){
                    var start_range = jQuery('input[name=start_range_'+key+']').val();
                    var end_range = jQuery('input[name=end_range_'+key+']').val();
                    if ((start_range.length >0 && end_range.length == 0) ||(end_range.length >0 && start_range.length == 0) ){
                        alert('Please choose both starting and ending range entries');
                        if (start_range.length == 0) {
                            jQuery('#start_range_total_amount_advanced').focus();
                        } else {
                            jQuery('#end_range_total_amount_advanced').focus();
                        }
                        show = 0;
                        return false;
                    } else {
                        date['key'] = key;
                        date['compare'] = where_field_val;
                        date['name'] = 'range';
                        date['start_range_'+key] = start_range;
                        date['end_range_'+key] = end_range;
                    }
                } else if (where_field_val == '=' || where_field_val == '!=' || where_field_val == '>' || where_field_val == '<') {
                    var date_range = jQuery('input[name=range_'+key+']').val();
                    date['key'] = key;
                    date['compare'] = where_field_val;
                    date['name'] = 'single';
                    date['range_'+key] = date_range;
                } else {
                    date['key'] = key;
                    date['compare'] = where_field_val;
                }
                where_condition[key] = date;
            } else {
                where_condition[key] = where_field_val;
            }
            
        }
    });
    
    if (show) {
        jQuery("#responsedata").addClass('bcp-loader');
        var data = {
            'action': 'bcp_list_module',
            'view': 'list',
            'modulename': module_name,
            'filter_where_condition': where_condition,
            'filter_where_currency_condition': currency,
        };
        jQuery.post(ajaxurl, data, function (response) {
            jQuery("#responsedata").removeClass('bcp-loader');
            if (jQuery.trim(response) != '-1') {
                jQuery('#responsedata').html(response);
            } else {
                var redirect_url = $(location).attr('href') + "?conerror=1";
                window.location.href = redirect_url;
            }
        });
    }
});

jQuery(document).on('click', '.btn-filter-clear', function (e) {
    jQuery('.reset_div').trigger('click');
});

jQuery(document).on('click', '.reset_div', function (e) {
    var module_name;

    module_name = jQuery('.scp-module-name').val();
    e.preventDefault();
    jQuery("#responsedata").addClass('bcp-loader');
    
    jQuery("body").removeClass("filter-body-open");
    var data = {
        'action': 'bcp_list_module',
        'view': 'list',
        'modulename': module_name,
    };
    jQuery.post(ajaxurl, data, function (response) {
        jQuery("#responsedata").removeClass('bcp-loader');
        if (jQuery.trim(response) != '-1') {
            jQuery('#responsedata').html(response);
        } else {
            var redirect_url = $(location).attr('href') + "?conerror=1";
            window.location.href = redirect_url;
        }
    });
});

function chek(id) {//Added by BC on 07-nov-2015
    if (jQuery('#' + id).is(':checked')) {
        jQuery('#' + id).val(1);
        jQuery("#" + id).prop('checked', true);
    } else {
        jQuery('#' + id).val(0);
        jQuery("#" + id).removeAttr('checked');
    }
}
function get_module_list() {
    var p_type = jQuery('#parent_name').val();
    data = {action: 'get_module_list', parent_type: p_type};
    jQuery.post(ajaxurl, data, function (response) {
        jQuery('#related_to_id2').html(response);
    });
}
function get_module_list_v7() {
    var p_type = jQuery('#parent_name').val();
    data = {action: 'get_module_list_v7', parent_name: p_type};
    jQuery.post(ajaxurl, data, function (response) {
        jQuery('#related_to_id2').html(response);
    });
}

function scp_export_data(module_name, export_type = "csv") {
        
    var ids = [];
    if (jQuery("#select_all").hasClass("scp_select_th_active")) {
    }else{
        var id_clone = jQuery(".scp_csv_module_id").clone();
        jQuery(".scp-checkbox:checked").each(function (index) {
            var id_clone = jQuery(".scp_csv_module_id").clone();
            id_clone.removeClass("scp_csv_module_id");
            ids.push(jQuery(this).attr('data-id'));
            var id = jQuery(this).attr('data-id');
            id_clone.val(id);
            jQuery(".scp_csv_module_id").after(id_clone);
        });
    }
    jQuery(".scp_csv_module_id").remove();
    document.getElementById("export_csv_submit").submit();
    return false;


   /* var data = {
        'action': 'scp_export_data',
        'ids': ids,
        'module_name': module_name,
        'export_type': export_type,
    };
    var el = jQuery("#responsedata");
    App.blockUI(el);
    jQuery.post(ajaxurl, data, function (response, status) {
        jQuery("#responsedata").html(response);
        App.unblockUI(el);
    });*/
}

jQuery(window).click(function(e){
    var scp_th_checkbox_div = jQuery('.scp-th-checkbox-div');
    var scp_bulk_action_btn = jQuery('.scp-bulk-action-btn');
    if (!scp_th_checkbox_div.is(e.target)) {
        jQuery(".scp-th-checkbox-ul").hide();
    }
    if (!scp_bulk_action_btn.is(e.target)) {
        jQuery(".bulk-action-ul").hide();
    }
});






function bcp_quote_accept_decline_btn(subject, modulename, module_id, action_performed, current_url) { //for Quote Accept-Decline

    if (action_performed == 'Decline') {

        jQuery('.quote-ajax-modal')[0].click();
        jQuery('.quote_subject').val(subject);
        jQuery('.quote_module_id').val(module_id);
        jQuery('.quote_current_url').val(current_url);
    } else {
        if (confirm('Are you sure you want to ' + action_performed + ' this quote?')) {
            jQuery("#responsedata").addClass('bcp-loader');
            var data = {
                'modulename': modulename,
                'id': module_id,
                'action_performed': action_performed,
                'current_url': current_url,
                'subject': subject,
            };
            jQuery.ajax({
                url: ajaxurl + '?action=bcp_quote_accept_decline_btn',
                type: 'POST',
                data: data,
                success: function (response) {
                    var response = jQuery.parseJSON(response);
                    if (response['data']['success']) {
                        bcp_module_call_view(modulename, module_id, 'detail', current_url);
                    } else {
                        document.getElementById("succid").innerHTML = response['data']['msg_show'];
                        jQuery('#succid').fadeIn();
//                        setTimeout(function () {
//                            jQuery('#succid').fadeOut().hide();
//                        }, 2000);
                    }
                    jQuery("#responsedata").removeClass('bcp-loader');

                }
            });
        }
    }
    return false;
}

function cancel_form_btn(modulename, redirect_url, redirect_flg) { //for cancel btn
    var el = jQuery("#responsedata");
//    App.blockUI(el);
    if (redirect_flg == 0) {
        window.location.hash = '#data/' + modulename + '/list/';
    } else {
        jQuery.ajax({
            url: ajaxurl + '?action=bcp_calendar_display',
            type: 'POST',
            success: function (response) {
//                App.unblockUI(el);
                if ($.trim(response) != '-1') {
                    jQuery('#responsedata').html(response);
                } else {
                    var redirect_url = $(location).attr('href') + "?conerror=1";
                    window.location.href = redirect_url;
                }
            }
        });
    }
    return false;
}

function bcp_module_order_by(page, modulename, order_by, order, view, get_current_url) { // for order by

    var searchval = jQuery('#search_name').val();
    var select_all = '0';
    if (jQuery("#select_all").hasClass("scp_select_th_active")) {
        select_all = '1';
    }
    jQuery("#responsedata").addClass('bcp-loader');
    var data = {
        'action': 'bcp_list_module',
        'view': view,
        'modulename': modulename,
        'page_no': page,
        'current_url': get_current_url,
        'order_by': order_by,
        'order': order,
        'searchval': searchval,
        'select_all': select_all,
    };
    //add relate_module for subpanel
    if (view == 'detail') {
        data.relate_to_module = jQuery('#relate_to_module').val();
        data.relate_to_id = jQuery('#relate_to_id').val();
        if(jQuery('#subpanelname').val()) {
            data.subpanelname = jQuery('#subpanelname').val();
        }
    }
    if (modulename == 'Notifications') {
        data.action = 'scp_notifications_page';
        if (jQuery('#scp_noti_is_search').val() == '1') {
            data.noti_type = jQuery('#noti_type').attr('data-search');
            data.noti_module = jQuery('#noti_module').attr('data-search');
            data.noti_from_date = jQuery('#noti_from_date').attr('data-search');
            data.noti_to_date = jQuery('#noti_to_date').attr('data-search');
            data.scp_noti_is_search = '1';
        }
    }
    jQuery.post(ajaxurl, data, function (response) {
        jQuery("#responsedata").removeClass('bcp-loader');
        if ($.trim(response) != '-1') {
            if (view == 'detail') {
                var response_div = '.scp_' + modulename + '_subpanel';
                jQuery(response_div).html(response);
            } else {
                jQuery('#responsedata').html(response);
                if (jQuery("#select_all").hasClass("scp_select_th_active")) {
                    jQuery(".scp-checkbox").prop('checked', "checked"); //change all ".checkbox" checked status
                    jQuery(".scp-checkbox").attr('disabled', "disabled");
                    jQuery("#select_all").prop('checked', "checked");
                }
            }
            // jQuery('#succid').hide();
        } else
        {
            var redirect_url = $(location).attr('href') + "?conerror=1";
            window.location.href = redirect_url;
        }
    });
}

// This function will use to show more products - proposal edit and add view will use
function bcp_product_show_more(modulename, view, get_current_url) {

    var pageno = parseInt(jQuery('#pageno').val());
    var order_by = jQuery('.order_by').val();
    var order = jQuery('.order').val();
    var selected_product_lineitems = jQuery(".selected_product_lineitems").val();
    var searchval = "";
    searchval = jQuery('#search-product').val();

    jQuery("#bcp_product_list_table").addClass('bcp-loader');
    var data = {
        'action': 'bcp_product_list_module',
        'view': view,
        'modulename': modulename,
        'page_no': pageno,
        'order_by': order_by,
        'order': order,
        'search_product': searchval,
        'current_url': get_current_url,
        'selected_product_lineitems': selected_product_lineitems,
        'request_type': 'show_more',
    };
    jQuery.post(ajaxurl, data, function (response) {
        jQuery("#bcp_product_list_table").removeClass('bcp-loader');
        if ($.trim(response) != '-1') {
            jQuery('#bcp_product_list_table tbody').append(response);
            if (jQuery('#totProductPage').val() == pageno) {
                jQuery('#show_more_btn').hide();
            } else {
                pageno += 1;
                jQuery('#pageno').val(pageno);
            }
        } else
        {
            var redirect_url = $(location).attr('href') + "?conerror=1";
            window.location.href = redirect_url;
        }
    });

}

// This function will use to search product - proposal edit and add view will use
function bcp_product_order(page, modulename, order_by, order, view, get_current_url) {
    var searchval = jQuery('#search-product').val();
    var type = 'reset';
    if(searchval !=''){
        type = 'search';
    }
    
    var selected_product_lineitems = jQuery(".selected_product_lineitems").val();
    jQuery("#bcp_product_list_table").addClass('bcp-loader');
    var data = {
        'action': 'bcp_product_list_module',
        'view': view,
        'modulename': modulename,
        'search_product': searchval,
        'page_no': '0',
        'current_url': get_current_url,
        'order_by': order_by,
        'order': order,
        'page': page,
        'selected_product_lineitems': selected_product_lineitems,
        'request_type': type,
    };

    jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data: data,
        async: true,
        success: function (response) {
            jQuery("#bcp_product_list_table").removeClass('bcp-loader');
            if ($.trim(response) != '-1') {
                jQuery("#bcp_product_list_table").html(response);
                var tot_product = 0, limit = 0;
                var tot_pages = 0;
                if (jQuery('#product_search_all').length > 0) {
                    tot_product = parseInt(jQuery('#product_search_all').val());
                    limit = parseInt(jQuery('#product_search_all').attr('data-limit'));
                    tot_pages = Math.ceil(tot_product / limit);
                    jQuery('.order_by').val(order_by);
                    jQuery('.order').val(order);
                }
                jQuery('#totProductPage').val(tot_pages);
                jQuery('#pageno').val('2');
                if (tot_pages < 2) {
                    jQuery('#show_more_btn').hide();
                } else {
                    jQuery('#show_more_btn').show();
                }

                if (type != 'search' && type != 'reset') {
                    // Check if any prduct qty is blank or not
                    check_blank_product_qty();
                }
            } else {
                var redirect_url = $(location).attr('href') + "?conerror=1";
                window.location.href = redirect_url;
            }
        },
    });
}

// This function will use to search product - proposal edit and add view will use
function bcp_product_search(modulename, view, get_current_url, type) {

    var searchval = "";
    if (type == 'search') {
        searchval = jQuery('#search-product').val();
        if (searchval == '' || searchval == null) {
            jQuery('.search-main-proposals .search-input').css('border', '1px solid red');
            return false;
        }
    } else {
        searchval = "";
        jQuery('#search-product').val('');
        jQuery('.search-input').css('border', '1px solid #8D8D8D');
    }
    
    var selected_product_lineitems = jQuery(".selected_product_lineitems").val();
    var order_by = jQuery('.order_by').val();
    var order = jQuery('.order').val();
    jQuery("#bcp_product_list_table").addClass('bcp-loader');
    var data = {
        'action': 'bcp_product_list_module',
        'view': view,
        'modulename': modulename,
        'search_product': searchval,
        'page_no': '0',
        'order_by': order_by,
        'order': order,
        'current_url': get_current_url,
        'selected_product_lineitems': selected_product_lineitems,
        'request_type': type,
    };

    jQuery.ajax({
        url: ajaxurl,
        type: 'POST',
        data: data,
        async: true,
        success: function (response) {
            jQuery("#bcp_product_list_table").removeClass('bcp-loader');
            if ($.trim(response) != '-1') {
                jQuery("#bcp_product_list_table").html(response);
                var tot_product = 0, limit = 0;
                var tot_pages = 0;
                if (jQuery('#product_search_all').length > 0) {
                    tot_product = parseInt(jQuery('#product_search_all').val());
                    limit = parseInt(jQuery('#product_search_all').attr('data-limit'));
                    tot_pages = Math.ceil(tot_product / limit);
                }
                jQuery('#totProductPage').val(tot_pages);
                jQuery('#pageno').val('2');
                if (tot_pages < 2) {
                    jQuery('#show_more_btn').hide();
                } else {
                    jQuery('#show_more_btn').show();
                }

                if (type != 'search' && type != 'reset') {
                    // Check if any prduct qty is blank or not
                    check_blank_product_qty();
                }
            } else {
                var redirect_url = $(location).attr('href') + "?conerror=1";
                window.location.href = redirect_url;
            }
        },
    });
}

// This function will use when anuy product selected or removed from selection - proposal edit and add view will use
function bcp_manage_selected_product_items(product_id, product_qty, action) {

    var i, old_selected_products, selected_product_json, final_selected_products, tmp_product;
    final_selected_products = [];

    old_selected_products = jQuery('.selected_product_lineitems').val();
    i = 0;

    if (old_selected_products != '') {

        old_selected_products = jQuery.parseJSON(old_selected_products);
        if (old_selected_products != null || old_selected_products != '') {

            for (var product in old_selected_products) {
                if (old_selected_products.hasOwnProperty(product)) {
                    if (old_selected_products[product]["product_id"] != product_id) {
                        tmp_product = {
                            "product_id": old_selected_products[product]["product_id"],
                            "product_qty": old_selected_products[product]["product_qty"],
                        };
                        final_selected_products.push(tmp_product);
                        i++;
                    }
                }
            }
            ;
        }
    }

    if (action != 'remove') { // If action is not remove than add / update product and qty

        tmp_product = {
            "product_id": product_id,
            "product_qty": product_qty,
        };
        final_selected_products.push(tmp_product);
    }

    selected_product_json = JSON.stringify(final_selected_products);
    jQuery('.selected_product_lineitems').val(selected_product_json);
}

function bcp_clear_search_txtbox(page, modulename, ifsearch, order_by, order, view, get_current_url) { //for clear search text box
    jQuery('#otherdata').hide();
    jQuery(this).parents('form').find('input[type="text"]').val('');
    jQuery("#responsedata").addClass('bcp-loader');
    var data = {
        'action': 'bcp_list_module',
        'view': view,
        'modulename': modulename,
        'page_no': page,
        'current_url': get_current_url,
        'order_by': order_by,
        'order': order,
    };
    jQuery.post(ajaxurl, data, function (response) {
        jQuery("#responsedata").removeClass('bcp-loader');
        if ($.trim(response) != '-1') {
            jQuery('#responsedata').html(response);
        } else
        {
            var redirect_url = $(location).attr('href') + "?conerror=1";
            window.location.href = redirect_url;
        }
    });

}

function bcp_module_call_view(modulename, id, view, curURL) { // for view particular record(detail page)
    jQuery("#responsedata").addClass('bcp-loader');
    var data = {
        'action': 'bcp_view_module',
        'view': view,
        'modulename': modulename,
        'id': id,
        'current_url': curURL
    };
    jQuery.post(ajaxurl, data, function (response) {
        jQuery("#responsedata").removeClass('bcp-loader');
        if ($.trim(response) != '-1') {
            jQuery('#responsedata').html(response);
            jQuery('#succid').hide();
        } else {
            var redirect_url = $(location).attr('href') + "?conerror=1";
            window.location.href = redirect_url;
        }
    });
}

function form_submit_note_document(id, field, view = '', case_id = '') {
    jQuery('#scp_note_id').val(id);
    jQuery('#scp_note_field').val(field);
    if (view != "") {
        jQuery('#scp_note_view').val(view);
    }
    if (case_id != "") {
        jQuery('#scp_note_case_id').val(case_id);
    }
    document.getElementById("download_note_id").submit();
    return false;
}
function form_submit_document(id, view = "", field_name='', moduel_name='') {
    jQuery('#scp_doc_id').val(id);
    if (view != "") {
        jQuery('#scp_doc_view').val(view);
    }
    if(field_name!=''){
        jQuery('#scp_doc_field_name').val(field_name);
    }
    if(moduel_name!=''){
        jQuery('#scp_module_name').val(moduel_name);
    }
    document.getElementById("doc_submit").submit();
    return false;
}

function form_submit_remove_document(id, field_name, module_name) {
    if (confirm(language_array.msg_confirm_delete_multiple)) {
        
        var data = {
            'action': 'scp_remove_document',
            'id': id,
            'field_name': field_name,
            'module_name': module_name,
        };
        jQuery("#responsedata").addClass('bcp-loader');
        jQuery.post(ajaxurl, data, function (response) {
            if (jQuery.trim(response) == '1') {
                window.location.reload();
            } 
            jQuery("#responsedata").removeClass('bcp-loader');
        });
    }
}

function form_sugar_submit_remove_image(id, module_name, field_name) {
    if (confirm(language_array.msg_confirm_delete_multiple)) {
        var data = {
            'action': 'scp_remove_image',
            'id': id,
            'field_name': field_name,
            'module_name': module_name,
        };
        jQuery("#responsedata").addClass('bcp-loader');
        jQuery.post(ajaxurl, data, function (response) {
            window.location.reload();
            jQuery("#responsedata").removeClass('bcp-loader');
        });
    }
}

function pdf_data_download(id, modulename) {
    jQuery('#scp_module_pdf_id').val(id);
    jQuery('#scp_module_name_download').val(modulename);
    document.getElementById("quote_pdf_submit").submit();
    return false;
}
//Added by BC on 22-jun-2016 for kbdocument attachment
function form_submit_KBdocument(id) {
    jQuery('#scp_kbdoc_id').val(id);
    document.getElementById("kbdoc_submit").submit();
    return false;
}

//printKB
function form_submit_KBdocument_Print(elem) {
    
    var kbtitle = jQuery(elem).parent().find("span").text();
    var kbcontent = jQuery(elem).parent().parent().parent().next(".collapse").html();
    jQuery(".print-kb-title").text(kbtitle);
    jQuery(".print-kb-description").html(kbcontent);
    $('#KBContent').printThis({
        debug: false,               // show the iframe for debugging
        importCSS: true,            // import parent page css
        importStyle: false,         // import style tags
        printContainer: true,       // print outer container/$.selector
        loadCSS: "",                // path to additional css file - use an array [] for multiple
        pageTitle: "",              // add title to print page
        removeInline: true,        // remove inline styles from print elements
        removeInlineSelector: "*",  // custom selectors to filter inline styles. removeInline must be true
        printDelay: 333,            // variable print delay
        header: $(".print-header").html(),               // prefix to html
        footer: null,               // postfix to html
        base: false,                // preserve the BASE tag or accept a string for the URL
        formValues: true,           // preserve input/form values
        canvas: false,              // copy canvas content
        doctypeString: ' ',       // enter a different doctype for older markup
        removeScripts: false,       // remove script tags from print content
        copyTagClasses: false,      // copy classes from the html & body tag
        beforePrintEvent: null,     // function for printEvent in iframe
        beforePrint: null,          // function called before iframe is filled
        afterPrint: null            // function called before iframe is removed
    });
}

//recent activity printKB
function form_submit_recent_KBdocument_Print(elem) {
    
    var kbtitle = jQuery(elem).attr('data-name');
    var kbcontent = jQuery(elem).attr('data-description');
    jQuery(".print-kb-title").text(kbtitle);
    jQuery(".print-kb-description").html(kbcontent);
    $('#KBContent').printThis({
        debug: false,               // show the iframe for debugging
        importCSS: true,            // import parent page css
        importStyle: false,         // import style tags
        printContainer: true,       // print outer container/$.selector
        loadCSS: "",                // path to additional css file - use an array [] for multiple
        pageTitle: "",              // add title to print page
        removeInline: true,        // remove inline styles from print elements
        removeInlineSelector: "*",  // custom selectors to filter inline styles. removeInline must be true
        printDelay: 333,            // variable print delay
        header: $(".print-header").html(),               // prefix to html
        footer: null,               // postfix to html
        base: false,                // preserve the BASE tag or accept a string for the URL
        formValues: true,           // preserve input/form values
        canvas: false,              // copy canvas content
        doctypeString: ' ',       // enter a different doctype for older markup
        removeScripts: false,       // remove script tags from print content
        copyTagClasses: false,      // copy classes from the html & body tag
        beforePrintEvent: null,     // function for printEvent in iframe
        beforePrint: null,          // function called before iframe is filled
        afterPrint: null            // function called before iframe is removed
    });
}


function search_gloabal() {
    var search_term,redirect_url, e, module_name;
    search_term = document.getElementById('g_search_name').value.trim();
    if ( search_term != '' && search_term.length >= 3 ) {

        module_name = jQuery(".sfcp-selected-module").attr("data-value");
        if ( module_name == "0" ) {
            redirect_url = homeurl+'portal-manage#data/SearchResult/' + search_term;
        } else {
            redirect_url = homeurl+'portal-manage#data/' + module_name + '/list/' + search_term + '/';
        }
        window.location.href = redirect_url;
    } else {
        alert("Enter atleast 3 characters to start search.");
        document.getElementById("g_search_name").style.border = "1px solid red";
        if ( search_term.length == '' ) {
            document.getElementById("g_search_name").value = "";
        }
    }

    return false;
}

/**
* Check if any product qty is blank or not, if blank than show red border in textbox
*
* @since 1.0.0
*/
function check_blank_product_qty() {

   var product_error, product_id, product_qty, productSelected, serviceSelected;

   product_error = false;
   productSelected = false;
   serviceSelected = false;

   jQuery('#bcp_product_list_table input.input_checkbox').each(function () {
       if (jQuery(this).is(':checked')) {
           product_id = jQuery(this).val();
           product_qty = jQuery("#qty_" + product_id).val();
           productSelected = true;
           if (product_qty == '0' || product_qty == '' || product_qty == null || product_qty <= 0) {
               jQuery("#qty_" + product_id).css('border', '1px solid red');
               product_error = true;
           }
       }
   });
   jQuery('.scp_proposal_input_service_name').each(function () {
       if (jQuery.trim(jQuery(this).val()) != "") {
           serviceSelected = true;
       }
   });
   if (jQuery('.error-line-item').length > 0 && productSelected == false && serviceSelected == false) {
       jQuery('.line-item-error').remove();
       jQuery('.error-line-item').html("<label class='error line-item-error'>"+language_array.msg_select_product_or_service+"</label>");
       return false;
   } else {
       jQuery('.line-item-error').remove();
       if (product_error) {
           return false;
       }
   }
}

function delete_multiple(delete_all, module_name) {
    if (confirm(language_array.msg_confirm_delete_multiple)) {
        var ids = [];
        jQuery(".scp-checkbox:checked").each(function (index) {
            if (module_name == "bc_proposal" && jQuery(this).hasClass("bc_proposal_not_pending")) {
            }else{
                ids.push(jQuery(this).attr('data-id'));
            }
        });
        if (ids.length == 0 && delete_all != '1') {
            jQuery("span.error").remove();
            jQuery("span.success").remove();
            var msg = "<span class='error'>"+language_array.msg_no_permission_action+"</span>";
            jQuery("#delete-notifications-btn").before(msg);
            return false;
        }
        if (jQuery("#select_all").hasClass("scp_select_th_active")) {
            delete_all = '1';
        }
        var data = {
            'action': 'scp_delete_multiple',
            'ids': ids,
            'delete_all': delete_all,
            'module_name': module_name,
        };
        jQuery("#responsedata").addClass('bcp-loader');
        jQuery.post(ajaxurl, data, function (response, status) {
            if (jQuery.trim(response) == '1') {
                window.location.reload();
            } else {
                jQuery('#notification_list_msg').html("<span class='error'>"+language_array.msg_error_in_delete_notifications+"</span>");
            }
            jQuery("#responsedata").removeClass('bcp-loader');
        });
    }
}

function bcp_module_call_add(htmlval, modulename, view, success, deleted, id, date,currentmodule='') { // for add record layout in module
    htmlval = (typeof htmlval === 'undefined') ? 0 : htmlval;
    success = (typeof success === 'undefined') ? null : success;
    deleted = (typeof deleted === 'undefined') ? null : deleted;
    id = (typeof id === 'undefined') ? null : id;
    var get_current_url = ajaxurl;
    // Added for working edit from calendar detail page
    var split_arry = get_current_url.split('?');
    split_arry[1] = (typeof split_arry[1] === 'undefined') ? null : split_arry[1];
    if (split_arry[1] != null) {
        var splited_url_var = split_arry[1].split('-')[0];
        if (splited_url_var == 'detail') {
            get_current_url = split_arry[0] + "?list-" + modulename;
        }
    }
    jQuery("#responsedata").addClass('bcp-loader');
    var data = {
        'action': 'bcp_add_module',
        'view': view,
        'modulename': modulename,
        'date': date,
        'success': success,
        'deleted': deleted,
        'id': id,
        'current_url': get_current_url

    };
    jQuery.post(ajaxurl, data, function (response) {
        jQuery("#responsedata").removeClass('bcp-loader');
        if ($.trim(response) != '-1') {
            if(currentmodule) {
                //window.location = '#data/'+modulename+'/edit';
            }
            if (htmlval == 0) {
                jQuery('#otherdata').hide();
                jQuery('#responsedata').html(response);
                jQuery('#succid').hide();
            } else {
                $('#otherdata').show();
                jQuery('#otherdata').html(response);
                jQuery('#succid').hide();
            }
            $("#otherdata").appendTo("#responsedata");
        } else {
            var redirect_url = $(location).attr('href') + "?conerror=1";
            window.location.href = redirect_url;
        }
//        setTimeout(function(){ jQuery( '.success' ).hide(); }, 2000);
//        setTimeout(function(){ jQuery( '.login_error' ).hide(); }, 2000);
    });
}


function bcp_module_add_casenote(htmlval, modulename, parentmodule, view, casenote) { // for add record layout in module
    htmlval = (typeof htmlval === 'undefined') ? 0 : htmlval;
    success = (typeof success === 'undefined') ? null : success;
    deleted = (typeof deleted === 'undefined') ? null : deleted;
    casenote = (typeof casenote === 'undefined') ? null : casenote;
    id = (typeof id === 'undefined') ? null : id;
    var get_current_url = ajaxurl;
    jQuery("#responsedata").addClass('bcp-loader');
    var data = {
        'action': 'bcp_add_module',
        'view': view,
        'modulename': modulename,
        'parentmodule': parentmodule,
        'current_url': get_current_url,
        'casenote': casenote

    };
    jQuery.post(ajaxurl, data, function (response) {
        jQuery("#responsedata").removeClass('bcp-loader');
        if ($.trim(response) != '-1') {
            if (htmlval == 0) {
                jQuery('#responsedata').html(response);
                jQuery('#succid').hide();
            } else {
                $('#otherdata').show();
                jQuery('#otherdata').html(response);
                jQuery('#succid').hide();
            }
            $("#otherdata").appendTo("#responsedata");
        } else
        {
            var redirect_url = $(location).attr('href') + "?conerror=1";
            window.location.href = redirect_url;
        }
    });
}

function scp_signup_form(modulename, action) {
    var data = {
        'modulename': modulename,
        "view": action,
        "action": "bcp_add_module",
    };
    jQuery(".scp-signup-form").addClass('bcp-loader');
    $.post(ajaxurl, data, function (response) {
        
        jQuery(".sfcp_login_tpl").show();
        if (response != '0' && response != '') {
            $('#responsedata').html(response);
        } else {
            response = "<label class='error'>"+language_array.msg_no_permission_data+"</label>";
            $('#responsedata').html(response);
        }
        jQuery(".scp-signup-form").removeClass('bcp-loader');
    });

}

jQuery(document).ready(function () {
    jQuery('#responsedata').on('change', '.conditionlogic_div', function (e) {
        var fieldname = jQuery(this).attr('id');
        var module_name = jQuery("input[name='module_name']").val();
        var view = jQuery("input[name='view']").val();
        var selectvalue = '';

        if (jQuery(this).hasClass('scp-multiselect')) {
            var val = [];
            jQuery(".scp-multiselect :selected").each(function (i) {
                val[i] = jQuery(this).val();
            });
            selectvalue = val;
        } else {
            selectvalue = jQuery(this).val();
        }

        var data = {
            'action': 'bcp_show_hide_fields',
            'selectvalue': selectvalue,
            'module_name': module_name,
            'view': view,
            'fieldname': fieldname,
        };

        jQuery("#responsedata").addClass('bcp-loader');
        jQuery.post(ajaxurl, data, function (datas) {
            var response = jQuery.parseJSON(datas);
            jQuery("#responsedata").removeClass('bcp-loader');
            jQuery.each(response, function (i, member) {
                if (member.Visibility == 'show') {
                    jQuery(".form-group").find('#' + member.FieldName).closest('.col-md-6, .col-lg-6').show();
                    if (member.portal_required == '1') {
                        jQuery(".form-group").find('#' + member.FieldName).attr("required", "required");
                        if (jQuery(".form-group").find('#' + member.FieldName).closest('.col-md-6, .col-lg-6').find('label span').length) {

                        } else {
                            jQuery(".form-group").find('#' + member.FieldName).closest('.col-md-6, .col-lg-6').find('label').append('<span class="required" aria-required="true">*</span>');
                        }
                    }
                } else if (member.Visibility == 'hide') {
                    jQuery(".form-group").find('#' + member.FieldName).closest('.col-md-6, .col-lg-6').hide();
                    jQuery(".form-group").find('#' + member.FieldName).removeAttr("required");
                    jQuery(".form-group").find('#' + member.FieldName).closest('.col-md-6, .col-lg-6').find('label span.required').remove();
                }
            });
        });
    });
});
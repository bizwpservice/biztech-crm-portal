<?php

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if ( ! function_exists( 'sfcp_solutions_searchpage' ) ) {
    
    function bcp_solutions_searchpage() {
        
        global $objSCP, $wpdb, $sugar_crm_version, $custom_relationships, $scp_modules, $lang_code, $language_array, $relate_to_module, $relate_to_id;
        //20-aug-2016
        $kb_name = "";
        $content = "";
        if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
            $sess_id = $objSCP->session_id;
            $kb_name = "AOK_KnowledgeBase";
        }
        if ($sugar_crm_version == 7) {
            $sess_id = $objSCP->access_token;
            $kb_name = "KBContents";
        }
        if ($sess_id != '') {
            $module_name_display = $_SESSION['module_array_without_s']['Cases'];
            $case_link = "#data/Cases/edit/";
            if ($relate_to_module != '') {
                $case_link = "#data/Cases/edit/Relate/".$relate_to_module."/".$relate_to_id;
            }
            ?>
            <div class="case-listing-block bg-white shadow">
                <div class="scp-action-header">
                    <div class="d-flex sfcp-heading">
                        <h2 id="dcp-head-incident"><?php echo $scp_modules['Cases']; ?></h2>
                    </div>
                </div>
                <div class="scp-page-action-title"></div>
                <form id="search_solutions_form">
                    <p class="solution-info"><?php echo $language_array['msg_solution_title']; ?> <a href="<?php echo $case_link; ?>"><?php echo $language_array['lbl_add']." ".$module_name_display; ?></a></p>
                    <div class="sfcp_form_search_box_div">
                        <input type="text" required="" id="sfcp_solution_keyword" name="sfcp_solution_keyword" placeholder="<?php echo $language_array['lbl_search_solution']; ?>" class="sfcp-required" />
                        <input class="sfcp_solution_search_btn" type="submit" value="<?php echo $language_array['lbl_search']; ?>">
                    </div>
                </form>
                <div id="solutionlist-items">

                </div>
            </div>
            <script>
                jQuery("#search_solutions_form").submit(function(e){

                    e.preventDefault();

                    var search_term, error_message, sfcp_required_error;
                    sfcp_required_error = 0;
                    jQuery( '.sfcp-required', this ).each( function() {

                        // If next element has class error-line, than remove it
                        if( jQuery( this ).next().hasClass('error-line') ) {
                             jQuery( this ).next('').remove();
                        }

                        this.value = jQuery.trim( jQuery( this ).val() );
                        search_term = jQuery( this ).val();
                        if ( ! search_term || search_term.length < 3 ) {

                            jQuery( this ).addClass( 'error-line' );
                            if ( ! search_term ) {
                                error_message = '<?php echo $language_array['msg_field_required']; ?>';
                            } else {
                                error_message = '<?php echo $language_array['msg_search_min_keyword']; ?>';
                            }
                            jQuery( '<label class="error-line">' + error_message + '</label>' ).insertAfter( this );
                            sfcp_required_error = 1;
                        }

                    });

                    if ( sfcp_required_error ) {
                        return false;
                    }
                    var data = {
                        'action': 'bcp_list_module',
                        'modulename': '<?php echo $kb_name; ?>',
                        'parentmodule': 'solutions',
                        'relate_to_module': "<?php echo $relate_to_module; ?>",
                        'relate_to_id': "<?php echo $relate_to_id; ?>",
                        'view': 'list',
                        'page_no': '0',
                        'order_by': '',
                        'order': '',
                        'searchval': search_term
                    };
                    var el = jQuery("#responsedata");
                    App.blockUI(el);
                    jQuery.post( ajaxurl, data, function( response ) {
                        App.unblockUI(el);
                        jQuery( '#solutionlist-items' ).html( response );
                        jQuery( '#solutionlist-items' ).removeClass('sfcp-loading');
                    });
                });
            </script>
            <?php
        } else {
            $objSCP = 0;
            $conn_err = $language_array['msg_auth_failed'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            $redirect_url = $_REQUEST['scp_current_url'] . '?conerror=1';
            wp_redirect($redirect_url);
        }
        $content .= ob_get_clean();
        ob_end_clean();
        return $content;
    }
    
}


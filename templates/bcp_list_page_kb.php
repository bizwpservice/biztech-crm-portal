<?php
$cnt = 0;

//Print Header ---------------------- V3.2.0
$module_without_s = $_SESSION['module_array_without_s'][$module_name];
$biztech_scp_name = fetch_data_option('biztech_scp_name');
if ($biztech_scp_name != NULL) {
    $biztech_scp_name = '<h3  class="scp-login-heading">' . $biztech_scp_name . '</h3>';
}
$biztech_upload_image = fetch_data_option('biztech_scp_upload_image');
if ($biztech_upload_image != NULL) {
    ?>
    <div class="scp-logo">
        <img src="<?php echo $biztech_upload_image; ?>"  width="100" alt=""/>
    </div>
    <?php
}
?>
<div class='print-header' style='display: none;'>
    <div class='print-header-div'>
        <div class='print-div' >
            <?php echo $biztech_upload_image . "" . $biztech_scp_name; ?>
        </div>
        <div class='print-div' >
            <h3 class='module-title'><?php echo $module_without_s; ?></h3>
        </div>
    </div>
</div>

<div style='display: none;' id='KBContent'>
    <div class='print-content'>
        <div class='print-kb-header'>
            <h3 class='print-kb-title'></h3>
            <span class='print-kb-subtitle'></span>
        </div>
        <div class='print-kb-description'>
        </div>
    </div>
</div>

<?php
if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
    $class = 'search-input-six';
    $cnt = count($list_result->entry_list);
    $list_res_kb = $list_result->entry_list;
}

if ($sugar_crm_version == 7) {
    $class = 'search-input-six';
    $cnt = count($list_result->records);
    $list_res_kb = $list_result->records;
}
if ($cnt != 0 || isset($_POST['searchval'])) {
    ?>

    <div class='scp-knowledgebase-wrapper'>

        <?php
        if ($sugar_crm_version == 7) {
            $list_knb_cat = $objSCP->getKBaseCategories($contact_group_id);
            $list_knb_cat = isset($list_knb_cat->entry_list) ? $list_knb_cat->entry_list : array();
            $list_knb_cat = (array) $list_knb_cat;
        }
        //For case solutions search
        
        if (!isset($_REQUEST['parentmodule']) || (isset($_REQUEST['parentmodule']) && $_REQUEST['parentmodule'] != 'solutions')) {
            ?>

            <div class='top-header-search mr-auto d-flex align-items-center body-header-serch'>
                <div class="knowledge-dropdown-group d-flex">
                    <div class="top-serch body-serch">
                        <input type='text' name='search_name' value='<?php echo $search_name; ?>' id='search_name' placeholder='<?php echo $language_array['lbl_search']; ?>' class='<?php echo $class ?> form-control bg-light border-0 small'/>
                    </div>
                    <div class="module-dropdown dropdown body-dropdown knowledge-dropdown">
                        <?php
                            if (($sugar_crm_version == 6 || $sugar_crm_version == 5) && $module_name == 'AOK_KnowledgeBase') {
                                //Addded by BC on 22-jun-2016
                                //knowledge Base Category List
                                $list_knb_cat = $objSCP->get_kb_categories_group_wise($contact_group_id);
                                ?>
                                <div class='select-style' >
                                    <select name='AOK_Knowledge_Base_Categories' id='AOK_Knowledge_Base_Categories'>
                                        <option value=''>Select KB Category</option>
                                        <?php
                                        foreach ($list_knb_cat->entry_list as $id => $list_result_s) {
                                            $name = $list_result_s->value;
                                            $selected = '';
                                            if ($id == $AOK_Knowledge_Base_Categories) {
                                                $selected = 'selected="selected"';
                                            }
                                            ?>
                                            <option value='<?php echo $id; ?>' <?php echo $selected; ?>><?php echo $name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            <?php } else { ?>

                                <div class='select-style' >
                                    <select name='AOK_Knowledge_Base_Categories' id='AOK_Knowledge_Base_Categories'>
                                        <option value=''>Select KB Category</option>
                                        <?php
                                        foreach ($list_knb_cat as $id => $name) {
                                            $selected = '';
                                            if ($id == $AOK_Knowledge_Base_Categories) {
                                                $selected = 'selected="selected"';
                                            }
                                            ?>
                                            <option value='<?php echo $id; ?>' <?php echo $selected; ?>><?php echo $name; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <?php } ?>
                    </div>
                </div>
                <div class="knowledge-btn-group d-flex">
                    <div class="search-btn-topbar">
                        <a href='javascript:void(0);' onclick='bcp_module_paging(0, "<?php echo $module_name; ?>", 1, "", "", "<?php echo $view; ?>", "<?php echo $current_url; ?>")' id='search_btn_id' class="btn btn-blue-curve btn-primary"><?php _e('Search'); ?></a>
                    </div>
                    <div class="clear-btn">
                        <a id='clear_btn_id' onclick='bcp_clear_search_txtbox(0, "<?php echo $module_name; ?>", "", "", "", "<?php echo $view; ?>", "<?php echo $current_url; ?>");' href='javascript:void(0);' class="btn btn-grey-curve btn-primary"><?php _e('Clear'); ?></a>
                    </div>
                </div>
            </div>
        <?php } else { ?>

            <input type='hidden' name='search_name' value='<?php echo $search_name; ?>' id='search_name' />
            <?php
        }
    }

    if ($cnt != 0) {
        ?>
            <div class="accordion-section">
                <div id="accordion">
            <?php
        foreach ($list_res_kb as $k => $list_result_s) {
            $kbcat_name = '';
            if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
                $id = $list_result_s->name_value_list->id->value;
                $kb_title = $list_result_s->name_value_list->name->value;
                $kb_created_date_origional = $list_result_s->name_value_list->date_entered->value;
                $kb_desc_full = html_entity_decode($list_result_s->name_value_list->description->value); //strip html tags if any
                $kb_desc_full = preg_replace("/<img src=\"rest[^>]+\>/i", "", $kb_desc_full);

                $kb_created_date = date('M d, Y', strtotime($kb_created_date_origional));
            }
            if ($sugar_crm_version == 7) {
                $id = $list_result_s->id;

                $kb_title = $list_result_s->name;
                $kb_desc_full = $list_result_s->kbdocument_body;

                $cate_id = $list_result_s->category_id;
                $kbcat_name = (isset($list_knb_cat[$cate_id]) ? $list_knb_cat[$cate_id] : $language_array['lbl_uncategorized']);
                $kb_desc_full = preg_replace("/<img src=\"rest[^>]+\>/i", "", $kb_desc_full);
                $kb_created_date_origional = $list_result_s->date_entered;

                $kb_created_date = date('M d, Y', strtotime($kb_created_date_origional));
            }
            ?>
            <div class='card'>
                <div class="card-header collapsed" id="heading<?php echo $k; ?>" data-toggle="collapse" data-target="#collapse<?php echo $k; ?>" aria-expanded="true" aria-controls="collapse<?php echo $k; ?>">
                    <h3 class="mb-0 d-flex justify-content-between align-items-center">
                        <button class="btn btn-link"><span><?php echo $kb_title; ?></span>
                            <em class='fa fa-print' title='<?php echo $language_array['lbl_print']; ?>' onclick='form_submit_KBdocument_Print(this);'></em>
			</button>
                        <div class="float-right card-header-right d-flex justify-content-between align-items-center"><em class='fa fa-key'></em><span><?php echo $kb_created_date; ?></span>
                        </div>
                    </h3>
                    <div class="card-right-arrow">
                        <em class="fa fa-angle-down" aria-hidden="true"></em>
                    </div> 
                </div>
                <div id="collapse<?php echo $k; ?>" class="collapse" aria-labelledby="heading<?php echo $k; ?>" data-parent="#accordion">
                    <div class="card-body">
                        <?php echo $kb_desc_full; ?>
                    </div>
                    <?php if ($module_name == 'KBContents' && count($list_result_s->attachment_list) > 0 ) { ?>
                        <div class="card-body"><div class="scp-col-12 panel-title"><span class="panel_name"><?php echo $language_array['lbl_attachments']; ?></span></div>
                            <?php foreach ($list_result_s->attachment_list as $k_attch => $v_attch) { ?>
                                <a href='javascript:void(0);' onclick='form_submit_KBdocument("<?php echo $v_attch->document_revision_id; ?>");' class='general-link-btn scp-download-btn scp-KBContents-font scp-default-font'><em class='fa fa-download' aria-hidden='true'></em>&nbsp;&nbsp;<?php echo $v_attch->name; ?></a><br>
                            <?php } ?>
                        </div>
                        <?php } ?>
                </div>
            </div>
            <?php } ?>
                    </div>
            </div>
                    <?php } else { ?>
            <div class='detail-part'>
                <?php $portal_message_list = isset($_SESSION['portal_message_list']->module_no_records) ? __($_SESSION['portal_message_list']->module_no_records) : '' ; ?>
                <strong><?php echo $portal_message_list; ?></strong>
                <?php if ((isset($_REQUEST['parentmodule']) && $_REQUEST['parentmodule'] == 'solutions')) { ?>
                <a class="btn btn-blue btn-primary" title="<?php echo $language_array['lbl_add']; ?>" href="#data/Cases/edit/"><?php echo $language_array['lbl_add']." ".$_SESSION['module_array_without_s']['Cases']; ?></a>
                <?php } ?>
            </div>
        <?php } ?>
</div>
<script>
    jQuery(document).ready(function () {
        jQuery('.detail-part .question').on('click', function (e) {
            var scp_print_icon = jQuery('.fa-print');
            if (scp_print_icon.is(e.target)) {
                return false;
            }
            if (!jQuery(this).hasClass('active')) {
                jQuery('.detail-part .answer').slideUp();
                jQuery('.detail-part .question').removeClass('active');
                jQuery(this).find('h4').attr('title', '<?php echo $language_array['lbl_click_here_to_hide']; ?>');
                jQuery(this).addClass('active');
                jQuery(this).parent('.detail-part').find('.answer').slideDown();
            } else {
                jQuery(this).find('h4').attr('title', '<?php echo $language_array['lbl_click_here']; ?>');
                jQuery(this).parent('.detail-part').find('.answer').slideUp();
                jQuery(this).parent('.detail-part').find('.question').removeClass('active');
            }
        });

        $('#search_name').keyup(function (event) {
            if (event.keyCode === 13) {
                $('#search_btn_id').click();
            }
        });

    });
</script>

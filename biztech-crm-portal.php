<?php
/**
 * Plugin Name: SuiteCRM Customer Portal
 * Description: Reduce operational costs and improve customer satisfaction by empowering customers to get online support and get their complains and queries addressed through Customer Portal V2 that integrates the user friendly front end interface of Wordpress and the back end data sourcing from SugarCRM.
 * Author: CRMJetty
 * Author URI: https://www.crmjetty.com/
 * Version: 4.3.0
 */

defined('ABSPATH') or die('No script kiddies please!');

if (is_multisite() && SUBDOMAIN_INSTALL) {
    //if user has multipledomain enable
    if (get_option('biztech_scp_muiltidomain') == 'multidomain-enable') {
        if (!defined('COOKIE_DOMAIN')) {
            define('COOKIE_DOMAIN', false);
        }
        define('BIZTECH_PORTAL_ENABLE', 'MULTIDOMAIN');
        define('DOMAIN_CURRENT_SITE', get_site_url());
    } else {
        $disallowed = array('http://', 'http://www.', 'https://', 'https://www.');
        $network_url = network_site_url();
        foreach ($disallowed as $d) {
            if (strpos($network_url, $d) === 0) {
                $network_url = str_replace($d, '.', $network_url);
            }
            $network_url = str_replace('/', '', $network_url);
        }
        ini_set('session.cookie_domain', $network_url);
    }
}

if ( (isset($_REQUEST['action']) && $_REQUEST['action'] == 'update_sugarportal_settings') ||
     (isset($_REQUEST['option_page']) && $_REQUEST['option_page'] == 'sugar_crm_portal-settings-group' ) ) {

    if (isset($_REQUEST['biztech_scp_rest_url']) && isset($_REQUEST['biztech_scp_sugar_crm_version'])) {

        $checking_rest_url = get_option('biztech_scp_rest_url');
        $new_rest_url = $_REQUEST['biztech_scp_rest_url'];

        if ($new_rest_url != $checking_rest_url) {

            global $wpdb;
            if (get_option('es_modules_label') !== false) {
                update_option('es_modules_label', '');
            }

            // Truncate layout and product table
            if (is_multisite()) {
                $blogids = $wpdb->get_results("SELECT blog_id FROM {$wpdb->blogs} WHERE site_id = '{$wpdb->siteid}' AND spam = '0' AND deleted = '0' AND archived = '0'");  //get all available sub websites
                foreach ($blogids as $blog) {
                    switch_to_blog($blog->blog_id); //switch to all websites one by one
                    $table_name = $wpdb->prefix . "module_layout";
                    $wpdb->query('TRUNCATE TABLE ' . $table_name);
                    restore_current_blog(); //restore to current blog, so that other options can't conflict
                }
            } else {
                $table_name = $wpdb->prefix . "module_layout";
                $wpdb->query('TRUNCATE TABLE ' . $table_name);
            }
        }
    }

    if (is_multisite()) {
        $myallkeysnetwork = array('biztech_scp_rest_url',
            'biztech_scp_username',
            'biztech_scp_sugar_crm_version');

        foreach ($myallkeysnetwork as $j) {  //update all options in network
            if (isset($_POST[$j])) {  //if we fetch value than set it otherwise set to null
                update_option($j, $_POST[$j]);
                update_site_option($j, $_POST[$j]);
            }
        }
    }
}

global $sugar_crm_version, $bcp_framework, $custom_relationships;
$sugar_crm_version = get_option('biztech_scp_sugar_crm_version');
$bcp_framework = 'wordPress';
if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
    if (file_exists(plugin_dir_path(__FILE__) . 'class/scp-class-6.php')) {
        include( plugin_dir_path(__FILE__) . 'class/scp-class-6.php');
    }
    if (file_exists(plugin_dir_path(__FILE__) . 'actions/bcp-action-v6.php')) {
        include( plugin_dir_path(__FILE__) . 'actions/bcp-action-v6.php');
    }
} else if ($sugar_crm_version == 7) {
    if (file_exists(plugin_dir_path(__FILE__) . 'class/scp-class-7.php')) {
        include( plugin_dir_path(__FILE__) . 'class/scp-class-7.php');
    }
    if (file_exists(plugin_dir_path(__FILE__) . 'actions/bcp-action-v7.php')) {
        include( plugin_dir_path(__FILE__) . 'actions/bcp-action-v7.php');
    }
} else {
    if (file_exists(plugin_dir_path(__FILE__) . 'class/scp-class-6.php')) {
        include( plugin_dir_path(__FILE__) . 'class/scp-class-6.php');
    }
    if (file_exists(plugin_dir_path(__FILE__) . 'actions/bcp-action-v6.php')) {
        include( plugin_dir_path(__FILE__) . 'actions/bcp-action-v6.php');
    }
}

include_once( plugin_dir_path(__FILE__) . 'templates/' . 'scp-class-page-template.php' ); //Include template file for manage-page
add_action('init', array('Scp_Page_Template_Plugin', 'get_instance'));


include( plugin_dir_path(__FILE__) . 'actions/bcp_common-action.php'); // include common actions for 6/7 versions
include( plugin_dir_path(__FILE__) . 'actions/function-array_column.php'); // column function, if not exits in lower php server
include( plugin_dir_path(__FILE__) . 'admin/settings.php'); //For include all settings of admin side
include( plugin_dir_path(__FILE__) . 'admin/extra-settings.php'); //For include all extra-settings of admin side
//include( plugin_dir_path(__FILE__) . 'admin/product-sync.php'); // For include product sync settings in admin side
include( plugin_dir_path(__FILE__) . 'shortcodes/bcp-shortcodes.php'); //For include all shortcodes
include( plugin_dir_path(__FILE__) . 'shortcodes/knowledge-base.php');  // Knowledge-base shortcode
include( plugin_dir_path(__FILE__) . 'admin/import-users.php');
include( plugin_dir_path(__FILE__) . 'admin/chat.php');

/* svg contnet get data */
include( plugin_dir_path(__FILE__) . 'templates/bcp_svg_content.php');

define('IMAGES_URL', plugin_dir_url(__FILE__) . 'assets/images/');
define('JS_PATH', plugin_dir_path(__FILE__) . 'assets/js/');
/* Addded by dharti.gajera */
define('ADMIN_TEMPLATE_PATH', plugin_dir_path(__FILE__) . 'admin/partials/');
define('TEMPLATE_PATH', plugin_dir_path(__FILE__) . 'templates/');
define('ACTIONS_PATH', plugin_dir_path(__FILE__) . 'actions/');
define('BCP_PLUGIN_PATH', plugin_dir_path(__FILE__));

if (!function_exists('network_new_blog_install')) {
    add_action('wpmu_new_blog', 'network_new_blog_install', 100);

    function network_new_blog_install($blog_id) {
        bcp_create_page();
        switch_to_blog($blog_id);
        //add for sugar connection
        add_option('biztech_scp_sugar_crm_version', get_site_option('biztech_scp_sugar_crm_version'));
        add_option('biztech_scp_rest_url', get_site_option('biztech_scp_rest_url'));
        add_option('biztech_scp_username', get_site_option('biztech_scp_username'));
        add_option('biztech_scp_password', get_site_option('biztech_scp_password'));
        //add all pages and set by default
        add_option('biztech_redirect_signup', get_page_by_path('portal-sign-up')->ID);
        add_option('biztech_redirect_login', get_page_by_path('portal-login')->ID);
        add_option('biztech_redirect_profile', get_page_by_path('portal-profile')->ID);
        add_option('biztech_redirect_forgotpwd', get_page_by_path('portal-forgot-password')->ID);
        add_option('biztech_redirect_resetpwd', get_page_by_path('portal-reset-password')->ID);
        add_option('biztech_redirect_manange', get_page_by_path('portal-manage-page')->ID);
        add_option('biztech_redirect_dashboard_gui', get_page_by_path('portal-dashboard-gui')->ID);
        //add for calendar
        add_option('biztech_scp_calendar_calls_color', "#7d9e12");
        add_option('biztech_scp_calendar_meetings_color', "#00585e");
        restore_current_blog();
    }

}

if (!function_exists('network_blog_delete')) {
    add_action('delete_blog', 'network_blog_delete', 10, 2);

    function network_blog_delete($blog_id, $drop = false) {
        global $wpdb;
        $table_name = $wpdb->prefix . 'module_layout';
        $sql = "DROP TABLE IF EXISTS $table_name";
        $wpdb->query($sql);
    }

}

if (!function_exists('bcp_create_page')) {
    register_activation_hook(__FILE__, "bcp_create_page");

    function bcp_create_page() {
        if (is_multisite()) {
            global $wpdb, $user_ID;
            $page_array = array('bcp-sign-up' => 'Portal Sign Up', 'bcp-login' => 'Portal Login', 'bcp-profile' => 'Portal Profile', 'bcp-forgot-password' => 'Portal Forgot Password', 'bcp-manage-page' => 'Portal Manage Page', 'bcp-reset-password' => 'Portal Reset Password');

            $blogids = $wpdb->get_results("SELECT blog_id FROM {$wpdb->blogs} WHERE site_id = '{$wpdb->siteid}' AND spam = '0' AND deleted = '0' AND archived = '0'");  //get all available sub websites
            foreach ($blogids as $blog) {
                switch_to_blog($blog->blog_id); //switch to all websites one by one
            
                require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
                dbDelta( $sql );

                // $filename should be the path to a file in the upload directory.
                $image_url = plugin_dir_path( __FILE__ ).'assets/images/bcp-login.jpg';
                $thumb_ids = 'bcp-login.jpg';
                if ( null == ($thumb_ids = sfcp_does_file_exists('bcp-login.jpg'))) {

                    $upload_dir = wp_upload_dir();
                    $image_data = file_get_contents( $image_url );
                    $filename = basename( $image_url );
                    if ( wp_mkdir_p( $upload_dir['path'] ) ) {
                      $file = $upload_dir['path'] . '/' . $filename;
                    } else {
                      $file = $upload_dir['basedir'] . '/' . $filename;
                    }

                    file_put_contents( $file, $image_data );
                    $wp_filetype = wp_check_filetype( $filename, null );
                    $attachment = array(
                      'post_mime_type' => $wp_filetype['type'],
                      'post_title' => sanitize_file_name( $filename ),
                      'post_content' => '',
                      'post_status' => 'inherit'
                    );

                    $attach_id = wp_insert_attachment( $attachment, $file );
                    require_once( ABSPATH . 'wp-admin/includes/image.php' );
                    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
                    wp_update_attachment_metadata( $attach_id, $attach_data );
                } else {
                    $attach_id = sfcp_does_file_exists('bcp-login.jpg');
                }
                
                /* signup */
                $register_image_url = plugin_dir_path( __FILE__ ).'assets/images/bcp-register.jpg';
                $register_thumb_ids = 'bcp-register.jpg';
                if ( null == ($register_thumb_ids = sfcp_does_file_exists('bcp-register.jpg'))) {

                    $upload_dir = wp_upload_dir();
                    $image_data = file_get_contents( $register_image_url );
                    $filename = basename( $register_image_url );
                    if ( wp_mkdir_p( $upload_dir['path'] ) ) {
                      $file = $upload_dir['path'] . '/' . $filename;
                    } else {
                      $file = $upload_dir['basedir'] . '/' . $filename;
                    }

                    file_put_contents( $file, $image_data );
                    $wp_filetype = wp_check_filetype( $filename, null );
                    $attachment = array(
                      'post_mime_type' => $wp_filetype['type'],
                      'post_title' => sanitize_file_name( $filename ),
                      'post_content' => '',
                      'post_status' => 'inherit'
                    );

                    $register_attach_id = wp_insert_attachment( $attachment, $file );
                    require_once( ABSPATH . 'wp-admin/includes/image.php' );
                    $attach_data = wp_generate_attachment_metadata( $register_attach_id, $file );
                    wp_update_attachment_metadata( $register_attach_id, $attach_data );
                } else {
                    $register_attach_id = sfcp_does_file_exists('bcp-register.jpg');
                }
                
                /* forgot password */
                $forgot_image_url = plugin_dir_path( __FILE__ ).'assets/images/forgot-password.jpg';
                $forgot_thumb_ids = 'forgot-password.jpg';
                if ( null == ($forgot_thumb_ids = sfcp_does_file_exists('forgot-password.jpg'))) {

                    $upload_dir = wp_upload_dir();
                    $image_data = file_get_contents( $forgot_image_url );
                    $filename = basename( $forgot_image_url );
                    if ( wp_mkdir_p( $upload_dir['path'] ) ) {
                      $file = $upload_dir['path'] . '/' . $filename;
                    } else {
                      $file = $upload_dir['basedir'] . '/' . $filename;
                    }

                    file_put_contents( $file, $image_data );
                    $wp_filetype = wp_check_filetype( $filename, null );
                    $attachment = array(
                      'post_mime_type' => $wp_filetype['type'],
                      'post_title' => sanitize_file_name( $filename ),
                      'post_content' => '',
                      'post_status' => 'inherit'
                    );

                    $forgot_attach_id = wp_insert_attachment( $attachment, $file );
                    require_once( ABSPATH . 'wp-admin/includes/image.php' );
                    $attach_data = wp_generate_attachment_metadata( $forgot_attach_id, $file );
                    wp_update_attachment_metadata( $forgot_attach_id, $attach_data );
                } else {
                    $forgot_attach_id = sfcp_does_file_exists('forgot-password.jpg');
                }

                $biztech_redirect_login = get_option( 'biztech_redirect_login' );
                if ( ! $biztech_redirect_login ) {
                    $id = wp_insert_post(
                            array(
                                'post_title'    => __( 'Portal Login' ),
                                'post_type'     => 'page',
                                'post_content'  => "[bcp-login]",
                                'post_status'   => 'publish',
                            )
                        );
                    update_option( 'biztech_redirect_login',  $id );
                    set_post_thumbnail( $id, $attach_id );
                    update_post_meta($id, '_wp_page_template', 'page-crm-standalone.php');
                }
                $biztech_redirect_signup = get_option( 'biztech_redirect_signup' );
                if ( ! $biztech_redirect_signup ) {
                    $id = wp_insert_post(
                            array(
                                'post_title'    => __( 'Portal Registration' ),
                                'post_type'     => 'page',
                                'post_content'  => '[bcp-sign-up]',
                                'post_status'   => 'publish',
                            )
                        );
                    update_option( 'biztech_redirect_signup',  $id );
                    set_post_thumbnail( $id, $register_attach_id );
                    update_post_meta($id, '_wp_page_template', 'page-crm-standalone.php');
                }

                $biztech_redirect_profile = get_option( 'biztech_redirect_profile' );
                if ( ! $biztech_redirect_profile ) {
                    $id = wp_insert_post(
                            array(
                                'post_title'    => __( 'Portal Profile' ),
                                'post_type'     => 'page',
                                'post_content'  => '[bcp-profile]',
                                'post_status'   => 'publish',
                            )
                        );
                    update_option( 'biztech_redirect_profile',  $id );
                    update_post_meta($id, '_wp_page_template', 'page-crm-standalone.php');
                }

                $biztech_redirect_forgotpwd = get_option( 'biztech_redirect_forgotpwd' );
                if ( ! $biztech_redirect_forgotpwd ) {
                    $id = wp_insert_post(
                            array(
                                'post_title'    => __( 'Portal Forgot Password' ),
                                'post_type'     => 'page',
                                'post_content'  => "[bcp-forgot-password]",
                                'post_status'   => 'publish',
                            )
                        );
                    update_option( 'biztech_redirect_forgotpwd',  $id );
                    set_post_thumbnail( $id, $forgot_attach_id );
                    update_post_meta($id, '_wp_page_template', 'page-crm-standalone.php');
                }

                $biztech_redirect_resetpwd = get_option( 'biztech_redirect_resetpwd' );
                if ( ! $biztech_redirect_resetpwd ) {
                    $id = wp_insert_post(
                            array(
                                'post_title'    => __( 'Portal Reset Password' ),
                                'post_type'     => 'page',
                                'post_content'  => "[bcp-reset-password]",
                                'post_status'   => 'publish',
                            )
                        );
                    update_option( 'biztech_redirect_resetpwd',  $id );
                    set_post_thumbnail( $id, $forgot_attach_id );
                    update_post_meta($id, '_wp_page_template', 'page-crm-standalone.php');
                }

                $biztech_redirect_manange = get_option( 'biztech_redirect_manange' );
                if ( ! $biztech_redirect_manange ) {
                    $id = wp_insert_post(
                            array(
                                'post_title'    => __( 'Portal Manage' ),
                                'post_type'     => 'page',
                                'post_content'  => '[bcp-manage-page]',
                                'post_status'   => 'publish',
                            )
                        );
                    update_option( 'biztech_redirect_manange',  $id );
                    update_post_meta($id, '_wp_page_template', 'page-crm-standalone.php');
                }

                $charset_collate = $wpdb->get_charset_collate();
                $table_name = $wpdb->prefix . 'module_layout';
                $sql = "CREATE TABLE IF NOT EXISTS $table_name(
                                `id` int(11) NOT NULL AUTO_INCREMENT,
                                `version` tinyint(4) NOT NULL,
                                `module_name` varchar(50) NOT NULL,
                                `layout_view` varchar(50) NOT NULL,
                                `json_data` longtext NOT NULL,
                                `all_fields` longtext NOT NULL,
                                `conditional_fields` longtext NOT NULL,
                                `date` varchar(255) DEFAULT NULL,
                                `contact_group` VARCHAR(50) NOT NULL,
                                `lang_code` VARCHAR(255) NOT NULL,
                                PRIMARY KEY ( `id` )) $charset_collate;";
                $wpdb->query($sql);
                $table_name = $wpdb->prefix . 'scp_portal_contact_info';
                $sql = "CREATE TABLE IF NOT EXISTS $table_name(
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `contact_id` varchar(50) NOT NULL,
                        `notification_permission` longtext NOT NULL,
                        `total_notification` int(11) NOT NULL,
                        `dashboard_preferences` text,
                      PRIMARY KEY ( `id` )) $charset_collate;";
                $wpdb->query($sql);
                
                //Create Table for multi language crm_language_messages
                //Since v3.2.0
                $table_name = $wpdb->prefix . 'crm_language_messages';
                $sql_lang = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `lang_code` varchar(100),
                    `response_data` text NOT NULL,
                    `modified_date` datetime NOT NULL,
                    `created_date` datetime NOT NULL,
                    PRIMARY KEY (`id`)
                  ) $charset_collate";
                $wpdb->query($sql_lang);
                
                //Create Table for login attempts bcp_login_attempts
                //Since v4.3.0
                $bcp_login_attempts_table_name = $wpdb->prefix . 'bcp_login_attempts';
                $sql_login_attempts = "CREATE TABLE IF NOT EXISTS " . $bcp_login_attempts_table_name . " (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `ip_address` varchar(150) NOT NULL,
                    `login_attempt` int(2),
                    `bcp_user_login` varchar(22),
                    `last_login_date` datetime,
                    `ip_status` ENUM('active','inactive'),
                    PRIMARY KEY (`id`)
                  ) $charset_collate";
                $wpdb->query($sql_login_attempts);
                
                update_option('biztech_portal_calendar','1');
                if (!get_option('biztech_scp_name')) {
                    update_option('biztech_scp_name', __('Customer Portal') );
                }

                restore_current_blog(); //restore to current blog, so that other options can't conflict
            }
        } else {
            global $wpdb, $user_ID;

            $page_array = array('bcp-sign-up' => 'Portal Sign Up', 'bcp-login' => 'Portal Login', 'bcp-profile' => 'Portal Profile', 'bcp-forgot-password' => 'Portal Forgot Password', 'bcp-manage-page' => 'Portal Manage Page', 'bcp-reset-password' => 'Portal Reset Password');
            
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );

            $image_url = plugin_dir_path( __FILE__ ).'assets/images/bcp-login.jpg';
            $thumb_id = 'bcp-login.jpg';
            if ( null == ($thumb_id = sfcp_does_file_exists('bcp-login.jpg'))) {
                $upload_dir = wp_upload_dir();
                $image_data = file_get_contents( $image_url );
                $filename = basename( $image_url );
                if ( wp_mkdir_p( $upload_dir['path'] ) ) {
                  $file = $upload_dir['path'] . '/' . $filename;
                } else {
                  $file = $upload_dir['basedir'] . '/' . $filename;
                }

                file_put_contents( $file, $image_data );
                $wp_filetype = wp_check_filetype( $filename, null );
                $attachment = array(
                  'post_mime_type' => $wp_filetype['type'],
                  'post_title' => sanitize_file_name( $filename ),
                  'post_content' => '',
                  'post_status' => 'inherit'
                );

                $attach_id = wp_insert_attachment( $attachment, $file );
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
                wp_update_attachment_metadata( $attach_id, $attach_data );
            } else {
                $attach_id = sfcp_does_file_exists('bcp-login.jpg');
            }
            
            /* signup */
            $register_image_url = plugin_dir_path( __FILE__ ).'assets/images/bcp-register.jpg';
            $register_thumb_ids = 'bcp-register.jpg';
            if ( null == ($register_thumb_ids = sfcp_does_file_exists('bcp-register.jpg'))) {

                $upload_dir = wp_upload_dir();
                $image_data = file_get_contents( $register_image_url );
                $filename = basename( $register_image_url );
                if ( wp_mkdir_p( $upload_dir['path'] ) ) {
                  $file = $upload_dir['path'] . '/' . $filename;
                } else {
                  $file = $upload_dir['basedir'] . '/' . $filename;
                }

                file_put_contents( $file, $image_data );
                $wp_filetype = wp_check_filetype( $filename, null );
                $attachment = array(
                  'post_mime_type' => $wp_filetype['type'],
                  'post_title' => sanitize_file_name( $filename ),
                  'post_content' => '',
                  'post_status' => 'inherit'
                );

                $register_attach_id = wp_insert_attachment( $attachment, $file );
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                $attach_data = wp_generate_attachment_metadata( $register_attach_id, $file );
                wp_update_attachment_metadata( $register_attach_id, $attach_data );
            } else {
                $register_attach_id = sfcp_does_file_exists('bcp-register.jpg');
            }
            
            $forgot_image_url = plugin_dir_path( __FILE__ ).'assets/images/forgot-password.jpg';
            $forgot_thumb_id = 'forgot-password.jpg';
            if ( null == ($forgot_thumb_id = sfcp_does_file_exists('forgot-password.jpg'))) {
                $upload_dir = wp_upload_dir();
                $image_data = file_get_contents( $forgot_image_url );
                $filename = basename( $forgot_image_url );
                if ( wp_mkdir_p( $upload_dir['path'] ) ) {
                  $file = $upload_dir['path'] . '/' . $filename;
                } else {
                  $file = $upload_dir['basedir'] . '/' . $filename;
                }

                file_put_contents( $file, $image_data );
                $wp_filetype = wp_check_filetype( $filename, null );
                $attachment = array(
                  'post_mime_type' => $wp_filetype['type'],
                  'post_title' => sanitize_file_name( $filename ),
                  'post_content' => '',
                  'post_status' => 'inherit'
                );

                $forgot_attach_id = wp_insert_attachment( $attachment, $file );
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                $attach_data = wp_generate_attachment_metadata( $forgot_attach_id, $file );
                wp_update_attachment_metadata( $forgot_attach_id, $attach_data );
            } else {
                $forgot_attach_id = sfcp_does_file_exists('forgot-password.jpg');
            }
            
            $biztech_redirect_login = get_option( 'biztech_redirect_login' );
            
            if ( ! $biztech_redirect_login ) {
                $id = wp_insert_post(
                        array(
                            'post_title'    => __( 'Portal Login' ),
                            'post_type'     => 'page',
                            'post_content'  => "[bcp-login]",
                            'post_status'   => 'publish',
                        )
                    );
                update_option( 'biztech_redirect_login',  $id );
                set_post_thumbnail( $id, $attach_id );
            }
            $biztech_redirect_signup = get_option( 'biztech_redirect_signup' );
            if ( ! $biztech_redirect_signup ) {
                $id = wp_insert_post(
                        array(
                            'post_title'    => __( 'Portal Registration' ),
                            'post_type'     => 'page',
                            'post_content'  => '[bcp-sign-up]',
                            'post_status'   => 'publish',
                        )
                    );
                update_option( 'biztech_redirect_signup',  $id );
                set_post_thumbnail( $id, $register_attach_id );
            }
            
            $biztech_redirect_profile = get_option( 'biztech_redirect_profile' );
            if ( ! $biztech_redirect_profile ) {
                $id = wp_insert_post(
                        array(
                            'post_title'    => __( 'Portal Profile' ),
                            'post_type'     => 'page',
                            'post_content'  => '[bcp-profile]',
                            'post_status'   => 'publish',
                        )
                    );
                update_option( 'biztech_redirect_profile',  $id );
            }
            
            $biztech_redirect_forgotpwd = get_option( 'biztech_redirect_forgotpwd' );
            if ( ! $biztech_redirect_forgotpwd ) {
                $id = wp_insert_post(
                        array(
                            'post_title'    => __( 'Portal Forgot Password' ),
                            'post_type'     => 'page',
                            'post_content'  => "[bcp-forgot-password]",
                            'post_status'   => 'publish',
                        )
                    );
                update_option( 'biztech_redirect_forgotpwd',  $id );
                set_post_thumbnail( $id, $forgot_attach_id );
            }

            $biztech_redirect_resetpwd = get_option( 'biztech_redirect_resetpwd' );
            if ( ! $biztech_redirect_resetpwd ) {
                $id = wp_insert_post(
                        array(
                            'post_title'    => __( 'Portal Reset Password' ),
                            'post_type'     => 'page',
                            'post_content'  => "[bcp-reset-password]",
                            'post_status'   => 'publish',
                        )
                    );
                update_option( 'biztech_redirect_resetpwd',  $id );
                set_post_thumbnail( $id, $forgot_attach_id );
            }

            $biztech_redirect_manange = get_option( 'biztech_redirect_manange' );
            if ( ! $biztech_redirect_manange ) {
                $id = wp_insert_post(
                        array(
                            'post_title'    => __( 'Portal Manage' ),
                            'post_type'     => 'page',
                            'post_content'  => '[bcp-manage-page]',
                            'post_status'   => 'publish',
                        )
                    );
                update_option( 'biztech_redirect_manange',  $id );
            }
                       
            //Added by BC on 17-jun-2016 Create table for storing module layout
            $charset_collate = $wpdb->get_charset_collate();
            $table_name = $wpdb->prefix . 'module_layout';
            $sql = "CREATE TABLE IF NOT EXISTS $table_name(
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `version` tinyint(4) NOT NULL,
                            `module_name` varchar(50) NOT NULL,
                            `layout_view` varchar(50) NOT NULL,
                            `json_data` longtext NOT NULL,
                            `all_fields` longtext NOT NULL,
                            `conditional_fields` longtext NOT NULL,
                            `date` varchar(255) DEFAULT NULL,
                            `contact_group` VARCHAR(50) NOT NULL,
                            `lang_code` VARCHAR(255) NOT NULL,
                            PRIMARY KEY ( `id` )) $charset_collate;";
            $wpdb->query($sql);
            $table_name = $wpdb->prefix . 'scp_portal_contact_info';
            $sql = "CREATE TABLE IF NOT EXISTS $table_name(
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `contact_id` varchar(50) NOT NULL,
                    `notification_permission` longtext NOT NULL,
                    `total_notification` int(11) NOT NULL,
                    `dashboard_preferences` text,
                  PRIMARY KEY ( `id` )) $charset_collate;";
            $wpdb->query($sql);
            
            //Create Table for multi language crm_language_messages
            //Since v3.2.0
            $table_name = $wpdb->prefix . 'crm_language_messages';
            $sql_lang = "CREATE TABLE IF NOT EXISTS " . $table_name . " (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `lang_code` varchar(100),
                `response_data` text NOT NULL,
                `modified_date` datetime NOT NULL,
                `created_date` datetime NOT NULL,
                PRIMARY KEY (`id`)
              ) $charset_collate";
            $wpdb->query($sql_lang);
            
            //Create Table for login attempts bcp_login_attempts
            //Since v4.3.0
            $bcp_login_attempts_table_name = $wpdb->prefix . 'bcp_login_attempts';
            $sql_login_attempts = "CREATE TABLE IF NOT EXISTS " . $bcp_login_attempts_table_name . " (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `ip_address` varchar(150) NOT NULL,
                `login_attempt` int(2),
                `bcp_user_login` varchar(22),
                `last_login_date` datetime,
                `ip_status` ENUM('active','inactive'),
                PRIMARY KEY (`id`)
              ) $charset_collate";
            $wpdb->query($sql_login_attempts);
            
            update_option('biztech_portal_calendar','1');
            if (!get_option('biztech_scp_name')) {
                update_option('biztech_scp_name', __('Customer Portal') );
            }
        }
    }

}

function _isCurl() {

    $conn_err = 1;
    if (!(version_compare(phpversion(), '5.0.0', '>='))) {
        $conn_err = __('Plugin requires minimum PHP 5.0.0 to function properly. Please upgrade PHP or deactivate Plugin');
        return $conn_err;
    }
    if (!function_exists('curl_version')) {
        $conn_err = __('Please enable PHP CURL extension to make this plugin work.');
        return $conn_err;
    }
    if (!function_exists('json_decode')) {
        $conn_err = __('Please enable PHP JSON extension to make this plugin work.');
        return $conn_err;
    }
    if ($conn_err == 1) {
        return 'yes';
    }
}

if (!function_exists('bcp_suger_connection')) {

    add_action('wp', 'bcp_suger_connection');
    function bcp_suger_connection() {

        global $sugar_crm_version, $objSCP, $bcp_framework, $lang_code;
        $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
        $check_var = _isCurl();
        if (($check_var == 'yes')) {//Added by BC on 03-jun-2016 for CURL CHECKING
            if (class_exists('SugarRestApiCall')) {
                $scp_sugar_rest_url = get_option('biztech_scp_rest_url');
                $scp_sugar_username = get_option('biztech_scp_username');
                $scp_sugar_password = scp_decrypt_string(get_option('biztech_scp_password'));
                if (isset($scp_sugar_rest_url) && !empty($scp_sugar_rest_url) && isset($scp_sugar_username) && !empty($scp_sugar_username) && isset($scp_sugar_password) && !empty($scp_sugar_password)) {
                    global $post;
                    $curent_page = isset( $post->ID ) ? $post->ID : '';
                    if (get_option('biztech_redirect_login') != NULL) {
                        $login = get_option('biztech_redirect_login');
                    } else {
                        $page = get_page_by_path('bcp-login');
                        $login = isset( $post->ID ) ? $post->ID : '';
                    }
                    if (get_option('biztech_redirect_forgotpwd') != NULL) {
                        $forgotpwd = get_option('biztech_redirect_forgotpwd');
                    } else {
                        $page = get_page_by_path('bcp-forgot-password');
                        $forgotpwd = isset( $post->ID ) ? $post->ID : '';
                    }
                    if (get_option('biztech_redirect_resetpwd') != NULL) {
                        $resetpwd = get_option('biztech_redirect_resetpwd');
                    } else {
                        $page = get_page_by_path('bcp-reset-password');
                        $resetpwd = isset( $post->ID ) ? $post->ID : '';
                    }
                    if (get_option('biztech_redirect_signup') != NULL) {
                        $signup = get_option('biztech_redirect_signup');
                    } else {
                        $dash_page = get_page_by_path('bcp-sign-up');
                        $signup = $dash_page->ID;
                    }
                    if ($login == $curent_page || $forgotpwd == $curent_page || $resetpwd == $curent_page || $signup == $curent_page) {
                        unset($_SESSION['scp_object']);
                    }
                    $objSCP = isset($_SESSION['scp_object']) ? $_SESSION['scp_object'] : "";
                    if ($objSCP == "" || ( ( ( $sugar_crm_version == 6 || $sugar_crm_version == 5 ) && $objSCP->session_id == '') ||
                        ( $sugar_crm_version == 7 && isset( $objSCP->access_token ) && $objSCP->access_token == '' ) ) ) { // if connection not established then call not made to crm

                            $objSCP = new SugarRestApiCall($scp_sugar_rest_url, $scp_sugar_username, $scp_sugar_password);
                            $_SESSION['scp_object'] = $objSCP;
                            $ismenable = $objSCP->check_enable_modules($lang_code);
                            $_SESSION['scp_admin_messages'] = ( isset( $ismenable->portal_connection_message_list ) ) ? $ismenable->portal_connection_message_list : '';
                    }
                    
                    if ($objSCP != "" && ((($sugar_crm_version == 6 || $sugar_crm_version == 5) && $objSCP->session_id != '') || ($sugar_crm_version == 7 && $objSCP->access_token != ''))) {//Updated on 15-jun-2016 for checking token
                        if (isset($ismenable->success) && $ismenable->success != 1) {
                            $error_msg = $ismenable->message;
                            $objSCP = 0;
                            setcookie('bcp_connection_error', $error_msg, time() + 3600, '/');
                            return 0;
                        }
                        elseif (isset($ismenable->fronend_framework) && $ismenable->fronend_framework != $bcp_framework) {
                            $error_msg = ( isset( $_SESSION['scp_admin_messages']->portal_disabled ) ? __( $_SESSION['scp_admin_messages']->portal_disabled ) : __('Portal is disabled. Please contact your administrator.') );
                            $objSCP = 0;
                            setcookie('bcp_connection_error', $error_msg, time() + 3600, '/');
                            return 0;
                        } else {
                            return $objSCP;
                        }
                    } else {
                        $objSCP = 0;
                        $conn_err = ( isset( $_SESSION['scp_admin_messages']->portal_disabled) ? __( $_SESSION['scp_admin_messages']->portal_disabled ) : __('Portal is disabled. Please contact your administrator.') );
                        setcookie('bcp_connection_error', $conn_err, time() + 3600, '/');
                        return 0;
                    }
                }
            } else {
                $objSCP = 0;
                $conn_err = __("Portal is disabled. Please contact your administrator.");
                setcookie('bcp_connection_error', $conn_err, time() + 3600, '/');
                return 0;
            }
        } else {//else CURL CHECKING
            $objSCP = 0;
            setcookie('bcp_connection_error', $check_var, time() + 3600, '/');
            return 0;
        }
        return false;
    }

}

add_action('init', 'bcp_suger_connection_set_data');
function bcp_suger_connection_set_data() {

    global $objSCP;
    $objSCP = isset($_SESSION['scp_object']) ? $_SESSION['scp_object'] : "";
}

if (!function_exists('bcp_check_login')) {
    add_action('wp', 'bcp_check_login', 99);

    function bcp_check_login() {
        
        if ( defined('DOING_AJAX') && DOING_AJAX ) { return; }
        
        if (!is_admin()) {
            global $post, $lang_code, $objSCP, $all_language;
            $curent_page = isset( $post->ID ) ? $post->ID : '';
            
            $login = get_option('biztech_redirect_login');
            $forgotpwd = get_option('biztech_redirect_forgotpwd');
            $signup = get_option('biztech_redirect_signup');
            $resetpwd = get_option('biztech_redirect_resetpwd');
            $profile_page = get_option('biztech_redirect_profile');
            $manage_page = get_option('biztech_redirect_manange');
            
            if (get_option('biztech_redirect_login') == NULL) {
                $page = get_page_by_path('bcp-login');
                $login = isset( $post->ID ) ? $post->ID : '';
            }
            if (get_option('biztech_redirect_forgotpwd') == NULL) {
                $page = get_page_by_path('bcp-forgot-password');
                $forgotpwd = isset( $post->ID ) ? $post->ID : '';
            }
            if (get_option('biztech_redirect_resetpwd') == NULL) {
                $page = get_page_by_path('bcp-reset-password');
                $resetpwd = isset( $post->ID ) ? $post->ID : '';
            }
            if (get_option('biztech_redirect_signup') == NULL) {
                $dash_page = get_page_by_path('bcp-sign-up');
                $signup = $dash_page->ID;
            }
            if (get_option('biztech_redirect_profile') == NULL) {
                $page = get_page_by_path('bcp-profile');
                $profile_page = isset( $page->ID ) ? $page->ID : '';
            }
            if (get_option('biztech_redirect_manange') == NULL) {
                $page = get_page_by_path('bcp-manage-page');
                $manage_page = isset( $page->ID ) ? $page->ID : '';
            }
            
            if (isset($_SESSION['scp_user_id']) != true) {
                
                $lang_code = isset($_COOKIE['scp_lang_code']) ? $_COOKIE['scp_lang_code'] : "";
                if ( ( $objSCP != NULL ) && ( $login == $curent_page || $forgotpwd == $curent_page || $resetpwd == $curent_page || $signup == $curent_page ) ) {
                    $all_language_obj = scp_get_all_language_lists();
                    $multi_lang_setting = $all_language_obj->multi_lang_setting;
                    $all_language = array();
                    $all_language_list = (isset($all_language_obj->enabled_lang)) ? $all_language_obj->enabled_lang : array();
                    $all_language_list = json_decode(json_encode($all_language_list),true);
                    if($multi_lang_setting == 1 || $multi_lang_setting == 'true'){
                        $all_language = $all_language_list;
                    }
                    $c_code_array = array_column($all_language_list, "module");
                    if(in_array($all_language_obj->default_lang, $c_code_array) && $all_language_obj->default_lang != "") {
                        if ( $lang_code == "" || !in_array($lang_code, $c_code_array) ) {
                            $lang_code = $all_language_obj->default_lang;
                        }
                    } else {
                        $lang_code = (isset($c_code_array[0]) ? $c_code_array[0] : "en_us");
                    }
                }
                
                if ($profile_page == $curent_page || $manage_page == $curent_page) {
                    $biztech_redirect_login = get_page_link(get_option('biztech_redirect_login'));
                    $redirect_url = home_url() . '/portal-login';
                    if (isset($biztech_redirect_login) && !empty($biztech_redirect_login)) {
                        $redirect_url = $biztech_redirect_login;
                    }
                    ob_clean();
                    wp_redirect($redirect_url);
                    die();
                }
            } else {
                if (!empty($_SESSION['scp_user_id'])) {
                    if ($login == $curent_page || $forgotpwd == $curent_page || $resetpwd == $curent_page || $signup == $curent_page) {
                        $biztech_redirect_dashboard_gui = get_page_link(get_option('biztech_redirect_manange'));
                        $redirect_url = home_url() . '/portal-manage-page/';
                        if (isset($biztech_redirect_dashboard_gui) && !empty($biztech_redirect_dashboard_gui)) {
                            $redirect_url = $biztech_redirect_dashboard_gui;
                        }
                        ob_clean();
                        wp_redirect($redirect_url);
                        die();
                    }
                }
            }
        }
    }

}

/**
 * Call for updating count notification of contact for v3.0
 */

if ( !function_exists('bcp_notifications_update_count') ) {
    add_action('init', 'bcp_notifications_update_count');

    function bcp_notifications_update_count(){
        global $wpdb,$sugar_crm_version, $objSCP, $lang_code;
        if (isset($_REQUEST['biztech_scp_key']) && isset($_REQUEST['contact_id'])) {
            $biztech_scp_key = $_REQUEST['biztech_scp_key'];
            $contact_id = $_REQUEST['contact_id'];
            $error_msg = "";
            $biz_scp_key = get_option('biztech_scp_key');
            if (is_multisite()) {
                $biz_scp_key = get_option('biztech_scp_key');
            }

            if ($biz_scp_key == $biztech_scp_key) {
                $noti_table = $wpdb->prefix . "scp_portal_contact_info";
                $sql = "SELECT * FROM ".$noti_table . " WHERE contact_id = '".$contact_id."'";
                $result = $wpdb->get_row($sql);
                $tot_count = 1;
                if ($result != NULL) {
                    $tot_count = $result->total_notification + 1;
                    $wpdb->update(
                            $noti_table,
                            array(
                                'contact_id' => $contact_id,
                                'total_notification' => $tot_count,	// integer (number)
                            ),
                            array( 'contact_id' => $contact_id ),
                            array(
                                    '%s',	// value1
                                    '%d'	// value2
                            ),
                            array( '%s' )
                    );
                } else {
                    $wpdb->insert($noti_table, array(
                        'contact_id' => $contact_id,
                        'total_notification' => $tot_count,
                    ));
                }
                $error_msg = __('Notification count updated successfully.');
            } else {
                $error_msg = __('biztech_scp_key not matched.');
            }
            echo $error_msg;
            exit();
        }
    }
}

/**
 * When New User created from CRM side, update profile in wordpress.
 */
if (!function_exists('bcp_check_sign_up')) {
    add_action('init', 'bcp_check_sign_up');

    function bcp_check_sign_up() {
        if (isset($_REQUEST) && !empty($_REQUEST)) {

            $biztech_scp_single_signin = get_option('biztech_scp_single_signin');
            if (isset($_REQUEST['sugar_side_portal_email']) && $_REQUEST['sugar_side_portal_email'] != '' && $biztech_scp_single_signin!='') {

                $username_c = sanitize_text_field($_REQUEST['user_login']);
                $password_c = sanitize_text_field($_REQUEST['user_pass']);
                $email = sanitize_email($_REQUEST['user_email']);
                $first_name = sanitize_text_field($_REQUEST['first_name']);
                $last_name = sanitize_text_field($_REQUEST['last_name']);
                $biztech_scp_key = sanitize_text_field($_REQUEST['biztech_scp_key']);
                $sugar_side_portal_email = (!empty($_REQUEST['sugar_side_portal_email'])) ? $_REQUEST['sugar_side_portal_email'] : 0; //Sugar side portal email

                if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) { //Added by BC on 08-aug-2015
                    $user = get_user_by('login', $username_c);
                    $user_ID = $user->ID;
                    $email_exists = email_exists($email);
                    $username_exists = username_exists($username_c);

                    $error_msg = '';
                    if ( !$username_exists && $email_exists ) {
                        $error_msg = __('Email Address already exists.');
                    } else if ( $username_exists && !$email_exists ) {
                        $error_msg = __('Username already exists.');
                    } else if ( !$username_exists && !$email_exists ) {
                        $add_data = array(
                            'user_login' => $username_c,
                            'user_pass' => $password_c,
                            'user_email' => $email,
                            'first_name' => $first_name,
                            'last_name' => $last_name,
                            'biztech_scp_key' => $biztech_scp_key
                        );
                        if (get_option('biztech_scp_key') == $biztech_scp_key) {
                            $user_id = wp_insert_user($add_data);
                        } else {
                            $error_msg = __('biztech_scp_key not matched.');
                        }
                    } else {
                        $add_data = array(
                            'ID' => $user_ID,
                            'user_login' => $username_c,
                            'user_pass' => $password_c,
                            'user_email' => $email,
                            'first_name' => $first_name,
                            'last_name' => $last_name
                        );
                        $user_id = wp_update_user($add_data);
                        if ($user_id != NULL) {
                            $error_msg = isset( $_SESSION['scp_admin_messages']->from_crm_update ) ? __( $_SESSION['scp_admin_messages']->from_crm_update ) : '';
                        } else {
                            $error_msg = isset( $_SESSION['scp_admin_messages']->from_crm_not_update ) ? __( $_SESSION['scp_admin_messages']->from_crm_not_update ) : '';
                        }
                    }
                } else {

                    $username_exists = username_exists($username_c);
                    $email_exists = email_exists($email);

                    $error_msg = '';
                    if ($username_exists && $email_exists) {
                        $error_msg = isset( $_SESSION['scp_admin_messages']->signup_exists_username_email ) ? __( $_SESSION['scp_admin_messages']->signup_exists_username_email ) : '';
                    } else if ($username_exists) {
                        $error_msg = isset( $_SESSION['scp_admin_messages']->signup_exists_username ) ? __( $_SESSION['scp_admin_messages']->signup_exists_username ) : '';
                    } else if ($email_exists) {
                        $error_msg = isset( $_SESSION['scp_admin_messages']->signup_exists_email ) ? __( $_SESSION['scp_admin_messages']->signup_exists_email ) : '';
                    } else {
                        $add_data = array(
                            'user_login' => $username_c,
                            'user_pass' => $password_c,
                            'user_email' => $email,
                            'first_name' => $first_name,
                            'last_name' => $last_name,
                            'biztech_scp_key' => $biztech_scp_key
                        );
                        if (get_option('biztech_scp_key') == $biztech_scp_key) {
                            $user_id = wp_insert_user($add_data);
                        }
                        if ($user_id != NULL) {
                            if ($sugar_side_portal_email == 1) {
                                // mail_user($username_c, $password_c, $first_name, $last_name, $email); //Added by BC on 23-sep-2015 send user mail
                            }
                            $error_msg = isset( $_SESSION['scp_admin_messages']->signup_success ) ? __( $_SESSION['scp_admin_messages']->signup_success ) : '';
                        } else {
                            $error_msg = __('You are not authenticate to register user.');
                        }
                    }
                }
                echo $error_msg;
                exit();
            }
            if (isset($_REQUEST['pass_stored_scp_key']) && $_REQUEST['pass_stored_scp_key'] != '') {
                echo get_option('biztech_scp_key');
                exit();
            }
        }
    }

}

/**
 * function to verify customer flow
 */
if (!function_exists('bcp_verify_portal_user')) {
    add_action('init', 'bcp_verify_portal_user');
    function bcp_verify_portal_user(){
        global $sugar_crm_version, $objSCP;
        if (isset($_REQUEST['scp_is_approve']) || isset($_REQUEST['scp_login'])) {
            if (isset($_SESSION['scp_otp_generated']) && isset($_SESSION['scp_otp_contact_id'])) {
                unset($_SESSION['scp_otp_generated']);
                unset($_SESSION['scp_otp_contact_id']);
            }
            $page_name = "biztech_redirect_login";
            $page = fetch_data_option($page_name);
            $redirect_url = get_permalink($page);
            wp_redirect($redirect_url);
            exit();
        }
        if (isset($_REQUEST['scp_access_token'])) {
            if ( $objSCP == NULL ) {
                bcp_suger_connection();
            }
            $token = urlencode($_REQUEST['scp_access_token']);
            $token = rawurldecode($token);
            $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "en_us";
            $verify_result = $objSCP->verifyPortalUser($token, $lang_code);
            $response = json_decode($verify_result);
            $page_name = $response->redirect_to;
            $page = get_option($page_name);
            $redirect_url = get_permalink($page);
            if (isset($response->success) && $response->success == true) {
                if ($page_name == 'biztech_redirect_resetpwd') {
                    $contact_id = $response->scp_contact_id;
                    $redirect_url .= "?scp_token=".$contact_id;
                } else {
                    if (isset($response->scp_contact_id)) {
                        $contact_id_v = $response->scp_contact_id;
                        $contact_id_s = decryptPass($contact_id_v);
                        $contact_id = json_decode($contact_id_s);
                        $module_name = "Contacts";
                        $pass_array = array();
                        $pass_array['id'] = $contact_id;
                        $pass_array['access_token_c'] = "";
                        $flag = $objSCP->set_entry($module_name, $pass_array);
                    }
                    $msg = $response->msg;
                    $_SESSION['bcp_login_suc'] = $msg;
                }
            } else {
                $msg = $response->msg;
                $_SESSION['bcp_login_error'] = $msg;
                $redirect_url .= "?error=1";
            }
            wp_redirect($redirect_url);
            exit();
        }
    }
}

if (!function_exists('sugar_crm_portal_uninstall')) {
    register_uninstall_hook(__FILE__, 'sugar_crm_portal_uninstall');

    function sugar_crm_portal_uninstall() {
        global $wpdb;
        $biztech_delete_options = array('biztech_scp_name',
            'biztech_scp_rest_url',
            'biztech_scp_username',
            'biztech_scp_password',
            'biztech_scp_case_per_page',
            'biztech_scp_tab_active',
            'biztech_scp_tab_inactive',
            'biztech_scp_upload_image',
            'biztech_scp_calendar_calls_color',
            'biztech_scp_calendar_meetings_color',
            'biztech_scp_calendar_tasks_color',
            'biztech_scp_portal_menu_title',
            'biztech_scp_custom_css',
            'biztech_scp_key',
            'biztech_scp_sugar_crm_version',
            'biztech_redirect_login',
            'biztech_redirect_signup',
            'biztech_redirect_profile',
            'biztech_redirect_forgotpwd',
            'biztech_redirect_resetpwd', //reset password page
            'biztech_redirect_manange',
            'biztech_redirect_dashboard_gui',
            'biztech_scp_theme_color',
            'biztech_scp_counter',
            'biztech_scp_recent_activity',
            'biztech_scp_top_dashboard',
            'biztech_portal_template',
            'biztech_portal_calendar',
            'biztech_scp_signup_link_enable',
            'biztech_scp_otp_enable',
            'biztech_multi_signin',
            'biztech_disable_portal',
            'biztech_scp_single_signin',
            'biztech_scp_portal_user_grp',
            'biztech_portal_user_approval',
            'biztech_scp_recaptcha_site_key',
            'biztech_scp_recaptcha_secret_key',
            'biztech_portal_visible_reCAPTCHA',
            'biztech_portal_enable_user_verification',
            'bcp_login_password_enable',
            'bcp_maximum_login_attempts',
            'bcp_lockout_effective_period',
            );

        if (is_multisite()) {
            foreach ($biztech_delete_options as  $j) {
                delete_site_option($j);
            }
            $all_subsites = get_sites();
            foreach ($all_subsites as $j) {
                switch_to_blog($j->blog_id);
                foreach ($biztech_delete_options as $j) {
                    delete_option($j);
                }
                // Delete Table
                $dropTable = $wpdb->prepare("DROP TABLE IF EXISTS " . $wpdb->prefix . "module_layout", 1);
                $wpdb->query($dropTable);
                $dropTable = $wpdb->prepare("DROP TABLE IF EXISTS " . $wpdb->prefix . "scp_portal_contact_info", 1);
                $wpdb->query($dropTable);
                //For delete table of static messages
                //since v3.2.0
                $dropTable = $wpdb->prepare("DROP TABLE IF EXISTS " . $wpdb->prefix . "crm_language_messages", 1);
                $wpdb->query($dropTable);
                
                /* Added by dharti.gajera 13-04-2022 */
                $dropTable = $wpdb->prepare("DROP TABLE IF EXISTS " . $wpdb->prefix . "bcp_login_attempts", 1);
                $wpdb->query($dropTable);
                
                // Delete pages
                $wpdb->query("DELETE FROM {$wpdb->posts} WHERE post_name IN ( 'portal-registration', 'portal-login','portal-profile','portal-forgot-password','portal-reset-password','portal-manage','portal-dashboard-gui' ) AND post_type = 'page'");
                restore_current_blog();
            }
        }
        else {
            global $wpdb;
            foreach ($biztech_delete_options as $j) {
                delete_option($j);
            }
            // Delete Table
            $dropTable = $wpdb->prepare("DROP TABLE IF EXISTS " . $wpdb->prefix . "module_layout", 1);
            $wpdb->query($dropTable);
            $dropTable = $wpdb->prepare("DROP TABLE IF EXISTS " . $wpdb->prefix . "scp_portal_contact_info", 1);
            $wpdb->query($dropTable);
            //For delete table of static messages
            //since v3.2.0
            $dropTable = $wpdb->prepare("DROP TABLE IF EXISTS " . $wpdb->prefix . "crm_language_messages", 1);
            $wpdb->query($dropTable);
            
            /* Added by dharti.gajera 13-04-2022 */
            $dropTable = $wpdb->prepare("DROP TABLE IF EXISTS " . $wpdb->prefix . "bcp_login_attempts", 1);
            $wpdb->query($dropTable);
            
            //Delete pages
            $wpdb->query("DELETE FROM {$wpdb->posts} WHERE post_name IN ( 'portal-registration', 'portal-login','portal-profile','portal-forgot-password','portal-reset-password','portal-manage','portal-dashboard-gui' ) AND post_type = 'page'");
        }
    }

}

if (!function_exists('sugar_crm_portal_deactivate')) {
    register_deactivation_hook(__FILE__, 'sugar_crm_portal_deactivate');

    function sugar_crm_portal_deactivate() {

    }

}

if (!function_exists('sugar_crm_portal_style_and_script')) {

    add_action('wp_enqueue_scripts', 'sugar_crm_portal_style_and_script', 95);
    function sugar_crm_portal_style_and_script() {

        global $post, $lang_code, $language_array;
        $profile_page = $manage_page = $dashboard_gui_page = $forgotpwd = $resetpwd = $signup = $login = '';
        $scp_custom_menu = get_option( 'scp_custom_menu' );
        $ids = array();
        if(!empty($scp_custom_menu)){
            foreach ( $scp_custom_menu as $value ) {
                if($value['type'] == 'post_type'){
                    $ids[] = $value['url'];
                }
            }
        }
        $curent_page = isset( $post->ID ) ? $post->ID : '';
        if (!empty($curent_page)) {

            if (get_option('biztech_redirect_profile') != NULL) {
                $profile_page = get_option('biztech_redirect_profile');
            }
            if (get_option('biztech_redirect_manange') != NULL) {
                $manage_page = get_option('biztech_redirect_manange');
            }
            if (get_option('biztech_redirect_dashboard_gui') != NULL) {
                $dashboard_gui_page = get_option('biztech_redirect_dashboard_gui');
            }
            if (get_option('biztech_redirect_forgotpwd') != NULL) {
                $forgotpwd = get_option('biztech_redirect_forgotpwd');
            }
            if (get_option('biztech_redirect_resetpwd') != NULL) {
                $resetpwd = get_option('biztech_redirect_resetpwd');
            }
            if (get_option('biztech_redirect_signup') != NULL) {
                $signup = get_option('biztech_redirect_signup');
            }
            if (get_option('biztech_redirect_login') != NULL) {
                $login = get_option('biztech_redirect_login');
            }
        }
        if ($curent_page == $profile_page || $curent_page == $manage_page || $curent_page == $dashboard_gui_page || $curent_page == $forgotpwd || $curent_page == $resetpwd || $curent_page == $signup || $curent_page == $login || in_array($curent_page, $ids)) {

            $language_array = scp_get_language_data($lang_code);
            
            // Make sure added new css add in function scp_standalone_remove_default_theme_css_js, so than that css will not ignore in standalone template
//            wp_enqueue_style('font-awesome.min', plugins_url('assets/css/font-awesome.min.css', __FILE__));
//            wp_enqueue_style( 'scp-bootstrap-style', plugin_dir_url( __FILE__ ) . '/assets/css/bootstrap.css' );
//            $scp_portal_template = get_option('biztech_portal_template');
//            if ($scp_portal_template) {
//                wp_enqueue_style( 'scp-style', plugin_dir_url( __FILE__ ) . '/assets/css/style.css', false, '1.0.0' );
//            } else {
//                wp_enqueue_style( 'scp-style', plugin_dir_url( __FILE__ ) . '/assets/css/fullwidth.css' );
//            }
//            wp_enqueue_style( 'bootstrap-min-style', plugin_dir_url( __FILE__ ) . '/assets/css/bootstrap.min.css' );
//            wp_enqueue_style( 'scp-custom-style', plugin_dir_url( __FILE__ ) . '/assets/css/scp-style.css' );
//            wp_enqueue_style( 'scp-responsive-style', plugin_dir_url( __FILE__ ) . '/assets/css/responsive.css' );
            wp_enqueue_style( 'scp-style', plugin_dir_url( __FILE__ ) . 'assets/scss/style.css', false, '1.0.0' );
            wp_enqueue_style( 'scp-login', plugin_dir_url( __FILE__ ) . 'assets/scss/login.css', false, '1.0.0' );
            wp_enqueue_style( 'scp-dashboard', plugin_dir_url( __FILE__ ) . 'assets/scss/dashboard.css', false, '1.0.0' );
            wp_enqueue_style( 'scp-preferences', plugin_dir_url( __FILE__ ) . 'assets/scss/preferences.css', false, '1.0.0' );
            wp_enqueue_style( 'scp-profile', plugin_dir_url( __FILE__ ) . 'assets/scss/profile.css', false, '1.0.0' );
            wp_enqueue_style( 'scp-list', plugin_dir_url( __FILE__ ) . 'assets/scss/case-list.css', false, '1.0.0' );
            wp_enqueue_style( 'scp-edit', plugin_dir_url( __FILE__ ) . 'assets/scss/case-edit.css', false, '1.0.0' );
            wp_enqueue_style( 'scp-proposals', plugin_dir_url( __FILE__ ) . 'assets/scss/add-proposals.css', false, '1.0.0' );
            wp_enqueue_style('flags', plugins_url('assets/scss/flags.css', __FILE__), false, '1.0.0');
            
            wp_enqueue_style('fullcalendar', plugins_url('assets/scss/fullcalendar.css', __FILE__));
            wp_enqueue_style('bootstrap-datetimepicker', plugins_url('assets/scss/bootstrap-datetimepicker.min.css', __FILE__));
            wp_enqueue_style('bootstrap-glyphicons', plugins_url('assets/scss/bootstrap-glyphicons.css', __FILE__));
//            wp_enqueue_style('bootstrap-multiselect', plugins_url('assets/scss/bootstrap-multiselect.css', __FILE__));
            wp_enqueue_style('bootstrap-multiselect', plugins_url('assets/scss/jquery.multiselect.css', __FILE__));

            // js
            // Make sure added new js add in function scp_standalone_remove_default_theme_css_js, so than that js will not ignore in standalone template
            if (function_exists('wp_deregister_script')) {
                wp_deregister_script('jquery');
                wp_deregister_script('jquery-migrate');
            }
            //change by default version 1.11.1 to 1.10.2 for set calendar

            wp_enqueue_script('jquery', plugins_url('assets/js/jquery.min.js', __FILE__), false, '1.10.2');
            wp_enqueue_script('bootstrap-bundle', plugins_url('assets/js/bootstrap.bundle.min.js', __FILE__), false, '1.10.2');
            wp_enqueue_script('perfect-scrollbar', plugins_url('assets/js/perfect-scrollbar.jquery.min.js', __FILE__), false, '1.10.2');
            wp_enqueue_script('fontawesome', 'https://kit.fontawesome.com/4a0efec2e7.js', false, '1.10.2');
//            wp_enqueue_script('jquery-migrate', plugins_url("assets/js/jquery-migrate.min.js", __FILE__), array('jquery'));
//            wp_enqueue_script('popper', plugins_url("assets/js/popper.min.js", __FILE__), array('jquery'));
//            wp_enqueue_script('scp-bootstrap-js', plugins_url("assets/js/bootstrap.js", __FILE__), array('jquery'));
            wp_enqueue_script('scp-global', plugins_url("assets/js/global.js", __FILE__), array('jquery'));
            
            wp_enqueue_script('moment.min', plugins_url('assets/js/moment.min.js', __FILE__), array('jquery'));
            wp_enqueue_script('fullcalendar.min', plugins_url('assets/js/fullcalendar.min.js', __FILE__), array('jquery'));
            //jquery ui
//            wp_enqueue_script('jquery-ui', plugins_url('assets/js/jquery-ui/jquery-ui-1.10.3.custom.min.js', __FILE__), false, '1.10.3');
            
//            //bootstrp,calendar js
//            wp_enqueue_script('bootstrap.min', plugins_url('assets/js/bootstrap.min.js', __FILE__), array('jquery'));
            wp_enqueue_script('jquery.cookie.min', plugins_url('assets/js/jquery.cookie.min.js', __FILE__), array('jquery'));
            wp_enqueue_script('fullcharts', plugins_url('assets/js/highcharts.js', __FILE__), false, '5.0.12');
//            wp_enqueue_script('app', plugins_url('assets/js/app.js', __FILE__), array('jquery'));
//            wp_enqueue_script('jquery.blockui.min', plugins_url('assets/js/jquery.blockui.min.js', __FILE__), array('jquery'));
//            // Date time picker
            wp_enqueue_script('bootstrap-datetimepicker', plugins_url('assets/js/bootstrap-datetimepicker.min.js', __FILE__));
//            wp_enqueue_script('bootstrap-multiselect', plugins_url('assets/js/bootstrap-multiselect.js', __FILE__));
            wp_enqueue_script('bootstrap-multiselect', plugins_url('assets/js/jquery.multiselect.js', __FILE__));
            //Validate jquery in all browsers
            wp_enqueue_script('jquery.validate', plugins_url('assets/js/jquery.validate.min.js', __FILE__), array('jquery'));
//            //////////Added by BC on 30-jul-2016/////////////
            wp_enqueue_script('scp-js', plugins_url('assets/js/scp-js.js', __FILE__), array('jquery'));
            
            wp_enqueue_script('scp-manage-page-js', plugins_url('assets/js/scp-manage-page-js.js', __FILE__), array('jquery'));
            wp_enqueue_script('printThis-js', plugins_url('assets/js/printThis.js', __FILE__), array('jquery'));
            
            
            wp_localize_script("scp-global",'my_ajax_object', array(
                'homeurl' =>home_url('/'),
            ));
            wp_localize_script("scp-js", "language_array", $language_array);
            wp_localize_script('scp-manage-page-js', 'my_ajax_object', array(
                'ajax_url' => admin_url('admin-ajax.php'),
                'language_array' => $language_array,
                'homeurl' =>home_url('/'),
            ));
        }
    }

}

if (!function_exists('bcp_js_variables')) {

    add_action('wp_head', 'bcp_js_variables');
    function bcp_js_variables() {

        $get_current_url = explode('?', $_SERVER['REQUEST_URI'], 2);
        $current_url = $get_current_url[0];
        $get_page_parameter = '';
        if (!isset($get_current_url[1])) {
            $get_current_url[1] = null;
        }
        if ($get_current_url[1]) {
            $get_page_parameter = explode('&', $get_current_url[1]);
            $get_page_parameter = $get_page_parameter[0];
        }
        if ($get_page_parameter != NULL) {
            $current_url .= "?" . $get_page_parameter;
        } else {
            $current_url .= "?scp-page=list-accounts";
        }
        ?>
        <script type="text/javascript">
            var ajaxurl = '<?php echo admin_url("admin-ajax.php"); ?>';
            var image_url = '<?php echo IMAGES_URL; ?>';
            var pathId = '<?php
        if (isset($_REQUEST['scp-page'])) {
            echo $_REQUEST['scp-page'];
        }
        ?>';
            //General function
            function form_submit(doc_id) {
                jQuery('#scp_id').val(doc_id);
                if (confirm("<?php echo ( isset($_SESSION['portal_message_list']->confirm_delete) ? __( $_SESSION['portal_message_list']->confirm_delete ) : '' ); ?>")) {
                    document.getElementById("actionform").submit();
                    return false;
                } else {
                    return false;
                }
            }

        </script>
        <style type="text/css">
        <?php
        $templatecolor = fetch_data_option('biztech_scp_theme_color');
        if ($templatecolor != '') {
            ?>
                .sidebar .nav .nav-item.active > .nav-link i, .sidebar .nav .nav-item.active > .nav-link .menu-title, .sidebar .nav .nav-item.active > .nav-link .menu-arrow,
                .sidebar .nav .nav-item:hover > .nav-link i, .sidebar .nav .nav-item:hover > .nav-link .menu-title, .sidebar .nav .nav-item:hover > .nav-link .menu-arrow,
                li.nav-item em.fas.fa-plus-circle,
                .imex-content-table .table-content tr td a:hover,
                .tabs-menu ul.nav .nav-item a.active,
                .preferences-content .preferences-tabs .common-block ul .nav-item.active .nav-link .menu-title,
                .preferences-content .preferences-tabs .common-block ul .nav-item .nav-link:hover .menu-title,
                a:hover,
                a,
                .sidebar .nav .nav-item.dcp-menu-AOS_Invoices .nav-link:hover .menu-title,
                nav ul.pagination li.active .page-link, nav ul.pagination li:hover .page-link,
                .scp-dashboard-list-view .imex-content-table .cases-heading a.see-all em,
                .scp-dashboard-list-view .imex-content-table .table-content tr td a:hover,
                h3.logo-text {
                    color: <?php echo $templatecolor; ?>;
                }
                
                .page-body-wrapper .main-panel .left-cases-block .counter-block:hover,
                .sidebar .nav .nav-item.active::after,
                .dropdown-item.active, .dropdown-item:active,
                .tabs-menu ul.nav .nav-item a.active:after,
                .preferences-content .preferences-tabs .preferences-button-group input[type="submit"],
                .styled-checkbox:hover + label:before,
                .styled-checkbox:checked + label:before,
                .imex-content-table .main-heading-table .add-more-btn:hover,
                .imex-content-table .case-heading-action .selected-case-button:hover,
                .checkbox-button .checkbox-button__input:checked + .checkbox-button__control:after,
                nav ul.pagination li.active .page-link::after, nav ul.pagination li:hover .page-link::after,
                .scp-dashboard-list-view .imex-content-table td .account-image, 
                .scp-dashboard-list-view .imex-content-table th .account-image,
                .scp-page-list-view .preferences-header .advance_filter a,
                #filter-panel .close-fliter,
                .settings-panel .input-group .btn,
                body .btn,
                .btn-primary:hover,
                .btn {
                    background: <?php echo $templatecolor; ?>;
                }
                .imex-content-table td svg:hover path, .imex-content-table th svg:hover path,
                .preferences-content .preferences-tabs .common-block ul .nav-item.active .nav-link svg path,
                .preferences-content .preferences-tabs .common-block ul .nav-item .nav-link:hover svg path,
                .sidebar .nav .nav-item.dcp-menu-AOS_Invoices .nav-link:hover svg path,
                .sidebar .nav .nav-item .nav-link:hover svg path,
                .sidebar .nav .nav-item.active > .nav-link svg path {
                    stroke: <?php echo $templatecolor; ?>;
                }
                
                .sidebar .nav .nav-item.dcp-menu-AOS_Invoices .nav-link:hover svg path:last-child {
                    fill: <?php echo $templatecolor; ?>;
                }
                
                .preferences-content .preferences-tabs .common-block ul .nav-item .nav-link:hover,
                .preferences-content .preferences-tabs .common-block ul .nav-item.active .nav-link {
                    border : 1px solid <?php echo $templatecolor; ?>;
                }
                
                .imex-content-table .main-heading-table .add-more-btn:hover,
                body .btn,
                .page-item.active .page-link,
                .file-form-group a:hover,
                .btn-primary:hover,
                .btn-primary {
                    border-color : <?php echo $templatecolor; ?>;
                }
            <?php
            }
            echo fetch_data_option('biztech_scp_custom_css');
        ?>
        </style>
        <?php
    }

}

if (!function_exists('bcp_admin_scripts')) {
    function bcp_admin_scripts() {
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('wp-color-picker');
        wp_enqueue_script('my-upload',plugins_url('/assets/js/scp-admin-js.js', __FILE__));
        wp_enqueue_style('wp-color-picker');
    }
}

if (!function_exists('bcp_admin_styles')) {
    function bcp_admin_styles() {
        wp_enqueue_style('thickbox');
    }
}

if (isset($_GET['page']) && $_GET['page'] == 'biztech-crm-portal') {
    add_action('admin_print_scripts', 'bcp_admin_scripts');
    add_action('admin_print_styles', 'bcp_admin_styles');
}

if (!function_exists('bcp_theme_custom_upload_mimes')) {
    add_filter('upload_mimes', 'bcp_theme_custom_upload_mimes');

    function bcp_theme_custom_upload_mimes($existing_mimes) {

        $allowed_mime_types = array('jpg|jpeg|jpe' => 'image/jpeg', 'gif' => 'image/gif', 'png' => 'image/png', 'bmp' => 'image/bmp', 'tif|tiff' => 'image/tiff', 'ico' => 'image/x-icon');

        return $allowed_mime_types;
    }

}

if (!function_exists('mail_user')) {
    /**
     * Mail user on registration.
     * 
     * Currently not used because all mail sent from crm.
     * 
     * @param type $username_c
     * @param type $password_c
     * @param type $first_name
     * @param type $last_name
     * @param type $email
     */
    function mail_user($username_c, $password_c, $first_name, $last_name, $email) {

        $biztech_redirect_login = get_page_link(get_option('biztech_redirect_login'));
        $url = $biztech_redirect_login;
        $admin_email = get_option('admin_email');

        //get admin username
        $user_info = get_user_by('email', $admin_email);
        $admin_username = $user_info->user_login;
        //name set to mail
        if ($first_name != '') {
            $name = $first_name . " " . $last_name;
        } else {
            $name = $last_name;
        }
        $to = $email;
        //subject
        $subject = fetch_data_option('biztech_scp_mail_subject');
        if ($subject == NULL) {
            $subject = get_bloginfo('name') . ' ' . __('Account Details');
        }
        //body
        $body = fetch_data_option('biztech_scp_mail_body');
        if ($body != NULL) {
            $body = __('Dear') . ' ' . $name . ',' . "<br> <br>" . $body . "<br> <br>" . __('Username') . ': ' . $username_c . "<br> " . __('Password') . ': ' . $password_c . "<br> <br>" . __('Log in at:') . ' ' . $url . ' ' . __('to get started');
        } else {
            $body = __('Dear') . ' ' . $name . ',' . "<br> <br>" . __("Thank you for creating an account with us. Now you can manage your different modules.") . "<br> <br>" . __('Username') . ': ' . $username_c . "<br> " . __('Password') . ': ' . $password_c . "<br> <br>" . __('Log in at:') . ' ' . $url . ' ' . __('to get started');
        }

        $crm_logo = fetch_data_option('biztech_scp_upload_image');
        if ($crm_logo != NULL) {
            $crm_logo = "<img src='" . $crm_logo . "' title='" . get_bloginfo('name') . "' alt='" . get_bloginfo('name') . "' style='border:none' width='100'>";
        } else {
            $crm_logo = get_bloginfo('name');
        }
        $body_full = "<table width='100%' cellspacing='0' cellpadding='0' border='0' style='background:#e4e4e4; padding:30px 0;'>
      <tbody>
        <tr>
          <td><center>
                <table width='800' cellspacing='0' cellpadding='0' border='0' style='font-family: arial,sans-serif; font-size:14px; border-top:4px solid #35b0bf; background:#fff; line-height: 22px;'>
                    <tr>
                        <td style='padding: 30px 40px 0;'>
                            <table width='100%' cellspacing='0' cellpadding='0' border='0' style='border-bottom:2px solid #ddd;'>
                                <tr>
                                    <td style='padding-bottom:20px;'>
                                        <a href='" . get_site_url() . "'>
                                            $crm_logo
                                        </a>
                                    </td>
                                    <td align='right' valign='top' style='padding-bottom:20px;'>
                                       <h2 style='margin:0; padding:0; color:#35b0bf; font-size:22px;'>" . get_option('biztech_scp_name') . "</h2>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                    <tr>
                        <td style='padding:30px 40px 0; color: #484848;' align='left' valign='top'>
                             <p>" . $body . "</p>
                            <p>" . __('Regards') . ", <br />
                            " . get_bloginfo('name') . "</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width='100%' cellspacing='0' cellpadding='0' border='0' style='background:#f7f7f7; padding:15px 40px;'>
                                <tr>
                                    <td align='left' valign='middle'>
                                        <a target='_blank' style='color: #35b0bf;text-decoration: none; font-size:14px;' href='" . get_site_url() . "'>" . get_site_url() . "</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center></td>
        </tr>
    </tbody>
    </table>";
        add_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
        wp_mail($to, $subject, $body_full);
        remove_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
    }

}

if (!function_exists('mail_user_forgotpwd')) {

    /**
     * Mail user to forgot password link.
     * 
     * Currently not used because all mail sent from crm.
     * 
     * @param string $username
     * @param string $password
     * @param string $url
     * @param string $email
     * @param string $redirection_blog_id
     * @return text
     */
    function mail_user_forgotpwd($username, $password, $url, $email, $redirection_blog_id) {

        switch_to_blog($redirection_blog_id);
        //for email goes to contact for password and we use that password to log in to the Portal sir comment
        $biztech_redirect_login = get_option('biztech_redirect_login');
        $url = $biztech_redirect_login;
        $admin_email = get_option('admin_email');

        //get admin username
        $user_info = get_user_by('email', $admin_email);
        $admin_username = $user_info->user_login;
        //name set to mail
        
        $to = $email;

        $crm_logo = fetch_data_option('biztech_scp_upload_image');
        if ($crm_logo != NULL) {
            $crm_logo = "<img src='" . $crm_logo . "' title='" . get_bloginfo('name') . "' alt='" . get_bloginfo('name') . "' style='border:none' width='100'>";
        } else {
            $crm_logo = get_bloginfo('name');
        }
        $body = __('Dear') . ' ' . $username . ',' . "<br> <br>" . __('Your') . " " . get_bloginfo('name') . " account details is as below : " . "<br> <br>" . 'Username: ' . $username . "<br>" . 'Password: ' . $password;
        $subject = get_bloginfo('name') . ': Password Recover';
        //sent mail
        $body_full = "<table width='100%' cellspacing='0' cellpadding='0' border='0' style='background:#e4e4e4; padding:30px 0;'>
      <tbody>
        <tr>
          <td><center>
                <table width='800' cellspacing='0' cellpadding='0' border='0' style='font-family: arial,sans-serif; font-size:14px; border-top:4px solid #35b0bf; background:#fff; line-height: 22px;'>
                    <tr>
                        <td style='padding: 30px 40px 0;'>
                            <table width='100%' cellspacing='0' cellpadding='0' border='0' style='border-bottom:2px solid #ddd;'>
                                <tr>
                                    <td style='padding-bottom:20px;'>
                                        <a href='" . get_site_url() . "'>
                                            $crm_logo
                                        </a>
                                    </td>
                                    <td align='right' valign='top' style='padding-bottom:20px;'>
                                       <h2 style='margin:0; padding:0; color:#35b0bf; font-size:22px;'>" . get_option('biztech_scp_name') . "</h2>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                    <tr>
                        <td style='padding:30px 40px 0; color: #484848;' align='left' valign='top'>
                             <p>" . $body . "</p>
                            <p>Regards, <br />
                            " . get_bloginfo('name') . "</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width='100%' cellspacing='0' cellpadding='0' border='0' style='background:#f7f7f7; padding:15px 40px;'>
                                <tr>
                                    <td align='left' valign='middle'>
                                        <a target='_blank' style='color: #35b0bf;text-decoration: none; font-size:14px;' href='" . get_site_url() . "'>" . get_bloginfo() . "</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </center></td>
        </tr>
    </tbody>
    </table>";

        add_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
        $return_var = wp_mail($to, $subject, $body_full);
        remove_filter('wp_mail_content_type', 'wpdocs_set_html_mail_content_type');
        restore_current_blog();
        return $return_var;
    }

}

if (!function_exists('get_timezone_offset')) {

    /**
     * function to get timezone offset.
     * 
     * @param type $remote_tz
     * @param type $origin_tz
     * @return boolean
     */
    function get_timezone_offset($remote_tz, $origin_tz = null) {

        if ($origin_tz === null) {
            if (!is_string($origin_tz = date_default_timezone_get())) {
                return false; // A UTC timestamp was returned -- bail out!
            }
        }
        $origin_dtz = new DateTimeZone($origin_tz);
        $remote_dtz = new DateTimeZone($remote_tz);
        $origin_dt = new DateTime("now", $origin_dtz);
        $remote_dt = new DateTime("now", $remote_dtz);
        $offset = $origin_dtz->getOffset($origin_dt) - $remote_dtz->getOffset($remote_dt);
        return $offset;
    }

}

if (!function_exists('do_output_buffer')) {
    add_action('init', 'do_output_buffer');

    function do_output_buffer() {
        ob_start();
    }

}

if (!function_exists('portal_logut_on_wplogout')) {
    add_action('wp_logout', 'portal_logut_on_wplogout');

    /**
     * function used for singlesingin logout from portal and wordpress site.
     */
    function portal_logut_on_wplogout() {
        
        if (get_option('biztech_scp_single_signin') != '' && is_user_logged_in() && !current_user_can('manage_options')) {//Added by BC on 02-aug-2016
            unset($_SESSION['scp_user_id']);
            unset($_SESSION['scp_user_account_name']);
            unset($_SESSION['module_array']);
            unset($_SESSION['user_timezone']);
            unset($_SESSION['assigned_user_list']);
            unset($_SESSION['user_date_format']);
            unset($_SESSION['user_time_format']);
            unset($_SESSION['module_array_without_s']);
            unset($_SESSION['Currencies']);
            unset($_SESSION['contact_group']);
            unset($_SESSION['contact_group_id']);
            unset($_SESSION['scp_knowledgebase_categories']);
            unset($_SESSION['scp_custom_relationships']);
            unset($_SESSION['scp_user_group_type']);
            unset($_SESSION['scp_account_id']);
            // $redirect_url = get_site_url(); // To set home page redirection after logout
            $redirect_url = explode('?', $_SERVER['REQUEST_URI'], 2);
            $redirect_url = $redirect_url[0];

            wp_redirect($redirect_url);
            exit;
        }
    }

}

if (!function_exists('wpdocs_set_html_mail_content_type')) {

    function wpdocs_set_html_mail_content_type() {

        return 'text/html';
    }

}

if (is_multisite()) {

    ///// Start User redirection url selection option /////
    add_action('show_user_profile', 'user_redirection_option_show');
    add_action('edit_user_profile', 'user_redirection_option_show');
    if (!function_exists('user_redirection_option_show')) {

        function user_redirection_option_show($user) {
            if (!in_array('administrator', wp_get_current_user()->roles)) {   //if user is not administrator than return null.
                return;
            }
            global $wpdb;
            $blogids = $wpdb->get_results("SELECT blog_id,spam,deleted,archived FROM {$wpdb->blogs} WHERE site_id = '{$wpdb->siteid}'");
            $redirection = get_the_author_meta('biztech_user_redirection_url', $user->ID);
            ?>

            <h3>Redirect User</h3>

            <table class="form-table">
                <tr>
                    <th><label><?php _e('Website Name'); ?></label></th>
                    <td>
                        <select name="biztech_redirection_url">
                            <option value="0"><?php _e('-- Select Any One --'); ?></option>
                            <?php
                            foreach ($blogids as $blog) {
                                switch_to_blog($blog->blog_id);
                                ?>
                                <option value="<?php echo 'blog_id_' . $blog->blog_id; ?>" <?php selected($redirection, 'blog_id_' . $blog->blog_id); ?>><?php
                                    echo get_bloginfo();
                                    if (isset($blog->span) && $blog->span == 1)
                                        { echo ' ' . __('(Spam)'); }
                                    if (isset($blog->deleted) && $blog->deleted == 1)
                                        { echo ' ' . __('(Deactivate)'); }
                                    if (isset($blog->archived) && $blog->archived == 1)
                                        { echo ' ' . __('(Archived)'); }
                                    ?></option>
                                <?php
                                restore_current_blog();
                                }
                                ?>
                        </select>
                        <span class="description"><?php _e('This is only for portal access'); ?></span>
                    </td>
                </tr>
            </table>
            <?php
        }

    }

    add_action('personal_options_update', 'user_redirect_option_save');
    add_action('edit_user_profile_update', 'user_redirect_option_save');
    if (!function_exists('user_redirect_option_save')) {

        function user_redirect_option_save($user_id) {
            if (!current_user_can('edit_user', $user_id)) {
                return false;
            }
            update_user_meta($user_id, 'biztech_user_redirection_url', $_POST['biztech_redirection_url']);
        }

    }
    ///// End User redirection url selection option /////
    ///// Start Template Redirection /////
    if (!is_admin()) {    //if we are not at admin page
        add_action('wp', 'biztech_template_change', 100);
        if (!function_exists('biztech_template_change')) {

            function biztech_template_change() {
                global $post;
                $all_pages = array('biztech_redirect_login', 'biztech_redirect_signup', 'biztech_redirect_profile', 'biztech_redirect_forgotpwd', 'biztech_redirect_resetpwd', 'biztech_redirect_manange', 'biztech_redirect_dashboard_gui');
                if (isset($_SESSION['scp_use_blog_url']) && $_SESSION['scp_use_blog_url'] != NULL) {
                    $currentblogid = get_current_blog_id();
                    if (($_SESSION['scp_use_blog_id'] != $currentblogid) && (get_option('biztech_multi_signin') != 'multi-signin-enable')) { //if user try to access another site after login in single site, redirect him
                        foreach ($all_pages as $j) {  //check all default page's id
                            if ($post->ID == get_option($j)) {    //if default page's id match to current page id than redirect it
                                $redirectblogdata = get_blog_details($_SESSION['scp_use_blog_id']);
                                if ($redirectblogdata->archived == '1' || $redirectblogdata->spam == '1' || $redirectblogdata->deleted == '1') { //if site is archived or spam or deleted(deactivated)
                                    sugar_crm_portal_logout('true');
                                }
                                wp_redirect($_SESSION['scp_use_blog_url']);
                                exit;
                            }
                        }
                    }
                }
                foreach ($all_pages as  $j) {
                    if (!isset($_SESSION['scp_user_id']) || $_SESSION['scp_user_id'] == NULL) {
                        if ($j == 'biztech_redirect_profile' || $j == 'biztech_redirect_manange' || $j == 'biztech_redirect_dashboard_gui') {
                            continue;
                        }
                    }
                    $all_bitech_pages[] = get_option($j);
                }
                
                if ($post != null && in_array($post->ID, $all_bitech_pages)) {
                    //to get page id
                    $current_template = get_post_meta($post->ID, '_wp_page_template', TRUE); //get current template for current page
                    if ($current_template == NULL) {   //if template not set
                        $current_template = get_option('biztech_portal_template');  //get network template value
                    }

                    if ($current_template == '0') {
                        load_template(plugin_dir_path(__FILE__) . 'templates/page-crm-fullwidth.php');    //replace template as per current template value
                        exit;
                    }
                    else if ($current_template == '1') {
                        load_template(plugin_dir_path(__FILE__) . 'templates/page-crm-standalone.php');
                        exit;
                    }
                }
            }

        }
    }
    ///// End Template Redirection /////

    /**
     * confirmation msg before delete,spam or archive site
     */
    add_action('wpmuadminedit', 'confirmationmsg_before');    
    if (!function_exists('confirmationmsg_before')) {

        function confirmationmsg_before() {
            $screen = get_current_screen();
            if ($screen->id == 'sites-network' && ($_GET['action2'] == 'deactivateblog' || $_GET['action2'] == 'archiveblog' || $_GET['action2'] == 'spamblog' || $_GET['action2'] == 'deleteblog')) {
                biztech_admin_notice__error();
            }
        }

    }
    if (!function_exists('biztech_admin_notice__error')) {

        function biztech_admin_notice__error() {
            $class = 'notice notice-error';
            $message = __('There are some users in this site, they will lost access of Portal. Please redirect them first.');
            printf('<div class="%1$s"><p>%2$s</p></div>', $class, $message);
        }

    }
}

if (!function_exists('fetch_data_option')) {

    /**
     * Function to fetch option based on singlesite or multisite.
     * 
     * @param string $optionname
     * @return string
     */
    function fetch_data_option($optionname) {
        if (is_multisite()) {
            $optionvalue = get_option($optionname) ? get_option($optionname) : get_site_option($optionname);
        } else {
            $optionvalue = get_option($optionname);
        }
        return $optionvalue;
    }

}

if (!function_exists('calculate_time_call')) {

    /**
     * Function that calculate time.
     * 
     * @return string
     */
    function calculate_time_call($id = "") {
        $dummy_html = '<script>'
                . 'jQuery("document").ready(function(){';
                if (!$id) {
                    $dummy_html .= 'jQuery("#duration_hours").val("0");';
                    $dummy_html .= 'jQuery("#duration_minutes").val("15");';
                }
        $dummy_html .= '});'
                . 'jQuery("#duration_hours").attr("disabled","disabled");'
                . 'jQuery("#duration_hours").attr("style","background: #eee");'
                . 'jQuery("#duration_minutes").attr("disabled","disabled");'
                . 'jQuery("#duration_minutes").attr("style","background: #eee");'
                . 'jQuery("#date_end").change(function(){'
                . 'if (jQuery("#date_start").val() != "") {'
                . 'get_duration();'
                . '}'
                . '});'
                . 'jQuery("#date_start").change(function(){'
                . 'if (jQuery("#date_end").val() != "") {'
                . 'get_duration();'
                . '} else{'
                . 'jQuery("#duration_hours").val("0");'
                . 'jQuery("#duration_minutes").val("15");'
                . '} '
                . '});'
                . 'function get_duration(){'
                . 'var start_date = new Date(jQuery("#date_start").val());'
                . 'var end_date = new Date(jQuery("#date_end").val());'
                . 'var timeDiff = Math.abs(end_date.getTime() - start_date.getTime());'
                . 'd = Number(timeDiff / 1000);'
                . 'var h = Math.floor(d / 3600);'
                . 'var m = Math.floor(d % 3600 / 60);'
                . 'var m = ((m == 0) ? "00" : ((m < 10) ? ("0"+m) : m));'
                . 'h = isNaN(h) ? "0" : (h);'
                . 'm = isNaN(m) ? "15" : (m);'
                . 'jQuery("#duration_hours").val(h);'
                . 'jQuery("#duration_minutes").val(m);'
                . '}'
                . '</script>';
        return $dummy_html;
    }

}

if (!function_exists('scp_get_notes_in_case_after_add')) {
    add_action('wp_ajax_get_notes_in_case_after_add', 'scp_get_notes_in_case_after_add');
    add_action('wp_ajax_nopriv_get_notes_in_case_after_add', 'scp_get_notes_in_case_after_add');

    function scp_get_notes_in_case_after_add() {
        echo get_notes_in_case($_REQUEST['id'], $_REQUEST['parent_module']);    //if request from sugar6 check sugar6-view-page otherwise sugar7-view-page
        wp_die();
    }

}

if (!function_exists('get_notes_in_case')) {

    /**
     * Function to get notes in case detail.
     * 
     * This function is not used currently because from v3.0.0 subpanel is used.
     * 
     * @global object $objSCP
     * @global int $sugar_crm_version
     * @param string $id
     * @param string $module_name
     * @return string
     */
    function get_notes_in_case($id, $module_name) {
        global $objSCP, $sugar_crm_version;
        $html = '';
        if ($sugar_crm_version == 5 || $sugar_crm_version == 6) {
            $select_fields_cases = array('id', 'name', 'description', 'filename');
            $getCurrentModuleNotes = $objSCP->get_relationships($module_name, $id, 'notes', $select_fields_cases, '', 'date_entered desc');

            if ($getCurrentModuleNotes->entry_list != NULL) {//Added by BC on 22-sep-2015
                $html .= "<ul class='scp-data-scroll'>";
                $cntnotes = 0;

                $countNotes = 0;
                foreach ($getCurrentModuleNotes->entry_list as $setCurrentModuleNotesObj) {
                    $countNotes++;
                }
                $countNotes = $countNotes - 1;

                foreach ($getCurrentModuleNotes->entry_list as $setCurrentModuleNotesObj) {
                    $setCurrentModuleNotes = $setCurrentModuleNotesObj->name_value_list;
                    if ($countNotes == $cntnotes) {
                        $last = 'last';
                    }
                    $desc = nl2br($setCurrentModuleNotes->description->value);
                    if (strlen($desc) > 100) {
                        $first_half = substr($desc, 0, 100);
                        $remain_half = substr($desc, 101, strlen($desc));
                        $desc = '<span class="quote ellipses-quote">' . $first_half . '
                                    <span class="moreellipses" style="display: inline;">...&nbsp;</span>
                                    <span class="morecontent">
                                        <span style="display: none;">' . $remain_half . '</span>&nbsp;&nbsp;
                                        <a href="javascript:void(0);" onclick="read_more_less(this)" class="readmorelink scp-Notes-font" title="Read more" style="">Read more</a>
                                    </span>
                                </span>
                                <input type="hidden" value="' . __('Read More') . '" class="bcp_crm_read_more_text" />
                                <input type="hidden" value="' . __('Read Less') . '" class="bcp_crm_read_less_text" />';
                    }
                    $html .= "<li class='" . $last . "'>
                                        <span class='name'><a href='#data/Notes/detail/" . $setCurrentModuleNotes->id->value . "'>" . $setCurrentModuleNotes->name->value . "</a></span>
                                        <span class='description'>" . $desc . "</span>";
                    if ($setCurrentModuleNotes->filename->value != NULL) {
                        $dwld_id = $setCurrentModuleNotes->id->value;
                        $dwload_icn = "<em class='fa fa-download' aria-hidden='true'></em> " . __('Download');
                        $cls_icn = "general-link-btn scp-download-btn scp-Notes-font";
                        $html .= "<a href='javascript:void(0);' onclick='form_submit_note_document(\"$dwld_id\",\"filename\");' class='$cls_icn'> $dwload_icn </a>";
                    }
                    $html .= "</li>";
                    $cntnotes++;
                    $last = '';
                }

                $html .= "</ul>";
            }
            else {
                $html .= "<strong>" . __('No Record(s) Found.') . "</strong>";
            }
        }
        else if ($sugar_crm_version == 7) {
            $select_fields_cases = "id,name,description,filename";
            $getCurrentModuleNotes = $objSCP->getRelationship($module_name, $id, 'notes', $select_fields_cases, array(), '', '', 'date_entered:DESC');
            if ($getCurrentModuleNotes->records != NULL) {//Added by BC on 22-sep-2015
                $html .= "<ul class='scp-data-scroll'>";
                $cntnotes = 0;
                $countNotes = 0;
                foreach ($getCurrentModuleNotes->records as $setCurrentModuleNotesObj) {
                    $countNotes++;
                }
                $countNotes = $countNotes - 1;

                foreach ($getCurrentModuleNotes->records as $setCurrentModuleNotesObj) {
                    $setCurrentModuleNotes = $setCurrentModuleNotesObj;
                    if ($countNotes == $cntnotes) {
                        $last = 'last';
                    }
                    else {
                        $last = '';
                    }
                    $desc = nl2br($setCurrentModuleNotes->description);
                    if (strlen($desc) > 100) {
                        $first_half = substr($desc, 0, 100);
                        $remain_half = substr($desc, 101, strlen($desc));
                        $desc = '<span class="quote ellipses-quote">' . $first_half . '
                                    <span class="moreellipses" style="display: inline;">...&nbsp;</span>
                                    <span class="morecontent">
                                        <span style="display: none;">' . $remain_half . '</span>&nbsp;&nbsp;
                                        <a href="javascript:void(0);" onclick="read_more_less(this)" class="readmorelink scp-Notes-font" title="Read more" style="">Read more</a>
                                    </span>
                                </span>
                                <input type="hidden" value="' . __('Read More') . '" class="bcp_crm_read_more_text" />
                                <input type="hidden" value="' . __('Read Less') . '" class="bcp_crm_read_less_text" />';
                    }
                    $html .= "<li class='" . $last . "'>
                                        <span class='name'><a href='#data/Notes/detail/$setCurrentModuleNotes->id'>" . $setCurrentModuleNotes->name . "</a></span>
                                        <span class='description'>" . $desc . "</span>";
                    if ($setCurrentModuleNotes->filename != NULL) {
                        $dwld_id = $setCurrentModuleNotes->id;
                        $dwload_icn = "<em class='fa fa-download' aria-hidden='true'></em> " . __('Download');
                        $cls_icn = "general-link-btn scp-download-btn scp-Notes-font";
                        $html .= "<a href='javascript:void(0);' onclick='form_submit_note_document(\"$dwld_id\",\"filename\");' class='$cls_icn'> $dwload_icn </a>";
                    }
                    $html .= "</li>";
                    $cntnotes++;
                    $last = '';
                }

                $html .= "</ul>";
            }
            else {
                $html .= "<strong>" . __('No Record(s) Found.') . "</strong>";
            }
        }

        return $html;
    }

}


if (!function_exists('scp_dashboard_page')) {

    add_action('wp_ajax_scp_dashboard_page', 'scp_dashboard_page_callback');
    add_action('wp_ajax_nopriv_scp_dashboard_page', 'scp_dashboard_page_callback');

    /**
     * Function of ajax call for dashboard.
     * 
     * @global int $objSCP
     * @global type $sugar_crm_version
     */
    function scp_dashboard_page_callback() {

        global $objSCP, $sugar_crm_version, $lang_code, $language_array;
        if ($sugar_crm_version == 7) {
            $sess_id = $objSCP->access_token;
        }
        if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
            $sess_id = $objSCP->session_id;
        }
        if ($sess_id != '') {
            $lang_code = (isset($_COOKIE['scp_lang_code'])) ? $_COOKIE['scp_lang_code'] : "";
            $language_array = scp_get_language_data($lang_code);
            echo do_shortcode('[bcp-dashboard-gui]');
            wp_die();
        }
        else {
            $objSCP = 0;
            $conn_err = $language_array['msg_portal_is_disabled_contact_admin'];
            $_SESSION['bcp_connection_error'] = $conn_err;
            echo "-1";
            wp_die();
        }
    }

}

if (!function_exists('scp_full_width_menu_part')) {
    
    /**
     * Function that display portal sidebar menu part.
     * 
     * @param string $biztech_scp_name
     * @param string $countAcc
     */
    function scp_full_width_menu_part( $biztech_scp_name, $countAcc, $template ) {

        global $lang_code, $language_array;
        $dashboard_key = 'Dashboard';
        $dashboard = $language_array['lbl_dashboard'];
        $calendar_key = 'Calendar';
        $calendar = $language_array['lbl_calendar'];
        $modules = $_SESSION['module_array'];
        $biztech_redirect_manage_page = get_page_link(get_option('biztech_redirect_manange'));
        if ($biztech_redirect_manage_page != NULL) {
            $manage_page_url = $biztech_redirect_manage_page;
        }
        else {
            $manage_page_url = home_url() . "/portal-manage-page/";
        }
        $module_action_array = $_SESSION['module_action_array'];
        
        
        $cal_checked = (isset($_SESSION['scp_additional_setting']->calender) ? $_SESSION['scp_additional_setting']->calender : 0);
        ?>
            <header class="entry-header entry-wrapper">
                <div class="container">
                    <div class="top-bar-alt">
                        <a href='javascript:void(0)' class="">
                            <h1 class="entry-title scp-fullwidth-menu scp-menu-modules">
                                <?php _e($biztech_scp_name); ?>
                            </h1>
                            
                        </a>
                        <div class="scp-leftpanel">
                            <ul class="scp-sidemenu scp-open-modules-menu">
                                <li class="dcp-menu-dashboard">
                                    <a href="<?php echo $manage_page_url; ?>#data/Dashboard/" title="<?php echo $dashboard; ?>">
                                        <span class="menu-icon fa fa-tachometer"></span>
                                        <span class="menu-text"><?php echo $dashboard; ?></span>
                                    </a>
                                </li>
                                <?php
                                $mlist = array();
                                $scp_custom_menu = get_option("scp_custom_menu");
                                foreach ($modules as $key => $values) {
                                    $url = $manage_page_url.'#data/'.$key.'/list/';
                                    foreach($scp_custom_menu as $value){
                                        $name = str_replace(" ", "_", $value['name']);
                                        if($key == $name) {
                                            if($value['type'] == 'post_type') {
                                                $dcp_add_class = ($post->ID == $value['url']) ? "active" : "";
                                                $url = get_permalink($value['url']);
                                            } else {
                                                $url = $value['url'];
                                            }
                                        }
                                    }
                                    ?>
                                    <li class="dcp-menu-<?php echo $key; ?>">
                                        <a href="<?php echo $url; ?>" title="<?php echo $values; ?>">
                                            <span class="scp-sidebar-icon menu-icon fa fa-<?php echo $key; ?>"></span>
                                            <span class="menu-text"><?php echo $values; ?></span>
                                        </a>
                                    </li>
                                    <?php
                                    $mlist[] = $key;
                                }
                                if ( ( (in_array('Calls', $mlist)) || in_array('Meetings', $mlist) || in_array('Tasks', $mlist) ) && $cal_checked ) {
                                    ?>
                                    <li class="dcp-menu-calendar">
                                        <a href="<?php echo $manage_page_url; ?>#data/<?php echo $calendar_key; ?>/list/" title="<?php echo $calendar; ?>">
                                            <span class="menu-icon fa fa-calendar"></span>
                                            <span class="menu-text"><?php echo $calendar; ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                            </ul>
                        </div>
                        <div class="top-right-menu">
                            <?php scp_standalone_header_part("tachometer"); ?>
                        </div>
                    </div>
                </div>
            </header>
        <?php
    }
}

if (!function_exists('scp_standalone_sidebar_menu_part')) {
    
    /**
     * Function that display portal sidebar menu part.
     * 
     * @param string $biztech_scp_name
     * @param string $countAcc
     */
    
    function scp_standalone_sidebar_menu_part( $biztech_scp_name, $countAcc, $template ) {

        global $lang_code, $language_array,$post, $module_icon_content;
        $dashboard_key = 'Dashboard';
        $dashboard = $language_array['lbl_dashboard'];
        $calendar_key = 'Calendar';
        $calendar = $language_array['lbl_calendar'];
        $module_array = $_SESSION['module_array'];
        $modules = $_SESSION['module_filter_array'];
        $biztech_redirect_manage_page = get_page_link(get_option('biztech_redirect_manange'));
        if ($biztech_redirect_manage_page != NULL) {
            $manage_page_url = $biztech_redirect_manage_page;
        } else {
            $manage_page_url = home_url() . "/portal-manage-page/";
        }
        $module_action_array = $_SESSION['module_action_array'];
        //hide title and heading in subpanel for v3.0
        $case_deflection = 0;
        //Check if solution categories are blank or not.
        if (isset($_SESSION['scp_knowledgebase_categories']) && isset($_SESSION['scp_additional_setting']) && isset($_SESSION['scp_additional_setting']->case_deflection) && $_SESSION['scp_additional_setting']->case_deflection) {
            $case_deflection = 1;
        }
        $cal_checked = (isset($_SESSION['scp_additional_setting']->calender) ? $_SESSION['scp_additional_setting']->calender : 0);
        ?>
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <div class="nav left-menu-panel">

                    <ul>
                        <li class="dcp-menu-dashboard nav-item">
                            <a href="<?php echo $manage_page_url; ?>#data/Dashboard/" title="<?php echo $dashboard; ?>" class="nav-link">
                                <div class="menu-image">
                                    <?php echo $module_icon_content['dashboard']; ?>
                                </div>
                                <span class="menu-title"><?php echo $dashboard; ?></span>
                            </a>
                        </li>
                    </ul>
                    <?php 
                        $es_modules_label = get_option("es_modules_label");
                        $scp_custom_menu = get_option("scp_custom_menu");
                        $modules_datas = array_values($modules);
                        $mlist = array_keys($modules);
                        
                        $j = 0;
                        foreach($es_modules_label as $key => $value){
                            $separator = substr_compare($key, 'separator',0,9);
                            if($j == 0) {
                            ?>
                            <ul>
                            <?php
                                $j++;
                            }
                            $mod_val = $value;
                            if($separator != 0 && in_array($value, $modules_datas)){
                                $key = array_search($value, $modules);
                                $url = $manage_page_url.'#data/'.$key.'/list/';
                                $add_url = $manage_page_url.'#data/'.$key.'/edit/';
                                if ($key == "Cases" && $case_deflection) {
                                    $add_url = $manage_page_url.'#data/Solutions/Search/';
                                }
                                $dcp_add_class = "";
                                $target = '';
                                if(!empty($scp_custom_menu)){
                                    foreach($scp_custom_menu as $val){
                                        $name = str_replace(" ", "_", $val['name']);
                                        if($key == $name) {
                                            if($val['type'] == 'post_type') {
                                                $dcp_add_class = ($post->ID == $val['url']) ? "active" : "";
                                                $url = get_permalink($val['url']);
                                            } else {
                                                $url = $val['url'];
                                            }
                                            $target = 'target="'.$val['target'].'"';
                                            $add_url = '';
                                            $mod_val = $key;
                                            break;
                                        }
                                    }
                                }
                            ?>
                                <li class="dcp-menu-<?php echo $key; ?> nav-item">
                                    <a href="<?php echo $url; ?>" title="<?php echo $value; ?>" class="nav-link" <?php echo $target; ?>>
                                        <div class="menu-image">
                                            <?php if(isset($module_icon_content[strtolower($key)])){
                                                echo $module_icon_content[strtolower($key)];
                                            } else {
                                                echo $module_icon_content['default'];
                                            }
                                            ?>
                                        </div>
                                        <span class="menu-title"><?php echo $value; ?></span>
                                    </a>
                                    <?php if (isset($module_action_array[$key]['create']) && $module_action_array[$key]['create'] == 'true' && $key != 'Accounts') { ?>
                                    <a class="case-add" href="<?php echo $add_url; ?>" title="<?php echo $language_array['lbl_add']; ?>">
                                        <em class="fas fa-plus-circle" aria-hidden="true"></em>
                                    </a>
                                    <?php } ?>
                                </li>
                            <?php
                            } else { 
                                $j = 0;
                            }
                            if((array_key_last($es_modules_label) == $mod_val && $j != 0) || $j == 0){
                            ?>
                                </ul>
                            <?php
                            }
                        }
                        if ( ( (in_array('Calls', $mlist)) || in_array('Meetings', $mlist) || in_array('Tasks', $mlist) ) && $cal_checked ) {
                        ?>
                            <ul>
                                <li class="dcp-menu-calendar nav-item">
                                    <a href="<?php echo $manage_page_url; ?>#data/<?php echo $calendar_key; ?>/list/" title="<?php echo $calendar; ?>" class="nav-link">
                                        <div class="menu-image">
                                            <?php echo $module_icon_content[strtolower($calendar_key)]; ?>
                                        </div>
                                        <span class="menu-title"><?php echo $calendar; ?></span>
                                    </a>
                                </li>
                            </ul>
                        <?php } ?>
                </div>
            </nav>
        <?php
    }
}

if (!function_exists('scp_standalone_header_part')) {

    /**
     * Function that display portal header part in standalone.
     * 
     * @global object $wpdb
     * @param string $redirect_part
     * @param string $redirect_url
     */
    function scp_standalone_header_part( $redirct_icon ) {
        global $wpdb, $lang_code, $language_array, $objSCP, $module_icon_content;
        $pagetemplate = get_post_meta(get_the_ID(), '_wp_page_template', true);
        if (is_multisite() && $pagetemplate == NULL) {  //if this is multisite and subsite not set any template than catch from network
            $pagetemplate = (get_option('biztech_portal_template') == 0) ? 'page-crm-fullwidth.php' : 'page-crm-standalone.php';
        }
        $template = str_replace('.php', "", $pagetemplate);
        if (!isset($_SESSION['scp_user_id'])) {
            $biztech_redirect_login = get_page_link(get_option('biztech_redirect_login'));
            if ($biztech_redirect_login != NULL) {
                $redirect_url_login = $biztech_redirect_login;
            } else {
                $redirect_url_login = home_url() . "/portal-login/";
            }
            wp_redirect($redirect_url_login);
            exit();
        }
        $biztech_redirect_profile = get_page_link(get_option('biztech_redirect_profile'));
        if ($biztech_redirect_profile != NULL) {
            $redirect_url = $biztech_redirect_profile;
        } else {
            $redirect_url = home_url() . "/portal-profile/";
        }
        $contact_id = $_SESSION['scp_user_id'];
        
        
        $limit = fetch_data_option('biztech_scp_case_per_page');
        $order_by = 'date';
        $order = '';
        
        $scp_key_modules = array_keys($_SESSION['module_array']);
        
        $sql = "SELECT * FROM ".$wpdb->prefix . "scp_portal_contact_info WHERE contact_id = '".$contact_id."'";
        $noti_rocord = $wpdb->get_row($sql);
        $noti_count = 0;
        if ($noti_rocord != NULL) {
            $noti_count = $noti_rocord->total_notification;
        }
        
        $notification_result = $objSCP->get_notification_list('0',$limit, '', array(), $scp_key_modules, $lang_code);
        
        $notyCount = array();
        if(!empty($notification_result->notification_records)) {
            $notificationlist = $notification_result->notification_records;
            foreach ($notificationlist as $notification) {
                if($notification->notification_status == 'unread') {
                    $notyCount[] = $notification;
                }
            }
        }
        $scp_modules = $_SESSION['module_array'];

        $modules = $_SESSION['module_array'];
        if (get_page_link(get_option('biztech_redirect_manange')) != NULL) {
            $redirect_manage_url = get_page_link(get_option('biztech_redirect_manange'));
        } else {
            $redirect_manage_url = home_url() . "/portal-manage-page/";
        }
        $biztech_scp_name = fetch_data_option('biztech_scp_name');
        $portal_name = ($biztech_scp_name != NULL) ? $biztech_scp_name : "All Modules";
        
        $biztech_upload_image = fetch_data_option('biztech_scp_upload_image');
        if($biztech_upload_image != NULL){
            $sfcp_logo_url = wp_get_attachment_image_src( $biztech_upload_image, 'full' );
            $biztech_upload_image = '<div class="d-block text-center"><img src="' . $sfcp_logo_url[0] . '" alt="'.$biztech_scp_name.'" /></div>';
            $sfcp_logo_url[0];
        }
        
        ?>
        <!-- header start -->
        <header class="topbar" >
            <nav class="navbar top-navbar navbar-expand-md">
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo $redirect_manage_url; ?>#data/Dashboard">
                        <h3 class="logo-text m-0">
                            <?php if($biztech_upload_image != NULL){ ?>
                                <img src="<?php echo $sfcp_logo_url[0]; ?>" alt="<?php echo $portal_name; ?>" class="light-logo" />
                            <?php } else { ?>
                                <?php echo $portal_name; ?>
                            <?php } ?>
                        </h3>
                    </a>
                    <a class="nav-toggler  d-block d-md-none" href="javascript:void(0)">
                        <em class="ti-menu ti-close"></em>
                    </a>
                </div>
                <!-- Topbar Search -->
                <!-- Nav Global Search -->
                <div class="navbar-collapse collapse" id="navbarSupportedContent" >
                    <!-- ul start -->
                    <ul class="navbar-nav float-start me-auto">
                        <li class="nav-item  sidebartoggler-li">
                            <a class="nav-link sidebartoggler" href="javascript:void(0)">
                                <img src="<?php echo IMAGES_URL;  ?>sidebartoggler.svg" alt="sidebartoggler">
                            </a>
                        </li>
                        <li class="nav-item dropdown all-module d-none d-lg-block">
                            <a class="nav-link dropdown-toggle" href="JavaScript:void(0);" id="navbarDropdown" role="button" data-bs-toggle="dropdown"
                               aria-expanded="false">
                                <span><?php echo $language_array['lbl_all_modules']; ?>
                                    <em class="fas fa-chevron-down"></em>
                                </span>
                            </a>
                            
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown" id="select_module_option">
                                <li>
                                    <input type="hidden" name="select_val" value="0"/>
                                    <a href="Javascript:void(0);" class="dropdown-item"><?php echo $language_array['lbl_all_modules']; ?></a>
                                </li>
                                <?php
                                reset($modules);
                                foreach ($modules as $key  => $value) {
                                ?>
                                <li>
                                    <input type="hidden" name="select_val" value="<?php echo $key; ?>"/>
                                    <a href="Javascript:void(0);" class="sfcp_global_module_options dropdown-item" data-module="<?php echo $key; ?>"><?php echo $value; ?></a>
                                </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <!-- Search start -->
                        <li class="nav-item search-box d-none d-lg-block">
                            <div id="header-1" class="header d-lg-none">
                                <nav class="header-nav">
                                    <div class="search-button">
                                        <a href="Javascript:void(0);" class="search-toggle" data-selector="#header-1"></a>
                                    </div>
                                    
                                    <input type="text" id="g_search_name" class="text search-input" placeholder="<?php echo $language_array['lbl_search']; ?>" >
                                    
                                </nav>
                            </div>
                            <div class="input-group d-none d-lg-block">
                                <div class="d-flex">
                                    <div class="search-input">
                                        <?php echo $module_icon_content['search']; ?>
                                        <input type="search" id="g_search_name" class="form-control rounded" placeholder="<?php echo $language_array['lbl_search']; ?>" aria-label="<?php echo $language_array['lbl_search']; ?>" aria-describedby="search-addon">
                                    </div>
                                    <a href="JavaScript:void(0);" id="btn_global_search" class="btn" title="<?php echo $language_array['lbl_search']; ?>"><?php echo $language_array['lbl_search']; ?></a>
<!--                                    <a href="JavaScript:void(0);" onclick="clearSearch();" class="btn btn-grey-curve btn-primary" title="<?php echo $language_array['lbl_clear']; ?>"><?php echo $language_array['lbl_clear']; ?></a>-->
                                    
                                </div>
                            </div>
                        </li>
                        <!-- Search end -->
                    </ul>
                    <!-- ul end -->
                    
                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav float-end">
                        <!-- Nav Item - Alerts -->
                        <li class="nav-item dropdown message-dropdown d-none d-lg-block">
                            <a class="nav-link dropdown-toggle d-flex h-100" href="<?php echo $redirect_manage_url ?>#data/Feedback"  title="<?php echo $language_array['lbl_feedback']; ?>">
                                <img src="<?php echo IMAGES_URL; ?>Chat.svg" alt="<?php echo $language_array['lbl_feedback']; ?>">
                            </a>
                        </li>
                        <li class="nav-item dropdown notification-dropdown d-none d-lg-block">
                            <a class="nav-link d-flex h-100" href="<?php echo $redirect_manage_url ?>#data/Notifications/" title="<?php echo $language_array['lbl_notifications']; ?>">
                                <img src="<?php echo IMAGES_URL; ?>Notification.svg" alt="<?php echo $language_array['lbl_notifications']; ?>">
                            </a>
                            <?php if($noti_count>0) { ?>
                                <a class="nav-link dropdown-toggle d-flex h-100" href="javascript:void(0);" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <span class="counter"><?php _e($noti_count); ?></span>
                            </a>
                            <?php } ?>
                            
                            <ul class=" dropdown-menu dropdown-menu-end mailbox" aria-labelledby="2">
                                <li>
                                    <!-- Message -->
                                    <?php foreach($notyCount as $value) { 
                                        $noti_url = "javascript:void(0);";
                                        if (isset($scp_modules[$value->module])) {
                                            $noti_url = "#data/".$value->module ."/detail/". $value->record_id;
                                        }
                                    ?>
                                    <a href="<?php echo $noti_url; ?>" class="link">
                                        <h5 class="mb-0"><?php echo $value->notification_subject ?></h5>
                                        <span class="mail-desc">
                                            <?php
                                            $val_date = $value->date_entered;
                                            if ( $_SESSION['browser_timezone'] != NULL ) {
                                                $val_date = scp_user_time_convert( $val_date );
                                            } else {
                                                 /****change****/
                                                 if (empty($_SESSION['user_timezone'])) {
                                                    $result_timezone = $objSCP->getUserTimezone();
                                                } else {
                                                    $result_timezone = $_SESSION['user_timezone'];
                                                }
                                                $UTC = new DateTimeZone("UTC");
                                                $newTZ = new DateTimeZone($result_timezone);
                                                $date = new DateTime($val_date, $UTC);
                                                $date->setTimezone($newTZ);
                                                //Updated by BC on 16-nov-2015
                                                $date_format = $_SESSION['user_date_format'];
                                                $time_format = $_SESSION['user_time_format'];
                                                $val_date = $date->format($date_format . " " . $time_format);
                                            }
                                            echo $val_date;
                                            ?>
                                        </span>
                                    </a>
                                    <?php } ?>
                                </li>
                            </ul>
                        </li>
                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown user-dropdown">
                            <a class=" nav-link dropdown-toggle text-muted  pro-pic" href="Javascript:void(0);" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <span>Hello, <strong><?php echo $_SESSION['scp_user_account_name']; ?></strong></span>
                                <div class="user-image">
                                    <?php if( isset($_SESSION['scp_user_profile_pic']) && $_SESSION['scp_user_profile_pic'] != '' && is_pro_img_exist($_SESSION['scp_user_profile_pic']) ){ ?>
                                        <img src="<?php echo $_SESSION['scp_user_profile_pic']; ?>" alt="user" />
                                    <?php } else { ?>
                                        <?php echo get_avatar($_SESSION['scp_user_mailid'], 51); ?>
                                    <?php } ?>
                                </div>
                                <em class="fas fa-chevron-down" aria-hidden="true"></em>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-end user-dd animated" aria-labelledby="navbarDropdown">
                                <?php
                                if ($redirct_icon == "tachometer") {
                                    $redirct_icon = "user";
                                    $redirect_name = $language_array['lbl_profile'];
                                } else {
                                    $redirct_icon = "tachometer";
                                    $redirect_name = $language_array['lbl_dashboard'];
                                }
                                ?>
                                <li>
                                    <a class="dropdown-item" href="<?php echo $redirect_url; ?>">
                                        <span><?php echo $redirect_name; ?></span> 
                                        <em class="far fa-user"></em>
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="<?php echo $redirect_manage_url; ?>#data/Settings">
                                        <span><?php echo $language_array['lbl_preference']; ?></span> 
                                        <em class="fas fa-sliders-h"></em>
                                    </a>
                                </li>
                                <li class="d-lg-none">
                                    <a class="dropdown-item" href="<?php echo $redirect_manage_url; ?>#data/Feedback">
                                        <span><?php echo $language_array['lbl_feedback']; ?></span> 
                                        <em class="far fa-comment-dots" aria-hidden="true"></em>
                                    </a>
                                </li>
                                <li class="d-lg-none">
                                    <a class="dropdown-item" href="<?php echo $redirect_manage_url; ?>#data/Notifications">
                                        <span><?php echo $language_array['lbl_notifications']; ?></span> 
                                        <em class="fas fa-bell" aria-hidden="true"></em>
                                    </a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="<?php echo $redirect_manage_url; ?>?logout=true">
                                        <span><?php echo $language_array['lbl_logout']; ?></span> 
                                        <em class="fas fa-power-off"></em>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="d-lg-none three-dots">
                            <em class="fas fa-ellipsis-v" aria-hidden="true"></em>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- nav end -->    
            <!-- right-sidebar settings-panel start -->    
            <div id="right-sidebar" class="settings-panel">
                <!-- nav-item dropdown all-module start -->
                <div class="nav-item dropdown all-module">
                    <a class="nav-link dropdown-toggle d-flex justify-content-between align-items-center" href="JavaScript:void(0);" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <span><?php echo $language_array['lbl_all_modules']; ?></span>
                        <em class="fas fa-chevron-down"></em>
                    </a>

                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown" id="select_module_option">
                        <li>
                            <input type="hidden" name="select_val" value="0"/>
                            <a href="Javascript:void(0);" class="dropdown-item"><?php echo $language_array['lbl_all_modules']; ?></a>
                        </li>
                        <?php
                        reset($modules);
                        foreach ($modules as $key  => $value) {
                        ?>
                        <li>
                            <input type="hidden" name="select_val" value="<?php echo $key; ?>"/>
                            <a href="Javascript:void(0);" class="sfcp_global_module_options dropdown-item" data-module="<?php echo $key; ?>"><?php echo $value; ?></a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <!-- nav-item dropdown all-module end -->
                <div class="input-group">
                    <div class="d-block">
                        <div class="search-input">
                            <img src="<?php echo IMAGES_URL;  ?>Search.svg" alt="<?php echo $language_array['lbl_search']; ?>">
                            <input type="search" id="g_search_name" class="form-control rounded" placeholder="<?php echo $language_array['lbl_search']; ?>" aria-label="<?php echo $language_array['lbl_search']; ?>" aria-describedby="search-addon">
                        </div>
                        <a href="JavaScript:void(0);" id="btn_global_search" class="btn" title="<?php echo $language_array['lbl_search']; ?>"><?php echo $language_array['lbl_search']; ?></a>
<!--                        <a href="JavaScript:void(0);" onclick="clearSearch();" class="btn btn-grey-curve btn-primary" title="<?php echo $language_array['lbl_clear']; ?>"><?php echo $language_array['lbl_clear']; ?></a>-->
                    </div>
                </div>
                <!-- input-group d end -->
            </div>
        </header>
        <!-- header end -->
        <?php
    }
}

if ( ! function_exists('scp_standalone_remove_default_theme_css_js') ) {

    add_action('wp_print_styles', 'scp_standalone_remove_default_theme_css_js',100);
    
    /**
     * Here we will remove all css and js which enqueue and register by default theme, so that our standalone template page will look same in all theme
     * Please note here we can not remove inline css and js, admin or user have to manually remove it
     * Add any css or js name in list which you don't want to remove from standalone template page
     * Remove css or js from list which you want to remove from standalone template page
     *
     * @since 2.5.0
     * 
     * @global object $post
     * @global object $wp_styles
     * @global object $wp_scripts
     */
    function scp_standalone_remove_default_theme_css_js() {

        global $post, $wp_styles, $wp_scripts;
        $myallkeysubpages = array(
            'biztech_redirect_signup',
            'biztech_redirect_login',
            'biztech_redirect_profile',
            'biztech_redirect_forgotpwd',
            'biztech_redirect_resetpwd',
            'biztech_redirect_manange',
        );

        // These css will not remove from our page, standalone template
        $plugin_css = array(
            'admin-bar',
//            'font-awesome.min',
//            'scp-bootstrap-style',
            'scp-style',
            'scp-login',
            'scp-dashboard',
            'scp-preferences',
            'scp-profile',
            'scp-list',
            'scp-edit',
            'scp-proposals',
//            'bootstrap-min-style',
//            'scp-custom-style',
//            'scp-responsive-style',
            'flags',
            'fullcalendar',
            'bootstrap-datetimepicker',
            'bootstrap-glyphicons',
            'bootstrap-multiselect',
        );

        // These js will not remove from our page, standalone template
        $plugin_script = array(
            'admin-bar',
            'jquery',
            'bootstrap-bundle',
            'perfect-scrollbar',
            'fontawesome',
//            'jquery-migrate',
//            'popper',
//            'scp-bootstrap-js',
            'scp-global',
            'moment.min',
//            'fullcalendar.min',
//            'jquery-ui',
//            'bootstrap.min',
            'jquery.cookie.min',
            'fullcharts',
//            'app',
//            'jquery.blockui.min',
            'bootstrap-datetimepicker',
            'bootstrap-multiselect',
            'jquery.validate',
            'scp-js',
            'scp-manage-page-js',
            'printThis-js',
        );

        foreach ( $myallkeysubpages as $pages ) {   //when first installed check if option exists for page otherwise set it

            if( isset( $post->ID ) && get_option( $pages ) == $post->ID ) {

                $pagetemplate = get_post_meta(get_the_ID(), '_wp_page_template', true);

                if (is_multisite() && $pagetemplate == NULL) {  //if this is multisite and subsite not set any template than catch from network
                    $pagetemplate = (get_option('biztech_portal_template') == 0) ? 'page-crm-fullwidth.php' : 'page-crm-standalone.php';
                }
                $template = str_replace('.php', "", $pagetemplate);
                if ( $template == 'page-crm-standalone' ) {
                    if ( isset( $wp_styles->queue ) && ! empty( $wp_styles->queue ) ) {
                        foreach( $wp_styles->queue as $key => $style ) {
                            if ( ! in_array( $style, $plugin_css ) ) {
                                unset( $wp_styles->queue[ $key ] );
                            }
                        }
                    }

                    if ( isset( $wp_scripts->queue ) && ! empty( $wp_scripts->queue ) ) {
                        foreach( $wp_scripts->queue as $key => $script ) {
                            if ( ! in_array( $script, $plugin_script ) ) {
                                unset( $wp_scripts->queue[ $key ] );
                            }
                        }
                    }
                }
            }
        }
    }
}

if ( ! function_exists('scp_user_time_convert') ) {

    /**
     * Function that convert to given timezone or browser timezone.
     * 
     * @since v1.2.1
     * 
     * @global object $objSCP
     * @global int $sugar_crm_version
     * @param DateTime $value
     * @return DateTime
     */
    function scp_user_time_convert( $value , $view = "") {

        global $objSCP, $sugar_crm_version;
        if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
            $date_format = $objSCP->date_format;
            $time_format = $objSCP->time_format;
            
        } else {
            $date_format = $_SESSION['user_date_format'];
            $time_format = $_SESSION['user_time_format'];
            
        }
        
        $browser_timezone = $_SESSION['browser_timezone'];
        $temp_timezone = (-1) * (int) $browser_timezone / 60;
        $dot_after = explode( ".", $temp_timezone );
        if ( $dot_after[1] != NULL && $dot_after[1] > 0 ) {
            $offset = floor( $dot_after[0] ) . ':30';
        } else {
            $offset = $dot_after[0];
        }

        list($hours, $minutes) = explode(':', $offset);
        $seconds = $hours * 60 * 60 + $minutes * 60;

        // Get timezone name from seconds
        $tz = timezone_name_from_abbr('', $seconds, 1);
        if($tz === false) { $tz = timezone_name_from_abbr('', $seconds, 0); }
        $result_timezone = $_SESSION['user_timezone'];
        if ($sugar_crm_version == 7) {
            $result_timezone = $result_timezone->timezone;
        }
        
        $newTZ2 = new DateTimeZone('UTC');
        $dt = new DateTime($value, $newTZ2);
        $tz1 = new DateTimeZone($result_timezone);
        $dt->setTimezone($tz1);
        $value = $dt->format($date_format . " " . $time_format);
        
        return $value;
    }
}

if (!function_exists("scp_browser_time_convert")) {
    
    /**
     * Function that convert browser timezone to crmtimezone
     * 
     * @global object $objSCP
     * @global type $sugar_crm_version
     * @param type $value
     * @return type
     */
    function scp_browser_time_convert( $value ) {

        global $objSCP, $sugar_crm_version;
        $result_timezone = $_SESSION['user_timezone'];
        if ($sugar_crm_version == 7) {
            $result_timezone = $result_timezone->timezone;
        }
        $browser_timezone = $_SESSION['browser_timezone'];
        
        $temp_timezone = (-1) * $browser_timezone / 60;
        $dot_after = explode( ".", $temp_timezone );
        if ( $dot_after[1] != NULL && $dot_after[1] > 0 ) {
            $offset = floor( $dot_after[0] ) . ':30';
        } else {
            $offset = $dot_after[0];
        }

        list($hours, $minutes) = explode(':', $offset);
        $seconds = $hours * 60 * 60 + $minutes * 60;

        // Get timezone name from seconds
        $tz = timezone_name_from_abbr('', $seconds, 1);
        if($tz === false) { $tz = timezone_name_from_abbr('', $seconds, 0); }
                
        if ($sugar_crm_version == 7) {
            $newTZ2 = new DateTimeZone($result_timezone);
            $dt = new DateTime($value, $newTZ2);
            $value = $dt->format('Y-m-d H:i:s');
            
        } else {
            $newTZ2 = new DateTimeZone($result_timezone);
            $dt = new DateTime($value, $newTZ2);
            $tz1 = new DateTimeZone("UTC");
            $dt->setTimezone( $tz1 );
            $value = $dt->format('Y-m-d H:i:s');
        }
        
        return $value;
    }
}

if (!function_exists("scp_get_all_language_lists")) {
    
    /**
     * Function to get language lists
     */
    function scp_get_all_language_lists(){
        global $objSCP, $sugar_crm_version, $lang_code, $language_array;
        if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
            $sess_id = $objSCP->session_id;
        }
        if ($sugar_crm_version == 7) {
            $sess_id = $objSCP->access_token;
        }
        if ($sess_id != '') {
            $language_lists = $objSCP->get_all_language_lists();
            if (($language_lists == null || empty($language_lists)) && $_COOKIE['scp_lang_code'] != "en_us") {
                $_COOKIE['scp_lang_code'] = "en_us";
                $lang_code = "en_us";
                $language_array = scp_get_language_data($lang_code);
            }
            return $language_lists;
        }
    }
}

if (!function_exists("scp_create_connection")) {
    
    /**
     * Function to create new connection with new session
     */
    function scp_create_connection(){
        global $objSCP;
        $scp_sugar_rest_url = get_option('biztech_scp_rest_url');
        $scp_sugar_username = get_option('biztech_scp_username');
        $scp_sugar_password = scp_decrypt_string(get_option('biztech_scp_password'));
        $objSCP = new SugarRestApiCall($scp_sugar_rest_url, $scp_sugar_username, $scp_sugar_password);
    }
}


/**
 * Ouput or response will print in app-debug.txt file in app's error folder
 * Note: Only use when you need to debug data in connection or server level, each store has its own debug function and debug file
 *
 * @param mix $data - Data which will print in debug file, data may array, object, string
 * @since 1.0.0
 */
function scp_custom_debug( $data ) {

    $file_name = 'log.txt';
    $file_path = plugin_dir_path(__FILE__).$file_name;
    $time = date( "Y-m-d H:i:s" );
    $backtrace = debug_backtrace();

    $old_data = file_get_contents( $file_path );

    if ( is_array( (array)$backtrace ) ) {
        $backtrace = print_r( $backtrace, true );
    }

    if ( is_array( (array)$data ) ) {
        $data = print_r( $data, true );
    }

    if ( $old_data != '' ) {
        $old_data .=  PHP_EOL . PHP_EOL . PHP_EOL;
    }

    $final_data = $old_data . $time . PHP_EOL . $data;

    file_put_contents( $file_path, $final_data );
}

/**
 * Function to print data
 * 
 * @param type $data
 * @param type $is_die
 * 
 * @since v3.3.1
 */
function bcp_print($data, $is_die = 0) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    if ($is_die) {
        exit();
    }
}

if (!function_exists("scp_add_body_class")) {
    /**
     * Add portal class to body tag
     *
     * @param [type] $classes
     * @return void
     */
    function scp_add_body_class($classes) {
        global $post;
        $body_class_array = array();
        if(!empty($post)){
            if ( get_option("biztech_redirect_signup") == $post->ID ) {
                array_push($body_class_array, "scp-signup-body");
            }
            if ( get_option("biztech_redirect_login") == $post->ID || get_option("biztech_redirect_resetpwd") == $post->ID || get_option("biztech_redirect_forgotpwd") == $post->ID ) {
                array_push($body_class_array, "scp-login-body");
            }
        }
        return array_merge( $classes, $body_class_array );
    }
    add_filter("body_class", "scp_add_body_class");
}

if ( ! function_exists( 'sfcp_does_file_exists' ) ) { 
    function sfcp_does_file_exists($filename) {
        global $wpdb;

        return intval( $wpdb->get_var( "SELECT post_id FROM {$wpdb->postmeta} WHERE meta_value LIKE '%/$filename'" ) );
    }
}


/**
* Function to get User IP Address
*/
if (!function_exists("bcp_getUserIpAddr")) {
    function bcp_getUserIpAddr() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
   }
}

/* scp user login lock unlock */
if (!function_exists('bcp_login_lock')) {

    function bcp_login_lock(WP_REST_Request $request) {
        global $wpdb;
        $biztech_scp_key = $request->get_param('biztech_scp_key');
        $biz_scp_key = get_option('biztech_scp_key');
        
        if (isset($biztech_scp_key)) {
            if ($biz_scp_key == $biztech_scp_key) {
                $bcp_user_login = $request->get_param('bcp_user_login');
                $bcp_user_lock = $request->get_param('bcp_user_lock');
                
                $bcp_maximum_login_attempts = fetch_data_option('bcp_maximum_login_attempts');
                $login_attempt = 0;
                if($bcp_user_lock){
                    $login_attempt = $bcp_maximum_login_attempts;
                }
                
                $record = $wpdb->get_row("SELECT * FROM " . $wpdb->base_prefix . "bcp_login_attempts WHERE bcp_user_login='{$bcp_user_login}'");
                
                if(!empty($record)){
                    $wpdb->update(
                        $wpdb->base_prefix . 'bcp_login_attempts', array(
                        'ip_address' => $record->ip_address,
                        'login_attempt' => $login_attempt,
                        'last_login_date' => date("Y-m-d H:i:s"),
                        'bcp_user_login' => $record->bcp_user_login,
                        'id' => $record->id,
                        'ip_status' => "active",
                            ), array('bcp_user_login' => $record->bcp_user_login)
                    );
                    return 1;
                }
            }
        }
    }

}

add_action('rest_api_init', function () {
    register_rest_route('biztech-crm-portal/v1', '/login_attempt/', array(
        'methods' => 'POST',
        'callback' => 'bcp_login_lock',
        'permission_callback' => '__return_true',
    ));
});



if (!function_exists('bcp_change_menu_biztech')) {
    /**
     * Add portal Menu in full width
     *
     * @param [type] $classes
     * @return void
     */
    add_filter('wp_nav_menu_objects', 'bcp_change_menu_biztech', 10, 2);
    function bcp_change_menu_biztech($items, $args) {
        global $objSCP, $lang_code;
        if($objSCP){
            $sess_id = $objSCP->session_id;
            if($sess_id){
                $crm_menuLocation = fetch_data_option('bcp_portal_menuLocation');
                $biztech_portal_template = fetch_data_option('biztech_portal_template');
                /* if location add and portal login */
                if(isset($_SESSION['scp_user_id']) && $_SESSION['scp_user_id'] && $crm_menuLocation && $args->theme_location == $crm_menuLocation && $biztech_portal_template == 0){

                    $items[] = (object) array(
                        'ID'                => 'bcp_custom_main_menu',
                        'title'             => __('Customer Portal'),
                        'url'               => '#',
                        'menu_item_parent'  => 0,
                        'menu_order'        => '',
                        'type'              => '',
                        'object'            => '',
                        'object_id'         => '',
                        'db_id'             => 'bcp_custom_main_menu',
                        'classes'           => array('menu-item','menu-item-has-children'),
                        'post_title'        => '',
                        'xfn'               => '',
                        'current'           => '',
                        'target'            => '',
                    );
                    
                    $language_array = scp_get_language_data($lang_code);
                    $dashboard = $language_array['lbl_dashboard'];
                    $calendar_key = 'Calendar';
                    $calendar = $language_array['lbl_calendar'];
                    $modules = $_SESSION['module_array'];
                    $biztech_redirect_manage_page = get_page_link(get_option('biztech_redirect_manange'));
                    if ($biztech_redirect_manage_page != NULL) {
                        $manage_page_url = $biztech_redirect_manage_page;
                    } else {
                        $manage_page_url = home_url() . "/portal-manage-page/";
                    }
                    
                    $cal_checked = (isset($_SESSION['scp_additional_setting']->calender) ? $_SESSION['scp_additional_setting']->calender : 0);

                    $items[] = (object) array(
                        'ID'                => 'dashboard',
                        'title'             => $dashboard,
                        'url'               => $manage_page_url.'#data/Dashboard/',
                        'menu_item_parent'  => 'bcp_custom_main_menu',
                        'menu_order'        => '',
                        'type'              => '',
                        'object'            => '',
                        'object_id'         => '',
                        'db_id'             => '',
                        'classes'           => array('menu-item'),
                        'post_title'        => '',
                        'xfn'               => '',
                        'current'           => '',
                        'target'            => '',
                    );

                    $mlist = array();
                    $scp_custom_menu = get_option("scp_custom_menu");
                    foreach ($modules as $key => $values) {
                        $url = $manage_page_url.'#data/'.$key.'/list/';
                        foreach($scp_custom_menu as $value){
                            $name = str_replace(" ", "_", $value['name']);
                            if($key == $name) {
                                if($value['type'] == 'post_type') {
                                    $dcp_add_class = ($post->ID == $value['url']) ? "active" : "";
                                    $url = get_permalink($value['url']);
                                } else {
                                    $url = $value['url'];
                                }
                            }
                        }
                        $items[] = (object) array(
                            'ID'                => $key,
                            'title'             => $values,
                            'url'               => $url,
                            'menu_item_parent'  => 'bcp_custom_main_menu',
                            'menu_order'        => '',
                            'type'              => '',
                            'object'            => '',
                            'object_id'         => '',
                            'db_id'             => '',
                            'classes'           => array('menu-item'),
                            'post_title'        => '',
                            'xfn'               => '',
                            'current'           => '',
                            'target'            => '',
                        );
                        $mlist[] = $key;
                    }
                    if ( ( (in_array('Calls', $mlist)) || in_array('Meetings', $mlist) || in_array('Tasks', $mlist) ) && $cal_checked ) {
                        $items[] = (object) array(
                            'ID'                => $calendar,
                            'title'             => $calendar,
                            'url'               => $manage_page_url.'#data/'.$calendar_key.'/list/',
                            'menu_item_parent'  => 'bcp_custom_main_menu',
                            'menu_order'        => '',
                            'type'              => '',
                            'object'            => '',
                            'object_id'         => '',
                            'db_id'             => '',
                            'classes'           => array('menu-item'),
                            'post_title'        => '',
                            'xfn'               => '',
                            'current'           => '',
                            'target'            => '',
                        );
                    }

                } else if($crm_menuLocation && $args->theme_location == $crm_menuLocation && $biztech_portal_template == 0){
                    /* if location add and portal not login */
                    $biztech_redirect_login_page = get_page_link(get_option('biztech_redirect_login'));
                    $items[] = (object) array(
                        'ID'                => 'bcp_custom_main_menu',
                        'title'             => __('Customer Portal'),
                        'url'               => $biztech_redirect_login_page,
                        'menu_item_parent'  => 0,
                        'menu_order'        => '',
                        'type'              => '',
                        'object'            => '',
                        'object_id'         => '',
                        'db_id'             => 'bcp_custom_main_menu',
                        'classes'           => array('menu-item'),
                        'post_title'        => '',
                        'xfn'               => '',
                        'current'           => '',
                        'target'            => '',
                    );
                }
            }
        }    
        return $items;
    }
}

<?php
/**
 * Template Name : CRM Standalone
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 */ 
?>
<!DOCTYPE html>
<html lang="en-US">
    <!--<![endif]-->
    <head>
        <title><?php echo get_the_title(); ?> | <?php bloginfo('name'); ?></title>
        <?php
        wp_head();
        ?>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body  <?php body_class(); ?>>

        <?php
        global $post, $objSCP;
        while (have_posts()) : the_post();
            $theme_name = wp_get_theme();
            $fullwidth_class_gen = '';
            if ($theme_name == "Twenty Fourteen") {
                $fullwidth_class_gen = "fullwidth-fourteen-theme";
            }
            if ($theme_name == "Twenty Fifteen") {
                $fullwidth_class_gen = "fullwidth-fifteen-theme";
            }
            if ($theme_name == "Twenty Sixteen") {
                $fullwidth_class_gen = "fullwidth-sixteen-theme";
            }
            if ($theme_name == "Twenty Seventeen") {
                $fullwidth_class_gen = "fullwidth-seventeen-theme";
            }
            if ($theme_name == "Twenty Nineteen") {
                $fullwidth_class_gen = "fullwidth-nineteen-theme";
            }
            if ($theme_name == "Twenty Twenty") {
                $fullwidth_class_gen = "fullwidth-twenty-theme";
            }
            $scp_custom_menu = get_option( 'scp_custom_menu' );
            $ids = array();
            if(!empty($scp_custom_menu)){
                foreach ( $scp_custom_menu as $key => $value ) {
                    if($value['type'] == 'post_type'){
                        $ids[] = $value['url'];
                    }
                }
            }
            
            if ( ( isset($_SESSION['bcp_auth_error']) && $_SESSION['bcp_auth_error'] != '' ) || ( !is_object($objSCP) && $objSCP == 0 ) ) {
            ?>
                <div id="fullwidth-wrapper" class="crm_standalone sfcp-manage-page <?php echo $fullwidth_class_gen; ?>">
                    <div class='alert alert-danger' id='setting-error-settings_updated'>
                        <p><strong><?php _e( 'Portal is disabled. Please contact your administrator.' ); ?></strong></p>
                    </div>
                </div>
                <?php
                return;
            }
            $biztech_scp_name = fetch_data_option('biztech_scp_name');
            $pagetemplate = get_post_meta($post->ID, '_wp_page_template', true);
            $template = str_replace('.php', "", $pagetemplate);
            
            $bcp_admin_class = "";
            if (is_user_logged_in()) {
                $bcp_admin_class = "scp-user-logged-in";
            }

            if ( get_option("biztech_redirect_manange") == $post->ID || get_option("biztech_redirect_profile") == $post->ID) {
            ?>    
            <div id="fullwidth-wrapper" class="crm_standalone sfcp-manage-page <?php echo $fullwidth_class_gen; ?>">
                <?php
                // Include the page content.
                the_content();
                ?>
            </div>
            <?php } else if ($template == 'page-crm-standalone' && in_array($post->ID, $ids)) { ?>
                <div id="fullwidth-wrapper" class="<?php echo $bcp_admin_class; ?> crm_standalone sfcp-manage-page <?php echo $fullwidth_class_gen; ?>">
                    <div id="wrapper">
                        <?php
                        if ($template == 'page-crm-standalone') {
                            scp_standalone_sidebar_menu_part($biztech_scp_name, '', $template);
                        }
                        ?>
                        <div id="content-wrapper" class="d-flex flex-column">
                            <div id="content">
                                <?php
                                if ($template == 'page-crm-standalone') {
                                    scp_standalone_header_part('tachometer');
                                }
                                ?>
                                <div class="container-fluid">
                                    <?php
                                    // Include the page content.
                                    the_content();
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            } else if ( get_option("biztech_redirect_login") == $post->ID || get_option("biztech_redirect_forgotpwd") == $post->ID || get_option("biztech_redirect_resetpwd") == $post->ID ) {
                if ( has_post_thumbnail( $post->ID ) ){
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                }
            ?>
                <div id="fullwidth-wrapper" class="crm_standalone sfcp-manage-page <?php echo $fullwidth_class_gen; ?>">
                    <section class="login-section">
                        <div class="row align-items-center login-section">
                            <?php if(isset($image)) { ?>
                                <div class="col-lg-6 p-0 align-self-stretch d-flex login-image-main align-items-center">
                                    <div class="login-image mx-auto">
                                        <img src=" <?php echo $image[0]; ?>" alt="CRMJetty">
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="col-lg-6 login-form mx-auto p-0 d-flex align-items-center position-relative">
                                <?php
                                // Include the page content.
                                the_content();
                                ?>
                            </div>
                        </div>
                    </section>
                    
                </div>
            <?php } else {
                $sec_class = "";
                if (get_option("biztech_redirect_signup") == $post->ID) {
                    $sec_class = "registration-section";
                }
                if ( has_post_thumbnail( $post->ID ) ){
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                }
                ?>
                <div id="fullwidth-wrapper" class="crm_standalone sfcp-manage-page <?php echo $fullwidth_class_gen; ?>">
                    <section class="login-section <?php echo $sec_class; ?>" <?php if(isset($image)) { ?> style="background-image: url(<?php echo $image[0]; ?>);" <?php } ?>>
                        <div class="container">
                            <?php
                            // Include the page content.
                            the_content();
                            ?>
                        </div>
                    </section>
                </div>
                <?php
            }
        endwhile;
        ?>
        <?php wp_footer(); ?>
    </body>
</html>
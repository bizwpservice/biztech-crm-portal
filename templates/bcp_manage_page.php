<?php
if (isset($_REQUEST['conerror']) && !empty($_REQUEST['conerror']) && (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error']!='')) {
    $bcp_connection_error = $_SESSION['bcp_connection_error'];
    unset($_SESSION['bcp_connection_error']);
    return "<div class='error settings-error' id='setting-error-settings_updated'>
        <p><strong>".$bcp_connection_error."</strong></p>
    </div>";
}
$biztech_redirect_manage_page = get_page_link(get_option('biztech_redirect_manange'));
if ($biztech_redirect_manage_page != NULL) {
    $manage_page = $biztech_redirect_manage_page;
} else {
    $manage_page = home_url() . "/portal-manage-page/";
}

$profile_name = $language_array['lbl_profile'];

$pagetemplate = get_post_meta(get_the_ID(), '_wp_page_template', true);
if (is_multisite() && $pagetemplate == NULL) {  //if this is multisite and subsite not set any template than catch from network
    $pagetemplate = (get_option('biztech_portal_template') == 0) ? 'page-crm-fullwidth.php' : 'page-crm-standalone.php';
}
$template = str_replace('.php', "", $pagetemplate);

if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
    $list_result_count_all = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], 'accounts', array('id'), '', '', 0);
    $countAcc = (isset($list_result_count_all->entry_list)) ? count($list_result_count_all->entry_list) : 0;
}
if ($sugar_crm_version == 7) {
    $list_result_count_all = $objSCP->getRelationship('Contacts', $_SESSION['scp_user_id'], 'accounts', 'id', array(), '', '', 'date_entered:DESC');
    if (!empty($list_result_count_all->records)) {
        $countAcc = count($list_result_count_all->records);
    } else {
        $countAcc = 0;
    }
}
$theme_name = wp_get_theme(); //20-aug-2016
if ($theme_name == "Twenty Fifteen") {
    $article_class_gen = "article-fifteen-theme";
}
if ($theme_name == "Twenty Sixteen") {
    $article_class_gen = "article-sixteen-theme";
}

$biztech_scp_name = fetch_data_option('biztech_scp_name');
$biztech_scp_name = ($biztech_scp_name != NULL) ? $biztech_scp_name : "All Modules";

reset($modules);
$dashboard_key = 'Dashboard';
$dashboard = $language_array['lbl_dashboard'];
$cal_checked = (isset($_SESSION['scp_additional_setting']->calender) ? $_SESSION['scp_additional_setting']->calender : 0);

//if ($template != 'page-crm-standalone') {
//    scp_full_width_menu_part($biztech_scp_name, $countAcc, $template);
//}
?>

    <?php
        if ($template == 'page-crm-standalone') {
            scp_standalone_header_part('tachometer');
        }
    ?>
    <div class="container-fluid page-body-wrapper">
        <?php
        if ($template == 'page-crm-standalone') {
            scp_standalone_sidebar_menu_part($biztech_scp_name, $countAcc, $template);
        }
        ?>
        <div class="main-panel">
            <div id="content-wrapper" class="content-wrapper">
                <div id="responsedata" data-count="0" class='scp-standalone-content'></div>
                <div id="otherdata" style="display:none;"></div>
                <input type="hidden" id="res_count" value="0" >
            </div>
        </div>
    </div>


<script type="text/javascript">
    var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
    jQuery(function ($) {
        $(window).on('hashchange', function (e) {

            var hash, action_name, breadcrumb, action_url, tmp_module_name;

            hash = window.location.hash;
            if (hash == '') { // if url without any parameter, redirect to dashboard
                hash = '#data/Dashboard/';
                window.location = hash;
                return;
            }
            if (typeof hash !== 'undefined' && hash != '') {
                var pagenotfound = 0;
                var parameters = hash.split('/');
                var module_name = 0;
                if (typeof parameters[1] !== 'undefined' && parameters[1] != '') {
                    module_name = parameters[1];
                }

                var module_action = 0;
                if (typeof parameters[2] !== 'undefined' && parameters[2] != '') {
                    module_action = parameters[2];
                }

                var record_id = 0;
                if (typeof parameters[3] !== 'undefined' && parameters[3] != '' && parameters[3] != 'Relate') {
                    record_id = parameters[3];
                }

                if (module_action || module_name == 'Dashboard' || module_name == 'SearchResult' || module_name == 'Settings' || module_name == 'Notifications' || module_name == 'Solutions' || module_name == 'Feedback') {
                    action_name = module_action;
                    var data = {
                        'modulename': module_name,
                    };
                    if (module_name != 'Dashboard' && module_name != 'SearchResult') {
                        data.view = module_action;
                    }
                    if (module_name == 'SearchResult') {
                        var searchterm = parameters.slice(2);
                        var search_term = searchterm.join("/");
                        search_term = search_term.slice(0);
                        $('#g_search_name').val(decodeURI(search_term));
                        data.view = search_term;
                    } else {
                        jQuery(".sfcp-selected-module").attr("data-value","0");
                        var allModule = jQuery("#select_module_option").find("ul").find("li").first().find("a").text();
                        jQuery(".sfcp-selected-module").text(allModule);
                    }
                    if (record_id) {
                        data.id = record_id;
                    }

                    if (module_action == 'list') {
                        jQuery('#edit-' + module_name + ' a').removeClass('scp-active-submenu');
                        jQuery('#list-' + module_name + ' a').addClass('scp-active-submenu');
                        data.action = 'bcp_list_module';
                        if (record_id != 0) {
                            var searchterm = parameters.slice(3);
                            var search_term = searchterm.join(" ");
                            search_term = search_term.slice(0);
                            $('#g_search_name').val(decodeURI(search_term));
                            jQuery("#moduleDropdown").find(".sfcp-selected-module").attr("data-value",module_name);
                            jQuery(".sfcp-selected-module").text(module_name);
                            data.searchval = search_term;
                            data.page_no = '0';
                        }
                    } else if (module_action == 'edit') {
                        jQuery('#edit-' + module_name + ' a').addClass('scp-active-submenu');
                        jQuery('#list-' + module_name + ' a').removeClass('scp-active-submenu');
                        data.action = 'bcp_add_module';
                        //sent relatemodule
                        if (parameters[3] == 'Relate' && parameters[5] == 'Relation') {
                            data.relate_to_module = parameters[4];
                            data.relate_to_id = parameters[7];
                            data.relationship = parameters[6];
                        } else{
                            if (parameters[3] == 'Relate') {
                                data.relate_to_module = parameters[4];
                                data.relate_to_id = parameters[5];
                            }
                        }
                    } else if (module_action == 'detail') {
                        if (record_id == "") {
                            pagenotfound = 1;
                        }
                        jQuery('#edit-' + module_name + ' a').removeClass('scp-active-submenu');
                        jQuery('#list-' + module_name + ' a').addClass('scp-active-submenu');
                        data.action = 'bcp_view_module';
                        module_action = 'list';

                        if (parameters[4] == 'relationship' && parameters[6] == 'response') {
                            data.response_module = parameters[7];
                            data.relationship = parameters[5];
                        } else{
                            if (parameters[4] == 'response') {
                                data.response_module = parameters[5];
                            }
                        }
                    } else if (module_action == 'Search' && module_name == 'Solutions') {
                        data.action = 'bcp_search_solutions';
                        //sent relatemodule
                        if (parameters[3] == 'Relate') {
                            data.relate_to_module = parameters[4];
                            data.relate_to_id = parameters[5];
                        }
                    } else {
                        if (module_name != 'SearchResult' && module_name != 'Settings' && module_name != 'Notifications' && module_name != 'Dashboard' && module_name != 'Feedback') {
                            pagenotfound = 1;
                        }
                    }


                    if (module_name == 'Dashboard') {
                        data.action = 'scp_dashboard_page';
                    }
                    //For Feedback page
                    if (module_name == 'Feedback') {
                        data.action = 'scp_feedback_page';
                    }
                    //add setting page
                    if (module_name == 'Settings') {
                        data.action = 'scp_settings_page';
                    }
                    //add notification page
                    if (module_name == 'Notifications') {
                        data.action = 'scp_notifications_page';
                        jQuery(".scp-notification-dropdown-div").hide();
                    }
                    if (module_name == 'Calendar') {
                        data.action = 'bcp_calendar_display';
                    }
                    if (module_name == 'SearchResult') {
                        data.action = 'bcp_global_search';
                    }
                    if (module_name != 'SearchResult' && (data.action == 'bcp_list_module' && record_id == 0)) {
                        if (jQuery('#g_search_name').val() != "" && jQuery(".sfcp-selected-module").attr("data-value") != "0") {
                        clearSearch();
                        } else {
                            jQuery('#g_search_name').val("");
                        }
                    }

                    $( '.left-menu-panel > ul > li' ).removeClass( 'active' );
                    if (module_name == 'Dashboard') {
                        $(".dcp-menu-dashboard").addClass("active");
                    } else {
                        $(".dcp-menu-"+module_name).addClass("active");
                    }

                    if (pagenotfound == 0) {
                        jQuery("#responsedata").addClass("bcp-loader");
                        
                        $('.scp-leftpanel .scp-sidemenu').removeClass('show-menu');
                        $.post(ajaxurl, data, function (response) {
                            if(module_name == 'Dashboard') {
                                $('#responsedata').addClass('scp-dashboard-list-view');
                                $('#responsedata').removeClass('scp-page-list-view');
                            } else {
                                $('#responsedata').removeClass('scp-dashboard-list-view');
                                $('#responsedata').addClass('scp-page-list-view');
                            }
                            if (response != '0' && response != '') {
                                $('#responsedata').html(response);
                            } else {
                                response = "<label class='errors'><?php echo $language_array['msg_no_permission_data']; ?></label>";
                                $('#responsedata').html(response);
                            }
                            jQuery("#responsedata").removeClass("bcp-loader");
//                            setTimeout(function(){ jQuery('.sfcp-validation, .success, .error, .login_error').hide(); }, 2000);
                        });
                    } else {
                        var response = "<label class='errors'><?php echo $language_array['msg_no_permission_data']; ?></label>";
                        $('#responsedata').html(response);
                        jQuery('.scp_breadcrumb').css({'display': 'none'});
                    }
                } else {
                    var response = "<label class='errors'><?php echo $language_array['msg_no_permission_data']; ?></label>";
                    $('#responsedata').html(response);
                    jQuery('.scp_breadcrumb').css({'display': 'none'});
                }
            } else {
<?php if ($template == 'page-crm-standalone') { ?>
                    window.location.hash = $('.scp-sidemenu li:nth-child(2) ul li a').attr('href');
    <?php
} else {
    ?>
                    window.location.hash = $('.scp-sidemenu li:first ul li a').attr('href');
    <?php
}
?>
            }
        }).trigger('hashchange');
    });

    

    function bcp_module_paging(page, modulename, ifsearch, order_by, order, view, get_current_url, advance_filter='', paging_obj = "") { // for paging
    
        var searchval = jQuery('#g_search_name').val();
        var select_all = '0';
        var limit = jQuery(".scp-select-list-paging").val();
        if (paging_obj != "") {
            limit = jQuery(paging_obj).val();
        }
        if (jQuery("#select_all").hasClass("scp_select_th_active")) {
            select_all = '1';
        }
        if (ifsearch == 1 || jQuery('#search_name').length > 0) {
            searchval = jQuery('#search_name').val();
        }
        
        if(advance_filter){
            var where_condition = {};
            var currency = {};
            var boolean = {};
            jQuery(".where_field").each(function() {
                var key = jQuery(this).attr("name");
                var where_field_val = jQuery(this).val();
                if(where_field_val != '' && where_field_val != null) {
                    var date = {};
                    var select = {};
                    if(jQuery(this).hasClass("scp_currency")){
                        if(where_field_val == 'between'){
                            var start_range = jQuery('#start_range_total_amount_advanced').val();
                            var end_range = jQuery('#end_range_total_amount_advanced').val();
                            
                            currency['key'] = key;
                            currency['compare'] = where_field_val;
                            currency['name'] = 'range';
                            currency['start_range_total_amount_advanced'] = start_range;
                            currency['end_range_total_amount_advanced'] = end_range;
                            
                        } else {
                            var total_amount = jQuery('#range_total_amount_advanced').val();
                            currency['key'] = jQuery(this).attr("name");
                            currency['compare'] = where_field_val;
                            currency['name'] = 'single';
                            currency['range_total_amount_advanced'] = total_amount;
                        }
                    } else if (jQuery(this).hasClass("scp_date")) {
                        where_condition['date'][key] = where_field_val;
                    } else if (jQuery(this).hasClass("multiselectbox")) {
                        select['key'] = key;
                        select['name'] = 'selectbox';
                        select['compare'] = where_field_val;
                        where_condition[key] = select;
                    } else if (jQuery(this).hasClass("scp_boolean")) {
                        boolean['name'] = 'boolean';
                        boolean['compare'] = where_field_val;
                        where_condition[key] = boolean;
                    } else if (jQuery(this).hasClass("scp_daterange")) {
                        if(where_field_val == 'between'){
                            var start_range = jQuery('input[name=start_range_'+key+']').val();
                            var end_range = jQuery('input[name=end_range_'+key+']').val();
                            
                            date['key'] = key;
                            date['compare'] = where_field_val;
                            date['name'] = 'range';
                            date['start_range_'+key] = start_range;
                            date['end_range_'+key] = end_range;
                            
                        } else if (where_field_val == '=' || where_field_val == '!=' || where_field_val == '>' || where_field_val == '<') {
                            var date_range = jQuery('input[name=range_'+key+']').val();
                            date['key'] = key;
                            date['compare'] = where_field_val;
                            date['name'] = 'single';
                            date['range_'+key] = date_range;
                        } else {
                            date['key'] = key;
                            date['compare'] = where_field_val;
                        }
                        where_condition[key] = date;
                    } else {
                        where_condition[key] = where_field_val;
                    }

                }
            });
        }
        
        var AOK_Knowledge_Base_Categories = jQuery('#AOK_Knowledge_Base_Categories').val();
        if (ifsearch == 1 && (searchval.trim() == '' || searchval == null) && (modulename != 'AOK_KnowledgeBase' && modulename != 'KBContents' )) {
            alert("<?php echo $language_array['msg_search_empty_keyword']; ?>");
            return false;
        }
        if (ifsearch == 1 && modulename == 'AOK_KnowledgeBase' && AOK_Knowledge_Base_Categories == '' && (searchval.trim() == '' || searchval == null)) {
            alert("<?php echo $language_array['msg_kb_search_empty_keyword']; ?>");
            return false;
        }
        jQuery("#responsedata").addClass('bcp-loader');
        var data = {
            'action': 'bcp_list_module',
            'view': view,
            'modulename': modulename,
            'page_no': page,
            'current_url': get_current_url,
            'order_by': order_by,
            'order': order,
            'searchval': searchval,
            'AOK_Knowledge_Base_Categories': AOK_Knowledge_Base_Categories,
            'select_all': select_all,
            'limit': limit,
        };

        // For solution paging
        if (modulename == "Solutions") {
            <?php if ($sugar_crm_version == 7) { ?>
            data.modulename = "KBDocuments";                        
            <?php } else { ?>
            data.modulename = "AOK_KnowledgeBase";
            <?php } ?>
            data.parentmodule = "solutions";
        }
        
        if(advance_filter){
            data.filter_where_condition = where_condition;
            data.filter_where_currency_condition = currency;
        }

        //add relate_module for subpanel
        if (view == 'detail') {
            data.relate_to_module = jQuery('#relate_to_module').val();
            data.relate_to_id = jQuery('#relate_to_id').val();
            if(jQuery('#subpanelname').val()) {
                data.subpanelname = jQuery('#subpanelname').val();
            }
        }
        //pagination for notification module
        if (modulename == 'Notifications') {
            data.action = 'scp_notifications_page';
            if (jQuery('#scp_noti_is_search').val() == '1') {
                data.noti_type = jQuery('#noti_type').attr('data-search');
                data.noti_module = jQuery('#noti_module').attr('data-search');
                data.noti_from_date = jQuery('#noti_from_date').attr('data-search');
                data.noti_to_date = jQuery('#noti_to_date').attr('data-search');
                data.scp_noti_is_search = '1';
            }
        }
        jQuery.post(ajaxurl, data, function (response) {
            jQuery("#responsedata").removeClass('bcp-loader');
            if (jQuery.trim(response) != '-1') {
                if (view == 'detail') {
                    jQuery('.scp_subpanel_body').html(response);
                } else if (modulename == "Solutions") { // for redner response when module is solution for paging
                    jQuery("#solutionlist-items").html(response);
                } else {
                    jQuery('#responsedata').html(response);
                    if (jQuery("#select_all").hasClass("scp_select_th_active")) {
                        jQuery(".scp-checkbox").prop('checked', "checked"); //change all ".checkbox" checked status
                        jQuery(".scp-checkbox").attr('disabled', "disabled");
                        jQuery("#select_all").prop('checked', "checked");
                        var check_count = jQuery("#scp-select-th-all").attr("data-count");
                        jQuery("#scp-checkbox-selected-span").text("<?php echo $language_array['lbl_selected']; ?>: "+check_count);
                    }
                }
                // jQuery('#succid').hide();
            } else
            {
                var redirect_url = $(location).attr('href') + "?conerror=1";
                window.location.href = redirect_url;
            }
        });
    }

    

    jQuery(document).ready(function ($) {
        var curURL = window.location.href;

        //for display listing page after record insert
        var param = curURL.split("?");
        if (param[1] != '' && param[1] != null && param[1] != 'undefined') {
            sucmsg = 1;
            bcp_common_call(param[1], sucmsg);
        }
        $(".inner_ul> li").click(function ()
        {
            var divId = $(this).attr('id');
            sucmsg = 0;
            //$('.no-toggle').removeAttr('style');
            // $('a').removeClass('scp-active-submenu');
            //$('#' + divId + ' a').addClass('scp-active-submenu');
            bcp_common_call(divId, sucmsg);
        });
        function bcp_common_call(divId, sucmsg) {
            jQuery('#otherdata').hide();
            var getdata = divId.split("-");
            var view = getdata[0];
            var modulename = getdata[1];
            var get_current_url = "<?php echo $_SERVER['REQUEST_URI']; ?>"
            var order_by = '';
            var order = '';
            var el = jQuery("#responsedata");
            if (modulename.indexOf("&") >= 0) { // in calnder,after click go to detail page
                getmodulename = modulename.split("&");
                modulename = getmodulename[0];

                if (divId.indexOf("detail") >= 0) {// display selected in module list
                    setview = "list";
                    //modulename1 = modulename.toLowerCase();
                    modulename1 = modulename;
                    //$('#' + modulename1 + '_id').addClass('noborder');
                    // $('#dropdown_' + modulename1 + '_id').css('display', 'block');
                    // $('#list-' + modulename + '  a').addClass('scp-active-submenu');
                    // $("#" + modulename1 + "_id a.label").addClass('scp-active-menu');
                }
            }
            if (divId.indexOf("list") >= 0) {// display selected in module list afetr record added
                //modulename1 = modulename.toLowerCase();
                modulename1 = modulename;
                // $('#' + modulename1 + '_id').addClass('noborder');
                // $('#dropdown_' + modulename1 + '_id').css('display', 'block');
                //$('#list-' + modulename + '  a').addClass('scp-active-submenu');
                //$("#" + modulename1 + "_id a.label").addClass('scp-active-menu');
            }
            //App.blockUI(el);
            if (view == 'edit') {
                var data = {
                    'action': 'bcp_add_module',
                    'view': view,
                    'modulename': modulename,
                    'id': "<?php
if (isset($_REQUEST['id'])) {
    echo $_REQUEST['id'];
}
?>",
                    'current_url': get_current_url
                };
            }
            if (view == 'view') {
                if (modulename == 'calendar') { // if calender module then call other page
                    var data = {
                        'action': 'bcp_calendar_display',
                    };
                } else {
                    var data = {
                        'action': 'bcp_view_module',
                        'view': view,
                        'modulename': modulename,
                        'id': "<?php
if (isset($_REQUEST['id'])) {
    echo $_REQUEST['id'];
}
?>",
                        'current_url': get_current_url
                    };
                }
            }
            if (view == 'list') {
                var data = {
                    'action': 'bcp_list_module',
                    'view': view,
                    'modulename': modulename,
                    'page_no': 0,
                    'current_url': get_current_url,
                    'order_by': order_by,
                    'order': order
                };
            }
            if (view == 'detail') {
                var data = {
                    'action': 'bcp_view_module',
                    'view': view,
                    'modulename': modulename,
                    'id': "<?php
if (isset($_REQUEST['id'])) {
    echo $_REQUEST['id'];
}
?>",
                    'current_url': get_current_url

                };
            }
        }
    });
</script>  
    
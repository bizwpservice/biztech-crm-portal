<?php
defined('ABSPATH') or die('No script kiddies please!');
global $custom_relationships;

if ($module_name == 'Quotes') {
    if (isset($_SESSION['bcp_accept_quote']) && $_SESSION['bcp_accept_quote'] != '') {
        ?>
        <div class='alert alert-success alert-dismissible fade show bcp-success'>
            <span><?php echo stripslashes($_SESSION['bcp_accept_quote']); ?></span>
            <button type='button' class='btn-close' data-bs-dismiss='alert'></button>
        </div>
        <?php
        unset($_SESSION['bcp_accept_quote']);
    }
    if (isset($_SESSION['bcp_decline_quote']) && $_SESSION['bcp_decline_quote'] != '') {
        ?>
        <div class='alert alert-success alert-dismissible fade show bcp-success'>
            <span><?php echo stripslashes($_SESSION['bcp_decline_quote']); ?></span>
            <button type='button' class='btn-close' data-bs-dismiss='alert'></button>
        </div>
        <?php
        unset($_SESSION['bcp_decline_quote']);
    }
}

//for displaying message of add record from subpanel in v3.0.0
if (isset($_SESSION['bcp_add_record']) && $_SESSION['bcp_add_record'] != '') {
    ?>
    <div class='alert alert-success alert-dismissible fade show bcp-success'>
        <span><?php echo stripslashes($_SESSION['bcp_add_record']); ?></span>
        <button type='button' class='btn-close' data-bs-dismiss='alert'></button>
    </div>
    <?php
    unset($_SESSION['bcp_add_record']);
}

$module_name_label = $_SESSION['module_array'][$module_name];
$module_without_s = $_SESSION['module_array_without_s'][$module_name];

if ($module_name == "Quotes" || $module_name == "ProductTemplates") {
    $biztech_scp_name = fetch_data_option('biztech_scp_name');
    $biztech_upload_image = fetch_data_option('biztech_scp_upload_image');
    ?>
    <div class='print-header' style='display: none;'>
        <div class='print-header-div'>
            <div class='print-div' >
    <?php if ($biztech_upload_image != NULL) { ?>
                    <div class="scp-logo">
                        <img src="<?php echo $biztech_upload_image; ?>"  alt="" width="100"/>
                    </div>
                <?php } ?>
                <?php if ($biztech_scp_name != NULL) { ?>
                    <h3  class="scp-login-heading"><?php echo $biztech_scp_name; ?></h3>
    <?php } ?>
            </div>
            <div class='print-div' >
                <h3 class='module-title'><?php echo $module_without_s; ?></h3>
                <?php
                $print_number = "";
                $print_date = "";
                if ($module_name == "Quotes") {
                    $print_number = $record_detail->quote_num;
                    $print_date = date("d-m-Y", strtotime($record_detail->date_entered));
                }
                ?>
                <div class='module-subinfo'>
                    <p><?php echo $print_date; ?></p>
                    <p><?php echo $print_number; ?></p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<div class="white-body-shadow preferences-content cases-details-bg">
    <div class="preferences-header white-bg-heading">
        <div class="d-flex justify-content-between">
            <h2><?php echo $module_without_s; ?>
            <?php
            if ($module_name == 'AOS_Quotes') {
                    foreach ($results as $k => $vals_lbl) {
                        foreach ($vals_lbl->rows as $k_lbl2 => $vals) {
                            foreach ($vals as $key_fields => $val_fields) {
                                if ($val_fields->name == 'stage') {
                                    $ar_val = $val_fields->name;
                                    $value = isset($record_detail->entry_list[0]->name_value_list->$ar_val->value) ? html_entity_decode($record_detail->entry_list[0]->name_value_list->$ar_val->value) : '';

                                    if ( $value == 'Closed Accepted' ) { ?>
                                        <span class='scp-dtl-statusbtn btn btn-sm btn-success'>Status: <?php echo $value; ?></span>
                                    <?php } else if ( $value == 'Closed Lost' ) { ?>
                                        <span class='scp-dtl-statusbtn btn btn-sm btn-danger'>Status: <?php echo $value; ?></span>
                                    <?php
                                    }
                                }
                            }
                        }
                    }
                }
                ?>
            </h2>
            <?php
            $case_deflection = 0;
            //Check if solution categories are blank or not.
            if (isset($_SESSION['scp_knowledgebase_categories']) && isset($_SESSION['scp_additional_setting']) && isset($_SESSION['scp_additional_setting']->case_deflection) && $_SESSION['scp_additional_setting']->case_deflection) {
                $case_deflection = 1;
            }
            $addRecordUrl = "#data/$module_name/edit/";
            if ($module_name == "Cases" && $case_deflection) {
                $addRecordUrl = "#data/Solutions/Search/";
            }
            if (isset($relate_to_module) && $relate_to_module != '') {
                $addRecordUrl .= "Relate/" . $relate_to_module . "/" . $relate_to_id;
            }
            ?>
            <div class="d-flex justify-content-between title-case-block">
                <div class="button-group d-flex flex-row-reverse align-items-center">
            <?php if ($_SESSION['module_action_array'][$module_name]['create'] == 'true' && $module_name!= 'Accounts') { ?>
                    <a href="<?php echo $addRecordUrl; ?>" class="dcp_ca_class btn" title='<?php echo $language_array['lbl_add']; ?>'>
                        <?php echo $language_array['lbl_add']; ?>
                    </a>
            <?php }

            if ($_SESSION['module_action_array'][$module_name]['edit'] == 'true') {
                    $edit_flag = 0;
                    $status = "";
                    if (isset($record_detail->status)) {
                        $status = strtolower($record_detail->status);
                        if ($status == "pending" || $status == "requested" || $status == "not held") {
                            $edit_flag = 1;
                        }
                    }
                    if ($module_name != 'bc_proposal' && $module_name != 'Calls' && $module_name != 'Meetings') {
                        $edit_flag = 1;
                    }
                    if ($edit_flag) {
                        ?>
                        <a href='#data/<?php echo $module_name; ?>/edit/<?php echo $id; ?>/' title='Edit' class='scp-<?php echo $module_name; ?> scp-default-bg-btn scp-dtl-editbtn'>
                            <?php echo $module_icon_content['edit']; ?>
                        </a>
                    <?php
                    }
                }
                if ($_SESSION['module_action_array'][$module_name]['delete'] == 'true' && $module_name != 'Accounts') {
                    if ( ( $module_name != 'bc_proposal' && $module_name != 'Meetings' ) || ( $module_name == 'bc_proposal' && isset($record_detail->proposal_status) && strtolower($record_detail->proposal_status) == 'pending' )  || ($module_name == 'Meetings' && ($status == "pending" || $status == "requested" || $status == "not held") ) ) {
                    ?>
                    <a href='javascript:void(0);' title='Delete' class='deletebtn scp-<?php echo $module_name; ?> scp-default-bg-btn scp-dtl-deletebtn' onclick='form_submit("<?php echo $id; ?>");'>
                        <?php echo $module_icon_content['delete']; ?>
                    </a>
                    <?php
                    }
                }

            if ($module_name == "AOS_Quotes" || $module_name == "AOS_Invoices" || $module_name == "AOS_Products" || $module_name == "AOS_Contracts") { ?>


                <a class='scp-<?php echo $module_name; ?> scp-default-bg-btn scp-dtl-viewbtn' onclick="PrintElem('responsedata')" title='<?php echo $language_array['lbl_print']; ?>'>
                    <?php echo $module_icon_content['print']; ?>
                </a>
                <a href='javascript:void(0);' onclick='pdf_data_download("<?php echo $id; ?>", "<?php echo $module_name; ?>");' class='scp-<?php echo $module_name; ?> scp-default-bg-btn scp-dtl-viewbtn' title='<?php echo $language_array['lbl_download']; ?>'>
                    <?php echo $module_icon_content['download']; ?>
                </a>

            <?php }
                if ($module_name == 'AOS_Quotes') {
                    foreach ($results as $k => $vals_lbl) {
                        foreach ($vals_lbl->rows as $k_lbl2 => $vals) {
                            foreach ($vals as $key_fields => $val_fields) {
                                if ($val_fields->name == 'stage') {
                                    $ar_val = $val_fields->name;
                                    $value = isset($record_detail->entry_list[0]->name_value_list->$ar_val->value) ? html_entity_decode($record_detail->entry_list[0]->name_value_list->$ar_val->value) : '';

                                    if ($value == 'Confirmed') {
                                        ?>
                                        <a id='accept_quote_btn_id' onclick='bcp_quote_accept_decline_btn("<?php echo $name; ?>", "<?php echo $module_name; ?>", "<?php echo $id; ?>", "Accept", "<?php echo $current_url; ?>");' href='javascript:void(0);' class='scp-<?php echo $module_name; ?> scp-dtl-acceptbtn' title='Accept'>
                                        <em class="fa fa-check"></em>
                                        </a>
                                        <a id='decline_quote_btn_id' onclick='bcp_quote_accept_decline_btn("<?php echo $name; ?>", "<?php echo $module_name; ?>", "<?php echo $id; ?>", "Decline", "<?php echo $current_url; ?>");' href='javascript:void(0);' class='scp-<?php echo $module_name; ?> scp-dtl-declinebtn' title='Decline'>
                                            <em class="fa fa-times"></em>
                                        </a>
                                        <?php
                                    }
                                }
                            }
                        }
                    }
                }
                ?>
                </div>
            </div>
        </div>
        <span id='succid' style='display:none'></span>
    </div>
    <div class="preferences-tabs white-main-body">
        <div class="tabs-menu">
    <?php
    /* ----------- Subpanel start----------------------- */
    $module_action_array = $_SESSION['module_action_array'];
    if ($module_name == 'Meetings' || $module_name == 'Calls') {
        $where_con = strtolower($module_name) . ".id = '{$id}'";
        $relation_name = strtolower($module_name);
        if($_SESSION['scp_user_group_type'] == "contact_based"){
            $record_detail1 = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], $relation_name, '', $where_con);
        }else{
            $record_detail1 = $objSCP->get_relationships('Accounts', $_SESSION['scp_account_id'], $relation_name, '', $where_con);    
        }
        if ($record_detail1->entry_list == NULL) {
            $flag = 1;
        }
    }

    unset($subpanels->Contacts);
  
    $class = 'col-lg-12';
    if ($module_name == 'AOS_Quotes' || $module_name == 'AOS_Invoices') {
        $class = 'col-lg-4';
    }
    ?>
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link link-secondary active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" href="JavaScript:void(0);">Case Detail</a>
        </li>
        <?php if ($module_action_array[$module_name]['subpanel'] == 'true' && ((array) $subpanels)) { 
            if (isset($subpanels) && $subpanels != "") {
                $scp_modules = $_SESSION['module_array'];
                $temp_subpanel = (array) $subpanels;
                foreach ($scp_modules as $module => $view_module) {
                    if (array_key_exists($module, $temp_subpanel) && $module != $module_name) {   /***** Change***/
                        foreach ($subpanels->$module as $subpanelsname) {
                            $add = 0;
                            if (($_SESSION['module_action_array'][$module]['create'] == 'true') && (($custom_relationships[$module]['Type'] != 'many-to-one' && $module != 'Accounts') || ($custom_relationships[$module]['Type'] == 'many-to-one' && $module != 'Accounts')) ) {
                                $add = 1;
                            }
                            ?>
                            <li class="nav-item <?php echo $module; ?>-subpanel">
                                <a href="JavaScript:void(0);" class='scp_subpanel_heading nav-link link-secondary' data-bs-toggle="tab" data-module='<?php echo $module; ?>' data-subpanel='<?php echo $subpanelsname; ?>'><?php echo $view_module; ?></a>
                                <?php if($add){ 
                                if($subpanelsname){ ?>
                                <a href="#data/<?php echo $module; ?>/edit/Relate/<?php echo $module_name; ?>/Relation/<?php echo $subpanelsname; ?>/<?php echo $id; ?>" class="scp_subpanel_add">
                                    <em class="fa fa-plus-circle" aria-hidden="true"></em>
                                </a>
                                <?php } else { ?>
                                <a href="#data/<?php echo $module; ?>/edit/Relate/<?php echo $module_name; ?>/<?php echo $id; ?>" class="scp_subpanel_add">
                                    <em class="fa fa-plus-circle" aria-hidden="true"></em>
                                </a>
                                <?php } 
                                } ?>
                            </li>
                            <?php
                        }
                    }
                }
            }
        } 
        ?>
    </ul>
    <div class="tab-content case-detais-tab-description" id="tabContent">
    <div class="tab-pane fade case-detail-first-div active show" id="home" role="tabpanel" aria-labelledby="home-tab">
        <?php if($module_name == 'Cases'){ ?>
        <div class="row">
            <div class="col-md-8">
        <?php } ?>
        <div class="case-details-tab">
            <?php
            $currency_symbol = isset($currency_symbol) ? $currency_symbol : '';
            foreach ($results as $k => $vals_lbl) {
                ?>
            <div class="case-details-main-information">
                <h3><?php echo $vals_lbl->lable_value; ?></h3>
                <div class="case-information">
                <?php
                foreach ($vals_lbl->rows as $k_lbl2 => $vals) {
                    foreach ($vals as $key_fields => $val_fields) {
                        $team_arr = array();
                        $labl_val = isset($val_fields->label_value) ? htmlentities($val_fields->label_value) : '';
                        if (( isset($val_fields->name) && $val_fields->name == '' ) || ( $labl_val == NULL || $labl_val == '' )) {
                            continue;
                        }
                        $ar_val = $k_fileds = isset($val_fields->name) ? $val_fields->name : '';
                        $hlink_acc_st = $hlink_acc_ed = '';
                        if ($k_fileds == "modified_user_id" || $k_fileds == "created_by") {
                            continue;
                        }
                        if ($val_fields->type == 'relate' && (!array_key_exists($val_fields->relate_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array
                            continue;
                        }

                        // tootlp displaying heltip. v3.0
                        $tooltip = "";
                        if (isset($val_fields->helptip) && !empty($val_fields->helptip) && $val_fields->helptip != 'null') {
                            $tooltip = "<span data-toggle='tooltip' data-placement='top' data-title='" . $val_fields->helptip . "'><em class='fa fa-info-circle'></em></span>";
                        }

                        switch ($ar_val) {
                            case 'assigned_user_id':
                                $ar_val = 'assigned_user_name';
                                break;
                            case 'modified_user_id':
                                $ar_val = 'modified_by_name';
                                break;
                            case 'created_by':
                                $ar_val = 'created_by_name';
                                break;
                            case 'account_id':
                                $ar_val = 'account_name';
                                break;
                        }

                        $value = isset($record_detail->entry_list[0]->name_value_list->$ar_val->value) ? $record_detail->entry_list[0]->name_value_list->$ar_val->value : '';
                        //Added by BC on 08-jul-2016
                        $dwload = $cls = '';

                        if(!empty($conditionlogic)){
                            $conditionlogickeys = array_keys($conditionlogic);

                            if(in_array($ar_val, $conditionlogickeys)) {
                                $field_selection = $conditionlogic[$ar_val]->origin_field_selection;

                                $datavalue = $record_detail->entry_list[0]->name_value_list->$field_selection->value;
                                $datavalue = $objSCP->unencodeMultienum( $datavalue );

                                if($conditionlogic[$ar_val]->matching_selection == 'Any') {
                                    if((count(array_intersect($datavalue,$conditionlogic[$ar_val]->matching_value)) >= 1)){
                                        if($conditionlogic[$ar_val]->visibility == 1){
                                            continue;
                                        }

                                    } else {
                                        if($conditionlogic[$ar_val]->visibility == 0){
                                            continue;
                                        }
                                    }

                                } else if($conditionlogic[$ar_val]->matching_selection == 'All') {
                                    if((count(array_intersect($datavalue,$conditionlogic[$ar_val]->matching_value)) == count($conditionlogic[$ar_val]->matching_value))){
                                        if($conditionlogic[$ar_val]->visibility == 1){
                                            continue;
                                        }

                                    } else {
                                        if($conditionlogic[$ar_val]->visibility == 0){
                                            continue;
                                        }
                                    }

                                }
                            }

                        }

                        if ($module_name == "AOS_Quotes" && $ar_val == 'rejection_history') {

                            $value = json_decode(html_entity_decode($value));
                            if (is_array($value) && !empty($value)) {
                                $tmp_value = array();
                                foreach ($value as $reason) {
                                    if ($_SESSION['browser_timezone'] != NULL) {
                                        $reason->date = scp_user_time_convert($reason->date);
                                    } else {
                                        $UTC = new DateTimeZone("UTC");
                                        $newTZ = new DateTimeZone($result_timezone);
                                        $date = new DateTime($reason->date, $UTC);
                                        $date->setTimezone($newTZ);
                                        $reason->date = $date->format($objSCP->date_format . " " . $objSCP->time_format);
                                    }
                                    $tmp_value[] = '<b>' . $language_array['lbl_reason'] . ":</b> $reason->rejection_history<br /><date><b>" . $language_array['lbl_date'] . ":</b> $reason->date</date>";
                                }
                                $value = '<span class="reason-history">' . implode('<br /><br />', $tmp_value) . '</span>';
                            }
                        }

                        if (!empty($value)) {
                            $dwload = "<em class='fa fa-download scp-$module_name-font scp-default-font' aria-hidden='true'></em>";
                            $cls = "general-link-btn scp-download-btn";
                        }
                        // for show formate as in sugar crm
                        if ($value != "" && ( $k_fileds == "date_modified" || $k_fileds == "date_start" || $k_fileds == "date_end" || $k_fileds == "date_entered" || $k_fileds == "date_due")) {
                            if ($_SESSION['browser_timezone'] != NULL) {
                                $value = scp_user_time_convert($value);
                            } else {
                                $UTC = new DateTimeZone("UTC");
                                $newTZ = new DateTimeZone($result_timezone);
                                $date = new DateTime($value, $UTC);
                                $date->setTimezone($newTZ);
                                $value = $date->format($objSCP->date_format . " " . $objSCP->time_format);
                            }
                        }
                        if (isset($val_fields->type) && $val_fields->type == 'date') {//for date field
                            $value = (!empty($value)) ? date($objSCP->date_format, strtotime($value)) : '';
                        }
                        if (isset($val_fields->name) && $val_fields->name == "contact_name") {
                            $where_con = "contacts.id = '{$_SESSION['scp_user_id']}'";
                            $record_detail_contact = $objSCP->get_entry_list('Contacts', $where_con);
                            $value = $record_detail_contact->entry_list[0]->name_value_list->name->value;
                        }
                        //Added by BC on 07-aug-2015 for special option case of add cases,update on 11-july-2016

                        if (isset($val_fields->type) && ($val_fields->type == 'enum' || $val_fields->type == 'multienum' || $val_fields->type == 'dynamicenum' || $val_fields->type == 'radioenum')) {
                            if ( $val_fields->type == 'multienum' ) {
                                $tmp_multi_val = array();
                                $tmp_multi_vals = $objSCP->unencodeMultienum( $value );
                                foreach( $tmp_multi_vals as $tmp_multi_val_ ) {
                                    $resultoption = $val_fields->options;
                                    $array = json_decode(json_encode($resultoption), true);
                                    $key = array_search($tmp_multi_val_, array_column($array, 'value'));
                                    $keys = array_keys($array);
                                    $tmp_multi_val[] = (!empty($tmp_multi_val_)) ? $keys[$key] : '';
                                }
                                $value = implode(', ', $tmp_multi_val);
                            } else {
                                $resultoption = $val_fields->options;
                                $array = json_decode(json_encode($resultoption), true);
                                $key = array_search($value, array_column($array, 'value'));
                                $keys = array_keys($array);

                                $value = (!empty($value)) ? $keys[$key] : '';

                            }
                        }

                        if (isset($val_fields->type) && $val_fields->type == 'bool') {
                            if ($value == "1") {
                                $value = $language_array['lbl_yes'];
                            } else {
                                $value = $language_array['lbl_no'];
                            }
                        }
                        if (isset($val_fields->type) && $val_fields->type == 'url' && isset($id) && !empty($id)) {
                            if (strpos($value, 'http') === false && $value != '') {
                                $value = 'http://' . $value;
                            }
                            $hlink_acc_st = "<a title='" . $value . "' href='" . $value . "' target='_blank'>";
                            $hlink_acc_ed = "</a>";
                        }

                        if ($val_fields->type == "relate") {
                            if ($id != "" && array_key_exists($val_fields->relate_module, (array) $_SESSION['module_array']) && (isset($val_fields->related_module) && $val_fields->related_module != $module_name)) { //if relate module is not in active module array and relate module is not with same module
                                $module_name_record = $val_fields->relate_module;
                                $select_fields = array('id', 'name');
                                $record_detail2 = $objSCP->get_relationships($module_name, $id, $val_fields->link, $select_fields);
                                $value = isset($record_detail2->entry_list[0]->name_value_list->name->value) ? $record_detail2->entry_list[0]->name_value_list->name->value : $value;
                            }
                        }

                        if (isset($val_fields->type) && ($val_fields->type == 'file' || $val_fields->type == 'image') && isset($id) && !empty($id)) {
                            if (!empty($value)) {
                                if ($module_name == "Notes") {
                                    $hlink_acc_st .= "<a title='" . $language_array['lbl_download'] . "' href='javascript:void(0);' onclick='form_submit_note_document(\"$id\",\"$val_fields->name\");' class='$cls scp-$module_name-font'>";
                                    $hlink_acc_ed = "</a>";
                                    $hlink_acc_ed .= "<a title='" . $language_array['lbl_preview'] . "' href='" . admin_url() . "admin-post.php?action=bcp_get_note_attachment&scp_note_id=$id&scp_note_field=$val_fields->name&scp_note_view=1' target='__blank' class='" . $cls . " scp-Notes-font' title='".$language_array['lbl_preview']."'>";
                                    $hlink_acc_ed .= '<span class="fa fa-eye preview-cstm" ></span>';
                                    $hlink_acc_ed .= "</a>";
                                }
                                if ($module_name == "Documents") {
                                    $hlink_acc_st .= "<a title='" . $language_array['lbl_download'] . "' href='javascript:void(0);' onclick='form_submit_document(\"$id\");' class='$cls scp-$module_name-font'>";
                                    $hlink_acc_ed = "</a>";
                                    $hlink_acc_ed .= "<a title='" . $language_array['lbl_preview'] . "' href='" . admin_url() . "admin-post.php?action=bcp_get_doc_attachment&module_name=$module_name&scp_doc_id=$id&scp_doc_view=1' target='__blank' class='$cls scp-Notes-font' title='".$language_array['lbl_preview']."'>";
                                    $hlink_acc_ed .= '<span class="fa fa-eye preview-cstm" ></span>';
                                    $hlink_acc_ed .= "</a>";
                                }
                                if($val_fields->type == 'image') {
                                    $hlink_acc_st .= "<a title='" . $language_array['lbl_download'] . "' href='javascript:void(0);' onclick='form_submit_document(\"$id\",\"\",\"$val_fields->name\",\"$module_name\");' class='$cls scp-$module_name-font'>";
                                    $hlink_acc_ed = "</a>";
                                    $hlink_acc_ed .= "<a title='" . $language_array['lbl_preview'] . "' href='" . admin_url() . "admin-post.php?action=bcp_get_doc_attachment&module_name=$module_name&scp_doc_id=$id&scp_doc_view=1&scp_doc_field_name=$val_fields->name' target='__blank' class='$cls scp-Notes-font' title='".$language_array['lbl_preview']."'>";
                                    $hlink_acc_ed .= '<span class="fa fa-eye preview-cstm" ></span>';
                                    $hlink_acc_ed .= "</a>";
                                }
                            } else {
                                $hlink_acc_st .= '-';
                            }
                        }
                        //for team names
                        if ($k_fileds == "team_name") {
                            foreach ($value as $teams) {
                                $team_arr[] = $teams->name;
                            }
                            $value = implode(', ', $team_arr);
                        }
                        if ($val_fields->name == "currency_id") {//see currency symbol as in add and edit page
                            if (isset($_SESSION['Currencies'])) {
                                $curr_arry = (array) $_SESSION['Currencies'];
                                foreach ($curr_arry as $k_curr => $v_curr) {
                                    if ($k_curr == $value) {
                                        $value = $v_curr->name . " : " . $v_curr->symbol;
                                    }
                                }
                            }
                        }
                        if ($ar_val == 'duration' && ( $value == '' || $value == NULL )) {
                            $value = ( isset($record_detail->entry_list[0]->name_value_list->duration_hours->value) ? html_entity_decode($record_detail->entry_list[0]->name_value_list->duration_hours->value) : '00' ) . 'h ' . ( isset($record_detail->entry_list[0]->name_value_list->duration_minutes->value) ? html_entity_decode($record_detail->entry_list[0]->name_value_list->duration_minutes->value) : '00' ) . 'm';
                        }
                        if (isset($val_fields->type) && $val_fields->type == 'currency' && !empty($value)) {//Added by BC on 01-07-2016 for currency
                            $value = $currency_symbol . number_format($value, 2);
                        }
                        if ($module_name == 'AOS_Products' && $val_fields->name == 'product_image') {
                            // Check if image exits in folder, if yes than save it in session
                            $glob_all_images = glob(plugin_dir_path(__FILE__) . "../assets/images/product-images/$id.*");
                            foreach ($glob_all_images as $tmp_image_path) {
                                $_SESSION["product-images"][$id] = IMAGES_URL . 'product-images/' . basename($tmp_image_path);
                            }
                            $tmp_image_url = isset($_SESSION["product-images"][$id]) && $_SESSION["product-images"][$id] != '' ? $_SESSION["product-images"][$id] : '';
                            if ($tmp_image_url == '' || !is_pro_img_exist($tmp_image_url)) {

                                $base64_image = $objSCP->getProductsImages($id);
                                if (isset($base64_image->records->image) && $base64_image->records->image != '') {
                                    $tmp_image_url = base64_to_image($base64_image->records->image, 'product-images', $id);
                                }
                            }

                            if ($tmp_image_url != '') {
                                $_SESSION["product-images"][$id] = $tmp_image_url;
                                $value = "<img src='$tmp_image_url' class='scp_product_image' />";
                            } else {
                                $value = "";
                            }
                        }
                        // nl2br and exclude tags
                        $value = html_entity_decode($value);
                        $value = nl2br($value);
                        $value = strip_tags($value, '<br /><br><br/>');
                        if ($value == '' || $value == NULL) {
                            $value = '-';
                        }
                        if (isset($val_fields->type) && ($val_fields->type == 'file' || $val_fields->type == 'image') && !empty($value)) {//Added by BC on 15-jul-2016
                            ?>
                            <div class='d-flex'>
                                <label><?php echo rtrim($labl_val, ':') . " : " . $tooltip; ?></label>
                                <p class='data-view'><?php echo $value . ' ' . $hlink_acc_st . $dwload . $hlink_acc_ed; ?></p>
                            </div>
                        <?php } else { ?>
                            <div class='d-flex'>
                                <label><?php echo rtrim($labl_val, ':') . " : " . $tooltip; ?></label>
                                <p class='data-view'><?php echo $hlink_acc_st . $value . $hlink_acc_ed; ?></p>
                            </div>
                        <?php
                    }
                }
            }
            ?>
                </div>
            </div>
        <?php } ?>
        </div>
        <?php if($module_name == 'Cases'){ ?>
            </div>
            <div class="col-md-4">
                <div id='case-updates' class='all-notes scp-section-heading'>
                    <?php include( TEMPLATE_PATH . 'bcp_case_updates_v6.php'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <?php
    if ($module_name == "bc_proposal") {
        $proposal_lineitem = isset($record_detail->entry_list[0]->name_value_list->line_items->value) ? html_entity_decode($record_detail->entry_list[0]->name_value_list->line_items->value) : "";
        if($proposal_lineitem){
            $proposal_lineitem = json_decode($proposal_lineitem);
            $product_lineitem = $proposal_lineitem->product_lineitem;
            $service_lineitem = $proposal_lineitem->service_lineitem;
            if (!empty($service_lineitem) || !empty($product_lineitem)) {
                ?>
                <div class='col-md-6 case-detail-thired-div'>
                    <div class='bg-white shadow border-radius-5'>
                        <div class='case-details-description'>
                            <?php
                            $product_array = array();
                            $qty_array = array();
                            foreach ($product_lineitem as $key => $value) {
                                array_push($product_array, $value->id);
                                $qty_array[$value->id] = $value->qty;
                            }
                            $product_string = implode("','", $product_array);

                            $old_module_name = $module_name;
                            $module_name = 'AOS_Products';
                            $view = "list";
                            $contact_group_id = isset($_SESSION['contact_group_id']) ? $_SESSION['contact_group_id'] : "";
                            $sql_count = "SELECT json_data FROM " . $wpdb->prefix . "module_layout WHERE version ='" . $sugar_crm_version . "' AND module_name='" . $module_name . "' AND layout_view='" . $view . "' AND contact_group='" . $contact_group_id . "' AND lang_code='" . $lang_code . "'";
                            $view = 'detail';
                            $result_count = $wpdb->get_row($sql_count);
                            $json_data = $result_count->json_data;
                            $results = json_decode($json_data);
                            $where_con = strtolower($module_name) . ".id IN ('" . $product_string . "')";
                            $order_by_query = "";
                            $offset = '0';
                            $limit = get_option('biztech_scp_case_per_page');
                            //End by BC on 20-jun-2016-----------------
                            if (!empty($results) || $module_name == 'AOK_KnowledgeBase') {
                                if (isset($order_by) == true && !empty($order_by) && isset($order) == true && !empty($order)) {
                                    $order_by_query = "$order_by $order";
                                } else {
                                    $order_by_query = "date_entered desc";
                                }
                                $id_show = false;
                                foreach ($results as $key_n => $val_n) {
                                    if ($key_n == 'ID') {
                                        $id_show = true;
                                    }

                                    if (!empty($val_n->related_module) && $val_n->type == 'relate' && (!in_array($val_n->related_module, (array) $_SESSION['module_array']))) {//if relate module is not in active module array
                                        continue;
                                    }

                                    $n_array[] = strtolower($key_n);
                                }
                                if (!in_array('id', $n_array)) {
                                    array_push($n_array, 'id');
                                }
                                array_push($n_array, 'currency_id');
                            }
                            $list_result = $objSCP->get_entry_list($module_name, $where_con, $n_array, $order_by_query, 0, '-1');

                            if (is_array((array) $_SESSION['module_array'])) {//Added for getting module key OR lable added from sugar side
                                $arry_lable = (array) $_SESSION['module_array'];
                                $product_name_label = $arry_lable[$module_name];
                            }
                            $cnt = count($list_result->entry_list);

                            if ($cnt > 0 && !empty($results) && isset($_SESSION['module_array']['AOS_Products'])) {
                                ?>
                                <h3><?php echo $product_name_label; ?></h3>
                                <div class='table-responsive'>
                                    <table id='example' class='display table table-striped table-hover' cellspacing='0' width='100%'>
                                <?php include( TEMPLATE_PATH . 'bcp_product_list_page.php'); ?>
                                    </table>
                                </div>
                <?php
            }
            $module_name = $old_module_name;

            if (!empty($service_lineitem)) {
                ?>
                                <h3><?php echo $language_array['lbl_service']; ?></h3>
                                <div class='table-responsive'>
                                    <table id='example' class='display table table-striped table-hover' cellspacing='0' width='100%'>
                                        <thead>
                                            <tr class='main-col'>
                                                <th><?php echo $language_array['lbl_service_name']; ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                <?php foreach ($service_lineitem as $key => $value) { ?>
                                                <tr>
                                                    <td><?php echo htmlentities($value->name); ?></td>
                                                </tr>
                <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                <?php } ?>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
    }
    if ($module_name == 'AOS_Quotes' || $module_name == 'AOS_Invoices' || $module_name == 'AOS_Contracts') { // To get all line-items for quote, invoice and contract
        if ($module_name == 'AOS_Invoices' || $module_name == 'AOS_Contracts') {
            $quote_entry = $objSCP->get_entry_quotes($module_name, $id);
        } else {
            $quote_entry = $record_detail;
        }
        if(!empty($quote_entry->Result->ProductBundle[0]->Product)) {
            include( TEMPLATE_PATH . 'bcp_view_page_v6_product_bundle.php');
        }
    }


        if ($module_action_array[$module_name]['subpanel'] == 'true' && ((array) $subpanels)) {
        ?>
        <div class="tab-pane fade subpanel-div" role="tabpanel" aria-labelledby="tab">
            <div class='scp_subpanel_body'></div>
        </div>
        <script>
            jQuery('#home-tab').click(function () {
                jQuery('.nav-link.link-secondary').removeClass('active');
                jQuery(this).addClass('active');
                jQuery('.subpanel-div').removeClass('active show');
                jQuery('.case-detail-first-div').addClass('active show');
            });
            jQuery('.scp_subpanel_heading').click(function () {
                jQuery('.nav-link.link-secondary').removeClass('active');
                jQuery('.case-detail-first-div').removeClass('active show');
                jQuery('.subpanel-div').addClass('active show');
                jQuery(this).toggleClass('active');
                if (jQuery(this).hasClass('active')) {
                    jQuery('.scp_subpanel_body').html('');
                    var respondDiv = jQuery('.scp_subpanel_body');
                    jQuery('.tab-content .scp_subpanel_body').addClass('bcp-loader');
                    var modulename = jQuery(this).attr('data-module');
                    var subpanelname = jQuery(this).attr('data-subpanel');
                    var data = {
                        'action': 'bcp_list_module',
                        'view': 'detail',
                        'modulename': modulename,
                        'subpanelname': subpanelname,
                        'relate_to_module': '<?php echo $module_name ?>',
                        'relate_to_id': '<?php echo $id ?>',
                    };
                    jQuery.post(ajaxurl, data, function (response) {
                        jQuery('.tab-content .scp_subpanel_body').removeClass('bcp-loader');
                        if (jQuery.trim(response) != '-1') {
                            respondDiv.html(response);
                        } else {
                            var redirect_url = $(location).attr('href') + '?conerror=1';
                            window.location.href = redirect_url;
                        }
                    });
                }
            });
        </script>
    <?php if (isset($_REQUEST['response_module']) && $_REQUEST['response_module'] != "") { ?>
            <script>
                jQuery(document).ready(function () {
                    jQuery(".<?php echo $_REQUEST['response_module']; ?>-subpanel .scp_subpanel_heading").trigger("click");
                });
            </script>
        <?php
    }
}


if ($module_name == 'AOS_Quotes') {
    ?>
    <button type="button" class="btn btn-primary quote-ajax-modal" data-toggle="modal" data-target="#quote-ajax-modal" style="display: none;"></button>

<!-- Modal -->
<div class="modal fade" id="quote-ajax-modal" tabindex="-1" role="dialog" aria-labelledby="reasonModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="<?php echo admin_url('admin-post.php'); ?>" method="post" enctype="multipart/form-data" id="quote_decline_form_id" class="add-record-form">
                <div class="modal-header">
                    <h5 class="modal-title" id="reasonModalLabel"><?php echo $language_array['lbl_reason_for_decline']; ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="response-message"></div>
                    <div class="form-body">
                        <div class="form-group">
                            <textarea class="decline-reson input-text form-control required" name="decline-reson" required="required"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="action" value="bcp_quote_accept_decline_btn" />
                    <input type="hidden" name="subject" class="quote_subject" value="" />
                    <input type="hidden" name="modulename" value="AOS_Quotes" />
                    <input type="hidden" name="id" class="quote_module_id" value="" />
                    <input type="hidden" name="action_performed" value="Decline" />
                    <input type="hidden" name="current_url" class="quote_current_url" value="" />

                    <input type="button" value="<?php echo $language_array['lbl_cancel']; ?>" class="hover active scp-button action-form-btn btn btn-gray btn-cncl" data-dismiss="modal" />
                    <input type="submit" value="<?php echo $language_array['lbl_save']; ?>" class="hover active scp-button action-form-btn scp-Quotes btn btn-blue" />

                </div>
            </form>
        </div>
    </div>
</div>
    <script>
        jQuery( "#quote_decline_form_id" ).validate();

        jQuery("#quote_decline_form_id").submit(function( e ){
            debugger;
            var decline_reason = $(".decline-reson").val().trim();
            $(".decline-reson").val(decline_reason);
            if (decline_reason == "" || decline_reason == null) {
               return false;
            }
            var $form = $(this);
            if( ! $form.valid() ) {
                App.unblockUI(el);
                return false;
            }

            var contents = new FormData(jQuery(this)[0]);
            var current_url = jQuery(".quote_current_url").val();
            var el = jQuery("#quote_decline_form_id");
            jQuery.ajax({
                url: "<?php echo admin_url('admin-ajax.php'); ?>?action=bcp_quote_accept_decline_btn",
                type: "POST",
                async:true,
                data: contents,
                async: true,
                cache: false,
                contentType: false,
                processData: false,
                beforeSend : function() {
                    App.blockUI(el);
                },
                success: function (data) {
                    App.unblockUI(el);
                    jQuery("#quote-ajax-modal").modal("toggle");
                    var response = jQuery.parseJSON(data);
                    if (response["data"]["success"]) {
                        bcp_module_call_view("AOS_Quotes", jQuery(".quote_module_id").val(), "detail", current_url);
                    } else {
                        document.getElementById("succid").innerHTML = response["data"]["msg_show"];
                        jQuery("#succid").fadeIn();
                        setTimeout(function () {
                            jQuery("#succid").fadeOut();
                        }, 2000);
                    }
                }
            });
            return false;
        });
    </script>
<?php } ?>
            
        </div>
    </div>
</div>
</div>
    
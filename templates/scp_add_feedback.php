<div class="row">
    <div class="col-xl-12">
        <div class="bg-white shadow change-password-block feedback-form">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-xl-4">

                    <h2 class="fa feedback side-icon-wrapper scp-Feedback-font scp-default-font"><?php echo $language_array['lbl_feedback']; ?></h2>

                    <?php if (isset($_SESSION['bcp_add_record']) && $_SESSION['bcp_add_record'] != '') { ?>
                        <span class='success' id='succid'><?php echo $_SESSION['bcp_add_record']; ?></span>
                        <?php
                        unset($_SESSION['bcp_add_record']);
                    }
                    ?>
                    <div class="scp-form scp-form-2-col">
                        <form action="<?php echo site_url(); ?>/wp-admin/admin-post.php" method="post" enctype="multipart/form-data" id="general_form_id" novalidate="novalidate">
                            <div class="scp-form-container">
                                <div class="panel Feedback Details scp-dtl-panel">
                                    <div class="form-group">
                                        <div class="d-flex align-items-center">
                                            <label><?php echo $language_array['lbl_subject']; ?>: </label><span class="required">*</span>
                                        </div>
                                        <input class="input-text form-control  avia-datepicker-div" type="text" id="name" name="name" required="" aria-required="true">

                                    </div>
                                    <div class="form-group">
                                        <div class="d-flex align-items-center">
                                            <label><?php echo $language_array['lbl_description']; ?>: </label><span class="required">*</span>
                                        </div>
                                        <textarea class="input-text form-control" id="" required="" name="description" cols="30" rows="2"></textarea>

                                    </div>
                                </div>
                                <div class="case-button-group">
                                    <?php wp_nonce_field('sfcp_portal_nonce', 'sfcp_portal_nonce_field'); ?>
                                    <input type="hidden" name="action" value="bcp_add_moduledata_call">
                                    <input type="hidden" name="module_name" value="bc_feedback">
                                    <input type="hidden" name="view" value="edit">
                                    <input type="hidden" name="current_url" value="detail">
                                    <span>
                                        <input type="submit" value="<?php echo $language_array['lbl_submit'] ?>" class="scp-button action-form-btn scp-Accounts btn btn-blue">
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    jQuery.extend(jQuery.validator.messages, {
        required: "<?php echo $language_array['msg_field_required']; ?>",
        email: "<?php echo $language_array['msg_enter_valid_email']; ?>",
    });
</script>

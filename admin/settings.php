<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if (!function_exists('scp_create_menu')) {
    add_action('admin_menu', 'scp_create_menu');
    add_action('network_admin_menu', 'scp_create_menu');
    function scp_create_menu() {

        //create admin side menu
        add_menu_page(__('Customer Portal'), __('Customer Portal'), 'administrator', 'biztech-crm-portal', 'sugar_crm_portal_settings_page');
        //call register settings function
        add_action('admin_init', 'register_sugar_crm_portal_settings');
    }

}

if (!function_exists('register_sugar_crm_portal_settings')) {
    
    function register_sugar_crm_portal_settings() {

        $active_tabs = isset($_REQUEST['tab']) ? $_REQUEST['tab'] : "authentication_tab";
        //register our settings
        if ($active_tabs == "authentication_tab") {
            if (!is_multisite()) {
                register_setting('sugar_crm_portal-settings-group', 'biztech_scp_rest_url');
                register_setting('sugar_crm_portal-settings-group', 'biztech_scp_username');
                register_setting('sugar_crm_portal-settings-group', 'biztech_scp_sugar_crm_version');
            }
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_name');
        }
        
        if ($active_tabs == "general_tab") {
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_portal_menu_title');
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_case_per_page');
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_tab_active');
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_tab_inactive');
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_upload_image');
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_single_signin'); //for Single Sign-in
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_signup_link_enable'); //for enable signup page access
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_otp_enable'); // Set enable otp
            register_setting('sugar_crm_portal-settings-group', 'biztech_portal_user_approval'); //for save user approval option
            register_setting('sugar_crm_portal-settings-group', 'biztech_portal_enable_user_verification'); //for save case deflection option
        }
        
        if ($active_tabs == "layout_tab") {
            register_setting('sugar_crm_portal-settings-group', 'biztech_portal_template'); //for save portal template
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_theme_color'); //for theme color
            register_setting('sugar_crm_portal-settings-group', 'bcp_portal_menuLocation'); //for theme color
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_calendar_calls_color');
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_calendar_meetings_color');
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_calendar_tasks_color');
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_custom_css');
            
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_recent_activity');
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_counter');
        }
        
        if ($active_tabs == "pages_tab") {
            register_setting('sugar_crm_portal-settings-group', 'biztech_redirect_login'); //for redirect page for login
            register_setting('sugar_crm_portal-settings-group', 'biztech_redirect_signup'); //for redirect page for sign up
            register_setting('sugar_crm_portal-settings-group', 'biztech_redirect_profile'); // for redirect page for profile chnages
            register_setting('sugar_crm_portal-settings-group', 'biztech_redirect_forgotpwd'); //for redirect page for forgot passwords
            register_setting('sugar_crm_portal-settings-group', 'biztech_redirect_resetpwd'); //for redirect page for manage page
            register_setting('sugar_crm_portal-settings-group', 'biztech_redirect_manange'); //for redirect page for manage page
        }
        
        /* Added by dharti.gajera 13-04-2022 */
        if ($active_tabs == "password_tab") {
            register_setting('sugar_crm_portal-settings-group', 'bcp_login_password_enable'); //for save password enable option
            register_setting('sugar_crm_portal-settings-group', 'bcp_maximum_login_attempts'); //for save password maximum login attempts option
            register_setting('sugar_crm_portal-settings-group', 'bcp_lockout_effective_period'); //for save password lockout effective period option
        }

        if ($active_tabs == "recaptcha_tab") {
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_recaptcha_site_key'); //for save reCaptcha Site Key option
            register_setting('sugar_crm_portal-settings-group', 'biztech_scp_recaptcha_secret_key'); //for save reCaptcha Secret Key option
            register_setting('sugar_crm_portal-settings-group', 'biztech_portal_visible_reCAPTCHA'); //for save reCaptcha visible in page option
        }
        
        if (is_multisite() && !is_network_admin()) {
            register_setting('sugar_crm_portal-settings-group', 'biztech_disable_portal');        //disable portal option only for network subsite
        }
    }
}

if (!function_exists('sugar_crm_portal_settings_page')) {
    function sugar_crm_portal_settings_page() {
        
        global $bcp_framework;
        $check_var = _isCurl();
        
        wp_enqueue_media();
        wp_enqueue_style('thickbox');
        wp_enqueue_script('thickbox');
        add_thickbox();
        $faq_text = __('Possible Reasons');
        $faq_url = '/#TB_inline?&inlineId=FAQ';
        $menu_tabs = array(
            "authentication_tab"    => __("Authentication"),
            "general_tab"           => __("General Settings"),
            "layout_tab"            => __("Layout Settings"),
            "pages_tab"             => __("Page Settings"),
            /* Added by dharti.gajera 13-04-2022 */
            "password_tab"             => __("Password Settings"),
            "recaptcha_tab"         => __("reCaptcha Settings")
        );
        $active_tabs = isset($_REQUEST['tab']) && array_key_exists($_REQUEST['tab'], $menu_tabs) ? $_REQUEST['tab'] : "authentication_tab";
        ?>
        <div class="wrap bcp_portal">
        <?php
        if ($check_var == 'yes') {//Added by BC on 06-jun-2016 for CURL CHECKING
            if (isset($_POST['action']) && $_POST['action'] == 'update_sugarportal_settings') { //check user pressed save button or not (for multisite)?
                global $wpdb;
                $myallkeysnetwork = array(
                    'biztech_scp_name',
                    'biztech_scp_case_per_page',
                    'biztech_scp_upload_image',
                    'biztech_scp_calendar_calls_color',
                    'biztech_scp_calendar_meetings_color',
                    'biztech_scp_custom_css',
                    'biztech_scp_portal_menu_title',
                    'biztech_scp_theme_color',
                    'biztech_scp_signup_link_enable',
                    'biztech_scp_otp_enable', // Set enable otp
                    'biztech_portal_template',
                    'biztech_multi_signin',
                    'biztech_portal_calendar',
                    'biztech_portal_user_approval',
                    'biztech_portal_enable_user_verification'
                );
                $myallkeyssub = array(
                    'biztech_scp_sugar_crm_version',
                    'biztech_scp_rest_url',
                    'biztech_scp_username',
                    'biztech_scp_password'
                );
                $myallkeysubpages = array(
                    array('biztech_redirect_signup', 'portal-sign-up'),
                    array('biztech_redirect_login', 'portal-login'),
                    array('biztech_redirect_profile', 'portal-profile'),
                    array('biztech_redirect_forgotpwd', 'portal-forgot-password'),
                    array('biztech_redirect_resetpwd', 'portal-forgot-password'),
                    array('biztech_redirect_manange', 'portal-manage-page'),
                );
                
                $blogids = $wpdb->get_results("SELECT blog_id FROM {$wpdb->blogs} WHERE site_id = '{$wpdb->siteid}' AND spam = '0' AND deleted = '0' AND archived = '0'");  //get all available sub websites
                if ( isset( $_POST['biztech_scp_password'] ) && $_POST['biztech_scp_password'] ) {
                    $_POST['biztech_scp_password'] = scp_encrypt_string($_POST['biztech_scp_password']);
                } else {
                    $_POST['biztech_scp_password'] = get_option('biztech_scp_password');
                }

                foreach ($blogids as $blog) {
                    switch_to_blog($blog->blog_id); //switch to all websites one by one
                    foreach ($myallkeyssub as $j) {  //update all options
                        update_option($j, $_POST[$j]);
                    }
                    foreach ($myallkeysubpages as $j) {   //when first installed check if option exists for page otherwise set it
                        if (!get_option($j[0])) {
                            $page = get_page_by_path($j[1]);
                            add_option($j[0], $page->ID);
                        }
                    }
                    restore_current_blog(); //restore to current blog, so that other options can't conflict
                }

                foreach ($myallkeysnetwork as $j) {  //update all options
                    if (isset($_POST[$j])) {  //if we fetch value than set it otherwise set to null
                        update_site_option($j, $_POST[$j]);
                    } else {
                        update_site_option($j, '');
                    }
                }
            }
            
            if (is_network_admin()) { //for Multisite, network side only

                $crmversionkey = get_site_option('biztech_scp_sugar_crm_version');
                $crmresturl = get_site_option('biztech_scp_rest_url');
                $crmusername = get_site_option('biztech_scp_username');
                $crmpassword = get_option('biztech_scp_password');

                $crmscpname = get_option('biztech_scp_name');
                $crmcaseperpage = ((get_option('biztech_scp_case_per_page') != NULL) ? get_option('biztech_scp_case_per_page') : "5");
                
                $crmmobileportal = ((get_option('biztech_scp_portal_menu_title') != NULL) ?
                                get_option('biztech_scp_portal_menu_title') : "Portal Menu" );
                $crmthemecolor = ((get_option('biztech_scp_theme_color') != NULL) ? get_option('biztech_scp_theme_color') : "" );
                $crmcalendarcall = ((get_option('biztech_scp_calendar_calls_color') != NULL) ?
                                get_option('biztech_scp_calendar_calls_color') : "#7d9e12" );
                $crmcalendarmetting = ((get_option('biztech_scp_calendar_meetings_color') != NULL) ?
                                get_option('biztech_scp_calendar_meetings_color') : "#00585e" );
                $crmcalendartask = ((get_option('biztech_scp_calendar_tasks_color') != NULL) ?
                                get_option('biztech_scp_calendar_tasks_color') : "#3a87ad" );
                $crmcustomcss = ((get_option('biztech_scp_custom_css') != NULL) ? get_option('biztech_scp_custom_css') : "" );
                $recentactivity = ((get_option('biztech_scp_recent_activity') != NULL) ? get_option('biztech_scp_recent_activity') : "" );
                $biztechcounter = ((get_option('biztech_scp_counter') != NULL) ? get_option('biztech_scp_counter') : "" );
                $crmtemplate = (get_option('biztech_portal_template') != NULL) ? get_option('biztech_portal_template') : "1";
                ( get_option('biztech_scp_signup_link_enable') === FALSE ? update_site_option( 'biztech_scp_signup_link_enable', '1' ) : '' ); // If signup page enable option is not available than set it
                $enable_signup_link = get_option('biztech_scp_signup_link_enable');
                ( get_option('biztech_scp_otp_enable') === FALSE ? update_site_option( 'biztech_scp_otp_enable', '1' ) : '' ); // If is not available than enable it
                $biztech_scp_otp_enable = get_option('biztech_scp_otp_enable');
                $crmmultisignin = (get_option('biztech_multi_signin') != NULL) ? get_option('biztech_multi_signin') : "";
                $biztech_portal_user_approval = (get_option('biztech_portal_user_approval') != NULL) ? get_option('biztech_portal_user_approval') : "";
                $biztech_portal_enable_user_verification = (get_option('biztech_portal_enable_user_verification') != NULL) ? get_option('biztech_portal_enable_user_verification') : ""; // For enable case deflection
                $biztech_scp_recaptcha_site_key = (get_option('biztech_scp_recaptcha_site_key') != NULL) ? get_option('biztech_scp_recaptcha_site_key') : "";
                $biztech_scp_recaptcha_secret_key = (get_option('biztech_scp_recaptcha_secret_key') != NULL) ? get_option('biztech_scp_recaptcha_secret_key') : "";
                $biztech_portal_visible_reCAPTCHA = (get_option('biztech_portal_visible_reCAPTCHA') != NULL) ? get_option('biztech_portal_visible_reCAPTCHA') : array();
                //$crmmultidomain = (get_option('biztech_scp_muiltidomain') != NULL) ? get_option('biztech_scp_muiltidomain') : "";
            } else {   //for subsite and signle site only

                $crmversionkey = get_option('biztech_scp_sugar_crm_version');
                $crmresturl = get_option('biztech_scp_rest_url');
                $crmusername = get_option('biztech_scp_username');
                $crmpassword = get_option('biztech_scp_password');

                $crmscpname = get_option('biztech_scp_name');
                
                if(get_option('biztech_scp_case_per_page')){
                    $crmcaseperpage = get_option('biztech_scp_case_per_page');
                } else {
                    update_option('biztech_scp_case_per_page', '5');
                    $crmcaseperpage = get_option('biztech_scp_case_per_page');
                }
                
                if(get_option('biztech_scp_portal_menu_title')!= NULL){
                    $crmmobileportal = get_option('biztech_scp_portal_menu_title');
                } else {
                    update_option('biztech_scp_portal_menu_title', 'Portal Menu');
                    $crmmobileportal = get_option('biztech_scp_portal_menu_title');
                }
                
                if(get_option('biztech_scp_theme_color')!= NULL){
                     $crmthemecolor = get_option('biztech_scp_theme_color');
                } else {
                    update_option('biztech_scp_theme_color', '');
                    $crmthemecolor = get_option('biztech_scp_theme_color');
                }
                
                if(get_option('biztech_scp_calendar_calls_color')!= NULL){
                     $crmcalendarcall = ((get_option('biztech_scp_calendar_calls_color') != NULL) ?get_option('biztech_scp_calendar_calls_color') : (!is_multisite() ? "#7d9e12" : '') );  //clear option work in subsite
                } else {
                    update_option('biztech_scp_calendar_calls_color', '#7d9e12');
                    $crmcalendarcall = get_option('biztech_scp_calendar_calls_color');
                }
                
                if(get_option('biztech_scp_calendar_meetings_color')!= NULL){
                     $crmcalendarmetting = ((get_option('biztech_scp_calendar_meetings_color') != NULL) ? get_option('biztech_scp_calendar_meetings_color') : (!is_multisite() ? "#00585e" : '' ) );
                } else {
                    update_option('biztech_scp_calendar_meetings_color', '#3a87ad');
                    $crmcalendarmetting = get_option('biztech_scp_calendar_meetings_color');
                }
                
                if(get_option('biztech_scp_calendar_tasks_color')!= NULL){
                     $crmcalendartask = ((get_option('biztech_scp_calendar_tasks_color') != NULL) ? get_option('biztech_scp_calendar_tasks_color') : (!is_multisite() ? "#3a87ad" : '' ) );
                } else {
                    update_option('biztech_scp_calendar_tasks_color', '');
                    $crmcalendartask = get_option('biztech_scp_calendar_tasks_color');
                }
                
                if(get_option('biztech_scp_custom_css')!= NULL){
                     $crmcustomcss = get_option('biztech_scp_custom_css');
                } else {
                    update_option('biztech_scp_custom_css', '');
                    $crmcustomcss = get_option('biztech_scp_custom_css');
                }
                
                if(get_option('biztech_portal_template')!= NULL){
                    $crmtemplate = get_option('biztech_portal_template');
                } else {
                    update_option('biztech_portal_template', '1');
                    $crmtemplate = get_option('biztech_portal_template');
                }
                
                
                ( get_option('biztech_scp_signup_link_enable') === FALSE ? update_option( 'biztech_scp_signup_link_enable', '1' ) : '' ); // If signup page enable option is not available than set it
                $enable_signup_link = get_option('biztech_scp_signup_link_enable');
                
                 
                if(get_option('biztech_scp_otp_enable')=== FALSE){
                    update_option('biztech_scp_otp_enable', '1');
                    $biztech_scp_otp_enable = get_option('biztech_scp_otp_enable');
                } else {
                    $biztech_scp_otp_enable = get_option('biztech_scp_otp_enable');
                }
                
                
                if(get_option('biztech_portal_user_approval')!= NULL){
                    $biztech_portal_user_approval = get_option('biztech_portal_user_approval');
                } else {
                    update_option('biztech_portal_user_approval', '');
                    $biztech_portal_user_approval = get_option('biztech_portal_user_approval');
                }
                
                if(get_option('biztech_portal_enable_user_verification')!= NULL){
                    $biztech_portal_enable_user_verification = get_option('biztech_portal_enable_user_verification'); // For enable case deflection
                } else {
                    update_option('biztech_portal_enable_user_verification', '');
                    $biztech_portal_enable_user_verification = get_option('biztech_portal_enable_user_verification');
                }
                
                if(get_option('biztech_scp_recaptcha_site_key')!= NULL){
                    $biztech_scp_recaptcha_site_key = get_option('biztech_scp_recaptcha_site_key');
                } else {
                    update_option('biztech_scp_recaptcha_site_key', '');
                    $biztech_scp_recaptcha_site_key = get_option('biztech_scp_recaptcha_site_key');
                }
                
                if(get_option('biztech_scp_recaptcha_secret_key')!= NULL){
                    $biztech_scp_recaptcha_secret_key = get_option('biztech_scp_recaptcha_secret_key');
                } else {
                    update_option('biztech_scp_recaptcha_secret_key', '');
                    $biztech_scp_recaptcha_secret_key = get_option('biztech_scp_recaptcha_secret_key');
                }
                
                if(get_option('biztech_portal_visible_reCAPTCHA')!= NULL){
                    $biztech_portal_visible_reCAPTCHA = get_option('biztech_portal_visible_reCAPTCHA');
                } else {
                    update_option('biztech_portal_visible_reCAPTCHA', '');
                    $biztech_portal_visible_reCAPTCHA = (get_option('biztech_portal_visible_reCAPTCHA') != NULL) ? get_option('biztech_portal_visible_reCAPTCHA') : array();
                }
                
                $myallkeysubpages = array(
                    array('biztech_redirect_signup', 'portal-sign-up'),
                    array('biztech_redirect_login', 'portal-login'),
                    array('biztech_redirect_profile', 'portal-profile'),
                    array('biztech_redirect_forgotpwd', 'portal-forgot-password'),
                    array('biztech_redirect_resetpwd', 'portal-forgot-password'),
                    array('biztech_redirect_manange', 'portal-manage-page'),
                );
                
                foreach ($myallkeysubpages as $j) {
                    if (!get_option($j[0])) {
                        $page = get_page_by_path($j[1]);
                        add_option($j[0], $page->ID);
                    }
                }
                
            }
            $connection_first_time = 0;
            if ( ! $crmversionkey && ! $crmresturl && ! $crmusername && ! $crmpassword ) {
                $connection_first_time = 1;
            }
            $scp_sugar_password = scp_decrypt_string($crmpassword);
            $biztechcounter = '';
            $recentactivity = '';
            if (class_exists('SugarRestApiCall')) {
                $objSCP = new SugarRestApiCall($crmresturl, $crmusername, $scp_sugar_password);
                if (isset($objSCP->session_id) && $objSCP->session_id != NULL) {                    
                    $ismenable = $objSCP->check_enable_modules();
                    $_SESSION['scp_admin_messages'] = ( isset( $ismenable->portal_connection_message_list ) ) ? $ismenable->portal_connection_message_list : '';
                }
                
                $modules_array = $objSCP->getAllAccessibleModules();
                $data = (array) $modules_array;
                $cnt = 0;
                $top_modules = array();
                $recent_modules = array();
                foreach ($data as $key => $value) {
                    if ($cnt < 4) {
                        array_push($top_modules, $key);
                    }
                    array_push($recent_modules, $key);
                    $cnt++;
                }

                if(empty(get_option('biztech_scp_recent_activity'))){
                    update_option('biztech_scp_recent_activity', $recent_modules);
                    $recentactivity = get_option('biztech_scp_recent_activity');
                } else {
                    $recentactivity = get_option('biztech_scp_recent_activity');
                }

                if(empty(get_option('biztech_scp_counter'))){
                    update_option('biztech_scp_counter', $top_modules);
                    $biztechcounter = get_option('biztech_scp_counter');
                } else {
                    $biztechcounter = get_option('biztech_scp_counter');
                }
                
                if(empty(get_option('es_modules_label'))){
                    update_option( 'es_modules_label',  $data );
                    update_option( 'scp_menu_modules_label',  $data );
                }
            }
            
            
            // Admin side page options
            $actionform = '';
            if (!is_network_admin()) {
                $actionform = "action='options.php'";
            }  //if this is not admin page than show action (for subsite and signle site only)
            ?>
            <h2><?php _e('Customer Portal Settings'); ?></h2>    
            <nav class="nav-tab-wrapper bcp-nav-tab-wrapper">
                <?php
                foreach ($menu_tabs as $key => $value) {
                    $active_class_tab = "";
                    if ($key == $active_tabs) {
                        $active_class_tab = "nav-tab-active";
                    }
                    if ( ( ( $crmversionkey == 6 || $crmversionkey == 5 ) && $objSCP->session_id != '') ||
                        ( $crmversionkey == 7 && $objSCP->access_token != '' ) || $key == "authentication_tab" ) {
                    ?>
                    <a href="<?php echo admin_url().'admin.php?page=biztech-crm-portal&tab='.$key; ?>" class="nav-tab <?php echo $active_class_tab; ?>">
                        <?php echo $value; ?>
                    </a>
                    <?php
                    }
                }
                ?>
            </nav>
            <form method='post' <?php echo $actionform; ?> class="form-wrap">
                <?php
                if (is_network_admin()) { //for Multisite, network side only
                    echo '<input type="hidden" name="action" value="update_sugarportal_settings" />';
                } else {
                    settings_fields('sugar_crm_portal-settings-group');
                    do_settings_sections('sugar_crm_portal-settings-group');
                }

                // added for single site to display message
                if (isset($_GET['settings-updated']) && $_GET['settings-updated'] == "true") {  //show update msg if data updated
                    ?>
                    <div class='updated settings-error hide_class' id='setting-error-settings_updated'>
                    <?php if (!is_multisite()) { ?>
                            <p><strong><?php echo isset( $_SESSION['scp_admin_messages']->conn_success ) ? __( $_SESSION['scp_admin_messages']->conn_success ) : ''; ?></strong></p>
                    <?php } ?>
                        <p><strong><?php echo isset( $_SESSION['scp_admin_messages']->setting_updated ) ? __( $_SESSION['scp_admin_messages']->setting_updated ) : ''; ?></strong></p>
                    </div>
                    <?php
                }
                // added for multisite to display message
                if (isset($_POST['action']) && $_POST['action'] == "update_sugarportal_settings") {  //show update msg if data updated
                    ?>
                    <div class='updated settings-error hide_class' id='setting-error-settings_updated'>
                    <?php if (is_network_admin() && is_multisite()) { ?>
                            <p><strong><?php echo isset( $_SESSION['scp_admin_messages']->conn_success ) ? __( $_SESSION['scp_admin_messages']->conn_success ) : ''; ?></strong></p>
                    <?php } ?>
                        <p><strong><?php echo isset( $_SESSION['scp_admin_messages']->setting_updated ) ? __( $_SESSION['scp_admin_messages']->setting_updated ) : ''; ?></strong></p>
                    </div>
                    <?php } ?>
                <?php
                /**
                 * To prevent delay loading of wp message js issue
                 */
                if ( !class_exists('SugarRestApiCall') ) {
                
                    if ( !$connection_first_time ) {
                    ?>
                    <div class='error settings-error' id='setting-error-settings_updated'>
                        <p><strong><?php _e('Connection not successful. Please check SugarCRM Version, URL, Username and Password.'); ?> <a title="<?php echo $faq_text;?>" href="<?php echo $faq_url; ?>" class="thickbox"><?php echo $faq_text;?></a></strong></p>
                    </div>
                    <?php
                    }
                    if (is_multisite() && !is_network_admin()) {
                        echo "<div class='error settings-error' id='setting-error-settings_updated'>";
                        echo '<p><strong>' . ( isset( $_SESSION['scp_admin_messages']->conn_problem ) ? __( $_SESSION['scp_admin_messages']->conn_problem ) : '' ) . '</strong></p>';
                        echo '<br><p><strong>' . __('You can set all values from') . ' <a href="' . network_site_url() . 'wp-admin/network/admin.php?page=biztech-crm-portal">' . __('here') . '</a>  ' . __('or contact your network administrator') . '</p>';
                        echo '<script type="text/javascript">
                            jQuery("#submit").hide();</script>';
                        echo "</div>";
                    }
                ?>
                <?php
                } else {
                    if ( ( ( $crmversionkey == 6 || $crmversionkey == 5 ) && $objSCP->session_id == '') ||
                        ( $crmversionkey == 7 && $objSCP->access_token == '' ) ) {
                        if (is_multisite() && !is_network_admin()) { // If multisite, network admin
                            ?>
                            <div class="error settings-error" id="setting-error-settings_updated">
                                <p><strong><?php _e('Connection not successful, please set all values from'); ?> <a href="<?php echo network_site_url(); ?>wp-admin/network/admin.php?page=biztech-crm-portal"><?php _e('here'); ?></a> <?php _e('or contact your network administrator.'); ?> <a title="<?php echo $faq_text;?>" href="<?php echo $faq_url; ?>" class="thickbox"><?php echo $faq_text;?></a></strong></p>
                            </div>
                            <script type="text/javascript">jQuery("#submit").hide();</script>
                            <?php
                        } else {
                            if ( ! $connection_first_time )  {  // If connection is not first time
                            ?>
                            <div class="error settings-error" id="setting-error-settings_updated">
                                <p><strong><?php _e('Connection not successful. Please check SugarCRM Version, URL, Username and Password.'); ?> <a title="<?php echo $faq_text;?>" href="<?php echo $faq_url; ?>" class="thickbox"><?php echo $faq_text;?></a></strong></p>
                            </div>
                                <?php
                            }
                        }
                    }
                }
                ?>
                <?php
                if ($active_tabs == "authentication_tab") {
                    if (is_network_admin() || !is_multisite()) {  //check if we are network admin or our site is single site
                        include( ADMIN_TEMPLATE_PATH . 'authentication.php');
                    }
                }
                
                if ($active_tabs == "general_tab") {
                    include( ADMIN_TEMPLATE_PATH . 'general.php');
                }
                
                if ($active_tabs == "layout_tab") {
                    include( ADMIN_TEMPLATE_PATH . 'layout.php');
                }
                
                if (!is_network_admin() && $active_tabs == "pages_tab") {
                    include( ADMIN_TEMPLATE_PATH . 'page.php');
                }
                
                /* Added by dharti.gajera 13-04-2022 */
                if (!is_network_admin() && $active_tabs == "password_tab") {
                    include( ADMIN_TEMPLATE_PATH . 'password.php');
                }
                
                if (!is_network_admin() && $active_tabs == "recaptcha_tab") {
                    include( ADMIN_TEMPLATE_PATH . 'recaptcha.php');
                } ?>
                
                <?php submit_button(); ?>
                <script type="text/javascript">
                    jQuery(document).ready( function() {
                        var click_event=0;
                        jQuery('.thickbox').click(function(event) {
                            window.setTimeout(function(){
                                if(click_event==0){
                                    jQuery('.thickbox').trigger('click');
                                    click_event=1;
                                }
                             }, 5);
                        });
                        /* Added by dharti.gajera 13-04-2022 */
                        jQuery("#bcp_login_password_enable").click(function () {
                            if (jQuery(this).is(":checked")) {
                                jQuery("#bcp_maximum_login_attempts").removeAttr("disabled");
                                jQuery("#bcp_lockout_effective_period").removeAttr("disabled");
                            } else {
                                jQuery("#bcp_maximum_login_attempts").attr("disabled", "disabled");
                                jQuery("#bcp_lockout_effective_period").attr("disabled", "disabled");
                            }
                        });
                        jQuery("#biztech_portal_template").change(function () {
                            var biztech_portal_template = jQuery(this).val();
                            if (biztech_portal_template == 0) {
                                jQuery(".menu-location").addClass('show');
                                jQuery(".menu-location").removeClass('hide');
                            } else {
                                jQuery(".menu-location").removeClass('show');
                                jQuery(".menu-location").addClass('hide');
                            }
                        });
                   });

                    jQuery(function () {
                        jQuery('#submit').click(function (e) {
                            //var txtPortalName = jQuery('#txtPortalName').val();
                            <?php if (is_network_admin() || !is_multisite()) { ?>
                                var biztech_scp_sugar_crm_version = jQuery('#biztech_scp_sugar_crm_version').val();
                                var sugar_crm_url = jQuery('#sugar_crm_url').val();
                                var sugar_username = jQuery('#sugar_username').val();
                                var sugar_password = jQuery('#sugar_password').val();

                                if (biztech_scp_sugar_crm_version == '') {
                                    alert('<?php _e('Please select version') ?>');
                                    return false;
                                } else if (sugar_crm_url == '') {
                                    alert('<?php _e('Please enter CRM URL') ?>');
                                    return false;
                                } else if (sugar_username == '') {
                                    alert('<?php _e('Please enter username') ?>');
                                    return false;
                                }
                            <?php } ?>
                            if (jQuery('#txtPortalName').val().length > 0 && !isNaN(jQuery('#txtPortalName').val())) {
                                alert('<?php _e('Only Numbers are not allowed'); ?>');
                                return false;
                            } else if (jQuery('#txtPortalName').val().length > 0 && !(jQuery('#txtPortalName').val().match(/\w/))) {
                                alert('<?php _e('Only Special characters are not allowed'); ?>');
                                return false;
                            } else {
                                return true;
                            }
                        });
                    });
                </script>
                <input type="hidden" name="tab" value="<?php echo $active_tabs; ?>" >
            </form>
            <?php
            if ( class_exists('SugarRestApiCall') ) {

                if ( ( ( $crmversionkey == 6 || $crmversionkey == 5 ) && $objSCP->session_id != '') ||
                        ( $crmversionkey == 7 && $objSCP->access_token != '' ) ) {
                    if ( ( isset( $ismenable->success ) && $ismenable->success != 1 ) ||
                            ( isset( $ismenable->fronend_framework ) && $ismenable->fronend_framework != $bcp_framework ) ) {
                        if ( $ismenable->success != 1 ) {
                            $error_msg = $ismenable->message;
                        } elseif ( $ismenable->fronend_framework != $bcp_framework ) {
                            $backend_error = ( $crmversionkey == 6 ) ? 'SugarCRM' : 'SuiteCRM';
                            $error_msg = ( isset( $_SESSION['scp_admin_messages']->combination_mismatch ) ? __( $_SESSION['scp_admin_messages']->combination_mismatch ) : '' ) . ' (' . $backend_error . ' + ' . $ismenable->fronend_framework . ')';
                        } else {
                            $error_msg = isset( $_SESSION['scp_admin_messages']->conn_problem ) ? __( $_SESSION['scp_admin_messages']->conn_problem ) : '';
                        }
                        if ( is_multisite() && !is_network_admin() ) {

                            $error_msg = isset( $_SESSION['scp_admin_messages']->conn_problem ) ? __( $_SESSION['scp_admin_messages']->conn_problem ) : '';
                            $error_msg .= '<br>' . __('Please configure portal from') . ' <a href="' . network_site_url() . 'wp-admin/network/admin.php?page=biztech-crm-portal">' . __('here') . '</a> ' . __('or contact your network administrator.');
                        }
                        ?>
                        <div class='error settings-error' id='setting-error-settings_updated'>
                            <p><strong><?php echo $error_msg; ?> <a title="<?php echo $faq_text;?>" href="<?php echo $faq_url; ?>" class="thickbox"><?php echo $faq_text;?></a></strong></p>
                        </div>
                        <script type="text/javascript">
                            jQuery(".hide_class").hide();
                            <?php if (is_multisite() && !is_network_admin()) { ?>
                            jQuery('#submit').hide();
                            <?php
                            }
                            if ($ismenable->fronend_framework != $bcp_framework) {
                            ?>
                            jQuery('#message').remove();
                            <?php } ?>
                        </script>
                        <?php
                    } else {
                        //Added by BC on 03-aug-2015 for generate key in option table
                        $generated_scp_key = md5($crmusername . $scp_sugar_password . time());
                        if (is_multisite()) { //if we are in multisite edit page
                            if (!get_site_option('biztech_scp_key') || !get_option('biztech_scp_key')) {
                                add_site_option('biztech_scp_key', $generated_scp_key);
                                add_option('biztech_scp_key', $generated_scp_key);
                                $results = $objSCP->store_wpkey($generated_scp_key);
                            } else {
                                $generated_scp_key = get_option('biztech_scp_key');
                                $results = $objSCP->store_wpkey($generated_scp_key);
                            }
                        } else {   //if we are at single site edit page
                            if (!get_option('biztech_scp_key')) {
                                add_option('biztech_scp_key', $generated_scp_key);
                                $results = $objSCP->store_wpkey($generated_scp_key);
                            } else {
                                $generated_scp_key = get_option('biztech_scp_key');
                                $results = $objSCP->store_wpkey($generated_scp_key);
                            }
                        }
                        ?>
                        <script type="text/javascript">jQuery(".hide_class").show();</script>
                        <?php
                        if (!is_network_admin()) {
                            $scp_portal_template = get_option('biztech_portal_template');
                            $scp_portal_template = ( $scp_portal_template != '0' && $scp_portal_template != '1' ) ? get_option('biztech_portal_template') : $scp_portal_template;
                            if ($scp_portal_template == 0) {
                                $set_portal_template = 'page-crm-fullwidth.php';
                            } else if ($scp_portal_template == 1) {
                                $set_portal_template = 'page-crm-standalone.php';
                            }
                            update_post_meta(get_option('biztech_redirect_login'), '_wp_page_template', $set_portal_template);
                            update_post_meta(get_option('biztech_redirect_signup'), '_wp_page_template', $set_portal_template);
                            update_post_meta(get_option('biztech_redirect_profile'), '_wp_page_template', $set_portal_template);
                            update_post_meta(get_option('biztech_redirect_forgotpwd'), '_wp_page_template', $set_portal_template);
                            update_post_meta(get_option('biztech_redirect_resetpwd'), '_wp_page_template', $set_portal_template);
                            update_post_meta(get_option('biztech_redirect_manange'), '_wp_page_template', $set_portal_template);
                        }
                    }
                } else {
                    ?>
                <script type="text/javascript">
                    jQuery(".hide_class").hide();
                    jQuery("#message").hide();
                </script>
                <?php
                }
            } else {
                ?>
            <script type="text/javascript">
                jQuery(".hide_class").hide();
            </script>
            <?php } ?>
                <div id="FAQ" style="display:none;">
                    <?php echo scp_faq_list(); ?>
                </div>
            <?php } else {//else CURL CHECKING ?>
            <div class='error settings-error' id='setting-error-settings_updated'>
                <p><strong><?php echo $check_var; ?></strong></p>
            </div>
            <?php } ?>
        </div>
        <style type="text/css">
            .bcp_portal form.form-wrap {
                margin: 20px;
            }
            .brush-question {
                font-weight: 600;
                margin: 0;
                margin-bottom: 5px;
                padding: 0 !important;
            }
            .brush-answer {
                margin: 0;
                padding: 0 !important;
            }
            #faq-list-set li {
                margin-bottom: 10px;
                border-bottom: 1px solid #c2c2c2;
                padding-bottom: 10px;
            }
        </style>
        <?php
    }
}

if (!function_exists('sugar_crm_portal_start_session')) {
    add_action('init', 'sugar_crm_portal_start_session', 1);
    function sugar_crm_portal_start_session() {

        if (!session_id()) {
            // session_save_path("/tmp"); //wpdemo server its needed
            session_start();
        }
        if ( isset( $_REQUEST['biztech_scp_password'] ) && $_REQUEST['biztech_scp_password'] ) {
            update_option('biztech_scp_password', scp_encrypt_string($_REQUEST['biztech_scp_password']));
        }
    }
}

if (!function_exists('sugar_crm_portal_logout')) {
    function sugar_crm_portal_logout($mustredirect = '') {

        if (isset($_SESSION['scp_user_profile_pic']) && $_SESSION['scp_user_profile_pic'] != '') {
            $filename = plugin_dir_path(__FILE__) . '../assets/images/user_profile_pic/' . basename($_SESSION['scp_user_profile_pic']);
            if (file_exists($filename)) {
                unlink($filename);
            }
            unset($_SESSION['scp_user_profile_pic']);
        }
        if ( isset( $_SESSION["product-images"] ) && $_SESSION["product-images"] != '' ) {
            foreach( $_SESSION["product-images"] as $product_image ) {
                $filename = plugin_dir_path(__FILE__) . '../assets/images/product-images/' . basename($product_image);
                if (file_exists($filename)) {
                    unlink($filename);
                }
            }
            unset( $_SESSION["product-images"] );
        }
        unset($_SESSION['scp_user_id']);
        unset($_SESSION['scp_user_account_name']);
        unset($_SESSION['module_array']);
        unset($_SESSION['user_timezone']);
        unset($_SESSION['assigned_user_list']);
        unset($_SESSION['user_date_format']);
        unset($_SESSION['user_time_format']);
        unset($_SESSION['scp_user_mailid']);
        unset($_SESSION['module_action_array']);
        unset($_SESSION['scp_all_currency']);
        unset($_SESSION['scp_sorting_module']);
        unset($_SESSION['module_array_without_s']);
        unset($_SESSION['Currencies']);
        unset($_SESSION['contact_group']);
        unset($_SESSION['contact_group_id']);
        unset($_SESSION['scp_knowledgebase_categories']);
        $redirect_url = get_site_url(); // To set home page redirection after logout
        unset($_SESSION['scp_use_blog_url']);
        unset($_SESSION['scp_use_blog_id']);
        unset($_SESSION['scp_custom_relationships']);
        if ( ( get_option( 'biztech_scp_single_signin' ) != '' && is_user_logged_in() ) ||
                ( isset( $_SESSION['biztech_single_signin'] ) && ( $_SESSION['biztech_single_signin'] == 'enable' ) ) ) {   //user logged in and single-sign-in enable
            if ( isset( $_SESSION['biztech_single_signin'] ) ) {
                unset( $_SESSION['biztech_single_signin'] );
            }
            wp_logout();
        }
        if ($mustredirect != 'true') {    //if user need redirection than dont send any parameter otherwise send parameter `true` to stop redirection
            wp_redirect($redirect_url);
        }
        //exit; //confusion
    }

}

if (isset($_REQUEST['logout']) == 'true') {

    add_action('init', 'sugar_crm_portal_logout', 1);
}

if (!function_exists('scp_deleteDirectory')) {
    function scp_deleteDirectory($dir) {

        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!scp_deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        return rmdir($dir);
    }
}

if (!function_exists('scp_encrypt_string')) {
    function scp_encrypt_string($input) {

        $inputlen = strlen($input); // Counts number characters in string $input
        $randkey = rand(1, 9); // Gets a random number between 1 and 9

        $i = 0;
        while ($i < $inputlen) {

            $inputchr[$i] = (ord($input[$i]) - $randkey); //encrpytion

            $i++; // For the loop to function
        }

        //Puts the $inputchr array togtheir in a string with the $randkey add to the end of the string
        $encrypted = implode('.', $inputchr) . '.' . (ord($randkey) + 50);
        return $encrypted;
    }
}

if (!function_exists('scp_decrypt_string')) {

    function scp_decrypt_string($input) {

        $input_count = strlen($input);

        $dec = explode(".", $input); // splits up the string to any array
        $x = count($dec);
        $y = $x - 1; // To get the key of the last bit in the array

        $calc = (int) $dec[$y] - 50;
        $randkey = chr($calc); // works out the randkey number

        $i = 0;

        $real = '';

        while ($i < $y) {

            $array[$i] = $dec[$i] + $randkey; // Works out the ascii characters actual numbers
            $real .= chr($array[$i]); //The actual decryption

            $i++;
        };

        $input = $real;
        return $input;
    }
}

if (!function_exists('scp_faq_list')) {

    function scp_faq_list() {
        ?>
        <ul id="faq-list-set">
            <li class="">
                <p class="brush-question"><?php _e('Does it support custom modules of CRM?'); ?></p>
                <p class="brush-answer"><?php _e('Yes,it is providing custom modules support with extra efforts. You can view details in Buy Now section.'); ?></p>
            </li>
            <li class="">
                <p class="brush-question"><?php _e('For adding custom module do customer need to provide any details?'); ?></p>
                <p class="brush-answer"><?php _e('Yes, customer need to provide their FTP and site admin details for both the frameworks(CRM and CMS)along with the list of modules which they want to enable for the portal.'); ?></p>
            </li>
            <li class="">
                <p class="brush-question"><?php _e('What data is required to make connection with CRM?'); ?></p>
                <p class="brush-answer"><?php _e('You just need your CRM instance URL, CRM admin username and CRM admin password to make connection. For further details you can review our user manual guide.'); ?></p>
            </li>
            <li class="">
                <p class="brush-question"><?php _e('What would I do if my credentials are correct and stilI I am getting connection failure message?'); ?></p>
                <p class="brush-answer"><?php echo sprintf('%s <b>%s</b> %s <b>%s</b> %s','Make sure that Customer Portal Plugins elated files has proper file permissions. As per the SugarCRM Install and upgrade guideline, We advice you to give 755 recursive file permission to','custom','and','modules','directory of your CRM instance.'); ?></p>
                <p class="brush-answer"><?php _e('To avoid possible issues for Portal Connectivity also follow this Troubleshooting steps for PHP Notices, Warnings, and Errors provided by SugarCRM.'); ?> <a target="_blank" href="http://support.sugarcrm.com/Knowledge_Base/Troubleshooting/About_PHP_Notices_Warnings_and_Errors/">http://support.sugarcrm.com/Knowledge_Base/Troubleshooting/About_PHP_Notices_Warnings_and_Errors/</a>
                </p>
            </li>
            <li class="">
                <p class="brush-question"><?php _e('Does it support multisite?'); ?></p>
                <p class="brush-answer"><?php _e('Yes,It is providing support to multisite wordpress.'); ?></p>
            </li>
            <li class="">
                <p class="brush-question"><?php _e('What is server requirement for make portal work?'); ?></p>
                <p class="brush-answer"><?php _e('Some PHP extension must be enabled for your server where you are running portal. They are CURL and JSON_ENCODE.'); ?></p>
            </li>
        </ul>
        <?php
    }
}
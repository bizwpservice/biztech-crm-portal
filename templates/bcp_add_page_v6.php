<?php
$divRow = '';
$divRClass = '';
$whiteclass = 'add-case-block bg-white shadow';
$rowClass = '';
$profileClass = '';
$header_title = '';
$divClass = '';
$form_class = 'module-form signup_front registration-form';
if ($module_name == 'Contacts') {
    $name = 'signup_form';
    $form_id = 'general_form_id';
}
if (isset($_SESSION['scp_user_id'])) {
    $divClass = 'col-lg-12';
    $name = '';
    $form_id = 'general_form_id';
    
    if ($module_name != 'Contacts') {
        $rowClass = 'white-body-shadow preferences-content';
    }
    if ($module_name == 'Contacts') {
        $divClass = 'col-lg-8';
        $form_class = "profile-form";
        $name = 'signup_form';
        $form_id = 'signup_form';
        $whiteclass = '';
        $profileClass = 'col profile-data-form';
    } else if ($module_name == 'Cases') {
        $divClass = '';
        $addLabel = $language_array['lbl_add'];
        if (isset($id) && !empty($id)) {
            $module_id = $id;
            $addLabel = $language_array['lbl_edit'];
        }
        $module_without_s = $_SESSION['module_array_without_s'][$module_name];
        $header_title = $addLabel." ".$module_without_s;
    } else {
        $addLabel = $language_array['lbl_add'];
        if (isset($id) && !empty($id)) {
            $module_id = $id;
            $addLabel = $language_array['lbl_edit'];
        }
        $module_without_s = $_SESSION['module_array_without_s'][$module_name];
        $header_title = $addLabel." ".$module_without_s;
    }
    if ($module_name == 'bc_proposal') {
        $divRow = 'preferences-button-group d-inline-block w-100 proposals-button';
    }
}
$redirectURL_manage = get_page_link(get_option('biztech_redirect_manange'));
?>
    <?php if (isset($_SESSION['scp_user_id']) && $module_name == 'Contacts') { 
        $biztech_redirect_profile = get_option('biztech_redirect_profile');
            include( TEMPLATE_PATH . 'bcp_profile_reset_password.php');
        } 
    ?>
    <?php if (isset($_SESSION['scp_user_id']) && $module_name == 'Contacts') { ?>
        <div class="col-lg-12 profile-details-edit mb-30">
            <div class="white-shadow-bg">
    <?php } ?>
    <form method="post" action="<?php echo site_url(); ?>/wp-admin/admin-post.php" name="<?php echo $name; ?>" id="<?php echo $form_id; ?>" class="<?php echo $form_class; ?>" enctype="multipart/form-data">
     <div class="<?php echo $rowClass; ?>">
     <?php 
        $cl = $divClass;
        if ($module_name == 'Cases' && empty($id)) { 
            $cl ='col-lg-8';
        }
        ?>
        <?php if($header_title){ ?>
        <div class="preferences-header white-bg-heading justify-content-between d-flex">
            <h2 class=''><?php echo $header_title; ?></h2>
            <input type='hidden' value='<?php echo $module_name; ?>' class='scp-module-name' />
        </div>
        <?php } ?>
     
     <?php 
        if ($module_name != 'Contacts') { ?>
        <div class='case-edit-block white-main-body'>
        <div class='row'>
        <div class='<?php echo $cl; ?>'>
        <?php } ?>
    
        <div class="login_error" style="display:none;">
            <span class="error" id="error_msg"></span>
        </div>
        
        <div class="response-div">
            <?php
            $res_module = $objSCP->get_module_fields($module_name, array()); //get module fields
            $res_module_fields = isset($res_module->module_fields) ? (array) $res_module->module_fields : array();
            
            if (isset($_SESSION['bcp_profile_succ']) && $_SESSION['bcp_profile_succ'] != '') {
                ?>
                <div class='alert alert-success alert-dismissible fade show bcp-success'>
                    <span><?php echo stripslashes($_SESSION['bcp_profile_succ']); ?></span>
                    <button type='button' class='btn-close' data-bs-dismiss='alert'></button>
                </div>
            <?php
                unset($_SESSION['bcp_profile_succ']);
            }
                
            if (isset($_SESSION['bcp_profile_error']) && $_SESSION['bcp_profile_error'] != '') {
                ?>
                <div class='alert alert-danger alert-dismissible fade show login_error'>
                    <span class='error'><?php echo  stripslashes($_SESSION['bcp_profile_error']); ?></span>
                    <button type='button' class='btn-close' data-bs-dismiss='alert'></button>
                </div>
                <?php
                unset($_SESSION['bcp_profile_error']);
            }
                
            foreach ($results as $k => $vals_lbl) {
                $readonly_fields = array('text','varchar','name','enum','bool','radioenum','multienum','url','phone','float','int');
                $text_sugar_array = array('name', 'phone', 'url', 'phone_fax', 'varchar', 'file', 'image', 'datetimecombo', 'datetime', 'date', 'id', 'float', 'decimal', 'int','time');
                $select_options_sugar_array = array('enum', 'multienum', 'dynamicenum');
                $select_relate_options_sugar_array = array('relate', 'parent');
                $textarea_sugar_array = array('text', 'address', 'wysiwyg');
                $check_box_sugar_array = array('bool');
                $radio_button_sugar_array = array('radioenum');
                
                $contact_id = isset($_SESSION['scp_user_id']) ? $_SESSION['scp_user_id'] : "";
            ?>
                
            <div class="row">
            <?php
                foreach ($vals_lbl->rows as $k_lbl2 => $vals) {
                    foreach ($vals as $key_fields => $val_fields) {
                        $name = isset($val_fields->name) ? $val_fields->name : '';
                        $fileds = isset($res_module_fields[$name]) ? $res_module_fields[$name] : array();
                        if( empty( $val_fields->type ) || ( in_array($val_fields->type, $select_relate_options_sugar_array) && $contact_id == "") ) {
                            continue;
                        }
                        
                        if(($module_name == 'Contacts' && isset($_SESSION['scp_user_id'])) && ((!empty( $val_fields->type ) && $val_fields->type == "image") || ( $val_fields->name == 'password_c') || ($val_fields->name == 'username_c')) ) {
                            continue;
                        }
                        
                        $class_name = 'input-text form-control';
                        $ftype = "text";
                        $required = '';
                        $required_text = '';
                        $readonly = "";
                        $disabled = '';
                        $tooltip = "";
                        $keydown = "";
                        $maxsize = "";
                        $min = "";
                        $max = "";
                        $style = "";
                        
                        $label = isset($val_fields->label_value) ? str_replace(':','',$val_fields->label_value) : '';
                        
                        if ( isset( $val_fields->required ) && $val_fields->required == 'true' ) {
                            $required = "required=''";
                            $required_text = '<span class="required">*</span>';
                        }

                        if (isset($val_fields->helptip) && !empty($val_fields->helptip) && $val_fields->helptip != 'null') {
                            $tooltip = "<span data-bs-toggle='tooltip' data-bs-placement='top' title='" . $val_fields->helptip . "'><em class='fa fa-info-circle'></em></span>";
                        }
                        
                        if (isset($val_fields->maxsize) && !empty($val_fields->maxsize) && $val_fields->maxsize != '') {
                            $maxsize = "maxlength='".$val_fields->maxsize."'";
                        }
                        if(isset($val_fields->min) && !empty($val_fields->min) && $val_fields->min != '') {
                            $min = "min='".$val_fields->min."'";
                        }
                        if(isset($val_fields->max) && !empty($val_fields->max) && $val_fields->max!='') {
                            $max = "max='".$val_fields->max."'";
                        }
                        
                        //Set input type according to field type or name
                        if ($name == "email1") {
                            $ftype = "email";
                            $class_name .= ' scp_email';
                        } else if ( $name == "password_c" ) {
                            $ftype = 'password';
                            $class_name .= ' scp_password';
                        } else if ( $val_fields->type == "file" ) {
                            $ftype = 'file';
                            $class_name .= ' scp_file';
                        } else if ( $val_fields->type == "image" ) {
                            $ftype = 'file';
                            $class_name .= ' scp_file scp_image';
                        }

                        // Add class according to type or name date type
                        if ($val_fields->type == 'date') {
                            $class_name .= " scp_date ";
                            $keydown .= 'onkeydown="return false"';
                        } else if ($val_fields->type == 'time') {
                            $class_name .= " scp_time ";
                            $keydown .= 'onkeydown="return false"';
                        } else if ( $val_fields->type == 'datetimecombo' || $val_fields->type == 'datetime' ) {
                            $class_name .= ' scp_datetime';
                            $keydown .= 'onkeydown="return false"';
                        } else if ($val_fields->type == 'float') {
                            $class_name .= " scp_float_number ";
                        } else if ($val_fields->type == 'phone') {
                            $class_name .= " scp_phone_number ";
                        } else if ($val_fields->type == 'url') {
                            $class_name .= " scp_url ";
                        } else if ($val_fields->type == 'decimal') {
                            $class_name .= " scp_decimal_number ";
                        } else if ($val_fields->type == 'int') {
                            $class_name .= " scp_int_number ";
                        }
                        
                        if (in_array($val_fields->type, $readonly_fields) && isset($val_fields->readonly) && ( $val_fields->readonly === true || strtolower($val_fields->readonly) === 'true' ) ) {
                            $disabled = 'disabled';
                        } else {
                            $disabled = '';
                        }
                        
                        if(($module_name == 'Contacts' && isset($_SESSION['scp_user_id'])) && ($name == 'username_c' || $name == 'email1')) {
                            $disabled = 'readonly="readonly"';
                        }

                        $data_value = "";
                        
                        if ($module_name != "Contacts") {                            
                            if (isset($id) && !empty($id)) {
                                $data_value = isset($record_detail->entry_list[0]->name_value_list->$name->value) ? $record_detail->entry_list[0]->name_value_list->$name->value : '';
                            } else {
                                if (isset($val_fields->default_value) && $val_fields->default_value != "" && $val_fields->default_value != "null" && $val_fields->default_value != "undefined") {
                                    $data_value = $val_fields->default_value;
                                } else {
                                    $data_value = "";
                                }
                            }
                        }
                        
                        if(!empty($conditionlogic)){
                            $conditionlogickeys = array_keys($conditionlogic);
                            
                            if(isset($id) && !empty($id) && in_array($name, $conditionlogickeys)) {
                                $field_selection = isset($conditionlogic[$name]) ? $conditionlogic[$name]->origin_field_selection : '';
                                
                                $datavalue = $record_detail->entry_list[0]->name_value_list->$field_selection->value;
                                $datavalue = $objSCP->unencodeMultienum( $datavalue );
                                
                                if($conditionlogic[$name]->matching_selection == 'Any'){
                                    if((count(array_intersect($datavalue,$conditionlogic[$name]->matching_value)) >= 1)){
                                        if($conditionlogic[$name]->visibility == 1){
                                            $style = 'style="display: none;"';
                                        }
                                        if($conditionlogic[$name]->portal_required){
                                            $required = "required=''";
                                            $required_text = '<span class="required">*</span>';
                                        }
                                    }
                                } else if($conditionlogic[$name]->matching_selection == 'All'){
                                    if((count(array_intersect($datavalue,$conditionlogic[$name]->matching_value)) == count($conditionlogic[$name]->matching_value))){
                                        if($conditionlogic[$name]->visibility == 1){
                                            $style = 'style="display: none;"';
                                        }
                                        if($conditionlogic[$name]->portal_required){
                                            $required = "required=''";
                                            $required_text = '<span class="required">*</span>';
                                        }
                                    }
                                }

                            } else if(in_array($name, $conditionlogickeys) && empty($id)){
                                $fieldName = str_replace('[]', '', $name);
                                if($conditionlogic[$name]->visibility == 0){
                                    $style = 'style="display: none;"';
                                }
                            }
                            foreach ($conditionlogic as $val){
                                $fieldName = str_replace('[]', '', $name);
                                $field_selection = isset($val->origin_field_selection) ? $val->origin_field_selection : '';
                                if($field_selection == $fieldName){
                                    $class_name .= ' conditionlogic_div';
                                }
                            }
                        }
                        

                        if ($name == "date_modified" || $name == "date_start" || $name == "date_end" || $name == "date_entered" || $name == "date_due") {
                            if ($data_value != '') { 
                                if ( $_SESSION['browser_timezone'] != NULL ) {
                                    $data_value = scp_user_time_convert( $data_value, 'edit' );
                                } else {
                                    $UTC = new DateTimeZone("UTC");
                                    $newTZ = new DateTimeZone($result_timezone);
                                    $date = new DateTime($data_value, $UTC);
                                    $date->setTimezone($newTZ);
                                    $data_value = $date->format($objSCP->date_format . " " . $objSCP->time_format);
                                }
                            }
                        }
                        if ($val_fields->type == 'date') {
                            $data_value = (!empty($data_value)) ? date($objSCP->date_format, strtotime($data_value)) : '';
                        }
                        if ($val_fields->type == 'time') {
                            $data_value = (!empty($data_value)) ? date($objSCP->time_format, strtotime($data_value)) : '';
                        }
                        
                        if ($module_name == "Calls" && $val_fields->name == "duration_hours" && $id == "") {
                            $data_value = 0;
                        }
                        if ($module_name == "Calls" && $val_fields->name == "duration_minutes") {
                            $minutes_arry = [0,15,30,45];
                            $val_fields->type = 'enum';
                        }
                        if (($module_name == 'Calls' || $module_name == 'Meetings') && $val_fields->name == "status") {
                            $disabled = 'disabled';
                            $data_value = "Requested";
                            $val_fields->type = "name";
                        }
                        if($module_name == 'Contacts' && isset($_SESSION['scp_user_id'])) {
                            if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
                                $data_value = isset( $getContactInfo->$name->value ) ? $getContactInfo->$name->value : '';
                            } else {
                                $data_value = isset( $getContactInfo->$name ) ? $getContactInfo->$name : '';
                            }
                        }
                        
                        $preview = $dwload = $cls = '';
                        if (!empty($data_value)) {
                            $dwload = $module_icon_content['download']." ". $language_array['lbl_download'];
                            $preview = $module_icon_content['view']." ". $language_array['lbl_preview'];
                            $cls = "general-link-btn scp-download-btn";
                        }
                        
                        if ( in_array($val_fields->type, $text_sugar_array) ) {
                            ?>
                            <div class="col-lg-6" <?php echo $style; ?>>
                                <div class="form-group">
                                    <div class="d-flex align-items-center">
                                        <label><?php echo $label.' ' . $tooltip.' '.$required_text; ?></label>
                                    </div>
                                    <?php if ($ftype == 'file' && isset($id) && !empty($id) && ($module_name != "Notes") && !empty($data_value)) { ?>
                                        <div class="file-form-group">
                                         <a href='javascript:void(0);' onclick='form_submit_document("<?php echo $id; ?>","", "<?php echo $name; ?>");' class='<?php echo $cls; ?> scp-<?php echo $module_name; ?>-font'>
                                             <?php echo $dwload; ?>
                                         </a>
                                        <a href='<?php echo admin_url(); ?>admin-post.php?action=bcp_get_doc_attachment&module_name=<?php echo $module_name; ?>&scp_doc_id=<?php echo $id; ?>&scp_doc_view=1&scp_doc_field_name=<?php echo $name; ?>' target='__blank' class='<?php echo $cls; ?> scp-<?php echo $module_name; ?>-font'>
                                            <?php echo $preview; ?>
                                        </a>
                                        <a href='javascript:void(0);' onclick='form_submit_remove_document("<?php echo $id; ?>", "<?php echo $name; ?>", "<?php echo $module_name; ?>");' class='<?php echo $cls; ?> scp-<?php echo $module_name; ?>-font'>
                                            <em class='fa fa-remove' aria-hidden='true'></em>
                                            <?php echo $language_array['lbl_remove']; ?>
                                        </a>
                                        <input type='hidden' name='<?php echo $name; ?>' value='<?php echo $data_value; ?>'>
                                        </div>
                                    <?php
                                    } else {
                                        
                                        if ($val_fields->type == 'url' && empty($id)) {
                                            $data_value .= 'http://';
                                        }
                                        if ($val_fields->type == 'datetimecombo' || $val_fields->type == 'datetime') {
                                            //Updated by BC on 17-jun-2016 for timezone store in session
                                            if (empty($_SESSION['user_timezone'])) {
                                                $result_timezone = $objSCP->getUserTimezone();
                                            } else {
                                                $result_timezone = $_SESSION['user_timezone'];
                                            }
                                            $UTC = new DateTimeZone("UTC");
                                            $newTZ = new DateTimeZone($result_timezone);
                                            $date = new DateTime($data_value, $newTZ);
                                            $date->setTimezone($newTZ);
                                            $min = $date->format('i');
                                            $min_array = [15,30,45,60];
                                            $min_add = 0;
                                            foreach ($min_array as  $min_value) {
                                                if ($min < $min_value && $min_add == 0) {
                                                    $min_add = $min_value - $min;
                                                }
                                            }
                                            $min_add = $min_add * 60;
                                            $data_value = $date->format('m/d/Y H:i');
                                            if ($id == "") {
                                                if ( $name == "date_start" && ( $module_name == "Tasks" || $module_name == "Meetings" || $module_name == "Calls" ) ) {
                                                    $check_if_start_date_is_exist = 1;
                                                }
                                                $data_value = date('m/d/Y H:i', strtotime($data_value) + $min_add);
                                            }
                                        }
                                        $autocomplete = "";
                                        if ($val_fields->type == 'date' || $val_fields->type == 'datetimecombo') {
                                            $autocomplete = "autocomplete='off'";
                                        }
                                        if (($val_fields->type == 'datetimecombo' && isset($_POST['date']) && $_POST['date'] != NULL)) {
                                            $data_value = $_POST['date'];
                                            $UTC = new DateTimeZone("UTC");
                                            $date = new DateTime($data_value, $UTC);
                                            $min = $date->format('i');
                                            $min_array = [15,30,45,60];
                                            $min_add = 0;
                                            foreach ($min_array as  $min_value) {
                                                if ($min < $min_value && $min_add == 0) {
                                                    $min_add = $min_value - $min;
                                                }
                                            }
                                            $min_add = $min_add * 60;
                                            if ($val_fields->name == 'date_end' || $val_fields->name == "date_due") {
                                                $min_add += 900;
                                            }
                                            $data_value = $date->format('m/d/Y H:i');
                                            $data_value = date('m/d/Y H:i', strtotime($data_value) + $min_add);
                                            
                                            $autocomplete = "autocomplete='off'";
                                        }
                                        if ($val_fields->type == 'datetimecombo' && ( $val_fields->name == 'date_end' || $val_fields->name == 'date_due' ) && $id == "" && !isset($_POST['date'])) {
                                            
                                            $data_value = date('m/d/Y H:i', strtotime($data_value) + 900);
                                            $autocomplete = "autocomplete='off'";
                                        }
                                        
                                        
                                    ?>
                                        <input id="<?php echo $name; ?>" type="<?php echo $ftype; ?>" name="<?php echo $name; ?>" value="<?php echo $data_value; ?>" class="<?php echo $class_name ?>" <?php echo $required; ?> <?php echo $disabled; ?> <?php echo $autocomplete; ?> <?php echo $keydown; ?> <?php echo $maxsize; ?> <?php echo $min; ?> <?php echo $max; ?>>
                                    <?php if ( $val_fields->type == "file" ) { ?>
                                            <p class="scp-information"><?php echo $language_array['lbl_maximum_upload_size'].': 2MB.' ; ?></p>
                                    <?php } else if ( $val_fields->type == "image" ) { ?>
                                            <p class="scp-information"><?php echo $language_array['lbl_maximum_upload_size'].': 1MB.' ; ?></p>
                                    <?php } 
                                    }
                                    if ($module_name == "Notes" && $val_fields->type == 'file' && isset($id) && !empty($id)) {
                                        if (!empty($data_value)) {
                                            ?>
                                            <div class="file-form-group">
                                            <a href='javascript:void(0);' onclick='form_submit_note_document("<?php echo $id; ?>","filename");' class='<?php echo $cls; ?> scp-<?php echo $module_name; ?>-font'><?php echo $dwload; ?></a>
                                            <a href='<?php echo admin_url(); ?>admin-post.php?action=bcp_get_note_attachment&scp_note_id=<?php echo $id; ?>&scp_note_field=<?php echo $val_fields->name; ?>&scp_note_view=1' target='__blank' class='<?php echo $cls; ?> scp-<?php echo $module_name; ?>-font'><?php echo $preview; ?></a>
                                            <input type='hidden' name='edit-file' value='<?php echo $value; ?>'>
                                            </div>
                                        <?php
                                        }
                                    }
                                    if ($ftype == 'file' && ($module_name == "Contacts") && !empty($data_value)) {
                                    if( isset($_SESSION['scp_user_profile_pic']) && $_SESSION['scp_user_profile_pic'] != '' && is_pro_img_exist($_SESSION['scp_user_profile_pic']) ){
                                        ?>
                                        <em id="profile_pic" aria-hidden="true">
                                            <img alt="" src="<?php echo $_SESSION['scp_user_profile_pic']; ?>" srcset="<?php echo $_SESSION['scp_user_profile_pic']; ?>" class="avatar avatar-51 photo" width="51" height="51">
                                        </em>
                                        <?php } else { ?>
                                        <em id="profile_pic" aria-hidden="true">
                                            <?php echo get_avatar($_SESSION['scp_user_mailid'], 51); ?>
                                        </em>
                                        <?php
                                    }
                            
                                    }
                                    
                                    ?>
                                </div>
                            </div>
                            <?php
                        } else if ( in_array($val_fields->type, $select_options_sugar_array) ) {  // If option field
                            $options = $val_fields->options;
                            $multiple = "";
                            $data_parent = "";
                            $id_name = $name;
                            $class_name .= ' scp-enum';
                            if ($val_fields->type == 'multienum') {
                                $multiple = "multiple='multiple'";
                                $name = $name."[]";
                                $class_name .= ' scp-multiselect';
                                if(!empty($data_value)){
                                    $data_value = $objSCP->unencodeMultienum( $data_value );
                                }
                            } else {
                                $data_value = (array) $data_value;
                            }
                            if ( $val_fields->type == 'dynamicenum' && isset($val_fields->parentenum) ) {
                                $class_name .= ' scp-dynamic-enum';
                                $parent_enum = trim($val_fields->parentenum);
                                $data_parent = "data-parent='".$parent_enum."'";
                                $options = array();
                                $parentvalue = isset($record_detail->entry_list[0]->name_value_list->$parent_enum->value) ? $record_detail->entry_list[0]->name_value_list->$parent_enum->value : '';
                                if($parentvalue){
                                    ?>
                                    <script>
                                        jQuery('.scp-enum').trigger('change');
                                    </script>
                                    <?php
                                }
                            }
                            if ( $val_fields->type == 'dynamicenum' && isset($val_fields->parentenum) && $module_name == "Cases" ) {
                                $options = $val_fields->options;
                            }
                            ?>
                            <div class="col-md-6" <?php echo $style; ?>>
                                <div class="form-group">
                                    <div class="d-flex align-items-center">
                                        <label><?php echo $label . $required_text . $tooltip; ?></label>
                                    </div>
                                    <select <?php echo $data_parent; ?> id="<?php echo $id_name; ?>" name="<?php echo $name; ?>" <?php echo $multiple; ?> class="<?php echo $class_name ?>">
                                    <?php
                                        if (($val_fields->type != 'multienum' && $module_name=='Accounts') || ($module_name == "Contacts") || (($val_fields->type == 'enum' || $val_fields->type == 'dynamicenum') && ($module_name !='Cases')) ) {
                                        ?>
                                        <option value="" disabled="" selected=""><?php _e('--None--'); ?></option>
                                        <?php
                                        }
                                        if ($module_name == "Calls" && $val_fields->name == "duration_minutes") {
                                            if ($id == "") {
                                                $data_value = array(15);
                                            }
                                            for ($m = 0; $m < count($minutes_arry); $m++) {
                                                $checked = "";
                                                if ($data_value[0] == $minutes_arry[$m]) {
                                                    $checked = "selected=''";
                                                }
                                                ?>
                                                    <option <?php echo $checked; ?> value='<?php echo $minutes_arry[$m]; ?>'><?php echo $minutes_arry[$m]; ?></option>
                                                <?php
                                            }
                                        }
                                        foreach ($options as $k_op => $v_op) {
                                            $checked = "";
                                            if (is_array($data_value) && in_array($options->$k_op->value, $data_value)) {
                                                $checked = "selected=''";
                                            }
                                            if ($options->$k_op->value == "") {
                                                continue;
                                            }
                                            ?>
                                            <option <?php echo $checked; ?> value='<?php echo $options->$k_op->value; ?>'><?php echo $options->$k_op->name; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                        } else if ( in_array($val_fields->type, $select_relate_options_sugar_array) ) {  // If relate field
                            if(isset($record_detail->entry_list)){
                                $parent_type = $record_detail->entry_list[0]->name_value_list->parent_type->value;
                                $parent_id = $record_detail->entry_list[0]->name_value_list->parent_id->value;
                            }
                            if ($val_fields->type == "parent") {
                                if ($relate_to_module != "") {
                                    $parent_type = $relate_to_module;
                                    $parent_id = $relate_to_id;
                                    $readonly = "disabled";
                                }
                                $options = $_SESSION['module_array'];
                                
                                if (empty($_SESSION['module_array'])) {//check session value in module array
                                    $modules_array2 = $objSCP->getPortal_accessibleModules();
                                    $modulesAry1 = array();
                                    foreach ($modules_array2->accessible_modules as $key_1 => $value_1) {
                                        $modulesAry1[$key_1] = $value_1->module_name;
                                    }
                                    $modules = $modulesAry1;
                                } else {
                                    $modules = $_SESSION['module_array'];
                                }

                                foreach ($modules as $key => $value) {
                                    if (!array_key_exists($key, $options)) {
                                        unset($modules[$key]);
                                    }
                                }
                                
                                ?>
                                <div class="col-md-6" <?php echo $style; ?>>
                                    <div class="form-group">
                                        <div class="d-flex align-items-center">
                                            <label><?php echo $label . $required_text . $tooltip; ?></label>
                                        </div>
                                        <select id="<?php echo $name; ?>" name="<?php echo $name; ?>" class="<?php echo $class_name ?>" <?php echo $readonly; ?> onchange="get_module_list()" >
                                            <option value=""><?php _e('--None--'); ?></option>
                                            <?php
                                            foreach ($modules as $k_op => $v_op) {
                                                if ($k_op != "Contacts" && $k_op != $module_name) {
                                                    $selected = '';
                                                    if ($k_op == $parent_type) {
                                                        $selected = 'selected';
                                                    }
                                                ?>
                                                <option value='<?php echo $k_op; ?>' <?php echo $selected; ?>><?php echo $v_op; ?></option>
                                                <?php
                                                }
                                            }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6" <?php echo $style; ?>>
                                    <div class="form-group">
                                        <div class="d-flex align-items-center">
                                            <label><?php echo $label . $required_text . $tooltip; ?></label>
                                        </div>
                                        <div id="related_to_id2" >
                                            <?php
                                            if ($relate_to_id != "" || !empty($data_value)) {
                                                 
                                                $module_name_parent = $parent_type;
                                            
                                                $select_fields = array('id', 'name');
                                                if($_SESSION['scp_user_group_type'] == "contact_based"){
                                                    $record_detail3 = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], strtolower($module_name_parent), $select_fields);
                                                }else{
                                                    $record_detail3 = $objSCP->get_relationships('Accounts', $_SESSION['scp_account_id'], strtolower($module_name_parent), $select_fields);
                                                }
                                            
                                            ?>
                                            <select id="parent_id" name="parent_id" class="<?php echo $class_name ?>" <?php echo $readonly; ?>>
                                                <option value=''></option>
                                                <?php
                                                for ($m = 0; $m < count($record_detail3->entry_list); $m++) {
                                                    $mod_id = $record_detail3->entry_list[$m]->name_value_list->id->value;
                                                    $mod_name = $record_detail3->entry_list[$m]->name_value_list->name->value;
                                                    $selected = '';
                                                    if ($mod_id == $parent_id) {
                                                        $selected = 'selected';
                                                    }
                                                ?>
                                                <option value="<?php echo $mod_id; ?>" <?php echo $selected; ?>><?php echo htmlentities($mod_name); ?></option>
                                                <?php } ?>
                                            </select>
                                            <?php } else { ?>
                                            <select id="parent_id" name="parent_id" class="<?php echo $class_name ?>" >
                                                <option value="" disabled="" selected=""></option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            } else {
                                $relate_to_module = $fileds->related_module;
                                if (array_key_exists($fileds->related_module, (array) $_SESSION['module_array']) && $fileds->related_module != $module_name) { //if relate module is not in active module array and relate module is not with same module
                                    $select_fields = array('id', 'name');
                                    $record_detail2_relation_name = ( isset( $custom_relationships[$relate_to_module]['Contacts'] ) && $custom_relationships[$relate_to_module]['Contacts'] != '' ) ? $custom_relationships[$relate_to_module]['Contacts'] : strtolower($relate_to_module);
                                    $record_detail2 = $objSCP->get_relationships('Contacts', $_SESSION['scp_user_id'], $record_detail2_relation_name, $select_fields);
                                    ?>
                                    <div class="col-md-6" <?php echo $style; ?>>
                                        <div class="form-group">
                                            <div class="d-flex align-items-center">
                                                <label><?php echo $label . $required_text . $tooltip; ?></label>
                                            </div>
                                            <select id="<?php echo $name; ?>" name="<?php echo $name; ?>" class="<?php echo $class_name ?>" >
                                                <option value="" disabled="" selected=""></option>
                                                <?php
                                                for ($m = 0; $m < count($record_detail2->entry_list); $m++) {
                                                    $mod_id = $record_detail2->entry_list[$m]->name_value_list->id->value;
                                                    $mod_name = $record_detail2->entry_list[$m]->name_value_list->name->value;
                                                    $selected = "";
                                                    if ($mod_id == $data_value) {
                                                        $selected = "selected='selected'";
                                                    }
                                                    ?>
                                                    <option value='<?php echo $mod_id; ?>' ><?php echo $mod_name; ?></option>
                                                    <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php
                                }
                            }
                        } else if ( in_array($val_fields->type, $textarea_sugar_array) ) {  // If textarea field
                            ?>
                            <div class="col-md-6" <?php echo $style; ?>>
                                <div class="form-group">
                                    <div class="d-flex align-items-center">
                                        <label><?php echo $label . $required_text . $tooltip; ?></label>
                                    </div>
                                    <textarea id="<?php echo $name; ?>" name="<?php echo $name; ?>" <?php echo $disabled; ?> class="input-text form-control" <?php echo $required; ?> <?php echo $maxsize; ?>><?php echo $data_value; ?></textarea>
                                </div>
                            </div>
                            <?php
                        } else if ( in_array($val_fields->type, $check_box_sugar_array) ) {  // If checkbox field
                            $checked = '';
                            if ( $data_value == TRUE ) {
                                $checked = ' checked="checked"';
                            }
                            ?>
                            <div class="col-md-6" <?php echo $style; ?>>
                                <div class="form-group">
                                    <div class="d-flex align-items-center">
                                        <label><?php echo $label . $required_text . $tooltip; ?></label>
                                    </div>
                                    <div class="checkbox form-checkbox">
                                        <input type="hidden" name="<?php echo $name; ?>" value="0" />
                                        <input id="<?php echo $name; ?>" type="checkbox" name="<?php echo $name; ?>" value="1" <?php echo $checked; ?> <?php echo $required; ?> <?php echo $disabled; ?> />
                                    </div>
                                </div>
                            </div>
                            <?php
                        } else if ( in_array($val_fields->type, $radio_button_sugar_array) ) {  // If radio button field
                            $options = $val_fields->options;
                            ?>
                            <div class="col-md-6" <?php echo $style; ?>>
                                <div class="form-group">
                                    <div class="d-flex align-items-center">
                                        <label><?php echo $label . $required_text; ?></label>
                                    </div>
                                    <?php
                                    foreach ($options as $op_key => $op_value) {
                                        $checked = '';
                                        if ( $data_value == $op_value->value ) {
                                            $checked = ' checked="checked"';
                                        }
                                        ?>
                                        <div class="form-radio">
                                            <label>
                                            <input type="radio" name="<?php echo $name; ?>" class="input-radio" value="<?php echo $op_value->value; ?>" <?php echo $checked; ?>>
                                            <?php echo $op_value->name; ?>
                                            </label>
                                        </div>
                                        <?php } ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
                </div>
            <?php } ?>
            </div>
        
        
        <?php
        wp_nonce_field('sfcp_portal_nonce', 'sfcp_portal_nonce_field');
        if ($module_name == "Contacts" && isset($_SESSION['scp_user_id'])) {
            $biztech_redirect_profile = get_option('biztech_redirect_profile');
        ?>
        <div class='scp-form-actions'>
            <input type='hidden' name='action' value='bcp_update_profile'>
            <input type='hidden' name='remove_pro_pic' id="remove_pro_pic" value='false'>
            <input type='hidden' name='scp_current_url' value='<?php echo get_permalink($biztech_redirect_profile); ?>'>
            <span class='case-button-group'>
                <input type='submit' name='update-profile' value='<?php echo $language_array['lbl_save']; ?>' class='scp-button btn' />
                <input type='button' value='<?php echo $language_array['lbl_cancel']; ?>' class='scp-button btn border-button' onclick="window.location = '<?php echo $redirectURL_manage; ?>#data/Dashboard/'"/></span>
        </div>
        <?php } else { ?>    
        <div class='row text-left'>
            <div class='col-md-12'>
                <input type='hidden' name='action' value='bcp_add_moduledata_call'>
                <input type='hidden' name='module_name' value='<?php echo $module_name; ?>'>
                <input type='hidden' name='view' value='<?php echo $view; ?>'>
                <input type="hidden" name="scp_current_url" value="">
                <input type="hidden" name="ajax_theme" value="<?php echo admin_url( 'admin-ajax.php' );?>">
                <?php
                $biztech_portal_visible_reCAPTCHA = (fetch_data_option("biztech_portal_visible_reCAPTCHA") != NULL) ? fetch_data_option("biztech_portal_visible_reCAPTCHA") : array();
                if ( $module_name == "Contacts" && !isset($_SESSION['scp_user_id']) && in_array("portal-sign-up", $biztech_portal_visible_reCAPTCHA) && get_option('biztech_scp_recaptcha_site_key') != "" && get_option('biztech_scp_recaptcha_secret_key') != "" ) {
                ?>
                <div id="scp-recaptcha"></div>
                <label id="scp-recaptcha-error-msg" style="display:none;" class="error">
                    <?php echo $language_array['msg_captcha_validate']; ?>
                </label>
                <?php
                }
                if (isset($_REQUEST['relate_to_id']) && isset($_REQUEST['relate_to_module'])) {
                    $relate_to_module = $_REQUEST['relate_to_module'];
                    $relate_to_id = $_REQUEST['relate_to_id'];
                ?>
                    <input type='hidden' name='relate_to_module' value='<?php echo $relate_to_module; ?>' >
                    <input type='hidden' name='relate_to_id' value='<?php echo $relate_to_id; ?>' >
                <?php
                }
                if (isset($module_id) && !empty($module_id)) {
                ?>
                    <input type='hidden' name='id' value='<?php echo $module_id; ?>'>
                <?php
                }
                if (isset($_REQUEST['relationship'])) {
                    $relationship = $_REQUEST['relationship'];
                    ?>
                    <input type='hidden' name='relationship' value='<?php echo $relationship; ?>' >
                <?php } ?>
            </div>
        </div>
        <?php } ?>
            
        
        <?php if ($module_name != 'Contacts') { ?>
        </div>
        <?php } ?>
        
            <?php if ($module_name == "Cases" && empty($id)) { ?>
            <div class="col-lg-4 case-attachement-right">
                <?php include( TEMPLATE_PATH . 'bcp_case_attachments.php'); ?>
            </div>
            <?php } ?>
            <?php if ($module_name != 'Contacts') { ?>
            </div>
            <!-- row -->
        </div>
        <!-- case-edit-block white-main-body end -->
        </div>
        <?php } ?>
        <?php if ($module_name == "bc_proposal") { ?>
                <div class="bc_proposal_div">
                    <?php include( TEMPLATE_PATH . 'bcp_add_page_v6_product_list.php'); ?>
                </div>
            <?php } ?>
        <?php
            $submit = 1;
            if ($module_name == "Contacts" && isset($_SESSION['scp_user_id'])) {
                $submit = 0;
            }
        ?>
        <?php if ($submit) { ?>
        <div class="row <?php echo $divRow; ?>">
            <div class="col-md-12 registration-button">
                <?php 
                $save_label = $language_array['lbl_save'];
                if ($module_name == "Contacts" && !isset($_SESSION['scp_user_id'])) { 
                    $save_label = $language_array['lbl_signup'];
                }
                ?>
                <button type='submit' class='btn'><?php echo $save_label; ?></button>
                <?php if ($module_name != "Contacts") { ?>
                <button class='btn border-button' type='button' onclick="history.back()" ><?php echo $language_array['lbl_cancel']; ?></button>
                <?php } ?>
            </div>
            <?php if ($module_name == "Contacts" && !isset($_SESSION['scp_user_id'])) { 
                $biztech_redirect_login = get_page_link(get_option('biztech_redirect_login'));
                if ($biztech_redirect_login != NULL) {
                    $redirect_url_login = $biztech_redirect_login;
                } else {
                    $redirect_url_login = home_url() . "/portal-login/";
                }    
            ?>
            <div class="col-md-12 text-center registration-footer">
                <?php echo $language_array['lbl_login_link_label'] ?>
                <a href="<?php echo $redirect_url_login; ?>"><?php echo $language_array['lbl_login']; ?></a>
            </div>
            <?php } ?>
        </div>
        <?php } ?>
        
        </form>
    
<?php if (isset($_SESSION['scp_user_id']) && $module_name == 'Contacts') { ?>
    </div>
</div>
<?php } else { ?>
    </div>
<?php  } ?>
        
<?php if ( $module_name == "Contacts" && !isset($_SESSION['scp_user_id']) && in_array("portal-sign-up", $biztech_portal_visible_reCAPTCHA) && get_option('biztech_scp_recaptcha_site_key') != "" && get_option('biztech_scp_recaptcha_secret_key') != "" ) { ?>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
    async defer>
</script>

<script type="text/javascript">
  var onloadCallback = function() {
    widget1 = grecaptcha.render('scp-recaptcha', {
      'sitekey' : '<?php echo get_option("biztech_scp_recaptcha_site_key"); ?>',
    });
  };
</script>
<?php } ?>
<script>
    jQuery(document).ready(function(){
        if (jQuery('.scp_datetime').length > 0) {
            jQuery('.scp_datetime').datetimepicker({
                format: 'MM/DD/YYYY HH:mm',
                minDate: moment().add(1),
                focusOnShow: false,
            });
        }
        
        if (jQuery('.scp_time').length > 0) {
            jQuery('.scp_time').datetimepicker({
                format: 'LT',
                format: 'HH:mm',
                focusOnShow: false,
            });
        }
        if (jQuery('.scp_date').length > 0) {
            jQuery('.scp_date').datetimepicker({
                format: 'L',
                format: 'MM/DD/YYYY',
                focusOnShow: false,
            });
        }
        
         jQuery('[data-bs-toggle="tooltip"]').tooltip();
    });
    
    jQuery('#general_form_id').validate({
        rules: {
            date_start: {
                checkDate: true
            },
            date_end:{
                greaterThan: '#date_start',
                checkDate: true
            },
            date_due:{
                greaterThan: '#date_start',
                checkDate: true
            },
            duration_minutes:{
                check_duration: true,
            },
            'username_c': {
                check_username: true,
            },
            'password_c': {
                minlength: 6,
                check_pwd: true,
            },

        },
        messages: {
            end_date: {
                greaterThan: language_array.msg_must_greate_than_start_date,
            },
            'password_c': {
                minlength: language_array.msg_pwd_min_length,
                check_pwd: language_array.msg_pwd_validation,
            }
        },
        errorPlacement: function(error, element) {
            if (element.attr('type') == 'radio') {
                error.insertAfter(element.parent().parent().parent().prev());
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    jQuery( "#signup_form" ).validate({
        errorPlacement: function(error, element) {
            if (element.attr("type") == "radio") {
                error.insertAfter(element.parent().parent().parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
    
    jQuery('#filename').bind('change', function() {
        //this.files[0].size gets the size of your file.
        if(this.files[0].size > 2097152){//2 MB size allowed
            alert("<?php echo ( isset($_SESSION['portal_message_list']->attach_error) ? __( $_SESSION['portal_message_list']->attach_error ) : '' ); ?>");
            jQuery('#filename').val('');
            return false;
        }
    });
    
    jQuery( '#change_pwd_form' ).validate({
        rules: {
            new_password: {
                minlength: 6,
                check_pwd: true,
            },
            cf_password:{
                equalTo   : "#new_password"
            },
        },
        messages: {
            new_password: {
                minlength: language_array.msg_pwd_min_length,
                check_pwd: language_array.msg_pwd_validation,
            },
            cf_password: {
                'equalTo'   : language_array.msg_cnfm_pwd_must_match
            },
        },
    });
    
    
   jQuery(document).ready(function () {
       jQuery(".conditionlogic_div").trigger( "change" );
        jQuery('.scp-enum').multiselect({
            closeOnSelect : true,
            tags: true,
        });
        jQuery('.scp-multiselect').multiselect({
            selectAll: true,
            placeholder : "<?php _e('--None--'); ?>",
        });
   });
</script>
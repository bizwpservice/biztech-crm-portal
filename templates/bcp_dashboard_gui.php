<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
date_default_timezone_set("Asia/Kolkata");
global $wpdb, $objSCP, $sugar_crm_version, $lang_code, $custom_relationships, $module_icon_content;
$custom_relationships = $_SESSION['scp_custom_relationships'];
//Get Dashboard Preferences
$sql = "SELECT * FROM ".$wpdb->prefix . "scp_portal_contact_info WHERE contact_id = '".$_SESSION['scp_user_id']."'";
$result = $wpdb->get_row($sql);
$dashboard_pref = isset( $result->dashboard_preferences ) ? json_decode($result->dashboard_preferences) : '';

$recent_activity_modules = fetch_data_option('biztech_scp_recent_activity');
if ((isset($_REQUEST['conerror']) && !empty($_REQUEST['conerror'])) && (isset($_SESSION['bcp_connection_error']) && $_SESSION['bcp_connection_error'] != '') ) {
    $bcp_connection_error = $_SESSION['bcp_connection_error'];
    unset($_SESSION['bcp_connection_error']);
    return "<div class='error settings-error' id='setting-error-settings_updated'>
        <p><strong>".$bcp_connection_error."</strong></p>
    </div>";
}

$biztech_redirect_profile = get_page_link(get_option('biztech_redirect_profile'));
if ($biztech_redirect_profile != NULL) {
    $redirect_url = $biztech_redirect_profile;
} else {
    $redirect_url = home_url() . "/portal-profile/";
}
$pagetemplate = get_post_meta(get_the_ID(), '_wp_page_template', true);
$template = str_replace('.php', "", $pagetemplate);

$biztechscpname = fetch_data_option('biztech_scp_name');

$scp_counter_block = array();
$scp_recent_block = array();
$scp_chart_block = array();
$scp_today_schedule = 0;
if (isset($dashboard_pref->scp_contact_counter_blocks_modules)) {
    $scp_counter_block = $dashboard_pref->scp_contact_counter_blocks_modules;
}
if (isset($dashboard_pref->scp_contact_recent_blocks_modules)) {
    $scp_recent_block = (array) $dashboard_pref->scp_contact_recent_blocks_modules;
}
if (isset($dashboard_pref->scp_contact_charts_blocks_modules)) {
    $scp_chart_block = $dashboard_pref->scp_contact_charts_blocks_modules;
}
if (isset($dashboard_pref->scp_contact_enable_schedule_blocks)) {
    $scp_today_schedule = $dashboard_pref->scp_contact_enable_schedule_blocks;
}

$module_chart_enable = array();
$chart_relation_name = array();
foreach ($scp_chart_block as $module){
    $module_chart_enable[$module] = TRUE;
    $chart_relation_name[$module] = isset($custom_relationships[$module]["Contacts"]) ? $custom_relationships[$module]["Contacts"] : strtolower($module);
}

$dashboardModules = array();
$scp_modules = $_SESSION['module_array'];
$arry_event_modules = array('Meetings', 'Calls', "Tasks");

$parent_module = "Contacts";
$parent_id = $_SESSION['scp_user_id'];
$contact_group_id = isset($_SESSION['contact_group_id']) ?$_SESSION['contact_group_id'] : "";

foreach ($scp_modules as $key => $value) {
    $counter_flag = 0;
    $chart_flag = 0;
    $recent_flag = 0;
    $today_schedule_flag = 0;
    $today_schedule_query = "";
    
    if (in_array($key, $scp_counter_block)) {
        $counter_flag = 1;
    }
    if (in_array($key, $scp_chart_block)) {
        $chart_flag = 1;
    }
    if (in_array($key, $scp_recent_block)) {
        $recent_flag = 1;
    }
    if ( in_array($key, $arry_event_modules) && $scp_today_schedule ) {
        $today_schedule_flag = 1;
        $today_schedule_query = strtolower($key).".date_start >= '".date("Y-m-d"). " 00:00:00'" ;
    }
    
    $fields = array('id', 'name', 'date_entered');
    if($value == 'Cases') {
        $fields = array('id', 'name', 'date_entered', 'priority');
    } else if($value == 'Products' || $value == 'Product Catalog'){
        if ($sugar_crm_version == 7) {
            $fields = array('id', 'name', 'date_entered','list_price');
        } else {
            $fields = array('id', 'name', 'date_entered','price');
        }
    } else if($value == 'Quotes'){
        if ($sugar_crm_version == 7) {
            $fields = array('id', 'name', 'date_entered', 'quote_stage');
        } else {
            $fields = array('id', 'name', 'date_entered', 'stage');
        }
    } else if($value == 'Meetings' || $value == 'Calls'){
        $fields = array('id', 'name', 'date_entered', 'status');
    } else if($value == 'Documents' || $value == 'Notes') {
        $fields = array('id', 'name', 'date_entered', 'filename');
    } else if($value == 'Knowledge Base') {
        $fields = array('id', 'name', 'date_entered', 'description');
    } else if($value == 'Proposals') {
        if ($sugar_crm_version == 7) {
            $fields = array('id', 'name', 'date_entered', 'proposal_status');
        } else {
            $fields = array('id', 'name', 'date_entered', 'status');
        }
    }
 

    if ($counter_flag || $chart_flag || $recent_flag || $today_schedule_flag) {
        $link_field_name = isset($custom_relationships[$key]["Contacts"]) ? $custom_relationships[$key]["Contacts"] : strtolower($key);
        if ($value == 'AOK_KnowledgeBase') {
            $parent_module = "bc_user_group";
            $parent_id = $contact_group_id;
        }
        
        if ($sugar_crm_version == 7) {
           $module_arr = array(
                "parent_module"         => $parent_module,
                "parent_id"             => $parent_id,
                "module_name"           => $key,
                "link_field_name"       => $link_field_name,
                "related_fields"        => $fields,
                "related_module_query"  => array(),
                "counter_block"         => $counter_flag,
                "chart_block"           => $chart_flag,
                "recent_block"          => $recent_flag,
                "today_schedule"        => $today_schedule_flag,
                "today_schedule_query"  => array(),
            );
        }
        if ($sugar_crm_version == 6 || $sugar_crm_version == 5) { 
            
            $module_arr = array(
                "parent_module"         => $parent_module,
                "parent_id"             => $parent_id,
                "module_name"           => $key,
                "link_field_name"       => $link_field_name,
                "related_fields"        => $fields,
                "related_module_query"  => "",
                "counter_block"         => $counter_flag,
                "chart_block"           => $chart_flag,
                "recent_block"          => $recent_flag,
                "today_schedule"        => $today_schedule_flag,
                "today_schedule_query"  => $today_schedule_query,
            );
        }
        
        array_push($dashboardModules, $module_arr);
    }
}
//bcp_print($dashboardModules);
$limit = fetch_data_option("biztech_scp_case_per_page");
$DashboardDetails = $objSCP->get_dashboard_data($dashboardModules, $limit, $lang_code);

$chartdetails = isset($DashboardDetails->chart_block) ? $DashboardDetails->chart_block : array();
$scp_counter_details = isset($DashboardDetails->counter_block) ? $DashboardDetails->counter_block : array();
$scp_recent_details = isset($DashboardDetails->recent_block) ? $DashboardDetails->recent_block : array();


$scp_today_schedule_details = isset($DashboardDetails->today_schedule) ? $DashboardDetails->today_schedule : array();
//get option to redirect to which page after login
$biztech_redirect_manage_dash = get_page_link(get_option('biztech_redirect_manange'));
if ($biztech_redirect_manage_dash != NULL) {
    $redirect_url_dash = $biztech_redirect_manage_dash;
} else {
    $redirect_url_dash = home_url() . "/portal-manage-page/";
}

?>

<div class="row">
    <!-- counter section -->
    <?php if ( is_array( $scp_counter_block ) && ! empty( $scp_counter_block ) ) { ?>
        <?php 
            $counter_cls = 'col-xl-12';
        if ($scp_today_schedule) {
            $counter_cls = 'col-xl-7';
        }
        ?>
    <div class="col-md-12 <?php echo $counter_cls; ?> left-cases-block mb-30">
        <div class="row">
            <?php foreach( $scp_counter_block as $key => $value){
                if ( $value == 'KBContents' || $value == 'AOK_KnowledgeBase' ) { 
                    continue;
                }
                $mod_name = $_SESSION['module_array'][$value];
                $count = isset($scp_counter_details->$value) ? $scp_counter_details->$value : 0;
                ?>
                <div class="col">
                    <div class="counter-block <?php echo strtolower($value); ?>-block text-center">
                        <?php if(file_exists(BCP_PLUGIN_PATH.'assets/images/counter-'.strtolower($value).'.svg')){ ?>
                            <img src="<?php echo IMAGES_URL; ?>counter-<?php echo strtolower($value); ?>.svg" class="active-image" alt="<?php echo strtolower($value); ?>">
                        <?php } else { ?>
                            <img src="<?php echo IMAGES_URL; ?>default.svg" class="active-image" alt="<?php echo strtolower($value); ?>">
                        <?php } ?>
                        <h2><?php echo ucfirst($mod_name); ?></h2>
                        <h3><?php echo $count; ?></h3>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php } ?>

    <!-- today schedule section -->
    <?php if ($scp_today_schedule) { ?>
        <div class="col-md-6 col-xl-5 mb-30">
            <div class="imex-content-table bg-white shadow today-schedule-table ">
                <div class="main-heading-table">
                    <h2 class="cases-heading d-flex align-items-center">
                        <span><?php echo $language_array['lbl_today_schedule']; ?></span>
                    </h2>
                </div>
                <div class="table-responsive table-content">
                    <?php if ( $scp_today_schedule_details != NULL ) { ?>
                    <table>
                        <tbody>
                        <?php
                        foreach ($scp_today_schedule_details as $rec) {
                            $module_name = (isset($rec->module_name) && $rec->module_name != "") ? $rec->module_name : "";
                            if ($sugar_crm_version == 7) {
                                $name = (isset($rec->name) && $rec->name != "") ? $rec->name : $rec->id;
                                $dateval = (isset($rec->date_entered) && $rec->date_entered != "") ? $rec->date_entered : "";
                            }
                            if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
                               $name = (isset($rec->name_value_list->name->value) && $rec->name_value_list->name->value != "") ? $rec->name_value_list->name->value : $rec->id;
                               $dateval = (isset($rec->name_value_list->date_entered->value) && $rec->name_value_list->name->value != "") ? $rec->name_value_list->date_entered->value : "";
                            }
                            
                            $datevalue = scp_user_time_convert($dateval);
                            $date_format = $objSCP->date_format;
                            $time_format = $objSCP->time_format;
                            $datevalue = str_replace("/", "-", $datevalue);
                            $datevalue = str_replace("-", "/", $datevalue);
                            $dateDate = date($date_format, strtotime($datevalue));
                            $dateTime = date($time_format, strtotime($datevalue));
                            ?>
                        <tr>
                            <td>
                                <p>
                                    <a href="<?php echo '#data/' . $module_name . '/detail/' . $rec->id; ?>/">
                                        <?php echo ( strlen($name) > 30 ? substr($name, 0, 30) . '...' : $name ); ?>
                                    </a>
                                </p>
                            </td>
                            <td class="date-time">
                                <?php if ($dateval != "") { ?>
                                    <p class="light"><?php echo $dateDate; ?></p>
                                    <span><?php echo $dateTime; ?></span>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <?php } else { ?>
                    <div class="scp-recent-data">
                        <span><?php echo $language_array['lbl_no_records']; ?></span>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<div class="row">
    <!-- chart section -->
    <?php
    $chart_in_array = (array) $chartdetails;
    $chart_ingroup = array();
    
    if( isset($chartdetails) && $chart_in_array != NULL ) {
        $chart_ingroup = array_chunk($chart_in_array, 3, true);
    }
    
    if(!is_array($scp_chart_block)){
        $scp_chart_block = (array) $scp_chart_block;
    }
    
    if (!empty($scp_chart_block)) {
        $count = count($scp_chart_block);
        $cls = 'col-md-6 mb-30';
        if($count == 3){
            $cls = 'col-md-6 col-xl-4 mb-30';
        }
        foreach ( $scp_chart_block as $module ) { ?>
        <div class="<?php echo $cls; ?>">
            <div class="imex-content-table bg-white shadow today-schedule-table ">
                <div class="main-heading-table">
                    <h2 class="cases-heading d-flex align-items-center">
                        <span>
                            <?php echo ucfirst( $_SESSION['module_array'][$module] ); ?> 
                            <?php echo $language_array['lbl_summary']; ?>
                        </span>
                    </h2>
                </div>
                <div class="text-center" id="chart-content-<?php echo $module; ?>" >
                    <div class="chartview-blur">
                        <img src="<?php echo IMAGES_URL; ?>no-chart.jpg" alt="">
                        <p><?php echo $language_array['lbl_chart_no_data_found']; ?></p>
                    </div>
                </div>
            </div>
        </div>    
        <?php
        } 
    }
    
    /* recent activity section */
    if (!empty($scp_recent_block)) {
        $module_action_array = $_SESSION['module_action_array'];
        $biztech_redirect_manage_page = get_page_link(get_option('biztech_redirect_manange'));
        if ($biztech_redirect_manage_page != NULL) {
            $manage_page_url = $biztech_redirect_manage_page;
        } else {
            $manage_page_url = home_url() . "/portal-manage-page/";
        }
        foreach ($scp_recent_block as $module_name) {
            $cls = 'col-md-6 mb-30';
            $arry_lable = (array) $_SESSION['module_array'];
            $module_name_label = $arry_lable[$module_name];
            ?>
            <div class="<?php echo $cls; ?>">
                <div class="imex-content-table bg-white shadow today-schedule-table ">
                    <div class="main-heading-table d-flex justify-content-between align-items-center">
                        <h2 class="cases-heading d-flex align-items-center">
                            <span><?php echo $language_array['lbl_recent'] . " " . $module_name_label; ?></span>
                            <a class="see-all" href="#data/<?php echo $module_name; ?>/list/" title="<?php echo $language_array['lbl_view_all']; ?>">
                                <span><?php echo $language_array['lbl_view_all']; ?></span>
                                <em class="fas fa-chevron-right"></em>
                            </a>
                        </h2>
                        <?php if (isset($module_action_array[$module_name]['create']) && $module_action_array[$module_name]['create'] == 'true' && $module_name != 'Accounts') { 
                            $add_url = $manage_page_url.'#data/'.$module_name.'/edit/';
                            if ($module_name == "Cases" && $case_deflection) {
                                $add_url = $manage_page_url.'#data/Solutions/Search/';
                            }
                        ?>
                        <a href="<?php echo $add_url; ?>" class="add-more-btn" title="<?php echo $language_array['lbl_add']; ?>"><?php echo $language_array['lbl_add']; ?></a>
                        <?php } ?>
                    </div>
                    <div class="table-responsive table-content">
                        <?php if (isset($scp_recent_details->$module_name) && $scp_recent_details->$module_name != NULL) {  ?>
                    <table>
                        <tbody>
                            <?php
                            foreach ($scp_recent_details->$module_name as $rec) {
                                if ($sugar_crm_version == 7) {
                                    if($rec->module_name == 'ProductTemplates' || $rec->module_name == 'KBContents') {
                                        $name = (isset($rec->name_value_list->name->value) && $rec->name_value_list->name->value != "") ? $rec->name_value_list->name->value : $rec->id;
                                        $dateval = (isset($rec->name_value_list->date_entered->value) && $rec->name_value_list->date_entered->value != "") ? $rec->name_value_list->date_entered->value : "";
                                        $filename = (isset($rec->name_value_list->filename->value) && $rec->name_value_list->filename->value != "") ? $rec->name_value_list->filename->value : "";
                                        
                                    } else {
                                        $name = (isset($rec->name) && $rec->name != "") ? $rec->name : $rec->id;
                                        $dateval = (isset($rec->date_entered) && $rec->date_entered != "") ? $rec->date_entered : "";
                                        $filename = (isset($rec->filename) && $rec->filename != "") ? $rec->filename : "";
                                    }
                                    
                                    $status_value = '';
                                    $price_value = '';
                                    if($module_name == 'Cases') {
                                        $status = (isset($rec->priority) && $rec->priority != "") ? $rec->priority : "";
                                        $res2 = $objSCP->getEnumValues($module_name, 'priority');
                                        $status_value = (isset($res2->$status) && $res2->$status != "") ? $res2->$status : "";
                                    } else if($module_name == 'Quotes') {
                                        $status_value = (isset($rec->quote_stage) && $rec->quote_stage != "") ? $rec->quote_stage : "";
                                    } else if($module_name == 'Meetings' || $module_name == 'Calls') {
                                        $status = (isset($rec->status) && $rec->status != "") ? $rec->status : "";
                                        $res2 = $objSCP->getEnumValues($module_name, 'status');
                                        $status_value = (isset($res2->$status) && $res2->$status != "") ? $res2->$status : "";
                                    } else if($module_name = 'bc_proposal') {
                                        $status = (isset($rec->proposal_status) && $rec->proposal_status != "") ? $rec->proposal_status : "";
                                        $res2 = $objSCP->getEnumValues($module_name, 'proposal_status');
                                        $status_value = (isset($res2->$status) && $res2->$status != "") ? $res2->$status : "";
                                    }
                                    if($rec->module_name == 'ProductTemplates') {
                                        $price_value = (isset($rec->name_value_list->list_price->value) && $rec->name_value_list->list_price->value != "") ? $rec->name_value_list->list_price->value : "";
                                    }
                                    
                                }
                                if ($sugar_crm_version == 6 || $sugar_crm_version == 5) {
                                    $name = (isset($rec->name_value_list->name->value) && $rec->name_value_list->name->value != "") ? $rec->name_value_list->name->value : $rec->id;
                                
                                    $dateval = (isset($rec->name_value_list->date_entered->value) && $rec->name_value_list->date_entered->value != "") ? $rec->name_value_list->date_entered->value : "";
                                    
                                    $filename = (isset($rec->name_value_list->filename->value) && $rec->name_value_list->filename->value != "") ? $rec->name_value_list->filename->value : "";
                                    
                                    $status_value = '';
                                    $price_value = '';
                                    if($rec->module_name == 'Cases') {
                                        $status_value = (isset($rec->name_value_list->priority->value) && $rec->name_value_list->priority->value != "") ? $rec->name_value_list->priority->value : "";
                                        
                                    } else if($rec->module_name == 'AOS_Quotes') {
                                        $status_value = (isset($rec->name_value_list->stage->value) && $rec->name_value_list->stage->value != "") ? $rec->name_value_list->stage->value : "";
                                    } else if($rec->module_name == 'Meetings' || $rec->module_name == 'Calls' || $rec->module_name = 'bc_proposal') {
                                        $status_value = (isset($rec->name_value_list->status->value) && $rec->name_value_list->status->value != "") ? $rec->name_value_list->status->value : "";
                                    }
                                    
                                    if($module_name == 'AOS_Products') {
                                        $price_value = (isset($rec->name_value_list->price->value) && $rec->name_value_list->price->value != "") ? $rec->name_value_list->price->value : "";
                                    }
                                    
                                    if($module_name == 'AOK_KnowledgeBase') {
                                        $description = (isset($rec->name_value_list->description->value) && $rec->name_value_list->description->value != "") ? $rec->name_value_list->description->value : "";
                                    }
                                }
                                
                                $datevalue = scp_user_time_convert($dateval);
                                $date_format = $objSCP->date_format;
                                $time_format = $objSCP->time_format;
                                if(strpos($datevalue, '-')){
                                    $datevalue = str_replace("-", "/", $datevalue);
                                } else {
                                    $datevalue = str_replace("/", "-", $datevalue);
                                }
                                
                                $dateDate = date($date_format, strtotime($datevalue));
                                $dateTime = date($time_format, strtotime($datevalue));
                            ?>
                                <tr>
                                    <td>
                                        <?php if($module_name == 'Accounts') { 
                                            $auth_names = explode(" ", $name);
                                            $Auth_letter = strtoupper($auth_names[0])[0];
                                            $Auth_letter .= (isset($auth_names[1])) ? strtoupper($auth_names[1])[0] : "";                 
                                            ?>
                                        <div class="d-flex align-items-center">
                                            <div class="account-image">
                                                <?php echo $Auth_letter; ?>
                                            </div>
                                                <p>     
                                                    <a href="<?php echo '#data/' . $module_name . '/detail/' . $rec->id; ?>/">
                                                        <?php echo ( strlen($name) > 30 ? substr($name, 0, 30) . '...' : $name ); ?>
                                                    </a>
                                                </p>
                                        </div>
                                    <?php } else { ?>
                                        <p>
                                            <?php
                                    if($sugar_crm_version == 7 && ($rec->module_name == 'ProductTemplates')) {
                                        $url = '#data/' . $rec->module_name . '/detail/' . $rec->id;
                                    } else if(($module_name == 'AOK_KnowledgeBase')) { 
                                        $url = '#data/' . $module_name . '/list';
                                    } else {
                                        if($sugar_crm_version == 7) {
                                            if($rec->module_name == 'KBContents'){
                                                $url = '#data/' . $rec->module_name . '/list';
                                            } else if($rec->module_name!='') {
                                                $url = '#data/' . $rec->module_name . '/detail/' . $rec->id;
                                            } else {
                                                $url = '#data/' . $rec->_module . '/detail/' . $rec->id;
                                            }
                                        } else {
                                            $url = '#data/' . $module_name . '/detail/' . $rec->id;
                                        }
                                    }
                                        ?>
                                        <a href="<?php echo $url; ?>/">
                                            <?php echo ( strlen($name) > 30 ? substr($name, 0, 30) . '...' : $name ); ?>
                                        </a>
                                    </p>
                                    <?php } ?>

                                    </td>
                                    <?php if($status_value!='') { ?>
                                    <td class="status-td">
                                        <p class="<?php echo str_replace(' ', '-', strtolower($status_value)); ?>">
                                            <?php echo $status_value; ?>
                                        </p>
                                    </td>
                                    <?php } ?>
                                    <?php if($price_value!='') { ?>
                                    <td class="product-price">
                                        <p>$<?php echo number_format($price_value, 2); ?></p>
                                    </td>
                                    <?php } ?>
                                    <?php if($filename && ($module_name == 'Documents' || ( isset($rec->_module) && $rec->_module == 'Documents'))) { ?>
                                    <td>
                                        <a title='<?php echo $language_array['lbl_download']; ?>' href='javascript:void(0);' class="scp-download-btn" onclick='form_submit_document("<?php echo $rec->id; ?>")'>
                                            <?php echo $module_icon_content['download']; ?>
                                        </a>
                                        <form action='<?php echo site_url(); ?>/wp-admin/admin-post.php' method='post' id='doc_submit'>
                                            <input type='hidden' name='action' value='bcp_get_doc_attachment'>
                                            <input type='hidden' name='module_name' value='Documents'>
                                            <input type = 'hidden' name = 'scp_current_url' value = '<?php echo $redirect_url_dash; ?>'>
                                            <input type='hidden' name='scp_doc_id' value='' id='scp_doc_id'>
                                            <input type='hidden' name='scp_doc_view' value='' id='scp_doc_view'>
                                        </form>
                                    </td>
                                    <?php } ?>
                                    <?php if($module_name == 'AOK_KnowledgeBase' || $rec->module_name == 'KBContents') { ?>
                                    <td>
                                        <a href="javascript:void(0);" class="scp-download-btn" onclick='form_submit_recent_KBdocument_Print(this);' title='<?php echo $language_array['lbl_print']; ?>' data-name="<?php echo $name; ?>" data-description="<?php echo $description; ?>">
                                            <?php echo $module_icon_content['print']; ?>
                                        </a>
                                    </td>
                                    <?php } ?>
                                    <?php if($filename && ($module_name == 'Notes' || ( isset($rec->_module) && $rec->_module == 'Notes'))) { ?>
                                    <td>
                                        <a title='<?php echo $language_array['lbl_download']; ?>' href='javascript:void(0);' class="scp-download-btn" onclick='form_submit_note_document("<?php echo $rec->id; ?>", "filename")'>
                                        <?php echo $module_icon_content['download']; ?>
                                     </a>
                                    <form action='<?php echo site_url(); ?>/wp-admin/admin-post.php' method='post' id='download_note_id'>
                                        <input type='hidden' name='action' value='bcp_get_note_attachment'>
                                        <input type = 'hidden' name = 'scp_current_url' value = ''>
                                        <input type='hidden' name='scp_note_id' value='' id='scp_note_id'>
                                        <input type='hidden' name='scp_note_field' value='' id='scp_note_field'>
                                        <input type='hidden' name='scp_note_view' value='' id='scp_note_view'>
                                    </form>
                                    </td>
                                    <?php }else if(!$filename && ($module_name == 'Notes' || ( isset($rec->_module) && $rec->_module == 'Notes'))){ ?>
                                    <td></td>
                                    <?php } ?>
                                    <td class="date-time">
                                        <?php if ($dateval != "") { ?>
                                            <p class="light"><?php echo $dateDate; ?></p>
                                            <span><?php echo $dateTime; ?></span>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                                <!-- Knowledge Base print data -->
                            <?php 
                            $module_without_s = $_SESSION['module_array_without_s']['AOK_KnowledgeBase'];
                            $biztech_scp_name = fetch_data_option('biztech_scp_name');
                            $biztech_upload_image = fetch_data_option('biztech_scp_upload_image');
                            ?>
                            <div class='print-header' style='display: none;'>
                                <div class='print-header-div'>
                                    <div class='print-div' >
                                        <?php if ($biztech_upload_image != NULL) {
                                        $sfcp_logo_url = wp_get_attachment_image_src( $biztech_upload_image, 'full' );
                                        ?>
                                        <div class="scp-logo text-center">
                                            <img src="<?php echo $sfcp_logo_url[0]; ?>"  width="300" alt=""/>
                                        </div>
                                        <?php } ?>
                                        <h3><?php echo $biztech_scp_name; ?></h3>
                                    </div>
                                    <div class='print-div' >
                                        <h3 class='module-title'><?php echo $module_without_s; ?></h3>
                                    </div>
                                </div>
                            </div>

                            <div style='display: none;' id='KBContent'>
                                <div class='print-content'>
                                    <div class='print-kb-header'>
                                        <h3 class='print-kb-title'></h3>
                                        <span class='print-kb-subtitle'></span>
                                    </div>
                                    <div class='print-kb-description'>
                                    </div>
                                </div>
                            </div>
                            </tbody>
                        </table>
                        <?php } else { ?>
                        <div class="scp-recent-data">
                            <span><?php echo $language_array['lbl_no_records']; ?></span>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php
            $j++;
        }
    }
    ?>
    
</div>

<script>
    // Build the chart
    jQuery(document).ready(function () {
        <?php
        if (!empty($scp_chart_block)) {
            $colors = array( '#9FA8DA', '#F48FB1','#34B2B2', '#FF4D4D', '#FFB74D', '#477F6C', '#64B5F6', '#FF8A65', '#81C784', '#C69C6D', '#CE93D8','#4DD0E1' );
            $col_cnt = 0;
            $scps_modules = $_SESSION['module_array'];
            
            foreach ( $chart_ingroup as $key => $value ) {
                foreach ( $value as $module => $status ) {
                    if ( ( !empty($scp_chart_block) && !in_array($module,$scp_chart_block) ) || empty($status) ) {
                        continue;
                    }
                    if ($module == 'invoice') {
                        $module = 'invoices';
                    }
                    if (is_array($scps_modules) && !array_key_exists(ucfirst($module), $scps_modules)) {
                        continue;
                    }
                    
                    $dataPoints = array();
                    foreach ( $status as $key => $value2 ) {
                        $dataPoints[$key]['name'] = $value2->status;
                        $dataPoints[$key]['y'] = $value2->count;
                        $dataPoints[$key]['color'] = $colors[$col_cnt];
                        if ( ++$col_cnt >= count($colors) ) {
                            $col_cnt = 0;
                        }
                    }
            ?>
            Highcharts.chart('chart-content-<?php echo $module; ?>', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                    height: 280,
                },
                title: {
                    text: '',
                    align: "left",
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false,
                        },
                        showInLegend: true,
                    }
                },
                series: [{
                    name: '<?php echo ucfirst( $_SESSION['module_array'][$module] ); ?>',
                    colorByPoint: true,
                    innerSize: '80%',
                    data: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>,
                }]
            });
        <?php
                }
            }
        }
        ?>
    });
    
    // Get Today's schedule
    <?php if (isset($dashboard_pref->scp_contact_enable_schedule_blocks)) { ?>
    jQuery( document ).ready(function() {
        var today = new Date();
        var dd = today.getDate(); var mm = today.getMonth()+1; /* January is 0! */ var yyyy = today.getFullYear();
        var hrs = today.getHours(); var mins = today.getMinutes(); var secs = today.getSeconds();
        if(dd<10) { dd='0'+dd; } if(mm<10) { mm='0'+mm; }
        if(hrs<10) { hrs='0'+hrs; } if(mins<10) { mins='0'+mins; } if(secs<10) { secs='0'+secs; }
        var date = yyyy + '-' + mm + '-' + dd;
        var time = hrs + ':' + mins + ':' + secs;

        jQuery.ajax({
            url: '<?php echo plugin_dir_url(__FILE__); ?>json_events.php?today_schedule=1&calendar=view_calendar&today_date=' + date + '&today_time=' + time,
            type: 'POST',
            success: function (response) {
                var schedule_data = jQuery.parseJSON(response);
                if ( schedule_data == null || schedule_data == '' ) {
                    html = "<h5><?php echo $language_array['msg_no_schedule_today']; ?></h5>"
                } else {
                    var html, start_time;
                    html = '<ul>';
                    jQuery.each(schedule_data, function ( index, value ) {
                        html += '<li><a href="<?php echo $redirect_url_dash; ?>' + value.url + '">' + value.title + '</a> ' + value.start + '</li>';
                        if( index == '4' ) {
                            html += '<li class="last"><a href="<?php echo $redirect_url_dash; ?>#data/Calendar/list/"><?php echo $language_array['lbl_view_all'] ?></a></li>';
                        }
                    });
                    html += '</ul>';
                }
                jQuery('.today-schedule-form').html(html);
                jQuery(".today-schedule-form.loading_data").removeClass('loading_data');
            }
        });
    });
    <?php } ?>
</script>

<?php ?>
<div class="col-md-6 profile-details mb-30">
    <div class="white-shadow-bg h-100">
        <div class="profile-heading">
            <h2><?php echo $language_array['lbl_profile_settting']; ?></h2>
        </div>
        <div class="col-12">
            <form method="post" action="<?php echo site_url(); ?>/wp-admin/admin-post.php" name="change_profile_img_form" id="change_profile_img_form" enctype="multipart/form-data">
                <?php if (isset($_SESSION['bcp_profile_img_succ']) && $_SESSION['bcp_profile_img_succ'] != '') { ?>
                <div class='alert alert-success alert-dismissible fade show bcp-success'>
                    <span><?php echo stripslashes($_SESSION['bcp_profile_img_succ']); ?></span>
                    <button type='button' class='btn-close' data-bs-dismiss='alert'></button>
                </div>
                <?php
                    unset($_SESSION['bcp_profile_img_succ']);
                }

                if (isset($_SESSION['bcp_profile_img_error']) && $_SESSION['bcp_profile_img_error'] != '') {
                    ?>
                    <div class='alert alert-danger alert-dismissible fade show login_error'>
                        <span class='error'><?php echo  stripslashes($_SESSION['bcp_profile_img_error']); ?></span>
                        <button type='button' class='btn-close' data-bs-dismiss='alert'></button>
                    </div>
                    <?php
                    unset($_SESSION['bcp_profile_img_error']);
                }

                $img_array = array();
                foreach ($results as $k => $vals_lbl) {
                    foreach ($vals_lbl->rows as $k_lbl2 => $vals) {
                        foreach ($vals as $key_fields => $val_fields) {
                            if( !empty( $val_fields->type ) && $val_fields->type == "image" ) {
                                $img_array[] = $val_fields;
                                break;
                            }
                        }
                    }
                }
                if(!empty($img_array)){
                    $required = '';
                    if ( isset( $val_fields->required ) && $val_fields->required == 'true' ) {
                        $required = 'required';
                    }

                    $data_value = isset( $getContactInfo->contact_photo->value ) ? $getContactInfo->contact_photo->value : '';

                    if( isset($_SESSION['scp_user_profile_pic']) && $_SESSION['scp_user_profile_pic'] != '' && is_pro_img_exist($_SESSION['scp_user_profile_pic']) ){
                        $imgUrl = $_SESSION['scp_user_profile_pic'];
                    } else {
                        $imgUrl = get_avatar_url($_SESSION['scp_user_mailid'], 51);
                    }
                    ?>
                    <div class="form-group d-flex">
                        <div class="preview-zone hidden">
                            <div class="box box-solid">
                                <div class="box-body">
                                    <img src="<?php echo $imgUrl; ?>" width="200" alt="<?php echo $_SESSION['scp_user_account_name']; ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="dropzone-wrapper">
                        <div class="dropzone-desc">
                            <?php echo $module_icon_content['image-upload']; ?>
                            <p><span>Click to replace</span> or drag and drop image.</p>
                         </div>
                        <input type="file" name="<?php echo $img_array[0]->name;  ?>" value="<?php echo $data_value; ?>" id="uploadBtn" class="upload dropzone" />
                        </div>
                    </div>
                <?php } ?>
                <?php 
                $user_name = array();
                foreach ($results as $k => $vals_lbl) {
                    foreach ($vals_lbl->rows as $k_lbl2 => $vals) {
                        foreach ($vals as $key_fields => $val_fields) {
                            if( !empty( $val_fields->name ) && $val_fields->name == "username_c" ) {
                                $user_name[] = $val_fields;
                                break;
                            }
                        }
                    }
                }
                if($user_name){
                    $data_value = isset( $getContactInfo->username_c->value ) ? $getContactInfo->username_c->value : '';
                ?>
                <div class="form-group">
                    <div class="d-flex align-items-center">
                        <label><?php echo $user_name[0]->label_value; ?></label>
                        <span class="required">*</span>
                    </div>
                    <input type="text" class="input-text form-control" name="<?php echo $user_name[0]->name; ?>" readonly="readonly" value="<?php echo $data_value; ?>" id="<?php echo $user_name[0]->name; ?>" />

                </div>
                <?php } ?>

                <span class='case-button-group'>
                    <input type='hidden' name='action' value='bcp_update_profile_img'>
                    <input type='hidden' name='remove_profile_pic' value='false'>
                    <input type='hidden' name='scp_current_url' value='<?php echo get_permalink($biztech_redirect_profile); ?>'>
                    <input type='submit' name='update-profile' value='<?php echo $language_array['lbl_update']; ?>' class='btn btn-primary' />
                </span>

            </form>
        </div>
    </div>
</div>
<!-- change password part -->
<div class="col-md-6 profile-password-changes mb-30">
    <div class="white-shadow-bg">
        <div id="pwd-msg"></div>
        <form method="post" action="<?php echo site_url(); ?>/wp-admin/admin-post.php" name="change_pwd_form" id="change_pwd_form">

            <h2><?php echo $language_array['lbl_change_password']; ?></h2>

            <div class="form-group">
                <div class="d-flex align-items-center">
                    <label><?php echo $language_array['lbl_old_pwd']; ?></label>
                    <span class="required">*</span>
                </div>
                <input type="password" required="" class="input-text form-control" name="old_password" id="old_password">

            </div>
            <div class="form-group">

                <div class="d-flex align-items-center">
                    <label><?php echo $language_array['lbl_new_pwd']; ?></label>
                    <span class="required">*</span>
                </div>
                <input type="password" required="" class="validate_pwd input-text form-control" name="new_password" id="new_password">

            </div>

            <div class="form-group">

                <div class="d-flex align-items-center">
                    <label><?php echo $language_array['lbl_cnfm_pwd']; ?></label>
                    <span class="required">*</span>
                </div>
                <input type="password" required="" class="validate_pwd input-text form-control" name="cf_password" id="cf_password">

            </div>

            <span class='case-button-group'>
                <input type='hidden' name='action' value='bcp_update_pwd'>
                <input type="hidden" id="scp-profile-notification-page" value="<?php echo $redirectURL_manage.'#data/Notifications' ?>" >
                <input type='hidden' name='scp_current_url' value='<?php echo $current_url; ?>'>
                <input type='submit' name='update-pwd' value='<?php echo $language_array['lbl_update']; ?>' class='hover active scp-button btn btn-blue scp-Accounts' />
            </span>

        </form>
    </div>
</div>
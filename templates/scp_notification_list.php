<div class="row">
<div class="col-lg-12">
    <div class="case-details-view bg-white shadow border-radius-5 notification-header">

        <div class="d-flex justify-content-between">
            <h2 class="d-none d-md-block"><?php echo $language_array['lbl_notifications']; ?></h2>
        </div>
        <div id="notification_list_msg">
            <?php if (isset($_SESSION['bcp_add_record']) && $_SESSION['bcp_add_record'] != '') { ?>
                <span class='success' id='succid'><?php echo $_SESSION['bcp_add_record']; ?></span>
                <?php
                unset($_SESSION['bcp_add_record']);
            }
            ?>
        </div>
        <?php if ($notificationlist == NULL && !isset($_REQUEST['scp_noti_is_search'])) { ?>
            <div class='scp-table-responsive'>
                <strong><?php echo ( isset($_SESSION['portal_message_list']->module_no_records) ? __($_SESSION['portal_message_list']->module_no_records) : '' ); ?></strong>
            </div>
        <?php } else { ?>
            <div class='scp-form scp-form-two-col'>
                <div id="notification_list_form" >
                    <div class="scp-col-full">
                        <div class="top-header-search mr-auto d-flex align-items-center body-header-serch">
                            <div class="scp-notification-search">
                                <form id="scp-notification-search-form">
                                    <?php
                                    $scp_is_search = (isset($_REQUEST['scp_noti_is_search'])) ? $_REQUEST['scp_noti_is_search'] : '0';
                                    $noti_from_date = (isset($_REQUEST['noti_from_date'])) ? $_REQUEST['noti_from_date'] : '';
                                    $noti_to_date = (isset($_REQUEST['noti_to_date'])) ? $_REQUEST['noti_to_date'] : '';
                                    $noti_module = (isset($_REQUEST['noti_module'])) ? $_REQUEST['noti_module'] : '';
                                    $noti_type = (isset($_REQUEST['noti_type'])) ? $_REQUEST['noti_type'] : '';
                                    ?>
                                    <input type="hidden" id="scp_noti_is_search" value="<?php echo $scp_is_search; ?>" >
                                    <div class="knowledge-dropdown-group d-flex">
                                        <div class="select-module">
                                            <div class="module-dropdown dropdown body-dropdown knowledge-dropdown">
                                                <select id="noti_type" class="" data-search="<?php echo $noti_type; ?>" >
                                                    <option value="0"><?php echo $language_array['lbl_select_type']; ?></option>
                                                    <?php
                                                    foreach ($notification_action as $key => $value) {
                                                        $checked_noti_type = "";
                                                        if (isset($_REQUEST['noti_type']) && $noti_type == $key) {
                                                            $checked_noti_type = "selected";
                                                        }
                                                        ?>
                                                        <option <?php echo $checked_noti_type; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="module-dropdown dropdown body-dropdown knowledge-dropdown">
                                                <select id="noti_module" class="" data-search="<?php echo $noti_module; ?>">
                                                    <option value="0"><?php echo $language_array['lbl_select_module']; ?></option>
                                                    <?php
                                                    foreach ($notification_modules as $key => $value) {
                                                        $checked_noti_module = "";
                                                        if (isset($_REQUEST['noti_module']) && $_REQUEST['noti_module'] == $key) {
                                                            $checked_noti_module = "selected";
                                                        }
                                                        ?>
                                                        <option <?php echo $checked_noti_module; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="datepicker-wrap d-flex notification-date">
                                            <div class="inline-wrap module-dropdown">
                                                <input type="text" id="noti_from_date" placeholder="<?php echo $language_array['lbl_start_date']; ?>" data-search="<?php echo $noti_from_date; ?>" value="<?php echo $noti_from_date; ?>">
                                                <span class="menu-icon fa fa-calendar"></span>
                                            </div>
                                            <div class="date-change-icon"><i class="fa fa-arrows-h" aria-hidden="true"></i></div>
                                            <div class="inline-wrap module-dropdown">
                                                <input type="text" id="noti_to_date" placeholder="<?php echo $language_array['lbl_end_date']; ?>" data-search="<?php echo $noti_to_date; ?>" value="<?php echo $noti_to_date; ?>">
                                                <span class="menu-icon fa fa-calendar"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="knowledge-btn-group d-flex">

                                        <div class="search-btn-topbar">
                                            <input type="submit" value="<?php echo $language_array['lbl_submit']; ?>" class="scp_notification_form_btn btn btn-blue-curve btn-primary">
                                        </div>
                                        <div class="clear-btn">
                                            <input type="reset" value="<?php echo $language_array['lbl_reset']; ?>" class="scp_notification_form_btn btn btn-grey-curve btn-primary" onclick="filter_notification('0')">
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <?php if ($notificationlist != NULL) { ?>
                                <div class="scp-top-button delete-notifications-btn-group">
                                    <a href='javascript:void(0);' id="delete-notifications-btn" title="<?php echo $language_array['lbl_delete']; ?>"  class='scp_notification_form_btn disabled deletebtn scp-<?php echo $module_name; ?> scp-dtl-deletebtn' onclick="delete_multiple('0', 'bc_wp_portal_notification');">
                                        <span class='fa fa-trash-o'></span>
                                        <span><?php echo $language_array['lbl_delete']; ?></span>
                                    </a>
                                    <a href='javascript:void(0);' title="<?php echo $language_array['lbl_delete_all']; ?>" class='scp_notification_form_btn deletebtn scp-<?php echo $module_name; ?> scp-dtl-deletebtn' onclick="delete_multiple('1', 'bc_wp_portal_notification');">
                                        <span class='fa fa-trash-o'></span>
                                        <span><?php echo $language_array['lbl_delete_all']; ?></span>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                        <div class='case-listing-block notification-details'>
                            <?php
                            if (isset($_REQUEST['scp_noti_is_search']) && $_REQUEST['scp_noti_is_search'] == '1' && $notificationlist == NULL) {
                                echo $language_array['lbl_no_match_found'];
                            } else {
                                ?>
                                <div class="scp-notification-wrapper table-responsive custom-scrollbar">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <div class="checkbox-wrap inline-item"><input type="checkbox" class="scp-check-all" id="select_all"></div>
                                                </th>
                                                <th>
                                                    <div class="detail-wrap inline-item">
                                                        <span class="scp-unread-icon"></span>
                                                            <?php echo $language_array['lbl_notifications']; ?>
                                                    </div>
                                                </th>
                                                <th>
                                                    <div class="date-wrap inline-item">
                                                        <?php
                                                        $field_label = $language_array['lbl_date'];
                                                        if ($order == 'desc') {
                                                            ?>
                                                            <a href='javascript:void(0);' onclick='bcp_module_order_by(0, "<?php echo $module_name ?>", "date", "asc", "", "");' class='scp-desc-sort'><?php echo $field_label; ?></a>
                                                        <?php } else if ($order == 'asc') { ?>
                                                            <a href='javascript:void(0);' onclick='bcp_module_order_by(0, "<?php echo $module_name ?>", "date", "desc", "", "");' class='scp-asc-sort'><?php echo $field_label; ?></a>
                                                        <?php } else { ?>
                                                            <a href='javascript:void(0);' onclick='bcp_module_order_by(0, "<?php echo $module_name ?>", "date", "asc", "", "");' class='scp-both-sort'><?php echo $field_label; ?></a>
                                                        <?php } ?>
                                                    </div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 0;
                                            foreach ($notificationlist as $key => $value) {
                                                ?>
                                                <tr>
                                                    <td>
                                                        <div class="checkbox-wrap inline-item">
                                                            <input type="checkbox" class="scp-checkbox scp-noti-checkbox" data-id="<?php echo $value->id; ?>">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="detail-wrap inline-item">
                                                            <span class="scp-unread-icon">
                                                                <?php if ($value->notification_status == 'unread') { ?>
                                                                    <span class="fa fa-bell"></span>
                                                                    <?php
                                                                }
                                                                $i++;
                                                                ?>
                                                            </span>
                                                            <?php
                                                            $noti_url = "javascript:void(0);";
                                                            if (isset($scp_modules[$value->module])) {
                                                                $noti_url = "#data/" . $value->module . "/detail/" . $value->record_id;
                                                            }
                                                            ?>
                                                            <a href="<?php echo $noti_url; ?>" ><?php echo $value->notification_subject; ?></a>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="date-wrap inline-item">
                                                            <?php
                                                            $val_date = $value->date_entered;
                                                            if ($_SESSION['browser_timezone'] != NULL) {
                                                                $val_date = scp_user_time_convert($val_date);
                                                            } else {
                                                                $UTC = new DateTimeZone("UTC");
                                                                $newTZ = new DateTimeZone($result_timezone);
                                                                $date = new DateTime($val_date, $UTC);
                                                                $date->setTimezone($newTZ);
                                                                //Updated by BC on 16-nov-2015
                                                                $date_format = $_SESSION['user_date_format'];
                                                                $time_format = $_SESSION['user_time_format'];
                                                                $val_date = $date->format($date_format . " " . $time_format);
                                                            }
                                                            echo $val_date;
                                                            ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function () {
        jQuery('#noti_from_date').datetimepicker({
            format: 'L',
            format: 'YYYY-MM-DD',
        });
        jQuery('#noti_to_date').datetimepicker({
            useCurrent: false,
            format: 'L',
            format: 'YYYY-MM-DD',
        });
        jQuery("#noti_from_date").on("dp.change", function (e) {
            jQuery('#noti_to_date').data("DateTimePicker").minDate(e.date);
        });
        jQuery("#noti_to_date").on("dp.change", function (e) {
            jQuery('#noti_from_date').data("DateTimePicker").maxDate(e.date);
        });
        jQuery('#scp-notification-counter').hide();
        jQuery('#scp-notification-search-form').on('submit', function (e) {
            e.preventDefault();
            filter_notification('1');
        });
    });
    function filter_notification(search) {

        var noti_type = '0';
        var noti_module = '0';
        var noti_from_date = '';
        var noti_to_date = '';

        if (search == '1') {
            noti_type = jQuery('#noti_type').val();
            noti_module = jQuery('#noti_module').val();
            noti_from_date = jQuery('#noti_from_date').val();
            noti_to_date = jQuery('#noti_to_date').val();
            if ((noti_from_date > noti_to_date) && noti_from_date != "" && noti_to_date != "") {
                alert("<?php echo $language_array['msg_end_date_must_greater_than_start_date']; ?>");
                return false;
            }
        }
        var order = "<?php echo $order ?>";
        var data = {
            'action': 'scp_notifications_page',
            'modulename': 'Notifications',
            'noti_type': noti_type,
            'noti_module': noti_module,
            'noti_from_date': noti_from_date,
            'noti_to_date': noti_to_date,
            'scp_noti_is_search': search,
            'order': order,
        };
        var el = jQuery("#responsedata");
        App.blockUI(el);
        jQuery.post(ajaxurl, data, function (response) {
            App.unblockUI(el);
            if ($.trim(response) != '-1') {
                jQuery('#responsedata').html(response);
                // jQuery('#succid').hide();
            } else {
                var redirect_url = $(location).attr('href') + "?conerror=1";
                window.location.href = redirect_url;
            }
        });
    }
</script>
<?php
}
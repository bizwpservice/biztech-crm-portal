<div class="case-attachments-section">
    <h3><?php echo  $module_without_s . " " . $language_array['lbl_attachments']; ?></h3>
    
</div>


<div class="form-group">
    <div class="preview-zone hidden">
        <div class="box box-solid">
            <div class="box-body d-flex">
            </div>
        </div>
    </div>
    <div class="dropzone-wrapper">
        <div class="dropzone-desc">
            <?php echo $module_icon_content['image-upload']; ?>
            <p><span>Click to replace</span> or drag and drop image.</p>
        </div>
        <input type="file" name="attachment[]" id="uploadBtn" class="upload case-upload" multiple="" />
        <input type="hidden" id="removed_files" name="removed_files" value="" />
    </div>
    <p class="scp-information"><?php echo $language_array['lbl_maximum_upload_size'].': 2MB.' ; ?></p>
</div>